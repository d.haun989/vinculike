find  -type f -name '*.lpr' -exec sed -i -- "s/-2022/-2023/g" {} +
find  -type f -name '*.pas' -exec sed -i -- "s/-2022/-2023/g" {} +
find  -type f -name '*.inc' -exec sed -i -- "s/-2022/-2023/g" {} +
find  -type f -name 'LICENSE' -exec sed -i -- "s/-2022/-2023/g" {} +
