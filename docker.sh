# This is a script to batch-build this game using Castle Game Engine Docker image
# see https://castle-engine.io/docker for details

# update Docker image by
# docker pull kambi/castle-engine-cloud-builds-tools:cge-none
# (without CGE, use a local copy in $CASTLE_ENGINE_PATH)

# First start the Docker image by
# docker run -d -i --name=cge -v <<Local working folder which will become "home">>:/home/ kambi/castle-engine-cloud-builds-tools:cge-none bash
# then
# docker container exec cge sh /home/<<project folder related to Local working folder>>/docker.sh

# The folders below are system-specific, you'll need to adjust them for your setup
export PROJECT_PATH=GIT/vinculike
export CASTLE_ENGINE_PATH=/home/castle_game_engine

export FPCLAZARUS_VERSION=3.3.1
export PATH=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/bin/:${PATH}
FPCLAZARUS_REAL_VERSION=`fpc -iV`
echo 'Real FPC version:' ${FPCLAZARUS_REAL_VERSION}
export FPCDIR=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/lib/fpc/${FPCLAZARUS_REAL_VERSION}/

export HOME=/home/
echo 'Building CGE build tool:'
cd "$CASTLE_ENGINE_PATH"
#rm -Rf ./tools/build-tool/castle-engine-output/ # to rebuild from scratch
rm -f ./tools/build-tool/castle-engine
./tools/build-tool/castle-engine_compile.sh
cp tools/build-tool/castle-engine /usr/local/bin/
castle-engine --version
#castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=win32 --cpu=i386 --mode=release --verbose >/home/${PROJECT_PATH}/build_win32.log
echo 'Building Android:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --target=android --mode=release --package-format=android-apk --verbose >/home/${PROJECT_PATH}/build_android.log
echo 'Building Win64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=win64 --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_win64.log
echo 'Building Linux64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=linux --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_linux.log
echo 'Building Debian64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=linux --cpu=x86_64 --mode=release --package-format=deb --verbose >/home/${PROJECT_PATH}/build_deb.log

(cd /home/${PROJECT_PATH}/; sh /home/${PROJECT_PATH}/purge.sh)
echo 'Done.'

# After all builds and operations on the container finished:
# docker container stop cge
