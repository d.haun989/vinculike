$version_base = Get-Content version_base.inc
$commit_number = (git log -n1 --format=format:"%h") | Out-String
$version = $version_base + $commit_number.Trim()
"'" + $version + "'" | Out-File -encoding ASCII code\generated\version.inc
"`<version value=`"" + $version + "`" `/`>" | Out-File -encoding ASCII code\generated\version.xml
