version=$(cat version_base.inc)$(git rev-list --count HEAD)$1
echo \'$version\' >code/generated/version.inc
echo \<version value=\"$version\" \/\> >code/generated/version.xml
echo $version
