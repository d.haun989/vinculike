{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameViewMainMenu;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls,
  GameScreenEffect;

type
  TViewMainMenu = class(TCastleView)
  strict private
    ScreenEffect: TScreenEffect;
    procedure ClickCredits(Sender: TObject);
    procedure ClickVinculopedia(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickQuit(Sender: TObject);
    procedure ClickPlay(Sender: TObject);
  public
    procedure UpdateScreenEffect;
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  ViewMainMenu: TViewMainMenu;

implementation
uses
  CastleControls, CastleWindow, CastleConfig, CastleApplicationProperties,
  GameFonts, GameSounds, GameSaveGame,
  GameViewCredits, GameViewGame, GameViewOptions, GameViewVinculopedia;

constructor TViewMainMenu.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewmainmenu.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewMainMenu.Start;
begin
  inherited;
  Music('main_menu');

  ScreenEffect := TScreenEffect.Create(FreeAtStop);
  ScreenEffect.Inject(Self);

  (DesignedComponent('ButtonQuit') as TCastleButton).OnClick := @ClickQuit;
  (DesignedComponent('ButtonQuit') as TCastleButton).Exists := not ApplicationProperties.TouchDevice;
  (DesignedComponent('ButtonQuit') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('ButtonStart') as TCastleButton).OnClick := @ClickPlay;
  (DesignedComponent('ButtonStart') as TCastleButton).CustomFont := FontSoniano90;
  if SaveGameExists then
    (DesignedComponent('ButtonStart') as TCastleButton).Caption := 'Continue'
  else
    (DesignedComponent('ButtonStart') as TCastleButton).Caption := 'New Game';

  (DesignedComponent('ButtonOptions') as TCastleButton).OnClick := @ClickOptions;
  (DesignedComponent('ButtonOptions') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('ButtonCredits') as TCastleButton).OnClick := @ClickCredits;
  (DesignedComponent('ButtonCredits') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('LabelVersion') as TCastleLabel).Caption := 'Version: ' + ApplicationProperties.Version;
  (DesignedComponent('LabelVersion') as TCastleLabel).CustomFont := FontBender40;

  (DesignedComponent('LabelSaveIncompatible') as TCastleLabel).CustomFont := FontBender40;
  (DesignedComponent('LabelSaveIncompatible') as TCastleLabel).Exists := not SaveGameCompatible;

  (DesignedComponent('ButtonVinculopedia') as TCastleButton).OnClick := @ClickVinculopedia;
  (DesignedComponent('ButtonVinculopedia') as TCastleButton).CustomFont := FontSoniano90;
end;

procedure TViewMainMenu.ClickCredits(Sender: TObject);
begin
  Sound('menu_button');
  Container.View := ViewCredits;
end;

procedure TViewMainMenu.ClickVinculopedia(Sender: TObject);
begin
  Sound('menu_button');
  Container.PushView(ViewVinculopedia);
end;

procedure TViewMainMenu.ClickOptions(Sender: TObject);
begin
  Sound('menu_button');
  Container.PushView(ViewOptions);
end;

procedure TViewMainMenu.ClickQuit(Sender: TObject);
begin
  Sound('menu_quit');
  Application.MainWindow.Close;
end;

procedure TViewMainMenu.ClickPlay(Sender: TObject);
begin
  if SaveGameExists then
  begin
    Sound('load_game');
  end else
  begin
    Sound('new_game');
  end;
  Container.View := ViewGame;
end;

procedure TViewMainMenu.UpdateScreenEffect;
begin
  if Active then
    ScreenEffect.Enable;
end;

end.

