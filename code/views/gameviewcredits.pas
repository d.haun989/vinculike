{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameViewCredits;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleKeysMouse;

type
  TViewCredits = class(TCastleView)
  strict private
    procedure ClickReturn(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewCredits: TViewCredits;

implementation
uses
  CastleControls, CastleWindow, CastleConfig,
  GameViewMainMenu, GameFonts, GameSounds, GameScreenEffect;

constructor TViewCredits.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewcredits.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewCredits.Start;
begin
  inherited;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;

  (DesignedComponent('LabelTitle') as TCastleLabel).CustomFont := FontBenderBold150;
  (DesignedComponent('LabelAuthor') as TCastleLabel).CustomFont := FontBender90;
  (DesignedComponent('LabelCastleEngine') as TCastleLabel).CustomFont := FontBender90;
  (DesignedComponent('LabelReturn') as TCastleLabel).CustomFont := FontBender40;
end;

function TViewCredits.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);
end;

procedure TViewCredits.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.View := ViewMainMenu;
end;

end.

