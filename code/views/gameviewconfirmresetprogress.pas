{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameViewConfirmResetProgress;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleKeysMouse;

type
  TViewConfirmResetProgress = class(TCastleView)
  strict private
    procedure ClickReset(Sender: TObject);
    procedure ClickReturn(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewConfirmResetProgress: TViewConfirmResetProgress;

implementation
uses
  CastleControls, CastleWindow, CastleConfig, CastleApplicationProperties,
  GameInitialize, GameViewContentWarning, GameFonts, GameSounds,
  GameViewOptions, GameViewMainMenu, GameScreenEffect;

constructor TViewConfirmResetProgress.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewconfirmresetprogress.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewConfirmResetProgress.Start;
begin
  inherited;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  (DesignedComponent('ButtonReset') as TCastleButton).OnClick := @ClickReset;
  (DesignedComponent('ButtonReset') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonCancel') as TCastleButton).OnClick := @ClickReturn;
  (DesignedComponent('ButtonCancel') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('LabelWarning') as TCastleLabel).CustomFont := FontBenderBold150;
  (DesignedComponent('LabelAreYouSure') as TCastleLabel).CustomFont := FontBender90;
end;

function TViewConfirmResetProgress.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);
end;

procedure TViewConfirmResetProgress.ClickReset(Sender: TObject);
begin
  Sound('menu_quit');
  UserConfig.Clear;
  UserConfig.Save;
  ApplySettings;
  Container.View:= ViewContentWarning;
end;

procedure TViewConfirmResetProgress.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.View := ViewMainMenu;
  Container.PushView(ViewOptions);
end;

end.

