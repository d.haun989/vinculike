{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameViewVinculopedia;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes, Generics.Collections,
  CastleControls, CastleUiControls, CastleKeysMouse,
  GameVinculopedia;

type
  TVinculopediaTab = (vtBasic);

type
  TViewVinculopedia = class(TCastleView)
  strict private
    CurrentTab: TVinculopediaTab;
    VerticalGroupTopics: TCastleVerticalGroup;
    VerticalGroupPage: TCastleVerticalGroup;
    procedure ClickReturn(Sender: TObject);
    procedure ClickTopic(Sender: TObject);
    procedure FillInTopics;
    procedure FillInPage;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewVinculopedia: TViewVinculopedia;

implementation
uses
  SysUtils,
  GameFonts, GameSounds, GameScreenEffect, GameUiUtils,
  GameVinculopediaPage;

type
  TVinculopediaTopicButton = class(TCastleButton)
  public
    Page: TVinculopediaPage;
  end;

constructor TViewVinculopedia.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewvinculopedia.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewVinculopedia.Start;
begin
  inherited;
  InterceptInput := true;

  TScreenEffect.Create(FreeAtStop).Inject(Self);

  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;
  (DesignedComponent('ButtonReturn') as TCastleButton).CustomFont := FontSoniano90;

  VerticalGroupTopics := DesignedComponent('VerticalGroupTopics') as TCastleVerticalGroup;
  VerticalGroupPage := DesignedComponent('VerticalGroupPage') as TCastleVerticalGroup;

  CurrentTab := vtBasic; // TODO: do not reset
  FillInTopics;
  FillInPage;
end;

function TViewVinculopedia.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);

  if Event.IsKey(keyF5) or Event.IsKey(keyF11) then
    Result := false; // let screenshot handle
end;

procedure TViewVinculopedia.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.PopView(Self);
end;

procedure TViewVinculopedia.ClickTopic(Sender: TObject);
begin
  if not (Sender is TVinculopediaTopicButton) then
    raise Exception.Create('not Sender is TVinculopediaTopicButton');
  //CurrentPage := ...
  //FillInPage
  VerticalGroupPage.ClearAndFreeControls;
  TVinculopediaTopicButton(Sender).Page.InsertEntry(VerticalGroupPage);
end;

procedure TViewVinculopedia.FillInTopics;
var
  I: Integer;
  B: TVinculopediaTopicButton;
begin
  VerticalGroupTopics.ClearAndFreeControls;
  for I := 0 to Pred(Vinculopedia.Pages.Count) do
  begin
    B := TVinculopediaTopicButton.Create(VerticalGroupTopics);
    if Vinculopedia.Pages[I].IsUnlocked then
    begin
      B.Caption := Vinculopedia.Pages[I].Caption;
      B.Page := Vinculopedia.Pages[I];
      B.OnClick := @ClickTopic;
    end else
      B.Caption := '???????';
    B.CustomFont := FontBender40;
    B.EnableParentDragging := true;
    VerticalGroupTopics.InsertFront(B);
  end;
end;

procedure TViewVinculopedia.FillInPage;
begin
  VerticalGroupPage.ClearAndFreeControls;
  Vinculopedia.Pages[0].InsertEntry(VerticalGroupPage);
end;


end.

