{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameViewGame;

{$INCLUDE compilerconfig.inc}

//{$DEFINE discrete_zoom}

interface

uses Classes,
  CastleVectors, CastleComponentSerialize, CastleColors,
  CastleUIControls, CastleControls, CastleKeysMouse,
  CastleUiShaker, CastleScrollViewBottom,
  GameRandom, GameMap, GameMapInterface, GamePlayerCharacter, GameItemData, GameItemDatabase,
  GameApparelSlots, GameMonster, GameDebugUi, GameScreenEffect, GamePaperDoll,
  GamePauseUi;

const
  FirstPatrolSpawnTime = Single(120);
  EnemyPatrolSpawnTime = Single(210);
  MouseScrollZoomSensitivity = Single(0.1);
  DefaultClickMargin = Single({$IFDEF Mobile}0.07{$ELSE}0.02{$ENDIF});
  DefaultLogLength = 12;
  RestingTimeSpeed = Single(30);
  MaxZoomTiles = 2 * 16;
  //DefaultZoomTiles = 2 * VisibleRange;
  MinZoomTiles = 256;

type
  TViewGame = class(TCastleView)
  strict private
    { Container for "global" screen effects:
      Shaders that affect all other UI elements
      Uses a special in-game class TScreenEffect as a wrapper around TCastleScreenEffecst }
    ScreenEffect: TScreenEffect;
    UiShakerMap: TCastleUiShaker;
    { Handles map displaying, scaling and reacting to clicks }
    MapInterface: TMapInterface;
    { Stat bars, "depleted stats" bars and stat values labels }
    VitalBar, VigorBar, PsycheBar, EnigmaBar, EnigmaBarBackground: TCastleImageControl;
    VitalAntiBar, VigorAntiBar, PsycheAntiBar, EnigmaAntiBar: TCastleImageControl;
    VitalLabel, VigorLabel, PsycheLabel, EnigmaLabel: TCastleLabel;
    { Click callback on "equip item" action }
    procedure EquipItem(Sender: TObject);
    { Find a monster or trap at coordinates }
    function GetMonster(const AX, AY: Single; const IsTrap: Boolean): TMonster;
    { Find an active/captured player character at coordinates }
    function GetPlayer(const AX, AY: Single; const IsCaptured: Boolean): TPlayerCharacter;
    { Try stealth attack into coordinates
      If a monster is found at the destination and is unsuspecting,
      will initiate a stealth attack and return true
      returns false if no stealth attack started }
    function TryStealthAttack(const ClickX, ClickY: Single): Boolean;
    { Calculate which margin should be at current zoom level in tiles
      Always returns at least "1" }
    function ClickAdditionalMargin: Single;
    { Perform short click on the map }
    procedure MapClick(const ClickX, ClickY: Single);
    { Perform long/right click on the map }
    procedure MapLongClick(const ClickX, ClickY: Single);
    { Perform swipe action on the map }
    procedure MapSwipe(const ClickX, ClickY: Single);
    { Click callback to repair item action }
    procedure RepairItem(Sender: TObject);
    { Click callback to unequip item action }
    procedure UnequipItem(Sender: TObject);
  public
    function InternalMapInterface: TMapInterface;
  strict private
    ButtonInventoryToggle: TCastleButton;
    { UI shaker containing the character paperdoll }
    UiShakerCharacter: TCastleUiShaker;
    { Current character's paperdoll }
    PaperDoll: TPaperDoll;
    { UI group containing inventory/ground items buttons }
    InventoryGroup: TCastleUserInterface;
    { UI group containing character's UIs
      Maybe temporary - todo; right now only used for scaling other UI elements }
    CharacterUi: TCastleUserInterface;
    { Layout groups for items buttons }
    VerticalGroupGround, VerticalGroupEquipped: TCastleVerticalGroup;
    { Scroll view for items on the ground }
    ScrollViewBottomGround: TCastleScrollViewBottom;
    procedure ToggleInventoryMode(Sender: TObject);
    { Updates inventory buttons and character's paperdoll }
    procedure DrawInventory;
    procedure DrawMapItemsInventory;
    { Displays a log line about captured characters on the current level }
    procedure ShowReportOnCapturedCharactersOnThisLevel;
    function GetInventoryWidth: Integer;
  strict private
    FirstFrame: Boolean;
    { Time flow speed }
    TimeSpeed: Single;
    // TEMPORARY
    ExitMapCounter: Single;
  strict private
    PlayerLogVerticalGroup: TCastleVerticalGroup;
    MaxZoom, MinZoom: Single;
    ShouldZoomIn, ShouldZoomOut: Boolean;
    ButtonZoomIn, ButtonZoomOut: TCastleButton;
    RestingOverlay: TCastleUserInterface;
    LabelResting: TCastleLabel;
    procedure PressZoomIn(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure PressZoomOut(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure ReleaseZoom(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure DoZoomIn;
    procedure DoZoomOut;
    procedure DoReleaseZoom;
    procedure DoZoom(const Value: Single);
  strict private
    ButtonCamp: TCastleButton;
    function CharacterNotInDanger: Boolean;
    function CharacterSeesMonsters: Boolean;
    function IsResting: Boolean;
    procedure ClickPause(Sender: TObject);
    procedure ClickCamp(Sender: TObject);
    procedure PauseGame;
    procedure TogglePause;
    procedure StopResting;
    procedure SetActivePause;
  public
    procedure StartResting;
    function WakeUp(const ForceWakeUp: Boolean; const ActivePause: Boolean): Boolean;
    function IsPause: Boolean;
    function IsActivePause: Boolean;
    procedure UnPauseGame;
  strict private
    StealthDangerMeter: TCastleImageControl;
    ButtonStealth: TCastleButton;
    LabelStealth: TCastleLabel;
    procedure ClickStealth(Sender: TObject);
  strict private
    class var DelayedUpdatePosition: Boolean;
    class var DelayedUpdateInventory: Boolean;
    class var FFullInventory: Boolean;
    procedure DoUpdatePosition;
    procedure DoUpdateInventory;
  strict private
    PauseUi: TPauseUi;
  public
    { Asks UI to update next frame.
      Only if Sender is CurrentCharacter.
      Will always ask to invalidate if Sender is nil }
    class procedure InvalidatePosition(Sender: TObject);
    class procedure InvalidateInventory(Sender: TObject);
    class function BuggyUpdateInventory: Boolean;
    class function FullInventory: Boolean;
  public
    procedure UpdateScreenEffect;
  public
    VisualizeNoise: Boolean;
    ShouldEndGame: Boolean;
    MonstersToIdlePending: Boolean;
    CurrentCharacter: TPlayerCharacter;
    {
      As I always have problems finiding the info and neither
      https://wiki.freepascal.org/Format_function
      nor
      https://www.freepascal.org/docs-html/rtl/sysutils/format.html
      seem to be anywhere near useful, and don't contain useful examples
      So the regular ones are:
      %s - string
      %.1n - floating-point 10.1
      %d - integer
    }
    procedure Log(const AMessage: String; const Args: array of Const; const AColor: TCastleColor); // some better way to send logs, like "global log" (e.g. when an actor blacks out) and "verbose log" which makes sense only for this character
    procedure ShakeCharacter;
    procedure ShakeMap;
    procedure ScheduleMonstersToIdle;
    procedure StopShakers;
    procedure ScheduleEndGame;
  public
    LabelFps: TCastleLabel;
    DebugUi: TDebugUi;
    constructor Create(AOwner: TComponent); override;
    function NewPlayerCharacter: TPlayerCharacter;
    procedure NewMap;
    procedure StartNewRun;
    procedure StartMusicForMap;
    procedure TeleportAllCharactersToEntrance;
    procedure Start; override;
    procedure Stop; override;
    procedure Resize; override;
    procedure BeforeRender; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    function Release(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewGame: TViewGame;

implementation
uses
  SysUtils, Math,
  CastleConfig,
  TempStamps,
  GameSerializableObject,
  GameActor, GameInventoryItem, GameBodyPart, GameCachedImages, GameMapItem, GameFonts,
  GameSounds, GameTranslation, GameMarkAbstract, GameUiUtils, GameMapTypes,
  TempData, GameVibrate, GameParticle,
  GameActionPlayerEquipItem, GameActionPlayerUnequipItem, GameActionIdle,
  GameActionPlayerStruggleUnequipItem, GameActionPlayerFixItem,
  GameActionPlayerRest, GameActionPlayerUnconscious,
  GameActionPlayerStunned, GameActionKnockback,
  GameActionPlayerDisarmTrap, GameActionStealthAttack, GameActionMove,
  GameActorData, GameMonsterData, GameSaveGame, GameLog, GameColors,
  GameViewEndGame, GameTilesetMap, GameInventoryButton, GameStats;

{ TViewGame ----------------------------------------------------------------- }

constructor TViewGame.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewgame.castle-user-interface';
  DesignPreload := true;
end;

function TViewGame.NewPlayerCharacter: TPlayerCharacter;
var
  E: TApparelSlot;
begin
  Result := TPlayerCharacter.Create;
  Result.Data := TempPlayerCharacter;
  Result.Reset;

  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['sandals']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['rags-top']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['rags-bottom']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['stick']));
  Result.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['old_cape']));
  for E in BodySlots do
    Result.Inventory.EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(E)));

  Map.PlayerCharactersList.Add(Result);
end;

procedure TViewGame.StartNewRun;
var
  PlayerCharacter, LastChar: TPlayerCharacter;
  FreeCharacters: Integer;
begin
  FreeCharacters := 0;
  for PlayerCharacter in Map.PlayerCharactersList do
    if not PlayerCharacter.IsCaptured then
    begin
      LastChar := PlayerCharacter;
      Inc(FreeCharacters);
    end;

  if FreeCharacters = 0 then
    CurrentCharacter := NewPlayerCharacter
  else
    CurrentCharacter := LastChar;

  Map.CurrentDepth := -1;

  UnPauseGame;

  NewMap;
end;

procedure TViewGame.StartMusicForMap;
var
  M: TMonster;
  TotalMonstersHp: Single;
begin
  if Map.CurrentDepth = 0 then
    Music('settlement_empty')
  else
  begin
    TotalMonstersHp := 0;
    for M in Map.MonstersList do
      if M.CanAct and M.Aggressive then
        TotalMonstersHp += M.MaxVital;
    DebugLog('TotalMonstersHp: %d', [Round(TotalMonstersHp)]);
    if map.CurrentDepth <= 10 - 2 + Sqrt(CurrentCharacter.Experience.Level) then // newbie comes to dungeon lvl.10 at lvl.4, so "hard" theme will start playing from lvl.10; for lvl.49 hero = from lvl.15
      Music('dungeon_easy')
    else
      Music('dungeon_hard');
  end;
end;

procedure TViewGame.TeleportAllCharactersToEntrance;
var
  PlayerCharacter: TPlayerCharacter;
begin
  for PlayerCharacter in Map.CharactersOnThisLevel do
    if not PlayerCharacter.IsCaptured and (PlayerCharacter.AtMap = Map.CurrentDepth) then
      PlayerCharacter.Teleport(Map.EntranceX, Map.EntranceY);
end;

procedure TViewGame.Start;
begin
  inherited;

  ScreenEffect := TScreenEffect.Create(FreeAtStop);
  ScreenEffect.Inject(Self);

  VisualizeNoise := false;
  ShouldEndGame := false;

  ButtonZoomIn := DesignedComponent('ButtonZoomIn') as TCastleButton;
  ButtonZoomIn.OnPress := @PressZoomIn;
  ButtonZoomIn.OnRelease := @ReleaseZoom;
  ButtonZoomOut := DesignedComponent('ButtonZoomOut') as TCastleButton;
  ButtonZoomOut.OnPress := @PressZoomOut;
  ButtonZoomOut.OnRelease := @ReleaseZoom;

  RestingOverlay := DesignedComponent('RestingOverlay') as TCastleUserInterface;
  LabelResting := DesignedComponent('LabelResting') as TCastleLabel;
  LabelResting.CustomFont := FontBenderBold150;
  ButtonCamp := DesignedComponent('ButtonCamp') as TCastleButton;
  ButtonCamp.OnClick := @ClickCamp;

  ButtonStealth := DesignedComponent('ButtonStealth') as TCastleButton;
  ButtonStealth.OnClick := @ClickStealth;
  LabelStealth := DesignedComponent('LabelStealth') as TCastleLabel;
  LabelStealth.CustomFont := FontSonianoNumbers30;
  StealthDangerMeter := DesignedComponent('StealthDangerMeter') as TCastleImageControl;

  (DesignedComponent('ButtonPause') as TCastleButton).OnClick := @ClickPause;

  CharacterUi := DesignedComponent('CharacterUi') as TCastleUserInterface;
  VitalBar := DesignedComponent('VitalBar') as TCastleImageControl;
  VigorBar := DesignedComponent('VigorBar') as TCastleImageControl;
  PsycheBar := DesignedComponent('PsycheBar') as TCastleImageControl;
  EnigmaBar := DesignedComponent('EnigmaBar') as TCastleImageControl;
  VitalAntiBar := DesignedComponent('VitalAntiBar') as TCastleImageControl;
  VigorAntiBar := DesignedComponent('VigorAntiBar') as TCastleImageControl;
  PsycheAntiBar := DesignedComponent('PsycheAntiBar') as TCastleImageControl;
  EnigmaAntiBar := DesignedComponent('EnigmaAntiBar') as TCastleImageControl;
  EnigmaBarBackground := DesignedComponent('EnigmaBarBackground') as TCastleImageControl;
  VitalLabel := DesignedComponent('VitalLabel') as TCastleLabel;
  VitalLabel.CustomFont := FontSonianoNumbers20;
  VigorLabel := DesignedComponent('VigorLabel') as TCastleLabel;
  VigorLabel.CustomFont := FontSonianoNumbers20;
  PsycheLabel := DesignedComponent('PsycheLabel') as TCastleLabel;
  PsycheLabel.CustomFont := FontSonianoNumbers20;
  EnigmaLabel := DesignedComponent('EnigmaLabel') as TCastleLabel;
  EnigmaLabel.CustomFont := FontSonianoNumbers20;

  LabelFps := DesignedComponent('LabelFps') as TCastleLabel;
  LabelFps.Exists := false;
  DebugUi := TDebugUi.Create(FreeAtStop);
  DebugUi.Parse(DesignedComponent('DebugUi') as TCastleDesign);

  PauseUi := TPauseUi.Create(FreeAtStop);
  PauseUi.Parse(DesignedComponent('PauseUi') as TCastleDesign);

  VerticalGroupGround := DesignedComponent('VerticalGroupGround') as TCastleVerticalGroup;
  VerticalGroupEquipped := DesignedComponent('VerticalGroupEquipped') as TCastleVerticalGroup;
  ScrollViewBottomGround := DesignedComponent('ScrollViewBottomGround') as TCastleScrollViewBottom;

  ButtonInventoryToggle := DesignedComponent('ButtonInventoryToggle') as TCastleButton;
  ButtonInventoryToggle.OnClick := @ToggleInventoryMode;

  UiShakerCharacter := DesignedComponent('UiShakerCharacter') as TCastleUiShaker;
  PaperDoll := TPaperDoll.Create(Self);
  UiShakerCharacter.InsertFront(PaperDoll);

  InventoryGroup := DesignedComponent('InventoryGroup') as TCastleUserInterface;

  PlayerLogVerticalGroup := DesignedComponent('PlayerLogVerticalGroup') as TCastleVerticalGroup;

  // Create Map Interface

  UiShakerMap := DesignedComponent('UiShakerMap') as TCastleUiShaker;
  MapInterface := TMapInterface.Create(FreeAtStop);
  MapInterface.FullSize := true;
  //MapInterface.Zoom := 1.0; We set it in FirstFrame
  MapInterface.OnMapClick := @MapClick;
  MapInterface.OnMapLongClick := @MapLongClick;
  MapInterface.OnMapSwipe := @MapSwipe;
  UiShakerMap.InsertBack(MapInterface);

  // Create character

  Map := TMap.Create;

  ClearObjectReferences;
  if SaveGameExists then
  begin
    try
      LoadGame;
    except
      ShowError('THERE WAS AN UNHANDLED EXCEPTION LOADING THE GAME.', []);
      ShowError('Unfortunately things like that may happen in the prototype.', []);
      ShowError('For now there is no way we can recover from this bug.', []);
      ShowError('So, the broken save was deleted and a new game has started.', []);
      StartNewRun;
      Exit;
    end;
    StartMusicForMap;
    MapInterface.TeleportTo(CurrentCharacter.CenterX, CurrentCharacter.CenterY);
    ShowLog('Game loaded. Current map level: %d', [Map.CurrentDepth], ColorLogEnterMap);
    ShowLog('%s current level: %d', [CurrentCharacter.Data.DisplayName, CurrentCharacter.Experience.Level], ColorLogExperience);
    ShowLog('Experience: %d; To next level: %d', [Round(CurrentCharacter.Experience.Xp), Round(CurrentCharacter.Experience.ExperienceToNextLevel)], ColorLogExperience);
  end else
    StartNewRun;

  LocalStats.MaxStat('game-started', 1);

  if not CharacterNotInDanger then
  begin
    ShowLog('There are monsters nearby! The game is set to pause', [], ColorLogEnterMap);
    SetActivePause;
  end else
    UnPauseGame; // this will also properly set Resting overlay

  ShowReportOnCapturedCharactersOnThisLevel;

  FirstFrame := true;
  FFullInventory := false;
  InvalidateInventory(nil);
  InvalidatePosition(nil);
end;

procedure TViewGame.NewMap;
var
  MapArea: Integer;
  MinMapSide: Integer;
  MapSizeX, MapSizeY: Int16;
begin
  MonstersToIdlePending := false;

  Inc(Map.CurrentDepth);
  CurrentCharacter.AtMap := Map.CurrentDepth;

  // movemap has insignificant but noticeable lags when calculating path on 512x512
  // frustrating lags on 1024x1024

  if Map.CurrentDepth = 0 then
  begin
    Map.Init(41, 41);
    Map.GenerateStamp(TownStamp);

    Map.GenerateMonsters(-1);
    Map.CacheCharactersOnThisLevel;
    TeleportAllCharactersToEntrance;
    Map.PositionCapturedCharacters; // we still need it if Player uses DEBUG to capture characters
    Map.GenerateItems(2 + Rnd.Random(5));
    Map.ClearGenerationCache;

    ShowLog('%s enters desolate settlement.', [CurrentCharacter.Data.DisplayName], ColorLogEnterMap);
    Map.EnemySpawnTime := Single.MaxValue;
  end else
  begin
    case Map.CurrentDepth of
      1..7: MapArea := 1024 * Map.CurrentDepth;
      else
        MapArea := 7168 + Map.CurrentDepth * 128;
    end;
    MinMapSide := 32 + Round(Sqrt(Map.CurrentDepth));
    MapSizeX := MinMapSide + Rnd.Random(MapArea div MinMapSide - MinMapSide);
    MapSizeY := Trunc(MapArea / MapSizeX);
    Map.Init(MapSizeX, MapSizeY);

    if (MapSizeX > 50) and (MapSizeY > 50) then
    begin
      if (Rnd.Random < 0.2) and (MapSizeX > 70) and (MapSizeY > 70) then
        Map.GenerateStamp(Twins12x12)
      else
      if (Rnd.Random < 0.2) and (MapSizeX > 70) and (MapSizeY > 70) then
        Map.GenerateStamp(Basic13x13)
      else
      if (Rnd.Random < 0.3) and (MapSizeX > 60) and (MapSizeY > 60) then
        Map.GenerateStamp(Basic11x11)
      else
      if Rnd.Random < 0.4 then
        Map.GenerateStamp(Basic10x10)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Basic8x8)
      else
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Basic6x6)
      else
      if Rnd.Random < 0.5 then
        Map.GenerateStamp(Cage12x12)
      else
        Map.GenerateStamp(Cage6x6);
    end else
    begin
      if Rnd.Random < 0.7 then
        Map.GenerateStamp(Basic8x8)
      else
      if Rnd.Random < 0.8 then
        Map.GenerateStamp(Basic6x6)
      else
        Map.GenerateStamp(Cage6x6);
    end;

    case Map.CurrentDepth of
      1: Map.GenerateMonsters(1);
      2: Map.GenerateMonsters(3);
      3: Map.GenerateMonsters(7);
      4: Map.GenerateMonsters(11);
      else
        Map.GenerateMonsters(2.6 * (Rnd.Random(Trunc(Map.FreeArea / 2048.0)) + Trunc(Map.FreeArea / 2048.0) + Trunc(1.15 * Map.CurrentDepth) + Rnd.Random(Map.CurrentDepth div 2)));
    end;
    Map.CacheCharactersOnThisLevel;
    TeleportAllCharactersToEntrance;
    Map.PositionCapturedCharacters;
    Map.GenerateItems(1 + Map.MonstersList.Count div 3 + Rnd.Random(Map.MonstersList.Count div 2)); { 1/3+1/3/2=50% too little; 1/2+1/4=75% too much; 1/3+1/4 = 7/12 =58% }
    Map.GenerateTraps(Rnd.Random(Map.MonstersList.Count div 2));
    Map.ClearGenerationCache;

    ShowLog('Entering abandoned mine level %d.', [Map.CurrentDepth], ColorLogEnterMap);
    Map.EnemySpawnTime := FirstPatrolSpawnTime;
  end;

  ShowLog('%s current level: %d', [CurrentCharacter.Data.DisplayName, CurrentCharacter.Experience.Level], ColorLogExperience);
  ShowLog('Experience: %d; To next level: %d', [Round(CurrentCharacter.Experience.Xp), Round(CurrentCharacter.Experience.ExperienceToNextLevel)], ColorLogExperience);
  ShowReportOnCapturedCharactersOnThisLevel;
  StartMusicForMap;

  MapInterface.TeleportTo(CurrentCharacter.CenterX, CurrentCharacter.CenterY);

  InvalidateInventory(nil);
  InvalidatePosition(nil);

  if Map.CurrentDepth > 1 then
    SaveGame;
end;

procedure TViewGame.UpdateScreenEffect;
begin
  if Active then
  begin
    ScreenEffect.Enable;
    //MapEffect.Enable;
  end;
end;

procedure TViewGame.PressZoomIn(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if not ButtonZoomIn.Enabled then Exit;
  if Event.EventType = itMouseButton then
    DoZoomIn;
end;

procedure TViewGame.PressZoomOut(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if not ButtonZoomOut.Enabled then Exit;
  if Event.EventType = itMouseButton then
    DoZoomOut;
end;

procedure TViewGame.ReleaseZoom(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
begin
  if Event.EventType = itMouseButton then
    DoReleaseZoom;
end;

procedure TViewGame.DoZoomIn;
begin
  ShouldZoomIn := true;
  ShouldZoomOut := false;
end;

procedure TViewGame.DoZoomOut;
begin
  ShouldZoomIn := false;
  ShouldZoomOut := true;
end;

procedure TViewGame.DoReleaseZoom;
begin
  ShouldZoomIn := false;
  ShouldZoomOut := false;
end;

procedure TViewGame.DoZoom(const Value: Single);
begin
  if ShouldZoomIn then
  begin
    {$IFDEF discrete_zoom}
    if MapInterface.Zoom > MaxZoom then Exit;
    if MapInterface.Zoom >= 1 then
      MapInterface.Zoom += 1
    else
      MapInterface.Zoom := 1.0 / (Trunc(1 / MapInterface.Zoom) - 1);
    ShouldZoomIn := false;
    {$ELSE}
    MapInterface.Zoom *= 1 + Value;
    if MapInterface.Zoom > MaxZoom then
      MapInterface.Zoom := MaxZoom;
    {$ENDIF}
  end;
  ButtonZoomIn.Enabled := not IsResting and (MapInterface.Zoom < MaxZoom - 1e-5);
  if ShouldZoomOut then
  begin
    {$IFDEF discrete_zoom}
    if MapInterface.Zoom < MinZoom then Exit;
    if MapInterface.Zoom > 1 then
      MapInterface.Zoom -= 1
    else
      MapInterface.Zoom := 1.0 / (Trunc(1 / MapInterface.Zoom) + 1);
    ShouldZoomOut := false;
    {$ELSE}
    MapInterface.Zoom *= 1 - Value;
    if MapInterface.Zoom < MinZoom then
      MapInterface.Zoom := MinZoom;
    {$ENDIF}
  end;
  ButtonZoomOut.Enabled := not IsResting and (MapInterface.Zoom > MinZoom + 1e-5);
end;

procedure TViewGame.Stop;
begin
  Map.Free; // Not FreeAndNil, because some things in Map.Destroy rely on Map variable (Yes, this is unsafe and ugly)
  Map := nil;
  inherited;
end;

procedure TViewGame.Resize;
begin
  inherited Resize;
  // Container.Width/Height in real device pixels
  MaxZoom := Container.Height / RenderTileSize / MaxZoomTiles;
  MinZoom := Container.Height / RenderTileSize / MinZoomTiles;
  // Self.Width/Height in unscaled pixels
  UiShakerMap.Width := Self.EffectiveWidth;
  UiShakerMap.Height := Self.EffectiveHeight;
  InvalidateInventory(nil);
  InvalidatePosition(nil);
end;

function TViewGame.GetMonster(const AX, AY: Single; const IsTrap: Boolean): TMonster;
var
  M: TMonster;
  DistanceSqr, D: Single;
begin
  DistanceSqr := Single.MaxValue;
  Result := nil;
  for M in Map.MonstersList do
    if M.CanAct and (M.MonsterData.Trap = IsTrap) and (M.IsVisible or M.IsHearable) then
    begin
      D := M.DistanceToSqr(CurrentCharacter);
      if (D < DistanceSqr) and (M.DistanceTo(AX, AY) <= M.HalfSize + ClickAdditionalMargin) then
      begin
        DistanceSqr := D;
        Result := M;
      end;
    end;
end;

function TViewGame.GetPlayer(const AX, AY: Single; const IsCaptured: Boolean): TPlayerCharacter;
var
  P: TPlayerCharacter;
  Distance, D: Single;
begin
  Distance := Single.MaxValue;
  Result := nil;
  for P in Map.CharactersOnThisLevel do
    if (P.IsCaptured = IsCaptured) and P.IsVisible then
    begin
      D := P.DistanceTo(AX, AY);
      if D < Distance then
      begin
        Distance := D;
        Result := P;
      end;
    end;
  if (Result <> nil) and (Distance > Result.HalfSize + ClickAdditionalMargin) then
    Result := nil;
end;

procedure TViewGame.ToggleInventoryMode(Sender: TObject);
begin
  FFullInventory := not FFullInventory;
  InvalidateInventory(nil);
end;

function TViewGame.TryStealthAttack(const ClickX, ClickY: Single): Boolean;
const
  SqrStealthAttackRange = Sqr(15 - 1); // = roll range - 1 ; monster visibility: 23 max; 5 min -> 8 -> 11 -> 14 (player can have 2 items equipped for a stealth attack)
var
  Monster, SelectedMonster: TMonster;
begin
  if (Sqr(CurrentCharacter.X - ClickX) + Sqr(CurrentCharacter.Y - ClickY) < SqrStealthAttackRange * 2) then
  begin
    SelectedMonster := nil;
    for Monster in Map.MonstersList do
      if Monster.CanAct and not Monster.MonsterData.Trap and not Monster.MonsterData.Chest and Monster.Unsuspecting and
        Monster.IsVisible and Monster.IsHere(ClickX, ClickY, ClickAdditionalMargin) and
        (Sqr(CurrentCharacter.CenterX - Monster.CenterX) + Sqr(CurrentCharacter.CenterY - Monster.CenterY) <= SqrStealthAttackRange) then
          SelectedMonster := Monster;
    if SelectedMonster <> nil then
    begin
      //if Map.Ray(CurrentCharacter.PredSize, CurrentCharacter.CenterX, CurrentCharacter.CenterY, SelectedMonster.CenterX, SelectedMonster.CenterY) then // I have no idea why this is broken
      if CurrentCharacter.LineOfSight(SelectedMonster) then
      begin
        CurrentCharacter.CurrentAction := TActionStealthAttack.NewAction(CurrentCharacter);
        TActionStealthAttack(CurrentCharacter.CurrentAction).Target := SelectedMonster;
        CurrentCharacter.CurrentAction.Start;
        Exit(true);
      end else
      begin
        ShowLog(GetTranslation('PlayerStealthAttackImpossible'), [CurrentCharacter.Data.DisplayName, SelectedMonster.Data.DisplayName], ColorLogAttack);
      end;
    end;
  end;
  Exit(false);
end;

function TViewGame.ClickAdditionalMargin: Single;
var
  ClickSize: Single;
begin
  ClickSize := UserConfig.GetFloat('click_margin', DefaultClickMargin) * RenderRect.Height; // in device pixels
  Result := 1.0 + ClickSize / (RenderTileSize * MapInterface.Zoom);
end;

procedure TViewGame.MapClick(const ClickX, ClickY: Single);
var
  MoveToX, MoveToY: Single;
  SelectedMonster: TMonster;
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if WakeUp(false, false) then
    Exit;

  MoveToX := ClickX - CurrentCharacter.HalfSize + 0.5; // I have absolutely no idea why 0.5
  MoveToY := ClickY - CurrentCharacter.HalfSize + 0.5;
  if (mkShift in Container.Pressed.Modifiers) then
    MapSwipe(ClickX, ClickY)
  else
  begin
    SelectedMonster := GetMonster(ClickX, ClickY, false);
    if (SelectedMonster <> nil) and (SelectedMonster.IsHearable or SelectedMonster.IsVisible) then
    begin
      if not TryStealthAttack(ClickX, ClickY) then
        CurrentCharacter.MoveAndAct(SelectedMonster, PlayerActionAttack);
    end else
      CurrentCharacter.MoveTo(MoveToX, MoveToY);
  end;
end;

procedure TViewGame.MapLongClick(const ClickX, ClickY: Single);
var
  Item: TMapItem;
  Monster: TMonster;
  NothingFound: Boolean;
  SelectedMonster: TMonster;
  SelectedPlayer: TPlayerCharacter;
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;
  if (CurrentCharacter.CurrentAction is TActionMove) and not IsPause then
    Exit;

  if WakeUp(false, false) then
    Exit;

  SelectedMonster := GetMonster(ClickX, ClickY, true);
  if SelectedMonster <> nil then
  begin
    if CurrentCharacter.Inventory.CanUseHands then
      CurrentCharacter.MoveAndAct(SelectedMonster, PlayerActionDisarm)
    else
      ShowLog('%s can''t disarm the trap with her own hands restrained', [CurrentCharacter.Data.DisplayName], ColorLogBondage);
    Exit;
  end;

  if TryStealthAttack(ClickX, ClickY) then
    Exit;

  SelectedPlayer := GetPlayer(ClickX, ClickY, true);
  if SelectedPlayer <> nil then
  begin
    if CurrentCharacter.Inventory.CanUseHands then
      CurrentCharacter.MoveAndAct(SelectedPlayer, PlayerActionRescue)
    else
      ShowLog('%s can''t rescue anyone with her hands restrained', [CurrentCharacter.Data.DisplayName], ColorLogBondage);
    Exit;
  end;

  //  if Map.PassablePlayerTiles[Round(ClickX) + Map.SizeX * Round(ClickY)]

  NothingFound := true;
  for Item in Map.MapItemsList do
    if Item.IsHere(ClickX, ClickY, ClickAdditionalMargin) and Item.IsVisible then
    begin
      NothingFound := false;
      if Item.Item.Broken then
        ShowLog(GetTranslation('MapItemBrokenHereLog'), [Item.Item.Data.DisplayName], ColorLogMapItemBroken)
      else
      if Item.Item.Durability > Item.Item.MaxDurability / 3 then
        ShowLog(GetTranslation('MapItemHereLog'), [Item.Item.Data.DisplayName, Round(Item.Item.Durability), Round(Item.Item.MaxDurability)], ColorLogMapItemGood)
      else
        ShowLog(GetTranslation('MapItemHereLog'), [Item.Item.Data.DisplayName, Round(Item.Item.Durability), Round(Item.Item.MaxDurability)], ColorLogMapItemDamaged)
    end;

  for Monster in Map.MonstersList do
    if (Monster.Vital > 0) and Monster.IsHere(ClickX, ClickY, ClickAdditionalMargin) and Monster.IsVisible then
    begin
      NothingFound := false;
      if Monster.Vital < Monster.MaxVital then
        ShowLog(GetTranslation('MonsterWoundedHereLog'), [Monster.Data.DisplayName, Round(Monster.Vital)], ColorLogMapMonster)
      else
        ShowLog(GetTranslation('MonsterHealthyHereLog'), [Monster.Data.DisplayName, Round(Monster.Vital)], ColorLogMapMonster);
      ShowLog(GetTranslation('MonsterStatsLog'), [Round(Monster.GetDamage), Round(Monster.GetSpeed)], ColorLogTemporary);
    end;

  if NothingFound then
    MapClick(ClickX, ClickY);
end;

procedure TViewGame.MapSwipe(const ClickX, ClickY: Single);
var
  RollVector: TVector2;
  MoveToX, MoveToY: Single;
begin
  {if not CurrentCharacter.CurrentAction.CanStop then
    Exit; --------- not sure about swipes. Those can be too useful to block}

  if ((CurrentCharacter.CurrentAction is TActionPlayerStunned) or (CurrentCharacter.CurrentAction is TActionKnockback)) and
    not CurrentCharacter.CurrentAction.CanStop {to show logs if any} then
    Exit; // for now manually 2 actions cannot be canceled by rolling TODO: CanStopForced

  if WakeUp(false, false) then
    Exit;

  MoveToX := ClickX - CurrentCharacter.HalfSize + 0.5; // I have absolutely no idea why 0.5
  MoveToY := ClickY - CurrentCharacter.HalfSize + 0.5;
  if (CurrentCharacter.Vigor > 0) then
  begin
    RollVector := CurrentCharacter.Data.RollRange * Vector2(MoveToX - CurrentCharacter.X, MoveToY - CurrentCharacter.Y).Normalize;
    CurrentCharacter.RollTo(CurrentCharacter.X + RollVector.X, CurrentCharacter.Y + RollVector.Y);
    CurrentCharacter.DegradeVigor(CurrentCharacter.GetRollCost);
  end else
  begin
    CurrentCharacter.MoveTo(MoveToX, MoveToY);
    ShowLog('Not enough vigor for dash roll', [], ColorLogNotEnoughVigor);
  end;
end;

procedure TViewGame.RepairItem(Sender: TObject);
var
  MapItem: TMapItem;
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  MapItem := (Sender as TMapItemButton).MapItem;
  if MapItem.Item.ItemData.CanBeRepaired then
  begin
    if not CurrentCharacter.Inventory.CanUseHands then
    begin
      ShowLog('%s can''t repair %s with her hands restrained', [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
    if not (CurrentCharacter.CurrentAction is TActionPlayerFixItem) or (TActionPlayerFixItem(CurrentCharacter.CurrentAction).MapItem <> MapItem) then
    begin
      CurrentCharacter.CurrentAction := TActionPlayerFixItem.NewAction(CurrentCharacter);
      TActionPlayerFixItem(CurrentCharacter.CurrentAction).MapItem := MapItem;
      CurrentCharacter.CurrentAction.Start;
    end;
  end else
    ShowLog(GetTranslation('ActorItemCannotBeRepaired'), [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
end;

procedure TViewGame.EquipItem(Sender: TObject);
var
  MapItem: TMapItem;
  E: TApparelSlot;
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if WakeUp(false, false) then
    Exit;

  MapItem := (Sender as TMapItemButton).MapItem;
  if not MapItem.Item.Broken then
  begin
    if not CurrentCharacter.Inventory.CanUseHands then
    begin
      ShowLog('%s can''t equip %s with her hands restrained', [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
    for E in MapItem.Item.Data.EquipSlots do
      if (CurrentCharacter.Inventory.Equipped[E] <> nil) and CurrentCharacter.Inventory.Equipped[E].ItemData.IsBondage then
      begin
        ShowLog(GetTranslation('ActorCannotEquipItemLog'), [CurrentCharacter.Data.DisplayName, MapItem.Item.Data.DisplayName, CurrentCharacter.Inventory.Equipped[E].Data.DisplayName, TActionPlayerEquipItem.Duration], ColorLogBondage);
        Exit;
      end;
    if not (CurrentCharacter.CurrentAction is TActionPlayerEquipItem) or (TActionPlayerEquipItem(CurrentCharacter.CurrentAction).MapItem <> MapItem) then
    begin
      CurrentCharacter.CurrentAction := TActionPlayerEquipItem.NewAction(CurrentCharacter);
      TActionPlayerEquipItem(CurrentCharacter.CurrentAction).MapItem := MapItem;
      CurrentCharacter.CurrentAction.Start;
    end;
  end else
  begin
    ShowLog(GetTranslation('ActorCannotEquipBrokenItemLog'), [MapItem.Item.Data.DisplayName], ColorLogMapItemBroken);
    RepairItem(Sender);
  end;
end;

procedure TViewGame.ClickStealth(Sender: TObject);
begin
  VisualizeNoise := not VisualizeNoise;
end;

function TViewGame.CharacterNotInDanger: Boolean;
begin
  Exit(Map.NoMonstersNearby(CurrentCharacter.LastTileX, CurrentCharacter.LastTileY, 3));
end;

function TViewGame.CharacterSeesMonsters: Boolean;
begin
  Exit(Map.MonsterSeen(CurrentCharacter.LastTileX, CurrentCharacter.LastTileY, TPlayerCharacter.VisibleRange));
end;

function TViewGame.IsResting: Boolean;
begin
  Exit(
    (CurrentCharacter.CurrentAction is TActionPlayerRest) or
    (CurrentCharacter.CurrentAction is TActionPlayerUnconscious)
  );
end;

function TViewGame.IsPause: Boolean;
begin
  Exit(PauseUi.Exists);
end;

procedure TViewGame.ClickPause(Sender: TObject);
begin
  if IsPause then
  begin
    Sound('unpause_game');
    UnPauseGame;
  end else
  begin
    Sound('pause_game');
    PauseGame;
  end;
end;

procedure TViewGame.ClickCamp(Sender: TObject);
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if IsResting then
  begin
    Sound('stop_rest');
    StopResting;
    if CharacterSeesMonsters then
    begin
      ShowLog('%s wakes up in danger! The game is set to pause', [CurrentCharacter.Data.DisplayName], ColorDefault);
      SetActivePause;
    end;
  end else
  begin
    if CharacterSeesMonsters then
    begin
      ShowLog('%s feels danger and can''t let her guard down', [CurrentCharacter.Data.DisplayName], ColorDefault);
      Exit;
    end;
    Sound('start_rest');
    StartResting;
  end;
end;

procedure TViewGame.PauseGame;
begin
  TimeSpeed := 0.0;
  PauseUi.Show;
  ButtonCamp.Exists := true;
  RestingOverlay.Exists := IsResting;
end;

procedure TViewGame.UnPauseGame;
begin
  if IsResting then
    TimeSpeed := RestingTimeSpeed
  else
    TimeSpeed := 1.0;
  PauseUi.Hide;
  ButtonCamp.Exists := IsResting;
  RestingOverlay.Exists := IsResting;
  if (CurrentCharacter.CurrentAction is TActionPlayerRest) then
    LabelResting.Caption := 'Resting...'
  else
  if (CurrentCharacter.CurrentAction is TActionPlayerUnconscious) then
    LabelResting.Caption := 'Unconscious.';
end;

procedure TViewGame.TogglePause;
begin
  if IsPause then
    UnPauseGame
  else
    PauseGame;
end;

procedure TViewGame.StartResting;
begin
  CurrentCharacter.CurrentAction := TActionPlayerRest.NewAction(CurrentCharacter);
  CurrentCharacter.CurrentAction.Start;
  if IsPause then
    UnPauseGame;
end;

procedure TViewGame.StopResting;
begin
  CurrentCharacter.CurrentAction := TActionIdle.NewAction(CurrentCharacter);
  CurrentCharacter.CurrentAction.Start;
  UnPauseGame;
end;

procedure TViewGame.SetActivePause;
begin
  PauseUi.PartyActive := false;
  PauseUi.PauseMenuActive := false;
  PauseGame;
end;

function TViewGame.WakeUp(const ForceWakeUp: Boolean;
  const ActivePause: Boolean): Boolean;
begin
  if not IsResting then
    Exit(false);

  Result := true; // an attempt was made, input should be discarded regardless of the result
  if ForceWakeUp or CurrentCharacter.CurrentAction.CanStop then
  begin
    StopResting;
    if ActivePause then
    begin
      ShowLog('%s wakes up in danger! The game is set to pause', [CurrentCharacter.Data.DisplayName], ColorDefault);
      SetActivePause;
    end;
  end;
end;

function TViewGame.IsActivePause: Boolean;
begin
  Exit(IsPause and not PauseUi.PauseMenuActive and not PauseUi.PartyActive);
end;

procedure TViewGame.UnequipItem(Sender: TObject);
var
  Item: TInventoryItem;
begin
  if not CurrentCharacter.CurrentAction.CanStop then
    Exit;

  if WakeUp(false, false) then
    Exit;

  Item := (Sender as TInventoryButton).Item;

  if not (esWeapon in Item.Data.EquipSlots) and not CurrentCharacter.Inventory.CanUseHands then // Todo: more general
  begin
    ShowLog('%s can''t unequip %s with her hands restrained', [CurrentCharacter.Data.DisplayName, Item.Data.DisplayName], ColorLogBondage);
    Exit;
  end;

  if esNeck in Item.Data.EquipSlots then // Todo: more general
  begin
    ShowLog('%s gropes the %s around her neck', [CurrentCharacter.Data.DisplayName, Item.Data.DisplayName], ColorLogBondage);
    ShowLog('It doesn''t take a degree in chemistry to say it''s made from solid titanium', [], ColorLogBondage);
    ShowLog('She has no idea who or how put it on, but one thing is sure: it won''t go off', [], ColorLogBondage);
    ShowLog('At least it''s light enough and doesn''t restrain movements', [], ColorLogBondage);
    ShowLog('A large symbol "∀" is embossed on the %s''s surface', [Item.Data.DisplayName], ColorLogBondage);
    Exit;
  end;

  if Item.ItemData.IsBondage then
  begin
    if CurrentCharacter.Vigor < 0 then // Todo: more general
    begin
      ShowLog('%s is completely exhausted and cannot struggle', [CurrentCharacter.Data.DisplayName], ColorLogBondage);
      Exit;
    end;
    CurrentCharacter.CurrentAction := TActionPlayerStruggleUnequipItem.NewAction(CurrentCharacter);
    TActionPlayerStruggleUnequipItem(CurrentCharacter.CurrentAction).ItemSlot := Item.Data.MainSlot;
    CurrentCharacter.CurrentAction.Start;
  end else
  begin
    CurrentCharacter.CurrentAction := TActionPlayerUnequipItem.NewAction(CurrentCharacter);
    TActionPlayerUnequipItem(CurrentCharacter.CurrentAction).ItemSlot := Item.Data.MainSlot;
    CurrentCharacter.CurrentAction.Start;
  end;
end;

function TViewGame.InternalMapInterface: TMapInterface;
begin
  Exit(MapInterface);
end;

procedure TViewGame.DrawInventory;
var
  E: TApparelSlot;
  I: Integer;
  ShouldBreak: Boolean;
  Button: TInventoryButtonAbstract;
begin
  InventoryGroup.Translation := Vector2(-CharacterUi.EffectiveWidth, InventoryGroup.Translation.Y);
  InventoryGroup.Width := GetInventoryWidth;
  VerticalGroupEquipped.ClearAndFreeControls;
  for E in UiApparelSlots do
  begin
    if CurrentCharacter.Inventory.Apparel[E] <> nil then
    begin
      // if has previous duplicates, they are already considered.
      ShouldBreak := false;
      for I := 0 to Pred(Ord(E)) do
        if CurrentCharacter.Inventory.Apparel[TApparelSlot(I)] = CurrentCharacter.Inventory.Apparel[E] then
        begin
          ShouldBreak := true;
          Break;
        end;
      if ShouldBreak then
        Continue;
    end;
    if CurrentCharacter.Inventory.Apparel[E] <> nil then
    begin
      Button := TInventoryButton.Create(VerticalGroupEquipped);
      Button.OnClick := @UnequipItem;
      TInventoryButton(Button).Item := CurrentCharacter.Inventory.Equipped[E];
    end else
    begin
      Button := TEmptyInventoryButton.Create(VerticalGroupEquipped);
      TEmptyInventoryButton(Button).Slot := E;
    end;
    Button.Setup;
    Button.Width := InventoryGroup.EffectiveWidth;
    VerticalGroupEquipped.InsertBack(Button);
  end;
end;

procedure TViewGame.DrawMapItemsInventory;
var
  MapItem: TMapItem;
  Button: TMapItemButton;
begin
  InventoryGroup.Translation := Vector2(-CharacterUi.EffectiveWidth, InventoryGroup.Translation.Y);
  InventoryGroup.Width := GetInventoryWidth;
  VerticalGroupGround.ClearAndFreeControls;
  for MapItem in Map.MapItemsList do
    if MapItem.CollidesInt(CurrentCharacter, 1) then
    begin
      Button := TMapItemButton.Create(VerticalGroupGround);
      Button.MapItem := MapItem;
      Button.OnShortClick := @EquipItem;
      Button.OnLongClick := @RepairItem;
      Button.Setup;
      Button.Width := InventoryGroup.EffectiveWidth;
      VerticalGroupGround.InsertBack(Button);
    end;
  if ScrollViewBottomGround.Scroll > ScrollViewBottomGround.ScrollMax then
    ScrollViewBottomGround.Scroll := ScrollViewBottomGround.ScrollMax;
  if ScrollViewBottomGround.Scroll < ScrollViewBottomGround.ScrollMin then
    ScrollViewBottomGround.Scroll := ScrollViewBottomGround.ScrollMin;
  ScrollViewBottomGround.DoScrollChange;
end;

procedure TViewGame.ShowReportOnCapturedCharactersOnThisLevel;
var
  CaptivesCount: Integer;
begin
  CaptivesCount := Map.CapturedCharactersAtLevel;
  case CaptivesCount of
    0: ; // show nothing
    1: ShowLog('There is a character imprisoned on this level. Find and long-press/right-click to rescue her.', [], ColorLogCapturedCharacterHere);
    else
      ShowLog('There are %d characters imprisoned on this level. Find and long-press/right-click to rescue them.', [CaptivesCount], ColorLogCapturedCharacterHere);
  end;
end;

function TViewGame.GetInventoryWidth: Integer;
begin
  if FullInventory then
    Exit(600)
  else
    Exit(140);
end;

class procedure TViewGame.InvalidateInventory(Sender: TObject);
begin
  if (Sender <> nil) and not (Sender is TPlayerCharacter) then
  begin
    ShowError('Cannot invalidate class %s, expected TPlayerCharacter', [Sender.ClassName]);
    DelayedUpdateInventory := true;
    Exit;
  end;
  if (Sender = ViewGame.CurrentCharacter) or (Sender = nil) then
    DelayedUpdateInventory := true;
end;

class function TViewGame.BuggyUpdateInventory: Boolean;
begin
  Exit(DelayedUpdateInventory);
end;

class function TViewGame.FullInventory: Boolean;
begin
  Exit(FFullInventory);
end;

class procedure TViewGame.InvalidatePosition(Sender: TObject);
begin
  if (Sender <> nil) and not (Sender is TPlayerCharacter) then
  begin
    ShowError('Cannot invalidate class %s, expected TPlayerCharacter', [Sender.ClassName]);
    DelayedUpdatePosition := true;
    Exit;
  end;
  if (Sender = ViewGame.CurrentCharacter) or (Sender = nil) then
    DelayedUpdatePosition := true;
end;

procedure TViewGame.DoUpdateInventory;
var
  P: TPlayerCharacter;
begin
  DelayedUpdateInventory := false;
  for P in Map.CharactersOnThisLevel do
    P.Inventory.PaperDollSprite.UpdateToInventory; // TODO: optimize, we often need to update just one
  PaperDoll.UpdateToInventory;
  DrawInventory;
  DoUpdatePosition; // TODO: Optimize, we don't always need this here
end;
procedure TViewGame.DoUpdatePosition;
begin
  DelayedUpdatePosition := false;
  DrawMapItemsInventory;
end;

procedure TViewGame.Log(const AMessage: String;
  const Args: array of const; const AColor: TCastleColor);
var
  NormalLabel, ShadowLabel: TCastleLabel;
  Removed: TCastleUserInterface;
begin
  ShadowLabel := TCastleLabel.Create(PlayerLogVerticalGroup);
  ShadowLabel.Caption := Format(AMessage, Args);
  ShadowLabel.Color := CastleColors.Black;
  ShadowLabel.CustomFont := FontBender20;
  NormalLabel := TCastleLabel.Create(ShadowLabel);
  NormalLabel.Caption := Format(AMessage, Args);
  NormalLabel.Color := AColor;
  NormalLabel.Translation := Vector2(1, -1);
  NormalLabel.CustomFont := FontBender20;
  ShadowLabel.InsertFront(NormalLabel);
  PlayerLogVerticalGroup.InsertFront(ShadowLabel);
  while PlayerLogVerticalGroup.ControlsCount > UserConfig.GetInteger('log_length', DefaultLogLength) do
  begin
    Removed := PlayerLogVerticalGroup.Controls[0];
    PlayerLogVerticalGroup.RemoveControl(Removed);
    Removed.Free;
  end;
end;

procedure TViewGame.ShakeCharacter;
begin
  if UserConfig.GetValue('shake_screen', true) then
    UiShakerCharacter.Shake(30, 0.5);
  Vibrate(200);
end;

procedure TViewGame.ShakeMap;
begin
  if UserConfig.GetValue('shake_screen', true) then
    UiShakerMap.Shake(30, 0.5);
  Vibrate(100);
end;

procedure TViewGame.ScheduleMonstersToIdle;
begin
  MonstersToIdlePending := true;
end;

procedure TViewGame.StopShakers;
begin
  UiShakerCharacter.StopShake;
  UiShakerMap.StopShake;
end;

procedure TViewGame.ScheduleEndGame;
begin
  ShouldEndGame := true;
  // make sure no longer existing items won't receive update - doesn't feel clean, maybe TODO?
  VerticalGroupEquipped.ClearAndFreeControls;
  VerticalGroupGround.ClearAndFreeControls;
end;

procedure TViewGame.BeforeRender;

  procedure DoFirstFrame;
  begin
    MapInterface.Zoom := Container.Height / RenderTileSize / 2.0 / TPlayerCharacter.VisibleRange;
  end;

var
  Ui: TCastleUserInterface;
begin
  inherited BeforeRender;

  if FirstFrame then
  begin
    DoFirstFrame;
    FirstFrame := False;
  end;

  if DelayedUpdateInventory then
    DoUpdateInventory
  else
  if DelayedUpdatePosition then
    DoUpdatePosition;
  for Ui in VerticalGroupEquipped do
    if Ui is TInventoryButton then
      TInventoryButton(Ui).UpdateCaption;
  for Ui in VerticalGroupGround do
    if Ui is TInventoryButton then
      TInventoryButton(Ui).UpdateCaption;
end;

procedure TViewGame.Update(const SecondsPassed: Single; var HandleInput: Boolean);
const
  SecondsWarning = 12;
  NextMapTimeout = 4;
var
  LX, LY: Int16;
  PhysicalSeconds: Single;
  M: TMonster;
  P: TParticle;
  PlayerChar: TPlayerCharacter;
  Mark: TMarkAbstract;
  TimeI: Integer;

  procedure SpawnPatrolMonsters;
  var
    AMonster: TMonster;
    MonsterData: TMonsterData;
    MonstersToSpawn: Integer;
    SpawnVacuumCleaner: Boolean;

    procedure DoSpawnPatrol;
    var
      I: Integer;
      SX, SY: Int16;
      GoX, GoY: Int16;
    begin
      repeat
        GoX := Rnd.Random(Map.SizeX);
        GoY := Rnd.Random(Map.SizeY);
      until Map.PassableTiles[Pred(MonsterData.Size)][GoX + Map.SizeX * GoY];

      for I := 0 to MonstersToSpawn do
      begin
        if MonsterData.CuriousAi then
          repeat
            GoX := Rnd.Random(Map.SizeX);
            GoY := Rnd.Random(Map.SizeY);
          until Map.PassableTiles[Pred(MonsterData.Size)][GoX + Map.SizeX * GoY];

        AMonster := TMonster.Create;
        AMonster.Data := MonsterData;
        AMonster.Reset;
        repeat
          SX := Map.ExitX - AMonster.PredSize + Rnd.Random(3 + AMonster.PredSize * 2);
          SY := Map.ExitY - AMonster.PredSize + Rnd.Random(3 + AMonster.PredSize * 2);
        until (SX > 0) and (SY > 0) and (SX < Map.SizeX - AMonster.Size) and (SY < Map.SizeY - AMonster.Size) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
        AMonster.Teleport(SX, SY);
        AMonster.Ai.Guard := false;
        AMonster.MoveTo(GoX, GoY);
        AMonster.Ai.Timeout := (AMonster.CurrentAction as TActionMove).RemainingTime;
        Map.MonstersList.Add(AMonster);
      end;
    end;

    function GetMonsterInPatrol(const IsRare: Boolean): TMonsterData;
    var
      M: Integer;
      ML: TActorDataList;
    begin
      ML := TActorDataList.Create(false);
      for M := 0 to Pred(Map.PatrolList.Count) do
        if (Map.PatrolList[M] as TMonsterData).Rare = IsRare then
          ML.Add(Map.PatrolList[M]);
      if ML.Count > 0 then
        Result := ML[Rnd.Random(ML.Count)] as TMonsterData
      else
        Result := nil;
      ML.Free;
    end;

  begin
    SpawnVacuumCleaner := (Map.MapItemsList.Count > 3) and (Rnd.Random < 0.2);
    if SpawnVacuumCleaner then
      for AMonster in Map.MonstersList do
        if AMonster.CanAct and AMonster.MonsterData.VacuumCleaner then
          SpawnVacuumCleaner := false;

    if SpawnVacuumCleaner then
    begin
      MonsterData := VacuumCleaner;
      MonstersToSpawn := 0;
      DoSpawnPatrol;
    end;

    //spawn regular monsters
    if Map.PatrolList.Count = 0 then
    begin
      ShowError('Cannot find any monster to spawn patrol at level %d', [Map.CurrentDepth]);
      Exit;
    end;

    MonsterData := GetMonsterInPatrol(false);
    if MonsterData <> nil then
    begin
      MonstersToSpawn := 1 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / MonsterData.Danger)) + 1); // 2 - 3 on lvl.1 2 - 8 on lvl.25
      DoSpawnPatrol;
    end;

    if Rnd.Random < 0.3 then
    begin
      MonsterData := GetMonsterInPatrol(true);
      if MonsterData <> nil then
      begin
        MonstersToSpawn := 0 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / MonsterData.Danger / 3)));
        DoSpawnPatrol;
      end;
    end;
  end;

  procedure UpdateDangerSensor;
  var
    MAttacking: Boolean;
  begin
    MAttacking := Map.MonstersAttacking;
    StealthDangerMeter.Exists := MAttacking or CharacterSeesMonsters;
    if MAttacking then
      StealthDangerMeter.Color := ColorStealthDanger
    else
      StealthDangerMeter.Color := ColorStealthWarning;
  end;

begin
  inherited;

  DoZoom(SecondsPassed);

  PhysicalSeconds := SecondsPassed * TimeSpeed;

  Map.TimeSpentOnTheMap += PhysicalSeconds;
  Map.UpdateVisible;

  UpdateDangerSensor;

  VitalBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Vital / CurrentCharacter.PlayerCharacterData.MaxVital);
  VitalAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxVital / CurrentCharacter.PlayerCharacterData.MaxVital);
  VitalLabel.Caption := Round(CurrentCharacter.Vital).ToString;
  VigorBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Vigor / CurrentCharacter.PlayerCharacterData.MaxVigor);
  VigorAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxVigor / CurrentCharacter.PlayerCharacterData.MaxVigor);
  VigorLabel.Caption := Round(CurrentCharacter.Vigor).ToString;
  PsycheBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Psyche / CurrentCharacter.PlayerCharacterData.MaxPsyche);
  PsycheAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxPsyche / CurrentCharacter.PlayerCharacterData.MaxPsyche);
  PsycheLabel.Caption := Round(CurrentCharacter.Psyche).ToString;
  if CurrentCharacter.PlayerCharacterData.MaxEnigma > 0 then
  begin
    EnigmaBarBackground.Exists := true;
    EnigmaBar.ClipLine := Vector3(-1, 0, CurrentCharacter.Enigma / CurrentCharacter.PlayerCharacterData.MaxEnigma);
    EnigmaAntiBar.ClipLine := Vector3(1, 0, -CurrentCharacter.MaxEnigma / CurrentCharacter.PlayerCharacterData.MaxEnigma);
    EnigmaLabel.Caption := Round(CurrentCharacter.Enigma).ToString;
  end else
    EnigmaBarBackground.Exists := false;

  LX := CurrentCharacter.LastTileX;
  LY := CurrentCharacter.LastTileY;

  for PlayerChar in Map.CharactersOnThisLevel do
    PlayerChar.Update(PhysicalSeconds);
  LabelStealth.Caption := Round(CurrentCharacter.GetNoise).ToString;

  for M in Map.MonstersList do
    if M.CanAct then
      M.Update(PhysicalSeconds)
    else
      if not (M.CurrentAction is TActionIdle) then
      begin
        // Ugly workaround
        M.CurrentAction := TActionIdle.NewAction(M);
        ShowError('Resetting action for %s to idle', [M.Data.DisplayName]);
      end;

  try
    for Mark in Map.MarksList do
      Mark.Update(PhysicalSeconds);
  except
    ShowError('ERROR: exception during mark.Update.', []);
  end;

  for P in Map.ParticlesList do
    P.Update(SecondsPassed);

  // show warning about incoming patrol
  if (Map.EnemySpawnTime < SecondsWarning) and (CurrentCharacter.DistanceTo(Map.ExitX, Map.ExitY) < 30) and
     MapInterface.ExitVisible then
  begin
    for TimeI := 0 to SecondsWarning div 2 do
      if (Map.EnemySpawnTime > 1 + 2 * TimeI) and (Map.EnemySpawnTime - PhysicalSeconds <= 1 + 2 * TimeI) then
      begin
        NewParticle(Map.ExitX + 1.5, Map.ExitY, 'WARNING', ColorParticleSpawn);
        break;
      end;
  end;
  Map.EnemySpawnTime -= PhysicalSeconds;
  if Map.EnemySpawnTime < 0 then
  begin
    Map.EnemySpawnTime := EnemyPatrolSpawnTime;
    SpawnPatrolMonsters;
  end;

  if not IsPause then
  begin
    MapInterface.TargetX := CurrentCharacter.CenterX;
    MapInterface.TargetY := CurrentCharacter.CenterY;
  end;

  // temporary?
  if (LX <> CurrentCharacter.LastTileX) or (LY <> CurrentCharacter.LastTileY) then
    InvalidatePosition(CurrentCharacter);

  if (CurrentCharacter.LastTileX >= Map.ExitX - 2) and (CurrentCharacter.LastTileX <= Map.ExitX + 2) and
     (CurrentCharacter.LastTileY >= Map.ExitY - 2) and (CurrentCharacter.LastTileY <= Map.ExitY + 2) and
     (CurrentCharacter.CurrentAction is TActionIdle) then
  begin
    for TimeI := 0 to Pred(NextMapTimeout) do
      if (ExitMapCounter + PhysicalSeconds > TimeI) and (ExitMapCounter <= TimeI) then
      begin
        NewParticle(Map.ExitX + 1.5, Map.ExitY, 'GO DOWN ' + IntToStr(NextMapTimeout - TimeI), ColorParticleExit);
        break;
      end;
    ExitMapCounter += PhysicalSeconds;
    if ExitMapCounter > NextMapTimeout then
      NewMap;
  end else
    ExitMapCounter := 0;

  if LabelFps.Exists then
    LabelFps.Caption := Format('%d', [Round(Container.Fps.RealFps)]);

  if MonstersToIdlePending then
  begin
    MonstersToIdlePending := false;
    Map.MonstersToIdle;
  end;

  if ShouldEndGame then
  begin
    Container.PushView(ViewEndGame);
    StartNewRun;
    SaveGame;
    ShouldEndGame := false;
    TimeSpeed := 0.0; // PauseGame? Todo
  end;

  if (SecondsSinceLastSave > 30) and (CurrentCharacter.CurrentAction is TActionIdle) then
    SaveGame;
end;

function TViewGame.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Result then
    Exit;

  if Event.IsKey(keyPlus) or Event.IsKey(keyEqual) or Event.IsKey(keyNumpadPlus) then
    DoZoomIn;
  if Event.IsKey(keyMinus) or Event.IsKey(keyReserved_95) or Event.IsKey(keyNumpadMinus) then
    DoZoomOut;
  if Event.EventType = itMouseWheel then
    if Event.MouseWheelScroll > 0 then
    begin
      DoZoomIn;
      DoZoom(Event.MouseWheelScroll * MouseScrollZoomSensitivity);
      DoReleaseZoom;
    end else
    if Event.MouseWheelScroll < 0 then // avoid changing anything if = 0
    begin
      DoZoomOut;
      DoZoom(-Event.MouseWheelScroll * MouseScrollZoomSensitivity);
      DoReleaseZoom;
    end;

  if Event.IsKey(keyEscape) then
    TogglePause;

  if Event.IsKey(keyF1) then
    DebugUi.Toggle;
end;

function TViewGame.Release(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyPlus) or Event.IsKey(keyEqual) or Event.IsKey(keyNumpadPlus) or
     Event.IsKey(keyMinus) or Event.IsKey(keyReserved_95) or Event.IsKey(keyNumpadMinus) then
    DoReleaseZoom;
end;

end.
