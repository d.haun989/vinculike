{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameViewOptions;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleControls, CastleUiControls, CastleKeysMouse,
  GameScreenEffect;

type
  TViewOptions = class(TCastleView)
  strict private
    ScreenEffect: TScreenEffect;
    ButtonMasterVolume0, ButtonMasterVolume7, ButtonMasterVolume15,
      ButtonMasterVolume25, ButtonMasterVolume50,
      ButtonMasterVolume75, ButtonMasterVolume100: TCastleButton;
    ButtonMusicVolume0, ButtonMusicVolume7, ButtonMusicVolume15,
      ButtonMusicVolume25, ButtonMusicVolume50,
      ButtonMusicVolume75, ButtonMusicVolume100: TCastleButton;
    ButtonFullScreen, ButtonVibration, ButtonShakeScreen,
      ButtonCensored, ButtonLimitFps, ButtonClickMargin, ButtonSpecialEffects,
      ButtonLogLength: TCastleButton;
    ScrollViewOptions: TCastleUserInterface;
    procedure ClickCensored(Sender: TObject);
    procedure ClickDeleteSave(Sender: TObject);
    procedure ClickEndRun(Sender: TObject);
    procedure ClickFullScreen(Sender: TObject);
    procedure ClickLimitFps(Sender: TObject);
    procedure ClickClickMargin(Sender: TObject);
    procedure ClickLogLength(Sender: TObject);
    procedure ClickResetProgress(Sender: TObject);
    procedure ClickReturn(Sender: TObject);
    procedure ClickMasterVolume(Sender: TObject);
    procedure ClickMusicVolume(Sender: TObject);
    procedure ClickShakeScreen(Sender: TObject);
    procedure ClickSpecialEffects(Sender: TObject);
    procedure ClickVibration(Sender: TObject);
    procedure SetMasterVolume(const Volume: Single);
    procedure SetMusicVolume(const Volume: Single);
    procedure UpdateButtonsCaptions;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Stop; override;
    function Press(const Event: TInputPressRelease): Boolean; override;
    procedure Resize; override;
  end;

var
  ViewOptions: TViewOptions;

implementation
uses
  SysUtils,
  CastleWindow, CastleConfig, CastleApplicationProperties, CastleXmlUtils,
  GameFonts, GameVibrate, GameSounds, GameSaveGame, GameLog, GameInitialize,
  GameColors,
  GameViewMainMenu, GameViewConfirmResetProgress, GameViewConfirmDeleteSave,
  GameViewConfirmNewRun, GameViewGame;

type
  EUnexpectedButton = class(Exception);

constructor TViewOptions.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewoptions.castle-user-interface';
  DesignPreload := false;
end;

procedure TViewOptions.Start;
begin
  inherited;
  InterceptInput := true;

  ScreenEffect := TScreenEffect.Create(FreeAtStop);
  ScreenEffect.Inject(Self);

  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ClickReturn;
  (DesignedComponent('ButtonReturn') as TCastleButton).CustomFont := FontSoniano90;

  (DesignedComponent('LabelMasterVolume') as TCastleLabel).CustomFont := FontBender90;
  ButtonMasterVolume0 := DesignedComponent('ButtonMasterVolume0') as TCastleButton;
  ButtonMasterVolume0.OnClick := @ClickMasterVolume;
  ButtonMasterVolume0.CustomFont := FontSoniano90;
  ButtonMasterVolume7 := DesignedComponent('ButtonMasterVolume7') as TCastleButton;
  ButtonMasterVolume7.OnClick := @ClickMasterVolume;
  ButtonMasterVolume7.CustomFont := FontSoniano90;
  ButtonMasterVolume15 := DesignedComponent('ButtonMasterVolume15') as TCastleButton;
  ButtonMasterVolume15.OnClick := @ClickMasterVolume;
  ButtonMasterVolume15.CustomFont := FontSoniano90;
  ButtonMasterVolume25 := DesignedComponent('ButtonMasterVolume25') as TCastleButton;
  ButtonMasterVolume25.OnClick := @ClickMasterVolume;
  ButtonMasterVolume25.CustomFont := FontSoniano90;
  ButtonMasterVolume50 := DesignedComponent('ButtonMasterVolume50') as TCastleButton;
  ButtonMasterVolume50.OnClick := @ClickMasterVolume;
  ButtonMasterVolume50.CustomFont := FontSoniano90;
  ButtonMasterVolume75 := DesignedComponent('ButtonMasterVolume75') as TCastleButton;
  ButtonMasterVolume75.OnClick := @ClickMasterVolume;
  ButtonMasterVolume75.CustomFont := FontSoniano90;
  ButtonMasterVolume100 := DesignedComponent('ButtonMasterVolume100') as TCastleButton;
  ButtonMasterVolume100.OnClick := @ClickMasterVolume;
  ButtonMasterVolume100.CustomFont := FontSoniano90;
  SetMasterVolume(UserConfig.GetFloat('master_volume', DefaultVolume));

  (DesignedComponent('LabelMusicVolume') as TCastleLabel).CustomFont := FontBender90;
  ButtonMusicVolume0 := DesignedComponent('ButtonMusicVolume0') as TCastleButton;
  ButtonMusicVolume0.OnClick := @ClickMusicVolume;
  ButtonMusicVolume0.CustomFont := FontSoniano90;
  ButtonMusicVolume7 := DesignedComponent('ButtonMusicVolume7') as TCastleButton;
  ButtonMusicVolume7.OnClick := @ClickMusicVolume;
  ButtonMusicVolume7.CustomFont := FontSoniano90;
  ButtonMusicVolume15 := DesignedComponent('ButtonMusicVolume15') as TCastleButton;
  ButtonMusicVolume15.OnClick := @ClickMusicVolume;
  ButtonMusicVolume15.CustomFont := FontSoniano90;
  ButtonMusicVolume25 := DesignedComponent('ButtonMusicVolume25') as TCastleButton;
  ButtonMusicVolume25.OnClick := @ClickMusicVolume;
  ButtonMusicVolume25.CustomFont := FontSoniano90;
  ButtonMusicVolume50 := DesignedComponent('ButtonMusicVolume50') as TCastleButton;
  ButtonMusicVolume50.OnClick := @ClickMusicVolume;
  ButtonMusicVolume50.CustomFont := FontSoniano90;
  ButtonMusicVolume75 := DesignedComponent('ButtonMusicVolume75') as TCastleButton;
  ButtonMusicVolume75.OnClick := @ClickMusicVolume;
  ButtonMusicVolume75.CustomFont := FontSoniano90;
  ButtonMusicVolume100 := DesignedComponent('ButtonMusicVolume100') as TCastleButton;
  ButtonMusicVolume100.OnClick := @ClickMusicVolume;
  ButtonMusicVolume100.CustomFont := FontSoniano90;
  SetMusicVolume(UserConfig.GetFloat('music_volume', 1.0));

  (DesignedComponent('GroupFullScreen') as TCastleUserInterface).Exists := {$IFDEF Mobile}false{$else}true{$endif};
  (DesignedComponent('LabelFullScreen') as TCastleLabel).CustomFont := FontBender90;
  ButtonFullScreen := DesignedComponent('ButtonFullScreen') as TCastleButton;
  ButtonFullScreen.OnClick := @ClickFullScreen;
  ButtonFullScreen.CustomFont := FontSoniano90;

  (DesignedComponent('GroupVibration') as TCastleUserInterface).Exists := {$IFDEF Mobile}true{$else}false{$endif};
  (DesignedComponent('LabelVibration') as TCastleLabel).CustomFont := FontBender90;
  ButtonVibration := DesignedComponent('ButtonVibration') as TCastleButton;
  ButtonVibration.OnClick := @ClickVibration;
  ButtonVibration.CustomFont := FontSoniano90;

  (DesignedComponent('LabelShakeScreen') as TCastleLabel).CustomFont := FontBender90;
  ButtonShakeScreen := DesignedComponent('ButtonShakeScreen') as TCastleButton;
  ButtonShakeScreen.OnClick := @ClickShakeScreen;
  ButtonShakeScreen.CustomFont := FontSoniano90;

  (DesignedComponent('LabelLogLength') as TCastleLabel).CustomFont := FontBender90;
  ButtonLogLength := DesignedComponent('ButtonLogLength') as TCastleButton;
  ButtonLogLength.OnClick := @ClickLogLength;
  ButtonLogLength.CustomFont := FontSoniano90;

  (DesignedComponent('LabelCensored') as TCastleLabel).CustomFont := FontBender90;
  ButtonCensored := DesignedComponent('ButtonCensored') as TCastleButton;
  ButtonCensored.OnClick := @ClickCensored;
  ButtonCensored.CustomFont := FontSoniano90;


  (DesignedComponent('LabelLimitFps') as TCastleLabel).CustomFont := FontBender90;
  ButtonLimitFps := DesignedComponent('ButtonLimitFps') as TCastleButton;
  ButtonLimitFps.OnClick := @ClickLimitFps;
  ButtonLimitFps.CustomFont := FontSoniano90;

  (DesignedComponent('LabelClickMargin') as TCastleLabel).CustomFont := FontBender90;
  (DesignedComponent('LabelClickMarginExplained') as TCastleLabel).CustomFont := FontBender40;
  ButtonClickMargin := DesignedComponent('ButtonClickMargin') as TCastleButton;
  ButtonClickMargin.OnClick := @ClickClickMargin;
  ButtonClickMargin.CustomFont := FontSoniano90;

  (DesignedComponent('LabelSpecialEffects') as TCastleLabel).CustomFont := FontBender90;
  ButtonSpecialEffects := DesignedComponent('ButtonSpecialEffects') as TCastleButton;
  ButtonSpecialEffects.OnClick := @ClickSpecialEffects;
  ButtonSpecialEffects.CustomFont := FontSoniano90;

  ScrollViewOptions := DesignedComponent('ScrollViewOptions') as TCastleUserInterface;

  (DesignedComponent('ButtonResetProgress') as TCastleButton).OnClick := @ClickResetProgress;
  (DesignedComponent('ButtonResetProgress') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonResetProgress') as TCastleButton).Exists := Container.ViewStack[0] = ViewMainMenu;

  (DesignedComponent('ButtonEndCurrentRun') as TCastleButton).OnClick := @ClickEndRun;
  (DesignedComponent('ButtonEndCurrentRun') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonEndCurrentRun') as TCastleButton).Exists := (Container.ViewStack[0] = ViewMainMenu) and SaveGameExists and (UserConfig.PathElement('save_game', true).AttributeQWord('CurrentCharacter') <> 0);

  (DesignedComponent('ButtonDeleteSave') as TCastleButton).OnClick := @ClickDeleteSave;
  (DesignedComponent('ButtonDeleteSave') as TCastleButton).CustomFont := FontSoniano90;
  (DesignedComponent('ButtonDeleteSave') as TCastleButton).Exists := (Container.ViewStack[0] = ViewMainMenu) and SaveGameExists;

  UpdateButtonsCaptions;
end;

procedure TViewOptions.Stop;
begin
  UserConfig.Save;
  inherited Stop;
end;

function TViewOptions.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;

  if Event.IsKey(keyEscape) then
    ClickReturn(Self);

  if Event.IsKey(keyF5) or Event.IsKey(keyF11) then
    Result := false; // let screenshot handle
end;

procedure TViewOptions.Resize;
begin
  inherited Resize;
  ScrollViewOptions.Height := Container.UnscaledHeight + ScrollViewOptions.Translation.Y - 35; // VerticalAnchorDelta is negative
end;

procedure TViewOptions.SetMasterVolume(const Volume: Single);
var
  SetVolume: Single;
  Font0, Font7, Font15, Font25, Font50, Font75, Font100: Single;
begin
  SetVolume := Volume;
  Font0 := 0.5;
  Font7 := 0.5;
  Font15 := 0.5;
  Font25 := 0.5;
  Font50 := 0.5;
  Font75 := 0.5;
  Font100 := 0.5;
  case Round(Volume * 100) of
    0: Font0 := 1.0;
    7: Font7 := 1.0;
    15: Font15 := 1.0;
    25: Font25 := 1.0;
    50: Font50 := 1.0;
    75: Font75 := 1.0;
    100: Font100 := 1.0;
    else
    begin
      DebugWarning('Unexpected volume: %.4n', [Volume]);
      SetVolume := 1.00;
      Font100 := 1.0;
    end;
  end;

  UserConfig.SetFloat('master_volume', SetVolume);
  ApplySettings;

  ButtonMasterVolume0.FontScale := Font0;
  ButtonMasterVolume0.Width := 160 + 180 * (Font0 - 0.5);
  ButtonMasterVolume7.FontScale := Font7;
  ButtonMasterVolume7.Width := 160 + 180 * (Font7 - 0.5);
  ButtonMasterVolume15.FontScale := Font15;
  ButtonMasterVolume15.Width := 160 + 180 * (Font15 - 0.5);
  ButtonMasterVolume25.FontScale := Font25;
  ButtonMasterVolume25.Width := 160 + 180 * (Font25 - 0.5);
  ButtonMasterVolume50.FontScale := Font50;
  ButtonMasterVolume50.Width := 160 + 180 * (Font50 - 0.5);
  ButtonMasterVolume75.FontScale := Font75;
  ButtonMasterVolume75.Width := 160 + 180 * (Font75 - 0.5);
  ButtonMasterVolume100.FontScale := Font100;
  ButtonMasterVolume100.Width := 160 + 180 * (Font100 - 0.5);
end;

procedure TViewOptions.ClickMasterVolume(Sender: TObject);
begin
  case (Sender as TCastleButton).Name of
    'ButtonMasterVolume0': SetMasterVolume(0.00);
    'ButtonMasterVolume7': SetMasterVolume(0.07);
    'ButtonMasterVolume15': SetMasterVolume(0.15);
    'ButtonMasterVolume25': SetMasterVolume(0.25);
    'ButtonMasterVolume50': SetMasterVolume(0.50);
    'ButtonMasterVolume75': SetMasterVolume(0.75);
    'ButtonMasterVolume100': SetMasterVolume(1.00);
    else
      raise EUnexpectedButton.Create('Unexpected volume button: ' + (Sender as TCastleButton).Name);
  end;
  Sound('menu_button');
end;

procedure TViewOptions.SetMusicVolume(const Volume: Single);
var
  SetVolume: Single;
  Font0, Font7, Font15, Font25, Font50, Font75, Font100: Single;
begin
  SetVolume := Volume;
  Font0 := 0.5;
  Font7 := 0.5;
  Font15 := 0.5;
  Font25 := 0.5;
  Font50 := 0.5;
  Font75 := 0.5;
  Font100 := 0.5;
  case Round(Volume * 100) of
    0: Font0 := 1.0;
    7: Font7 := 1.0;
    15: Font15 := 1.0;
    25: Font25 := 1.0;
    50: Font50 := 1.0;
    75: Font75 := 1.0;
    100: Font100 := 1.0;
    else
    begin
      DebugWarning('Unexpected volume: %.4n', [Volume]);
      SetVolume := 1.00;
      Font100 := 1.0;
    end;
  end;

  UserConfig.SetFloat('music_volume', SetVolume);
  ApplySettings;

  ButtonMusicVolume0.FontScale := Font0;
  ButtonMusicVolume0.Width := 160 + 180 * (Font0 - 0.5);
  ButtonMusicVolume7.FontScale := Font7;
  ButtonMusicVolume7.Width := 160 + 180 * (Font7 - 0.5);
  ButtonMusicVolume15.FontScale := Font15;
  ButtonMusicVolume15.Width := 160 + 180 * (Font15 - 0.5);
  ButtonMusicVolume25.FontScale := Font25;
  ButtonMusicVolume25.Width := 160 + 180 * (Font25 - 0.5);
  ButtonMusicVolume50.FontScale := Font50;
  ButtonMusicVolume50.Width := 160 + 180 * (Font50 - 0.5);
  ButtonMusicVolume75.FontScale := Font75;
  ButtonMusicVolume75.Width := 160 + 180 * (Font75 - 0.5);
  ButtonMusicVolume100.FontScale := Font100;
  ButtonMusicVolume100.Width := 160 + 180 * (Font100 - 0.5);
end;

procedure TViewOptions.UpdateButtonsCaptions;

  function OnOff(const Value: Boolean): String;
  begin
    if Value then
      Result := 'ON'
    else
      Result := 'OFF';
  end;

begin
  ButtonFullScreen.Caption := OnOff(UserConfig.GetValue('fullscreen', true));
  ButtonVibration.Caption := OnOff(UserConfig.GetValue('vibration', true));
  ButtonShakeScreen.Caption := OnOff(UserConfig.GetValue('shake_screen', true));
  ButtonCensored.Caption := OnOff(UserConfig.GetValue('censored', false));
  ButtonLogLength.Caption := UserConfig.GetInteger('log_length', DefaultLogLength).ToString;
  ButtonSpecialEffects.Caption := OnOff(UserConfig.GetValue('screen_effects', true));
  ButtonClickMargin.Caption := Round(100 * UserConfig.GetFloat('click_margin', DefaultClickMargin)).ToString + '%';
  if ApplicationProperties.LimitFPS = 0 then
    ButtonLimitFps.Caption := 'OFF'
  else
    ButtonLimitFps.Caption := Round(ApplicationProperties.LimitFPS).ToString;
  ButtonLimitFps.CustomTextColorUse := true;
  if (ApplicationProperties.LimitFPS > 0) and (ApplicationProperties.LimitFPS < 24) then
    ButtonLimitFps.CustomTextColor := ColorRed
  else
    ButtonLimitFps.CustomTextColor := ColorDefault;
end;

procedure TViewOptions.ClickMusicVolume(Sender: TObject);
begin
  case (Sender as TCastleButton).Name of
    'ButtonMusicVolume0': SetMusicVolume(0.00);
    'ButtonMusicVolume7': SetMusicVolume(0.07);
    'ButtonMusicVolume15': SetMusicVolume(0.15);
    'ButtonMusicVolume25': SetMusicVolume(0.25);
    'ButtonMusicVolume50': SetMusicVolume(0.50);
    'ButtonMusicVolume75': SetMusicVolume(0.75);
    'ButtonMusicVolume100': SetMusicVolume(1.00);
    else
      raise EUnexpectedButton.Create('Unexpected volume button: ' + (Sender as TCastleButton).Name);
  end;
  Sound('menu_button');
end;

procedure TViewOptions.ClickShakeScreen(Sender: TObject);
begin
  Sound('menu_button');
  UserConfig.SetValue('shake_screen', not UserConfig.GetValue('shake_screen', true));
  UpdateButtonsCaptions;
end;

procedure TViewOptions.ClickSpecialEffects(Sender: TObject);
begin
  UserConfig.SetValue('screen_effects', not UserConfig.GetValue('screen_effects', true));
  ScreenEffect.Enable;
  ViewGame.UpdateScreenEffect;
  ViewMainMenu.UpdateScreenEffect;
  UpdateButtonsCaptions;
  Sound('menu_button');
end;

procedure TViewOptions.ClickVibration(Sender: TObject);
begin
  UserConfig.SetValue('vibration', not UserConfig.GetValue('vibration', true));
  UpdateButtonsCaptions;
  Sound('menu_button');
  Vibrate(500);
end;

procedure TViewOptions.ClickResetProgress(Sender: TObject);
begin
  Sound('menu_quit');
  Container.View := ViewConfirmResetProgress;
end;

procedure TViewOptions.ClickFullScreen(Sender: TObject);
begin
  Sound('menu_button');
  Application.MainWindow.FullScreen := not Application.MainWindow.FullScreen;
  UserConfig.SetValue('fullscreen', Application.MainWindow.FullScreen);
  UpdateButtonsCaptions;
end;

procedure TViewOptions.ClickLimitFps(Sender: TObject);
begin
  Sound('menu_button');
  case Round(ApplicationProperties.LimitFPS) of
    15: UserConfig.SetValue('limit_fps', 20);
    20: UserConfig.SetValue('limit_fps', 25);
    25: UserConfig.SetValue('limit_fps', 30);
    30: UserConfig.SetValue('limit_fps', 60);
    60: UserConfig.SetValue('limit_fps', 75);
    75: UserConfig.SetValue('limit_fps', 90);
    90: UserConfig.SetValue('limit_fps', 100);
    100: UserConfig.SetValue('limit_fps', 120);
    120: UserConfig.SetValue('limit_fps', 0);
    0: UserConfig.SetValue('limit_fps', 15);
  end;
  ApplySettings;
  UpdateButtonsCaptions;
end;

procedure TViewOptions.ClickClickMargin(Sender: TObject);
begin
  Sound('menu_button');
  case Round(100 * UserConfig.GetFloat('click_margin', DefaultClickMargin)) of
    0: UserConfig.SetFloat('click_margin', 0.01);
    1: UserConfig.SetFloat('click_margin', 0.02);
    2: UserConfig.SetFloat('click_margin', 0.03);
    3: UserConfig.SetFloat('click_margin', 0.05);
    5: UserConfig.SetFloat('click_margin', 0.07);
    7: UserConfig.SetFloat('click_margin', 0.09);
    9: UserConfig.SetFloat('click_margin', 0.11);
    11: UserConfig.SetFloat('click_margin', 0.13);
    13: UserConfig.SetFloat('click_margin', 0.15);
    15: UserConfig.SetFloat('click_margin', 0);
    else
      UserConfig.SetFloat('click_margin', DefaultClickMargin);
  end;
  UpdateButtonsCaptions;
end;

procedure TViewOptions.ClickLogLength(Sender: TObject);
begin
  Sound('menu_button');
  case UserConfig.GetInteger('log_length', DefaultLogLength) of
    10: UserConfig.SetValue('log_length', 12);
    12: UserConfig.SetValue('log_length', 15);
    15: UserConfig.SetValue('log_length', 20);
    20: UserConfig.SetValue('log_length', 25);
    25: UserConfig.SetValue('log_length', 32);
    32: UserConfig.SetValue('log_length', 10);
    else UserConfig.SetValue('log_length', DefaultLogLength);
  end;
  UpdateButtonsCaptions;
end;

procedure TViewOptions.ClickCensored(Sender: TObject);
begin
  Sound('menu_button');
  UserConfig.SetValue('censored', not UserConfig.GetValue('censored', false));
  ViewGame.InvalidateInventory(nil);
  UpdateButtonsCaptions;
end;

procedure TViewOptions.ClickDeleteSave(Sender: TObject);
begin
  Sound('menu_quit');
  Container.View := ViewConfirmDeleteSave;
end;

procedure TViewOptions.ClickEndRun(Sender: TObject);
begin
  Sound('menu_quit');
  Container.View := ViewConfirmNewRun;
end;

procedure TViewOptions.ClickReturn(Sender: TObject);
begin
  Sound('menu_back');
  Container.PopView(Self);
end;

end.

