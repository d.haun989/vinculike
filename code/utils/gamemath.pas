{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMath;

{$INCLUDE compilerconfig.inc}

interface

{uses
  Classes, SysUtils;}

function Sign(const A: Integer): Integer; inline;
implementation

function Sign(const A: Integer): Integer; inline;
begin
  if A > 0 then
    Result := 1
  else
  if A < 0 then
    Result := -1
  else
    Result := 0;
end;

end.

