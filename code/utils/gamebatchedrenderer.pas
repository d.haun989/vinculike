{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameBatchedRenderer; // TODO: Temporary?

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleGlImages, CastleRectangles;

//{$define GLIndexesShort}

type
  TBatchedRenderer = class helper for TDrawableImage
  public
    procedure DrawBatched(const ScreenRects, ImageRects: array of TFloatRectangle; const Count: SizeInt);
  end;

implementation
uses
  CastleVectors,
  GameLog;

procedure TBatchedRenderer.DrawBatched(const ScreenRects,
  ImageRects: array of TFloatRectangle; const Count: SizeInt);
{$IFDEF GLIndexesShort}
const
  IndexesPerQuad = 6;
  MaxRenderQuads = 65535 div IndexesPerQuad - 2;
var
  I: SizeInt;
{$ENDIF}
begin
  {$IFDEF GLIndexesShort}
  if Count >= MaxRenderQuads then
  begin
    I := 0;
    while I <= Count - MaxRenderQuads do
    begin
      Draw(@ScreenRects[I], @ImageRects[I], MaxRenderQuads);
      I += MaxRenderQuads;
    end;
    Draw(@ScreenRects[I], @ImageRects[I], Count - I);
  end else
  {$ENDIF}
  Draw(@ScreenRects[0], @ImageRects[0], Count);
end;

end.

