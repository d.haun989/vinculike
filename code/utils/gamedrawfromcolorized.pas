{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameDrawFromColorized;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleImages, CastleColors;

type
  TDrawFromColorized = class helper for TRgbAlphaImage
  public
    { Does the same as DrawFrom with dmBlendSmart
      But also multiplies the source image by a specific color
      Note: for now it demands images of identical size
      It will not work with TShiftedImage (maybe TODO) }
    procedure DrawFromColorized(const Source: TRgbAlphaImage; const MultiplyColor: TCastleColor);
  end;

implementation
uses
  CastleVectors,
  GameLog;

{ TDrawFromColorized }

procedure TDrawFromColorized.DrawFromColorized(
  const Source: TRgbAlphaImage; const MultiplyColor: TCastleColor);
var
  Src, Dst: PVector4Byte;
  I: SizeInt;
  Alpha1, Alpha1d, Alpha2, AlphaSum: Single;
begin
  if (Width <> Source.Width) or (Height <> Source.Height) then
    raise EImageDrawError.CreateFmt('DrawFromColorized needs images with identical size, but got %dx%d vs %dx%d', [Width, Height, Source.Width, Source.Height]);
  Dst := Pixels;
  Src := Source.Pixels;
  for I := 0 to Pred(Width * Height) do
  begin
    // get alpha in 0..1 range
    Alpha1 := Dst^.W / 255.0;
    Alpha2 := Src^.W / 255.0;
    // calculate alpha-sums according to https://en.wikipedia.org/wiki/Alpha_compositing
    Alpha1d := Alpha1 * (1 - Alpha2);
    AlphaSum := Alpha1 + (1 - Alpha1) * Alpha2;
    if AlphaSum > 0 then
    begin
      Dst^.X := Round((Dst^.X * Alpha1d + MultiplyColor.X * Src^.X * Alpha2) / AlphaSum);
      Dst^.Y := Round((Dst^.Y * Alpha1d + MultiplyColor.Y * Src^.Y * Alpha2) / AlphaSum);
      Dst^.Z := Round((Dst^.Z * Alpha1d + MultiplyColor.Z * Src^.Z * Alpha2) / AlphaSum);
      Dst^.W := Round(255 * AlphaSum);
    end;
    Inc(Dst);
    Inc(Src);
  end;
end;

end.

