{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAverageColorCalculator;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleImages, CastleColors;

type
  TAverageColorCalculator = class helper for TRgbAlphaImage
  public
    { Average of all at least partially opaque pixels of the image
      Note: Pure black color is excluded (borders) }
    function AverageColor: TCastleColor;
  end;

implementation
uses
  CastleVectors,
  GameLog;

function TAverageColorCalculator.AverageColor: TCastleColor;
var
  P: PVector4Byte;
  Weight: Single;
  I: SizeInt;
begin
  Result := TCastleColor.Zero;
  Weight := 0;
  P := Pixels;
  for I := 0 to Pred(Width * Height) do
  begin
    if (P^.W > 0) and ((P^.X > 0) or (P^.Y > 0) or (P^.Z > 0)) then
    begin
      Weight += P^.W / 255.0;
      Result += P^.W / 255.0 * Vector4(P^.X / 255.0, P^.Y / 255.0, P^.Z / 255.0, P^.W / 255.0);
    end;
    Inc(P);
  end;
  if Weight > 0 then
    Result := Result / Weight
  else
    DebugWarning('Image %s did not contain opaque pixels, average color calculation failed', [URL]);
    // Reulst will just stay black
end;

end.

