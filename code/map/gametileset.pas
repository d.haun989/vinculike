{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameTileset;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  Generics.Collections,
  CastleImages;

type
  TCastleImageList = specialize TObjectList<TCastleImage>;

type
  TTileset = class(TObject)
  public
    WallLightness: Single;
    FloorLightness: Single;
    WallBlur: Single;
    FloorBlur: Single;
    Wall: TCastleImageList;
    Floor: TCastleImageList;
    constructor Create(const Element: TDOMElement);
    destructor Destroy; override;
  end;
  TTilesetsList = specialize TObjectList<TTileset>;

var
  AllTilesets: TTilesetsList;

procedure LoadTilesets;

implementation
uses
  SysUtils,
  CastleXMLUtils,
  GameCachedImages;

procedure LoadTilesets;
var
  Tileset: TTileset;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  AllTilesets := TTilesetsList.Create(true);
  Doc := URLReadXML('castle-data:/map/tileset/tilesets.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('Tileset');
    try
      while Iterator.GetNext do
      begin
        Tileset := TTileset.Create(Iterator.Current);
        AllTilesets.Add(Tileset);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

constructor TTileset.Create(const Element: TDOMElement);

  function ReadSet(const Signature: String): TCastleImageList;
  var
    Img: TCastleImage;
    Iterator: TXMLElementIterator;
  begin
    Result := TCastleImageList.Create(false);
    Iterator := Element.ChildrenIterator(Signature);
    try
      while Iterator.GetNext do
      begin
        // TODO: RGBA? not RGB?
        Img := LoadRgba('castle-data:/map/tileset/' + Iterator.Current.AttributeString('Url'));
        Result.Add(Img);
      end;
    finally FreeAndNil(Iterator) end;
  end;

begin
  WallLightness := Element.AttributeSingleDef('WallLightness', 1.0);
  FloorLightness := Element.AttributeSingleDef('FloorLightness', 1.0);
  WallBlur := Element.AttributeSingleDef('WallBlur', -1.0);
  FloorBlur := Element.AttributeSingleDef('FloorBlur', -1.0);
  Floor := ReadSet('Floor');
  Wall := ReadSet('Wall');
end;

destructor TTileset.Destroy;
begin
  Wall.Free;
  Floor.Free;
  inherited Destroy;
end;

finalization
  AllTilesets.Free;
end.

