{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMap;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Generics.Collections, DOM,
  CastleVectors,
  TempStamps,
  GameMapTypes, GameMarkAbstract, GameMonster, GamePlayerCharacter, GameMapItem,
  GameParticle, GameActorData;

type
  ENavGridError = class(Exception);

type
  TMap = class(TObject)
  strict private
    FSizeX: UInt16;
    FSizeY: UInt16;
    MaxDistanceFromEntrance: TDistanceQuant;
    DistanceFromEntranceCache: TDistanceMapArray;
    procedure CropMap;
    procedure GeneratePassableTilesForColliders;
    procedure CalculateRenderRects;
    procedure FindDistanceMapExtrema;
    function FloodFill(const SrcArray: TMoveArray; const StartX, StartY: UInt32): TMoveArray;
    procedure LookAround(const AX, AY: Int16);
    procedure ClearVisible;
    procedure ClearActiveVisible;
    procedure RememberVisible;
  strict private
    FScheduleUpdateVisible: Boolean;
  public
    procedure GenerateRandom;
    procedure GenerateStamp(const Stamp: TStamp);
  public
    CurrentDepth: Int16;
    Tileset: Int16;
    PredSizeX: UInt16;
    PredSizeY: UInt16;
    EntranceX: Int16;
    EntranceY: Int16;
    ExitX: Int16;
    ExitY: Int16;
    FMap: TMapArray;
    PassableTiles: array [0..PredMaxColliderSize] of TMoveArray;
    PassablePlayerTiles: TMoveArray;
    Visible: TVisibleArray;
    Extrema: TExtremaList;
    ExtremaGeneration: TExtremaList;
    RenderRects: Integer;
    // Flood Fill is faster than Dijkstra if we don't need pathfinding
    procedure UpdatePassableVisibleTiles(const OriginX, OriginY: Int16);
    procedure MakeGenerationCache;
    function MaxDistance(const MapArray: TDistanceMapArray): TDistanceQuant;
    procedure GenerateEntryAndExit;
    procedure ClearGenerationCache;
    function DistanceMap(const PredSize: Byte; const Seed: TCoordList): TDistanceMapArray;
    function DistanceMap(const Actor: TObject): TDistanceMapArray;
    procedure FloodFillPassablePlayerTilesWithVisible;
    function GenerateMoveMap(const PredColliderSize: Integer; const ConsiderVisible: Boolean): TMoveArray;
    function GenerateDijkstra(const CurrentPassableTiles: TMoveArray; const TargetX, TargetY: Int16; const MaxMovementRange: UInt32; const IgnoreVisible: Boolean; const FromX, FromY: Int16; const ConsiderFromPosition: Boolean): TDirectionArray;
    { Generate list of waypoints based on a TDirectionArray
      Note we don't need ToX, ToY to generate the path because TDirectionArray is build
      for some target destination(s)
      However, we still need them for fraction part of coordinates }
    function GetWaypoints(const Directions: TDirectionArray; const IncludeNextTile: Boolean; const FromXTile, FromYTile: Int16; const FromX, FromY, ToX, ToY: Single): TWaypoints;
    function GetPathSeed(const Directions: TDirectionArray; const FromX, FromY: Int16): TCoordList;
    procedure ScheduleUpdateVisible;
    procedure UpdateVisible;
    procedure ForgetVisible(const DisorientationAmount: SizeInt);
    function Ray(const PredSize: Byte; const FromX, FromY: Int16; var ToX, ToY: Int16): Boolean;
    function Ray(const PredSize: Byte; const FromX, FromY, ToX, ToY: Single): Boolean;
    procedure ShowAllMap;
    class function DirectionToVector(const Dir: TMovementDirection): TVector2;
    function DirectionToIncrement(const Dir: TMovementDirection): SizeInt;
  public
    function CanMove(const Tile: SizeInt): Boolean; inline;
    function CanSee(const Tile: SizeInt): Boolean; inline;
  public
    Disorientation: SizeInt;
    MarksList: TMarksList;
    MonstersList: TMonstersList;
    PlayerCharactersList: TPlayerCharactersList;
    { All characters on this dungeon level
      both captured and free }
    CharactersOnThisLevel: TPlayerCharactersList;
    PatrolList: TActorDataList;
    MapItemsList: TMapItemsList;
    ParticlesList: TParticlesList;
    EnemySpawnTime: Single; // Call Map.Update(secondsPassed) and hide this one
    { Time the character has spent on this map }
    TimeSpentOnTheMap: Single;
    procedure MonstersToIdle;
    function MonsterSeen(const AX, AY: Int16; const Range: Single): Boolean;
    function NoMonstersNearby(const AX, AY: Int16; const CollisionMargin: Int16): Boolean;
    function MonstersAttacking: Boolean;
  public const Signature = 'map';
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
  public
    FreeArea: UInt32;
    property SizeX: UInt16 read FSizeX;
    property SizeY: UInt16 read FSizeY;
    procedure GenerateMonsters(const TotalDanger: Single);
    procedure GenerateTraps(const NumberOfTraps: Integer);
    procedure GenerateItems(const GenerateItems: Integer);
    procedure CacheCharactersOnThisLevel;
    procedure PositionCapturedCharacters;
    function CapturedCharactersAtLevel: Integer;
    procedure Init(const ASizeX, ASizeY: UInt16);
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  Map: TMap;

implementation
uses
  Base64, Math,
  CastleXmlUtils,
  GameRandom, GameMath,
  GameInventoryItem, GameItemData, GameItemDatabase, GameMonsterData, GamePositionedObject,
  GameActionIdle, GameLog, GameColors, GameTileset, GameTilesetMap,
  GameViewGame;

procedure TMap.Init(const ASizeX, ASizeY: UInt16);
var
  I: Integer;
begin
  FSizeX := ASizeX;
  FSizeY := ASizeY;
  PredSizeX := Pred(FSizeX);
  PredSizeY := Pred(FSizeY);
  FMap := nil;
  SetLength(FMap, FSizeX * FSizeY);
  Visible := nil;
  SetLength(Visible, FSizeX * FSizeY);
  for I := 0 to PredMaxColliderSize do
    PassableTiles[I] := nil;
  ClearVisible;
  PassablePlayerTiles := nil;
  MonstersList.Clear;
  PatrolList.Clear;
  MapItemsList.Clear;
  MarksList.Clear;
  ParticlesList.Clear;
  TimeSpentOnTheMap := 0;
  FScheduleUpdateVisible := true;
  Disorientation := 0;
end;

constructor TMap.Create;
begin
  inherited; //parent is empty
  PatrolList := TActorDataList.Create(false);
  MarksList := TMarksList.Create(true);
  MonstersList := TMonstersList.Create(true);
  PlayerCharactersList := TPlayerCharactersList.Create(true);
  MapItemsList := TMapItemsList.Create(true);
  ParticlesList := TParticlesList.Create(true);
  CharactersOnThisLevel := TPlayerCharactersList.Create(false); // it's a cache, doesn't own children
  Extrema := TExtremaList.Create(true);
end;

destructor TMap.Destroy;
begin
  PatrolList.Free;
  MonstersList.Free; // Monsters must be freed before Marks because their actions still reference marks
  PlayerCharactersList.Free;
  MapItemsList.Free;
  MarksList.Free;
  ParticlesList.Free;
  CharactersOnThisLevel.Free;
  Extrema.Free;
  inherited Destroy;
end;

procedure TMap.ClearVisible;
var
  I: SizeInt;
begin
  for I := 0 to Pred(SizeX * SizeY) do
    Visible[I] := 0;
  // TODO: FillByte
end;

procedure TMap.ClearActiveVisible;
var
  I: SizeInt;
begin
  for I := 0 to Pred(SizeX * SizeY) do
    if Visible[I] = DirectlyVisible then
      Visible[I] := RememberedVisible;
end;

procedure TMap.RememberVisible;
var
  I: SizeInt;
begin
  ShowLog('%s recalls this place', [ViewGame.CurrentCharacter.Data.DisplayName], ColorDefault);
  for I := 0 to Pred(SizeX * SizeY) do
    if Visible[I] = ForgottenVisible then
      Visible[I] := RememberedVisible;
end;

procedure TMap.ForgetVisible(const DisorientationAmount: SizeInt);
var
  I: SizeInt;
begin
  if DisorientationAmount <= 0 then
    raise Exception.CreateFmt('Unexpected DisorientationAmount value: %d', [DisorientationAmount]);
  Disorientation := DisorientationAmount;
  for I := 0 to Pred(SizeX * SizeY) do
    if Visible[I] > ForgottenVisible then
      Visible[I] := ForgottenVisible;
end;

procedure TMap.ShowAllMap;
var
  I: SizeInt;
begin
  for I := 0 to Pred(SizeX * SizeY) do
    Visible[I] := DirectlyVisible;
end;

procedure TMap.CropMap;
var
  StartX, StartY, EndX, EndY: UInt16;
  OldSizeX{, OldSizeY}: UInt16;
  X, Y: Int16;
  PassageFound: Boolean;
  OldMap: TMapArray;
  I: SizeInt;
begin
  X := -1;
  PassageFound := false;
  repeat
    Inc(X);
    for Y := 0 to PredSizeY do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  StartX := X;

  X := SizeX;
  PassageFound := false;
  repeat
    Dec(X);
    for Y := 0 to PredSizeY do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  EndX := X;

  Y := -1;
  PassageFound := false;
  repeat
    Inc(Y);
    for X := 0 to PredSizeX do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  StartY := Y;

  Y := SizeY;
  PassageFound := false;
  repeat
    Dec(Y);
    for X := 0 to PredSizeX do
      if CanMove(X + SizeX * Y) then
      begin
        PassageFound := true;
        break;
      end;
  until PassageFound;
  EndY := Y;

  if (StartX > 1) or (EndX < PredSizeX - 1) or
     (StartY > 1) or (EndY < PredSizeY - 1) then
  begin
    OldMap := FMap;
    OldSizeX := FSizeX;
    //OldSizeY := FSizeY;
    EntranceX := EntranceX - StartX + 1;
    EntranceY := EntranceY - StartY + 1;
    DebugLog('Cropping map from %dx%d to %dx%d', [FSizeX, FSizeY, EndX - StartX + 3, EndY - StartY + 3]);
    Init(EndX - StartX + 3, EndY - StartY + 3);
    I := 0;
    for Y := StartY - 1 to EndY + 1 do
      for X := StartX - 1 to EndX + 1 do
      begin
        FMap[I] := OldMap[X + OldSizeX * Y];
        Inc(I);
      end;
    // We call Cropmap from generation algorithm right before executing those:
    //GeneratePassableTilesForColliders;
    //CalculateRenderRects;
  end;

end;

procedure TMap.GeneratePassableTilesForColliders;
var
  K: Integer;
begin
  // TODO : cache move maps for different possible sizes + faster generation of "next steps" as if the tile is not passable by 2-sized chreature, it's most certainly not passable by 3-sized one
  for K := 0 to PredMaxColliderSize do
    PassableTiles[K] := GenerateMoveMap(K, false);
end;

procedure TMap.GenerateRandom;
var
  X, Y: Integer;
  I: SizeInt;
  PassableTilesPlayerTemp: TMoveArray;

  function CannotStepHere: Boolean;
  var
    X1, Y1: Integer;
    J: SizeInt;
  begin
    for Y1 := 0 to 2 do
      begin
        J := (Y - Y1) * SizeX + X;
        for X1 := 0 to 2 do
        begin
          if (Y - Y1 >= 0) and (X - X1 >= 0) and PassableTilesPlayerTemp[J] then
            Exit(false);
          Dec(J);
        end;
      end;
    Exit(true);
  end;

  function Wall: TMapTile; inline;
  var
    K: Integer;
  begin
    Result := nil;
    SetLength(Result, MaxWallHeight);
    for K := 0 to PredMaxWallHeight do
      Result[K] := HalfTilesPerTexture + K * WallHeightTiles + Rnd.Random(WallHeightTiles);
  end;

  function Floor: TMapTile; inline;
  begin
    Result := nil;
    SetLength(Result, 1);
    Result[0] := Rnd.Random(HalfTilesPerTexture);
  end;

var
  Count: SizeInt;
  LastTileset: Int16;
begin
  LastTileset := Tileset;
  repeat
    Tileset := Rnd.Random(AllTilesets.Count);
  until Tileset <> LastTileset;
  PrepareTilesetMap;

  repeat
    // Generate random map
    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if (X = 0) or (Y = 0) or (X = PredSizeX) or (Y = PredSizeY) then
          FMap[I] := Wall
        else
          if Rnd.Random < 0.50 then
            FMap[I] := Wall
          else
            FMap[I] := Floor;
        Inc(I);
      end;
    // Make entry
    EntranceX := 10;
    EntranceY := SizeY - 10;
    for X := -3 to 3 do
      for Y := -3 to 3 do
        FMap[(Y + EntranceY) * SizeX + X + EntranceX] := Floor;
    // Clean columns
    for Count := 0 to 10 do
    begin
      I := 0;
      for Y := 0 to PredSizeY do
        for X := 0 to PredSizeX do
        begin
          if not CanMove(I) then
            if ((X > 0) and CanMove(I - 1) and
                (X < PredSizeX) and CanMove(I + 1)) or
               ((Y > 0) and CanMove(I - SizeX) and
                (Y < PredSizeY) and CanMove(I + SizeX)) then
                  FMap[I] := Floor;
          Inc(I);
        end;
    end;

    PassableTilesPlayerTemp := FloodFill(GenerateMoveMap(PredPlayerColliderSize, false), EntranceX, EntranceY);

    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if CanMove(I) and CannotStepHere then
          FMap[I] := Wall;
        Inc(I);
      end;

    Count := 0;
    for I := 0 to Pred(SizeX * SizeY) do
      if CanMove(I) then
        Inc(Count);
    DebugLog('Map area: %.4n', [Single(Count) / Single(SizeX * SizeY)]);
  until (Count > 0.5 * SizeX * SizeY) and (Count < 0.7 * SizeX * SizeY);
  FreeArea := Count;

  CropMap;

  GeneratePassableTilesForColliders;

  CalculateRenderRects;
end;

procedure TMap.GenerateStamp(const Stamp: TStamp);

  function Wall: TMapTile; inline;
  var
    K: Integer;
  begin
    Result := nil;
    SetLength(Result, MaxWallHeight);
    for K := 0 to PredMaxWallHeight do
      Result[K] := HalfTilesPerTexture + K * WallHeightTiles + Rnd.Random(WallHeightTiles);
  end;

  function Floor: TMapTile; inline;
  begin
    Result := nil;
    SetLength(Result, 1);
    Result[0] := Rnd.Random(HalfTilesPerTexture);
  end;

var
  PassableTilesPlayerTemp: TMoveArray;

  function CannotStepHere(const AX, AY: Int16): Boolean;
  var
    X1, Y1: Integer;
    J: SizeInt;
  begin
    if (AX <= 0) or (AY <= 0) or (AX >= PredSizeX) or (AY >= PredSizeY) then
      Exit(true);
    for Y1 := 0 to PredPlayerColliderSize do
      begin
        J := (AY - Y1) * SizeX + AX;
        for X1 := 0 to 2 do
        begin
          if (AY - Y1 >= 0) and (AX - X1 >= 0) and PassableTilesPlayerTemp[J] then
            Exit(false);
          Dec(J);
        end;
      end;
    Exit(true);
  end;

var
  StampX, StampY, DX, DY, X, Y: Integer;
  SeedX, SeedY: Int16;
  I: SizeInt;
  Count: SizeInt;
  ThisStamp: Integer;
  LastTileset: Int16;
begin
  LastTileset := Tileset;
  repeat
    Tileset := Rnd.Random(AllTilesets.Count);
  until Tileset <> LastTileset;
  PrepareTilesetMap;

  repeat
    // Apply stamps
    for StampX := 0 to PredSizeX div Stamp.SizeY do
      for StampY := 0 to PredSizeY div Stamp.SizeX do
      begin
        ThisStamp := Rnd.Random(Length(Stamp.Stamps));
        for DX := 0 to Pred(Stamp.SizeX) do
          for DY := 0 to Pred(Stamp.SizeY) do
          begin
            X := StampX * Stamp.SizeX + DX;
            Y := StampY * Stamp.SizeY + DY;
            if (X < 0) or (Y < 0) or (X > PredSizeX) or (Y > PredSizeY) then
              Continue;
            if (X = 0) or (Y = 0) or (X = PredSizeX) or (Y = PredSizeY) then
              FMap[X + SizeX * Y] := Wall
            else
              if Stamp.Stamps[ThisStamp][DY][DX] = 0 then
                FMap[X + SizeX * Y] := Floor
              else
                FMap[X + SizeX * Y] := Wall;
          end;
      end;

    PassableTilesPlayerTemp := GenerateMoveMap(PredPlayerColliderSize, false);

    // Make entry
    repeat
      SeedX := Rnd.Random(SizeX - 10) + 5;
      SeedY := Rnd.Random(SizeY - 10) + 5;
    until PassableTilesPlayerTemp[SeedX + SizeX * SeedY];

    PassableTilesPlayerTemp := FloodFill(GenerateMoveMap(PredPlayerColliderSize, false), SeedX, SeedY);

    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if CanMove(I) and CannotStepHere(X, Y) and
          CannotStepHere(X + 1, Y) and CannotStepHere(X - 1, Y) and
          CannotStepHere(X, Y + 1) and CannotStepHere(X, Y - 1) then
            FMap[I] := Wall;
        Inc(I);
      end;

    Count := 0;
    for I := 0 to Pred(SizeX * SizeY) do
      if CanMove(I) then
        Inc(Count);
    DebugLog('Map area: %.4n', [Single(Count) / Single(SizeX * SizeY)]);
  until (Count > 0.3 * SizeX * SizeY) and (Count < 0.6 * SizeX * SizeY);
  FreeArea := Count;

  EntranceX := SeedX; // TODO: Temporary, to pass those to MakeGenerationCache
  EntranceY := SeedY;

  if not PassableTilesPlayerTemp[EntranceX + SizeX * EntranceY] then // it may so happen that during the clean-ups of the map the original EntranceXY is no longer walkable (or could it so happen that it never was? shouldn't be possible)
    repeat
      EntranceX := Rnd.Random(SizeX - 10) + 5;
      EntranceY := Rnd.Random(SizeY - 10) + 5;
    until PassableTilesPlayerTemp[EntranceX + SizeX * EntranceY];

  CropMap; // note, updates EntranceX, EntranceY

  // note, PassableTilesPlayerTemp from previous steps no longer makes sense after cropping, we need to recalculate it again
  GeneratePassableTilesForColliders;
  PassableTilesPlayerTemp := FloodFill(PassableTiles[PredPlayerColliderSize], EntranceX, EntranceY); // we don't need this for anything but the next check which shouldn't ever matter

  if not PassableTilesPlayerTemp[EntranceX + SizeX * EntranceY] then // it may so happen that during the clean-ups of the map the original EntranceXY is no longer walkable (or could it so happen that it never was? shouldn't be possible)
    raise ENavGridError.CreateFmt('Entry point (%d,%d) is not walkable after cropping the map to %dx%d!', [EntranceX, EntranceY, SizeX, SizeY]);

  CalculateRenderRects;

  FindDistanceMapExtrema;
  GenerateEntryAndExit;
  MakeGenerationCache;
end;

procedure TMap.GenerateMonsters(const TotalDanger: Single);
const
  MaxTotalSize = Integer(Trunc(Single(TextureSize)/Single(TileSizeWithPadding))) - 3 - 2 * 4; //TODO: We consider Vacuum Cleaner here or chests (mimic) manually
var
  I, J: Integer;
  SpawnTypesCount: Integer;
  TotalSize: Integer;
  RemainingDanger: Single;
  X, Y: Int16;
  AMonster: TMonster;
  SpawnableList, SpawnList: TActorDataList;
  MinSpawnDistance: TDistanceQuant;
  MinSpawnRange: Single;
  NotOnlyRareMonsters: Boolean;
begin
  if TotalDanger <= 0 then
    Exit;

  SpawnableList := TActorDataList.Create(false);
  for I := 0 to Pred(MonstersData.Count) do
    if not (MonstersData[I] as TMonsterData).Trap and
       not (MonstersData[I] as TMonsterData).Chest and
       ((MonstersData[I] as TMonsterData).StartSpawningAtDepth <= CurrentDepth) and
       ((MonstersData[I] as TMonsterData).StopSpawningAtDepth > CurrentDepth) then
      SpawnableList.Add(MonstersData[I]);

  SpawnList := TActorDataList.Create(false);
  SpawnTypesCount := Rnd.Random(SpawnableList.Count);
  TotalSize := 0;
  NotOnlyRareMonsters := false; // Make sure there is no level with only rare monsters
  I := 0;
  repeat
    J := Rnd.Random(SpawnableList.Count);
    Inc(I);
    Inc(TotalSize, SpawnableList[J].Size);
    // TODO: SpawnableList.Delete(J) before continue
    if TotalSize > MaxTotalSize then // Make sure all monsters are fit into a single texture
      continue;
    SpawnList.Add(SpawnableList[J]);
    if not (SpawnableList[J] as TMonsterData).Rare then
      NotOnlyRareMonsters := true;
    SpawnableList.Delete(J);
  until (I > SpawnTypesCount) and NotOnlyRareMonsters; // Note, we can still break NotOnlyRareMonsters check by overflowing our render size, but it's impossible now, and unlikely to be anywhere possible in hte future

  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot find any monsters to spawn at level %d', [Self.CurrentDepth]);
    Exit;
  end;

  // create monsters
  MinSpawnDistance := DistanceStep * Min(Round(2 * TPlayerCharacter.VisibleRange) + 1, Min(SizeX div 2, SizeY div 2));
  MinSpawnRange := Min(TPlayerCharacter.VisibleRange + 1, Min(SizeX div 2, SizeY div 2));
  RemainingDanger := TotalDanger;
  while RemainingDanger > 0 do
  begin
    AMonster := TMonster.Create;
    if MonstersList.Count < SpawnList.Count then
      AMonster.Data := SpawnList[MonstersList.Count] // make sure each monster is represented at least once
    else
      repeat
        AMonster.Data := SpawnList[Rnd.Random(SpawnList.Count)];
      until Rnd.Random < AMonster.MonsterData.SpawnFrequency;
    AMonster.Reset;
    repeat
      X := Rnd.Random(SizeX);
      Y := Rnd.Random(SizeY);
    until PassableTiles[AMonster.PredSize][X + SizeX * Y] and (Sqr(EntranceX - X) + Sqr(EntranceY - Y) > Sqr(MinSpawnRange)) and (Rnd.Random < Sqr(Single(DistanceFromEntranceCache[X + SizeX * Y] - MinSpawnDistance) / Single(MinSpawnDistance))) and not Ray(0, EntranceX, EntranceY, Single(X), Single(Y));
    AMonster.Teleport(X, Y);
    AMonster.Ai.SoftGuard := Rnd.RandomBoolean and not AMonster.MonsterData.CuriousAi;
    AMonster.Ai.Guard := Rnd.RandomBoolean and not AMonster.MonsterData.CuriousAi and not AMonster.Ai.SoftGuard;
    AMonster.Ai.GuardPointX := AMonster.LastTileX;
    AMonster.Ai.GuardPointY := AMonster.LastTileY;
    MonstersList.Add(AMonster);
    RemainingDanger -= AMonster.MonsterData.Danger;
  end;
  if MonstersList.Count < SpawnList.Count then
    DebugWarning('%d monsters were generated out of %d monster types can spawn', [MonstersList.Count, SpawnList.Count]);

  //Prepre PatrolList
  PatrolList.Clear;
  SpawnableList.Clear;
  for J := 0 to Pred(MonstersData.Count) do
    if ((MonstersData[J] as TMonsterData).StartPatrolAtDepth <= Map.CurrentDepth) and ((MonstersData[J] as TMonsterData).StopPatrolAtDepth > Map.CurrentDepth) then
      SpawnableList.Add(MonstersData[J]);
  // already spawned monsters are always in the PatrolList
  // Also making sure that they can patrol at this depth in the loop above
  for I := 0 to Pred(SpawnList.Count) do
    if SpawnableList.Contains(SpawnList[I]) then
    begin
      PatrolList.Add(SpawnList[I]);
      SpawnableList.Remove(SpawnList[I]);
    end;
  // Also add a random number of other valid types
  if SpawnableList.Count > 0 then
  begin
    SpawnTypesCount := Rnd.Random(SpawnableList.Count);
    for I := 0 to SpawnTypesCount do
    begin
      J := Rnd.Random(SpawnableList.Count);
      // Note, we keep TotalSize from previous step
      Inc(TotalSize, SpawnableList[J].Size);
      if TotalSize > MaxTotalSize then // Make sure all monsters are fit into a single texture
        continue;
      PatrolList.Add(SpawnableList[J]);
      SpawnableList.Delete(J);
    end;
  end;
  if PatrolList.Count = 0 then
    ShowError('Failed to generate PatrolList', []);

  SpawnList.Free;
  SpawnableList.Free;
end;

procedure TMap.GenerateTraps(const NumberOfTraps: Integer);
var
  I: Integer;
  X, Y: Int16;
  ATrap: TMonster;
  SpawnList: TActorDataList;
begin
  if NumberOfTraps = 0 then
    Exit;
  SpawnList := TActorDataList.Create(false);
  for I := 0 to Pred(TrapsData.Count) do
    if ((TrapsData[I] as TMonsterData).StartSpawningAtDepth <= CurrentDepth) and
      ((TrapsData[I] as TMonsterData).StopSpawningAtDepth > CurrentDepth) then
      SpawnList.Add(TrapsData[I]);
  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot find any traps to spawn at level %d', [Self.CurrentDepth]);
    Exit;
  end;

  // TEMPORARY TODO
  // create trap
  for I := 0 to Pred(NumberOfTraps) do
  begin
    ATrap := TMonster.Create;
    repeat
      ATrap.Data := SpawnList[Rnd.Random(SpawnList.Count)];
    until Rnd.Random < ATrap.MonsterData.SpawnFrequency;
    ATrap.Reset;
    repeat
      X := Rnd.Random(SizeX);
      Y := Rnd.Random(SizeY);
    until PassableTiles[ATrap.PredSize][X + SizeX * Y] and (DistanceFromEntranceCache[X + SizeX * Y] > TPlayerCharacter.VisibleRange * DistanceStep);
    ATrap.Teleport(X, Y);
    MonstersList.Add(ATrap);
  end;
  SpawnList.Free;
end;

procedure TMap.GenerateItems(const GenerateItems: Integer);
var
  I, K, L: Integer;
  X, Y: Int16;
  ItemData: TItemData;
  SpawnList: TItemsDataList;
  NewChest: TMonster;
begin
  if GenerateItems = 0 then
    Exit;
  SpawnList := TItemsDataList.Create(false);
  for I := 0 to Pred(ItemsDataList.Count) do
    if ItemsDataList[I].StartSpawningAtDepth <= CurrentDepth then
      SpawnList.Add(ItemsDataList[I]);
  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot find any items to spawn at level %d', [Self.CurrentDepth]);
    SpawnList.Free;
    Exit;
  end;

  // TEMPORARY TODO
  // create items
  K := GenerateItems;
  while K > 0 do
  begin
    if (Rnd.Random < 0.8) and (CurrentDepth > 0) then
    begin
      NewChest := TMonster.Create;
      if Rnd.Random < 0.001 * (CurrentDepth - 1) then // 0.1% at lvl.2 -> 3.0% at lvl.30
        NewChest.Data := MimicsData[Rnd.Random(MimicsData.Count)]
      else
        NewChest.Data := ChestsData[Rnd.Random(ChestsData.Count)];
      NewChest.Reset;
      if ExtremaGeneration.Count > 0 then
      begin
        L := Rnd.Random(ExtremaGeneration.Count);
        X := ExtremaGeneration[L].X;
        Y := ExtremaGeneration[L].Y;
        ExtremaGeneration.Delete(L);
      end else
      begin // this shouldn't happen normally
        DebugWarning('Ran out of ExtremaGeneration in GenerateItems', []);
        repeat
          X := Rnd.Random(SizeX);
          Y := Rnd.Random(SizeY);
        until PassableTiles[NewChest.PredSize][X + SizeX * Y];
      end;
      for I := 0 to Min(K, Trunc(5 * Sqr(Rnd.Random)) - 1) do
      begin
        repeat
          ItemData := SpawnList[Rnd.Random(SpawnList.Count)];
        until Rnd.Random < ItemData.SpawnFrequency;
        NewChest.Loot.Add(TInventoryItem.NewItem(ItemData));
        Dec(K);
      end;
      NewChest.Teleport(X, Y);
      MonstersList.Add(NewChest);
    end else
    begin
      repeat
        X := Rnd.Random(SizeX);
        Y := Rnd.Random(SizeY);
      until PassableTiles[0][X + SizeX * Y];
      repeat
        ItemData := SpawnList[Rnd.Random(SpawnList.Count)];
      until Rnd.Random < ItemData.SpawnFrequency;
      MapItemsList.Add(TMapItem.CreateItem(X, Y, TInventoryItem.NewItem(ItemData)));
      Dec(K);
    end;
  end;
  SpawnList.Free;
end;

procedure TMap.CacheCharactersOnThisLevel;
var
  P: TPlayerCharacter;
begin
  CharactersOnThisLevel.Clear;
  for P in PlayerCharactersList do
    if P.AtMap = CurrentDepth then
      CharactersOnThisLevel.Add(P);
end;

procedure TMap.PositionCapturedCharacters;

  procedure SpawnGuards(const Player: TPlayerCharacter); // todo: PositionedObject, guards type
  var
    Dst: TDistanceMapArray;
    GuardsQuantity: Integer;
    Guard: TMonster;
    GuardList: TActorDataList;
    AX, AY: Int16;
    I: Integer;
  begin
    Dst := DistanceMap(Player);
    GuardList := TActorDataList.Create(false);
    GuardList.Add(MonstersDataDictionary['yellow_guard']);
    GuardList.Add(MonstersDataDictionary['black_guard']);
    GuardList.Add(MonstersDataDictionary['cyan_guard']);
    GuardList.Add(MonstersDataDictionary['red_guard']);
    GuardList.Add(MonstersDataDictionary['purple_guard']);
    GuardList.Add(MonstersDataDictionary['blue_guard']);
    GuardList.Add(MonstersDataDictionary['bug_guard']);
    GuardsQuantity := Min(Rnd.Random(Round(Sqrt(CurrentDepth))) + 1, GuardList.Count); // lvl 0,1 = 1; lvl3 = 1-2; lvl10 = 1-3; lvl25 = 1-5 // capped at 4 now
    for I := 0 to Pred(GuardsQuantity) do
    begin
      repeat
        AX := Rnd.Random(SizeX);
        AY := Rnd.Random(SizeY);
      until PassableTiles[Player.PredSize][AX + SizeX * AY] and (Dst[AX + SizeX * AY] < 10 * 5);
      Guard := TMonster.Create;
      Guard.Data := GuardList[Rnd.Random(GuardList.Count)];
      GuardList.Remove(Guard.Data);
      Guard.Reset;
      Guard.Teleport(AX, AY);
      Guard.Ai.Guard := true;
      Guard.Ai.GuardPointX := AX;
      Guard.Ai.GuardPointY := AY;
      MonstersList.Add(Guard);
    end;
    GuardList.Free;
  end;

var
  I, SelectedExtrema: Integer;
  P: TPlayerCharacter;
  Captives: TPlayerCharactersList;
  Seed: TCoordList;
  Dist: TDistanceMapArray;
  MaxDst: TDistanceQuant;
  AX, AY: Int16;
begin
  Captives := TPlayerCharactersList.Create(false);
  for P in CharactersOnThisLevel do
    if P.IsCaptured then
      Captives.Add(P);

  Seed := GetPathSeed(
    Map.GenerateDijkstra(PassableTiles[PredPlayerColliderSize], ExitX, ExitY, 1024, true, EntranceX, EntranceY, true),
    EntranceX, EntranceY
  );

  {for P in CharactersOnThisLevel do
    if not P.IsCaptured then
      P.AddSeed(Seed);} // let's remove this for now, and use EntranceX, EntranceY explicitly

  for P in Captives do
  begin
    Dist := DistanceMap(P.PredSize, Seed);
    //MaxDst := MaxDistance(Dist);
    if ExtremaGeneration.Count > 0 then
    begin
      MaxDst := 0;
      SelectedExtrema := -1;
      for I := 0 to Pred(ExtremaGeneration.Count) do
        if Dist[ExtremaGeneration[I].X + SizeX * ExtremaGeneration[I].Y] >= MaxDst then // >= to avoid extreme possible case that the only extremas exist have zero distance, won't hurt performance too much
        begin
          SelectedExtrema := I;
          MaxDst := Dist[ExtremaGeneration[I].X + SizeX * ExtremaGeneration[I].Y];
        end;
      if SelectedExtrema < 0 then
        raise Exception.Create('SelectedExtrema < 0 in PositionCapturedCharacters');
      AX := ExtremaGeneration[SelectedExtrema].X;
      AY := ExtremaGeneration[SelectedExtrema].Y;
      ExtremaGeneration.Delete(SelectedExtrema);
    end else
    begin
      DebugWarning('Ran out of ExtremaGeneration in PositionCapturedCharacters', []);
      repeat
        AX := Rnd.Random(SizeX);
        AY := Rnd.Random(SizeY);
      until PassableTiles[P.PredSize][AX + SizeX * AY] and (Dist[AX + SizeX * AY] >= MaxDst - 5);
    end;
    Dist := nil;
    P.Teleport(AX, AY);
    P.Reset; // TODO: workaround, that P.CanAct = false and therefore actions with them as targets don't work
    P.AddSeed(Seed);
    SpawnGuards(P);
  end;
  Seed.Free;
  Captives.Free;
end;

function TMap.CapturedCharactersAtLevel: Integer;
var
  P: TPlayerCharacter;
begin
  Result := 0;
  for P in CharactersOnThisLevel do
    if P.IsCaptured then
      Inc(Result);
end;

procedure TMap.UpdatePassableVisibleTiles(const OriginX, OriginY: Int16);
begin
  if PassablePlayerTiles = nil then
  begin
    SetLength(PassablePlayerTiles, Length(FMap));
    FillByte(PassablePlayerTiles[0], Length(PassablePlayerTiles), Byte(ByteBool(false)));
    PassablePlayerTiles[OriginX + SizeX * OriginY] := true;
  end;
  FloodFillPassablePlayerTilesWithVisible;
end;

procedure TMap.MakeGenerationCache;
var
  Seed: TCoordList;
  I: Integer;
begin
  Seed := TCoordList.Create;
  Seed.Add(IntCoord(EntranceX, EntranceY));
  DistanceFromEntranceCache := DistanceMap(PredPlayerColliderSize, Seed);
  Seed.Free;
  MaxDistanceFromEntrance := MaxDistance(DistanceFromEntranceCache);
  ExtremaGeneration := TExtremaList.Create(false);
  for I := 0 to Pred(Extrema.Count) do
    if (Abs(Extrema[I].X - EntranceX) > 2) or (Abs(Extrema[I].Y - EntranceY) > 2) then
      ExtremaGeneration.Add(Extrema[I]);
end;

function TMap.MaxDistance(const MapArray: TDistanceMapArray): TDistanceQuant;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Pred(SizeX * SizeY) do
    if (Result < MapArray[I]) and (MapArray[I] < UndefinedDistance) then
      Result := MapArray[I];
end;

procedure TMap.GenerateEntryAndExit;
var
  I, J: SizeInt;
  Seed: TCoordList;
  Dist: TDistanceMapArray;

  D, MaxDist: TDistanceQuant;
  MaxI, MaxJ: SizeInt;
begin
  Seed := TCoordList.Create;
  MaxDist := 0;
  for I := 0 to Pred(Extrema.Count) do
  begin
    Seed.Clear;
    Seed.Add(IntCoord(Extrema[I].X, Extrema[I].Y));
    Dist := DistanceMap(PredPlayerColliderSize, Seed);
    for J := 0 to Pred(Extrema.Count) do
    begin
      D := Dist[Extrema[J].X + SizeX * Extrema[J].Y];
      if D > MaxDist then
      begin
        MaxDist := D;
        MaxI := I;
        MaxJ := J;
      end;
    end;
  end;

  EntranceX := Extrema[MaxI].X;
  EntranceY := Extrema[MaxI].Y;
  ExitX := Extrema[MaxJ].X;
  ExitY := Extrema[MaxJ].Y;

  //Extrema.Delete(MaxI); // entrance - do not delete it; monsters might want to check it from time to time
  //Extrema.Delete(MaxJ); -- this one may be changed by the previous one. We'll delete it by next check safer

  Seed.Clear;
  Seed.Add(IntCoord(ExitX, ExitY));
  Dist := DistanceMap(PredPlayerColliderSize, Seed); // TODO: cache, do not recalculate
  I := 0;
  while I < Extrema.Count do
    if Dist[Extrema[I].X + SizeX * Extrema[I].Y] <= DistanceStep * 10 then
      Extrema.Delete(I)
    else
      Inc(I);
  Seed.Free;

  for I := 0 to Pred(Extrema.Count) do
    Extrema[I].LastChecked := Dist[Extrema[I].X + SizeX * Extrema[I].Y] - MaxDist; // this is distance from Exit, see above ---> in an inverted way = 0 at Entry, = -MaxDist at Exit
end;

procedure TMap.ClearGenerationCache;
begin
  DistanceFromEntranceCache := nil;
  FreeAndNil(ExtremaGeneration);
end;

procedure TMap.CalculateRenderRects;
var
  I: SizeInt;
begin
  RenderRects := 0;
  for I := 0 to Pred(SizeX * SizeY) do
    RenderRects += Length(FMap[I]);
  DebugLog('Render rects: %d', [RenderRects]);
end;

procedure TMap.FindDistanceMapExtrema;
var
  Seed: TCoordList;
  Dist: TDistanceMapArray;
  IX, IY: Integer;
  J: SizeInt;
  E: TExtremum;
begin
  Seed := TCoordList.Create;
  Seed.Add(IntCoord(EntranceX, EntranceY)); // TODO: It's seed of the map, not real entrance
  Dist := DistanceMap(PredPlayerColliderSize, Seed);
  Seed.Free;
  J := 0;
  for IY := 0 to PredSizeY do
    for IX := 0 to PredSizeX do
    begin
      if Dist[J] = UndefinedDistance then
        Dist[J] := 0;
      Inc(J);
    end;

  Extrema.Clear;
  //J := 0;
  for IY := 1 to PredSizeY - 1 do
    for IX := 1 to PredSizeX - 1 do
    begin
      J := IX + SizeX * IY; // TODO: Optimize
      if (Dist[J] > 0) and
         (Dist[J] > Dist[J - 1]) and (Dist[J] > Dist[J + 1]) and
         (Dist[J] > Dist[J - SizeX]) and (Dist[J] > Dist[J + SizeX]) and
         (Dist[J] > Dist[J - 1 - SizeX]) and (Dist[J] > Dist[J - 1 + SizeX]) and
         (Dist[J] > Dist[J + 1 - SizeX]) and (Dist[J] > Dist[J + 1 + SizeX]) then
      begin
        E := TExtremum.Create;
        E.X := IX;
        E.Y := IY;
        //E.LastChecked := -(Sqr(EntranceX - E.X) + Sqr(EntranceY - E.Y)); This no longer makes sense, TODO
        Extrema.Add(E);
      end;
      //Inc(J);
    end;
  Dist := nil;
  DebugLog('Extremas Found %d', [Extrema.Count]);
end;

function TMap.GenerateMoveMap(const PredColliderSize: Integer; const ConsiderVisible: Boolean): TMoveArray;
var
  X, Y: Integer;
  I: SizeInt;

  function CanStepHere: ByteBool;
  var
    X1, Y1: Integer;
    J: SizeInt;
  begin
    for Y1 := 0 to PredColliderSize do
      begin
        J := (Y + Y1) * SizeX + X;
        for X1 := 0 to PredColliderSize do
        begin
          // TODO: Cache zero-size-map?
          if not CanMove(J) or (ConsiderVisible and (Visible[J] < RememberedVisible)) then
            Exit(false);
          Inc(J);
        end;
      end;
    Exit(true);
  end;

begin
  Result := nil;
  SetLength(Result, FSizeX * FSizeY);
  I := 0;
  for Y := 0 to PredSizeY do
    for X := 0 to PredSizeX do
    begin
      if (X = 0) or (Y = 0) or (X >= PredSizeX - PredColliderSize) or (Y >= PredSizeY - PredColliderSize) then
        Result[I] := false
      else
        Result[I] := CanStepHere;
      Inc(I);
    end;
end;

function TMap.DistanceMap(const PredSize: Byte; const Seed: TCoordList): TDistanceMapArray;
var
  X, Y: Integer;
  MinX, MaxX, MinY, MaxY: Integer;
  NoMoreMoves: Boolean;
  I: SizeInt;
  Target: TIntCoord;
  CurrentPassableTiles: TMoveArray;

  procedure TryMovementWithDeltas(const DeltaX, DeltaY: Integer);
  var
    ThatDistance, NewDistance, DeltaDistance: TDistanceQuant;
  begin
    ThatDistance := Result[X + DeltaX + (Y + DeltaY) * SizeX];
    if ThatDistance < UndefinedDistance then
    begin
      if (DeltaX <> 0) and (DeltaY <> 0) then
        DeltaDistance := DistanceStepDiagonal
      else
        DeltaDistance := DistanceStep;
      NewDistance := ThatDistance + DeltaDistance;
      if Result[I] > NewDistance then
      begin
        Result[I] := NewDistance;
        NoMoreMoves := false;
        if (X = MinX) and (MinX > 0) then
          Dec(MinX);
        if (X = MaxX) and (MaxX < PredSizeX) then
          Inc(MaxX);
        if (Y = MinY) and (MinY > 0) then
          Dec(MinY);
        if (Y = MaxY) and (MaxY < PredSizeY) then
          Inc(MaxY);
      end;
    end;
  end;

begin
  CurrentPassableTiles := PassableTiles[PredSize];
  Result := nil;
  SetLength(Result, Length(CurrentPassableTiles));
  FillByte(Result[0], Length(Result) * SizeOf(TDistanceQuant), 255);
  MinX := SizeX;
  MaxX := 0;
  MinY := SizeY;
  MaxY := 0;
  for Target in Seed do
  begin
    if MinX > Target[0] then
      MinX := Target[0];
    if MaxX < Target[0] then
      MaxX := Target[0];
    if MinY > Target[1] then
      MinY := Target[1];
    if MaxY < Target[1] then
      MaxY := Target[1];
    Result[Target[0] + SizeX * Target[1]] := 0;
  end;
  if MinX > 0 then
    Dec(MinX);
  if MaxX < PredSizeX then
    Inc(MaxX);
  if MinY > 0 then
    Dec(MinY);
  if MaxY < PredSizeY then
    Inc(MaxY);

  repeat
    NoMoreMoves := true;
    for X := MinX to MaxX do
      for Y := MinY to MaxY do
      begin
        I := X + Y * SizeX;
        if CurrentPassableTiles[I] then
        begin
          TryMovementWithDeltas(-1,  0);
          TryMovementWithDeltas(+1,  0);
          TryMovementWithDeltas( 0, -1);
          TryMovementWithDeltas( 0, +1);
          TryMovementWithDeltas(-1, +1);
          TryMovementWithDeltas(+1, +1);
          TryMovementWithDeltas(-1, -1);
          TryMovementWithDeltas(+1, -1);
        end;
      end;
  until NoMoreMoves;
end;

function TMap.DistanceMap(const Actor: TObject): TDistanceMapArray;
var
  Seed: TCoordList;
begin
  if not (Actor is TPositionedObject) then
    raise Exception.CreateFmt('DistanceMap can work only on TPositionedObject, got %s instead', [Actor.ClassName]);
  Seed := TCoordList.Create;
  TPositionedObject(Actor).AddSeed(Seed);
  Result := DistanceMap(TPositionedObject(Actor).PredSize, Seed);
  Seed.Free;
end;

function TMap.FloodFill(const SrcArray: TMoveArray; const StartX, StartY: UInt32): TMoveArray;
var
  X, Y: Integer;
  I: SizeInt;
  Finished: Boolean;
begin
  Result := nil;
  SetLength(Result, SizeX * SizeY);
  FillByte(Result[0], Length(Result), Byte(ByteBool(false)));
  Result[StartX + SizeX * StartY] := true;
  repeat
    Finished := true;
    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if not Result[I] and SrcArray[I] then
         if ((X > 0) and Result[I - 1]) or
            ((X < PredSizeX) and Result[I + 1]) or
            ((Y > 0) and Result[I - SizeX]) or
            ((Y < PredSizeY) and Result[I + SizeX]) then
         begin
           Result[I] := true;
           Finished := false;
         end;
        Inc(I);
      end;
  until Finished;
end;

procedure TMap.FloodFillPassablePlayerTilesWithVisible;
var
  X, Y: Integer;
  I: SizeInt;
  Finished: Boolean;
begin
  repeat
    Finished := true;
    I := 0;
    for Y := 0 to PredSizeY do
      for X := 0 to PredSizeX do
      begin
        if not PassablePlayerTiles[I] and PassableTiles[PredPlayerColliderSize][I] and (Visible[I] >= RememberedVisible) then
         if ((X > 0) and PassablePlayerTiles[I - 1]) or
            ((X < PredSizeX) and PassablePlayerTiles[I + 1]) or
            ((Y > 0) and PassablePlayerTiles[I - SizeX]) or
            ((Y < PredSizeY) and PassablePlayerTiles[I + SizeX]) then
         begin
           PassablePlayerTiles[I] := true;
           Finished := false;
         end;
        Inc(I);
      end;
  until Finished;
end;

function TMap.GenerateDijkstra(const CurrentPassableTiles: TMoveArray; const TargetX, TargetY: Int16; const MaxMovementRange: UInt32; const IgnoreVisible: Boolean; const FromX, FromY: Int16; const ConsiderFromPosition: Boolean): TDirectionArray;
var
  DX, DY: Integer;
  MinX, MaxX, MinY, MaxY: Integer;
  NoMoreMoves: Boolean;
  TotalMovementDistance: Integer;
  NewMovementMap, OldMovementMap: TDirectionArray;
  I: SizeInt;

  function TryMovementWithDeltas(const DeltaX, DeltaY: Integer; const MovementDirection: TMovementDirection): Boolean;
  begin
    if OldMovementMap[TargetX + DX + DeltaX + (TargetY + DY + DeltaY) * SizeX] > CanGoMD then
    begin
      NewMovementMap[I] := MovementDirection;
      NoMoreMoves := false;
      if (DX = MinX) and (TargetX + MinX >= 0) then
        Dec(MinX);
      if (DX = MaxX) and (TargetX + MaxX <= PredSizeX) then
        Inc(MaxX);
      if (DY = MinY) and (TargetY + MinY >= 0) then
        Dec(MinY);
      if (DY = MaxY) and (TargetY + MaxY <= PredSizeY) then
        Inc(MaxY);
      Result := true;
    end else
      Result := false;
  end;

begin
  NewMovementMap := nil;
  OldMovementMap := nil;
  SetLength(NewMovementMap, Length(CurrentPassableTiles));
  SetLength(OldMovementMap, Length(CurrentPassableTiles));
  FillByte(NewMovementMap[0], Length(NewMovementMap), mdUndefined);
  NewMovementMap[TargetX + TargetY * SizeX] := mdZero;
  if (TargetX = FromX) and (TargetY = FromY) and (ConsiderFromPosition) then
  begin
    // We return something initialized here, to avoid returning nil
    Result := NewMovementMap;
    Exit;
  end;

  TotalMovementDistance := 0;
  MinX := -1;
  MaxX := 1;
  MinY := -1;
  MaxY := 1;
  repeat
    Move(NewMovementMap[0], OldMovementMap[0], Length(NewMovementMap));
    NoMoreMoves := true;
    for DX := MinX to MaxX do
      for DY := MinY to MaxY do
      begin
        I := TargetX + DX + (TargetY + DY) * SizeX;
        if (OldMovementMap[I] = mdUndefined) then
        begin
          if CurrentPassableTiles[I] and
            (IgnoreVisible or (Visible[I] >= RememberedVisible)) then
          begin
            if not TryMovementWithDeltas(-1,  0, mdLeft) then
            if not TryMovementWithDeltas(+1,  0, mdRight) then
            if not TryMovementWithDeltas( 0, -1, mdDown) then
            if not TryMovementWithDeltas( 0, +1, mdUp) then
            if not TryMovementWithDeltas(-1, +1, mdUpLeft) then
            if not TryMovementWithDeltas(+1, +1, mdUpRight) then
            if not TryMovementWithDeltas(-1, -1, mdDownLeft) then
                   TryMovementWithDeltas(+1, -1, mdDownRight);
          end else
            NewMovementMap[I] := mdWall;
        end;
      end;
    Inc(TotalMovementDistance);
  until (TotalMovementDistance > MaxMovementRange) or NoMoreMoves or (ConsiderFromPosition and (NewMovementMap[FromX + FromY * SizeX] <> mdUndefined));

  Result := NewMovementMap;
end;

function TMap.GetWaypoints(const Directions: TDirectionArray; const IncludeNextTile: Boolean; const FromXTile, FromYTile: Int16;
  const FromX, FromY, ToX, ToY: Single): TWaypoints;

  function Waypoint(const X, Y: Single): TFloatCoord; inline;
  begin
    Result[0] := X;
    Result[1] := Y;
  end;

var
  IX, IY: Int16;
  I: SizeInt;

  procedure FindNearestNavPointWorkaround;
  var
    Dist, DX, DY: Int16;
  begin
    Dist := 1;
    while true do
    begin
      for DX := -Dist to Dist do
        for DY := -Dist to Dist do
          if Directions[IX + DX + SizeX * (IY + DY)] > CanGoMD then
          begin
            IX := IX + DX;
            IY := IY + DY;
            I := IX + SizeX * IY;
            Exit;
          end;
      Inc(Dist);
    end;
  end;
begin
  Result := TWaypoints.Create;
  // trying to guess memory volume to avoid resize in future
  Result.Capacity := (Abs(Trunc(FromX - ToX)) + Abs(Trunc(FromY - ToY)) + 2) * 2;
  Result.Add(Waypoint(FromX, FromY));
  IX := FromXTile;
  IY := FromYTile;
  if IncludeNextTile then
    Result.Add(Waypoint(IX + Frac(ToX), IY + Frac(ToY)));
  I := IX + SizeX * IY;
  if Directions[I] <= CanGoMD then
  begin
    DebugWarning('NAV GRID ERROR in GetWaypoints. Directions[%d,%d] are not reachable from the destination or maybe not walkable at all.', [IX, IY]);
    //Exit(nil); // might still need to return this feature if larger enemies - they may be unable to move beyond some dungeon areas and thus exception instead of "doing something else"
    FindNearestNavPointWorkaround;
    DebugWarning('Workaround for nav grid error finished. New Directions[%d,%d]', [IX, IY]);
  end;
  repeat
    IX += Trunc(DirectionToVector(Directions[I]).X); // TODO optimize;
    IY += Trunc(DirectionToVector(Directions[I]).Y); // TODO optimize;
    Result.Add(Waypoint(IX + Frac(ToX), IY + Frac(ToY)));
    I := IX + SizeX * IY;
  until (Directions[I] = mdZero);
end;

function TMap.GetPathSeed(const Directions: TDirectionArray; const FromX,
  FromY: Int16): TCoordList;
var
  IX, IY: Int16;
  I: SizeInt;

  procedure FindNearestNavPointWorkaround;
  var
    Dist, DX, DY: Int16;
  begin
    Dist := 1;
    while true do
    begin
      for DX := -Dist to Dist do
        for DY := -Dist to Dist do
          if Directions[IX + DX + SizeX * (IY + DY)] > CanGoMD then
          begin
            IX := IX + DX;
            IY := IY + DY;
            I := IX + SizeX * IY;
            Exit;
          end;
      Inc(Dist);
    end;
  end;

begin
  Result := TCoordList.Create;
  Result.Add(IntCoord(FromX, FromY));
  IX := FromX;
  IY := FromY;
  I := IX + SizeX * IY;
  if Directions[I] <= CanGoMD then
  begin
    DebugWarning('NAV GRID ERROR in GetWaypoints. Directions[%d,%d] are not reachable from the destination or maybe not walkable at all.', [IX, IY]);
    //Exit(nil); // might still need to return this feature if larger enemies - they may be unable to move beyond some dungeon areas and thus exception instead of "doing something else"
    FindNearestNavPointWorkaround;
    DebugWarning('Workaround for nav grid error finished. New Directions[%d,%d]', [IX, IY]);
  end;
  repeat
    IX += Trunc(DirectionToVector(Directions[I]).X); // TODO optimize;
    IY += Trunc(DirectionToVector(Directions[I]).Y); // TODO optimize;
    Result.Add(IntCoord(IX, IY));
    I := IX + SizeX * IY;
  until (Directions[I] = mdZero);
end;

procedure TMap.ScheduleUpdateVisible;
begin
  FScheduleUpdateVisible := true;
end;

procedure TMap.UpdateVisible;
var
  P: TPlayerCharacter;
  ProcessDisorientation: Boolean;
begin
  if FScheduleUpdateVisible then
  begin
    FScheduleUpdateVisible := false;
    ClearActiveVisible;
    ProcessDisorientation := Disorientation > 0;
    for P in CharactersOnThisLevel do
      if P.CanAct then
        LookAround(P.LastTileX + P.Size div 2, P.LastTileY + P.Size div 2);
    if ProcessDisorientation and (Disorientation <= 0) then
      RememberVisible;
  end;
end;

procedure TMap.LookAround(const AX, AY: Int16);

  procedure WriteVisible(const A: SizeInt); inline;
  begin
    if Visible[A] = ForgottenVisible then
      Dec(Disorientation);
    Visible[A] := DirectlyVisible;
  end;

var
  DX, DY, IX, IY: Integer;
  Slope: Single;
  I: SizeInt;
  VisibilityRange: Integer;
begin
  VisibilityRange := Ceil(TPlayerCharacter.VisibleRange);
  //ClearCurrentlyVisible;
  for IX := -VisibilityRange to VisibilityRange do
    for IY := -VisibilityRange to VisibilityRange do
      if Sqr(IX) + Sqr(IY) <= Sqr(TPlayerCharacter.VisibleRange) then
        if (IX = 0) and (IY = 0) then
          WriteVisible(AX + SizeX * AY)
        else
        if Abs(IX) > Abs(IY) then
        begin
          DX := 0;
          DY := 0;
          Slope := Single(IY) / Single(IX);
          repeat
            DX += Sign(IX);
            I := AX + DX + SizeX * (AY + DY);
            WriteVisible(I);
            if not CanSee(I) then
              break;
            if Trunc(DX * Slope) <> Trunc((DX + Sign(IX)) * Slope) then
            begin
              DY += Sign(IY);
              I := AX + DX + SizeX * (AY + DY);
              WriteVisible(I);
              if not CanSee(I) then
                break;
            end;
          until (DX = IX) and (DY = IY);
        end else
        begin
          DX := 0;
          DY := 0;
          Slope := Single(IX) / Single(IY);
          repeat
            DY += Sign(IY);
            I := AX + DX + SizeX * (AY + DY);
            WriteVisible(I);
            if not CanSee(I) then
              break;
            if Trunc(DY * Slope) <> Trunc((DY + Sign(IY)) * Slope) then
            begin
              DX += Sign(IX);
              I := AX + DX + SizeX * (AY + DY);
              WriteVisible(I);
              if not CanSee(I) then
                break;
            end;
          until (DX = IX) and (DY = IY);
        end;
end;

function TMap.Ray(const PredSize: Byte; const FromX, FromY: Int16; var ToX, ToY: Int16): Boolean;
var
  IX, IY, DX, DY: Integer;
  Slope: Single;
begin
  if (FromX = ToX) and (FromY = ToY) then
    Exit(true);

  IX := ToX - FromX;
  IY := ToY - FromY;
  if Abs(IX) > Abs(IY) then
  begin
    DX := 0;
    DY := 0;
    Slope := Single(IY) / Single(IX);
    repeat
      ToX := FromX + DX;
      ToY := FromY + DY;
      DX += Sign(IX);
      if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then // = Can't Pass.
        Exit(false);
      if Trunc(DX * Slope) <> Trunc((DX + Sign(IX)) * Slope) then
      begin
        DY += Sign(IY);
        if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then
          Exit(false);
      end;
    until (DX = IX) and (DY = IY);
  end else
  begin
    DX := 0;
    DY := 0;
    Slope := Single(IX) / Single(IY);
    repeat
      ToX := FromX + DX;
      ToY := FromY + DY;
      DY += Sign(IY);
      if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then
        Exit(false);
      if Trunc(DY * Slope) <> Trunc((DY + Sign(IY)) * Slope) then
      begin
        DX += Sign(IX);
        if not PassableTiles[PredSize][FromX + DX + SizeX * (FromY + DY)] then
          Exit(false);
      end;
    until (DX = IX) and (DY = IY);
  end;
  ToX := FromX + IX;
  ToY := FromY + IY;
  Result := true;
end;

function TMap.Ray(const PredSize: Byte; const FromX, FromY, ToX, ToY: Single): Boolean;
var
  TX, TY: Int16;
begin
  TX := Round(ToX); // TEMPORARY TODO
  TY := Round(ToY);
  Result := Ray(PredSize, Int16(Round(FromX)), Int16(Round(FromY)), TX, TY);
end;

class function TMap.DirectionToVector(const Dir: TMovementDirection): TVector2;
begin
  case Dir of
    mdWall, mdUndefined, mdZero: Result := TVector2.Zero;
    mdUp: Result := Vector2(0, 1);
    mdUpRight: Result := Vector2(1, 1);
    mdRight: Result := Vector2(1, 0);
    mdDownRight: Result := Vector2(1, -1);
    mdDown: Result := Vector2(0, -1);
    mdDownLeft: Result := Vector2(-1, -1);
    mdLeft: Result := Vector2(-1, 0);
    mdUpLeft: Result := Vector2(-1, 1);
    // else - raise exception
  end;
end;

function TMap.DirectionToIncrement(const Dir: TMovementDirection): SizeInt;
begin
  case Dir of
    mdWall, mdUndefined, mdZero: Result := 0;
    mdUp: Result := SizeX;
    mdUpRight: Result := 1 + SizeX;
    mdRight: Result := 1;
    mdDownRight: Result := 1 - SizeX;
    mdDown: Result := -SizeX;
    mdDownLeft: Result := -1 - SizeX;
    mdLeft: Result := -1;
    mdUpLeft: Result := -1 + SizeX;
    // else - raise exception
  end;
end;

function TMap.CanMove(const Tile: SizeInt): Boolean;
begin
  Exit(Length(FMap[Tile]) = 1);
end;

function TMap.CanSee(const Tile: SizeInt): Boolean;
begin
  Exit(Length(FMap[Tile]) = 1);
end;

procedure TMap.MonstersToIdle;
const
  MaxRetryCount = Integer(10000);
var
  X, Y: Int16;
  Monster: TMonster;
  RetryCount: Integer;
begin
  // TODO: if target is "current player", no need to reset if attacking a different one
  for Monster in MonstersList do
    if Monster.CanAct then
    begin
      Monster.CurrentAction := TActionIdle.NewAction(Monster);
      if Monster.Aggressive and Monster.LineOfSight(ViewGame.CurrentCharacter) then
      begin
        RetryCount := 0;
        repeat
          repeat
            X := Rnd.Random(SizeX);
            Y := Rnd.Random(SizeY);
          until PassableTiles[Monster.PredSize][X + SizeX * Y];
          Inc(RetryCount);
        until not Monster.LineOfSight(ViewGame.CurrentCharacter) or (RetryCount > MaxRetryCount);
        Monster.Teleport(X, Y);
        Monster.Ai.Guard := false;
        Monster.Ai.SoftGuard := false;
      end;
    end;
  MarksList.Clear; // Ugh? For some reason resetting purple slime to idle doesn't clear it's marks TODO
end;

function TMap.MonsterSeen(const AX, AY: Int16; const Range: Single): Boolean;
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive and (M.DistanceTo(AX, AY) <= Range) and M.LineOfSight(AX, AY) then
      Exit(true);
  Exit(false);
end;

function TMap.NoMonstersNearby(const AX, AY: Int16; const CollisionMargin: Int16): Boolean;
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct and not M.MonsterData.Chest and M.CollidesInt(AX, AY, CollisionMargin) then
      Exit(false);
  if MonsterSeen(AX, AY, TPlayerCharacter.VisibleRange + CollisionMargin) then
    Exit(false);
  Exit(true);
end;

function TMap.MonstersAttacking: Boolean;
var
  M: TMonster;
begin
  for M in MonstersList do
    if not M.Unsuspecting then
      Exit(true);
  Exit(false);
end;

procedure TMap.Save(const Element: TDOMElement);
var
  MapElement: TDOMElement;
  SaveElement: TDOMELement;
  Mark: TMarkAbstract;
  Monster: TMonster;
  Player: TPlayerCharacter;
  MapItem: TMapItem;

  PatrolRecord: TActorData;
begin
  MapElement := Element.CreateChild(Signature);
  MapElement.AttributeSet('CurrentDepth', CurrentDepth);
  MapElement.AttributeSet('SizeX', SizeX);
  MapElement.AttributeSet('SizeY', SizeY);
  MapElement.AttributeSet('Tileset', Tileset);
  MapElement.AttributeSet('EntranceX', EntranceX);
  MapElement.AttributeSet('EntranceY', EntranceY);
  MapElement.AttributeSet('ExitX', ExitX);
  MapElement.AttributeSet('ExitY', ExitY);
  MapElement.AttributeSet('Map', EncodeStringBase64(MapArrayToString(FMap)));
  MapElement.AttributeSet('Visible', EncodeStringBase64(VisibleArrayToString(Visible)));
  MapElement.AttributeSet('EnemySpawnTime', EnemySpawnTime);
  MapElement.AttributeSet('TimeSpentOnTheMap', TimeSpentOnTheMap);

  // TODO: More universal saving of SerializableObjects
  for Mark in MarksList do
  begin
    SaveElement := MapElement.CreateChild(TMarkAbstract.Signature);
    try
      Mark.Save(SaveElement);
    except
      ShowError('ERROR: Failed to save mark %s.', [Mark.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for Monster in MonstersList do
  begin
    SaveElement := MapElement.CreateChild(TMonster.Signature);
    try
      Monster.Save(SaveElement);
    except
      if Monster.Data <> nil then
        ShowError('ERROR: Failed to save monster %s.', [Monster.Data.Id])
      else
        ShowError('ERROR: Failed to save monster %s.', [Monster.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for MapItem in MapItemsList do
  begin
    SaveElement := MapElement.CreateChild(TMapItem.Signature);
    try
      MapItem.Save(SaveElement);
    except
      if (MapItem.Item <> nil) and (MapItem.Item.Data <> nil) then
        ShowError('ERROR: Failed to save item %s.', [MapItem.Item.Data.Id])
      else
        ShowError('ERROR: Failed to save item %s.', [MapItem.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for Player in PlayerCharactersList do
  begin
    SaveElement := MapElement.CreateChild(TPlayerCharacter.Signature);
    try
      Player.Save(SaveElement);
    except
      if Player.Data <> nil then
        ShowError('ERROR: Failed to save player %s.', [Player.Data.DisplayName])
      else
        ShowError('ERROR: Failed to save player %s.', [Player.ClassName]);
      MapElement.RemoveChild(SaveElement).Free;
    end;
  end;

  for PatrolRecord in PatrolList do
  begin
    SaveElement := MapElement.CreateChild('Patrol');
    SaveElement.AttributeSet('Id', PatrolRecord.Id);
  end;
end;

procedure TMap.Load(const Element: TDOMElement);
var
  I: TXMLElementIterator;
begin
  Init(Element.AttributeInteger('SizeX'), Element.AttributeInteger('SizeY'));
  CurrentDepth := Element.AttributeInteger('CurrentDepth');
  Tileset := Element.AttributeInteger('Tileset');
  EntranceX := Element.AttributeInteger('EntranceX');
  EntranceY := Element.AttributeInteger('EntranceY');
  ExitX := Element.AttributeInteger('ExitX');
  ExitY := Element.AttributeInteger('ExitY');
  FMap := StringToMapArray(DecodeStringBase64(Element.AttributeString('Map')), SizeX * SizeY);
  Visible := StringToVisibleArray(DecodeStringBase64(Element.AttributeString('Visible')), SizeX * SizeY);
  EnemySpawnTime := Element.AttributeSingle('EnemySpawnTime');
  TimeSpentOnTheMap := Element.AttributeSingleDef('TimeSpentOnTheMap', 60.0); // TODO: not def
  CalculateRenderRects;
  GeneratePassableTilesForColliders;

  I := Element.ChildrenIterator(TMonster.Signature);
  try
    while I.GetNext do
      try
        MonstersList.Add(TMonster.LoadClass(I.Current) as TMonster);
      except
        ShowError('ERROR: Failed to load monster "%s"', [I.Current.AttributeStringDef('Data.Id', 'N/A')]);
      end;
  finally
    FreeAndNil(I)
  end;

  I := Element.ChildrenIterator(TMapItem.Signature);
  try
    while I.GetNext do
      try
        MapItemsList.Add(TMapItem.LoadClass(I.Current) as TMapItem);
      except
        ShowError('ERROR: Map item was not loaded', []);
      end;
  finally
    FreeAndNil(I)
  end;

  I := Element.ChildrenIterator(TPlayerCharacter.Signature);
  try
    while I.GetNext do
      try
        PlayerCharactersList.Add(TPlayerCharacter.LoadClass(I.Current) as TPlayerCharacter);
      except
        ShowError('ERROR: Player Character was not loaded', []);
      end;

  finally
    FreeAndNil(I)
  end;
  CacheCharactersOnThisLevel;

  { We can't load marks yet: need a way to get Target properly. Just drop them all. }
  I := Element.ChildrenIterator(TMarkAbstract.Signature);
  try
    while I.GetNext do
      ;
      //MarksList.Add(TMarkAbstract.Load(I.Current));
  finally
    FreeAndNil(I)
  end;

  I := Element.ChildrenIterator('Patrol');
  try
    while I.GetNext do
      PatrolList.Add(MonstersDataDictionary[I.Current.AttributeString('Id')]);
  finally
    FreeAndNil(I)
  end;

  ParticlesList.Clear; // no need to load them, they no longer make sense? Todo

  FindDistanceMapExtrema;
  PrepareTilesetMap;
end;

end.

