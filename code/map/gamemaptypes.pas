{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMapTypes;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections;

const
  PredMaxColliderSize = 2;
  PredPlayerColliderSize = 2; // All player characters in a mission must have the same collider size, otherwise it doesn't make sense?

type
  TMovementDirection = Byte;
  TFloatCoord = array [0..1] of Single;
  TWaypoints = specialize TList<TFloatCoord>;
  TIntCoord = array [0..1] of Int16;
  TCoordList = specialize TList<TIntCoord>;

const
  { Movement direction aka enum, but 1-byte length }
  mdUndefined = TMovementDirection(0);
  mdWall = TMovementDirection(1);

  CanGoMD = TMovementDirection(9);

  mdUp = TMovementDirection(11);
  mdUpRight = TMovementDirection(12);
  mdRight = TMovementDirection(13);
  mdDownRight = TMovementDirection(14);
  mdDown = TMovementDirection(15);
  mdDownLeft = TMovementDirection(16);
  mdLeft = TMovementDirection(17);
  mdUpLeft = TMovementDirection(18);
  mdZero = TMovementDirection(19);

const
  Undefined = High(UInt32);

type
  TMapTile = array of UInt32;
  TMapArray = array of TMapTile;
  TDistanceQuant = UInt32;
  TDistanceMapArray = array of TDistanceQuant;
  TMovable = ByteBool;
  TMoveArray = packed array of TMovable;
  TDirectionArray = packed array of TMovementDirection;
  TVisibleGrade = Byte;
  TVisibleArray = packed array of TVisibleGrade;

const
  DistanceStepDiagonal = TDistanceQuant(7); // sqrt(2) * 5 = 7.07
  DistanceStep = TDistanceQuant(5);

type
  TExtremum = class(TObject) // TODO: Serializable class
    X: Int16;
    Y: Int16;
    LastChecked: Single;
  end;
  TExtremaList = specialize TObjectList<TExtremum>;

const
  NotVisible = TVisibleGrade(0);
  ForgottenVisible = TVisibleGrade(1);
  RememberedVisible = TVisibleGrade(2);
  DirectlyVisible = TVisibleGrade(255);

const
  UndefinedDistance = High(TDistanceQuant);

function IntCoord(const AX, AY: Int16): TIntCoord; inline;
function MapArrayToString(const AMap: TMapArray): String;
function StringToMapArray(const AString: String; const Size: SizeInt): TMapArray;
function VisibleArrayToString(const AVisible: TVisibleArray): String;
function StringToVisibleArray(const AString: String; const Size: SizeInt): TVisibleArray;
implementation
uses
  SysUtils;

type
  EInconsistentMapSizeInSaveGame = class(Exception);

function IntCoord(const AX, AY: Int16): TIntCoord; inline;
begin
  Result[0] := AX;
  Result[1] := AY;
end;

function MapArrayToString(const AMap: TMapArray): String;
var
  I, J: Integer;
begin
  Result := '';
  for I := 0 to Pred(Length(AMap)) do
  begin
    Result += Char(Length(AMap[I]));
    for J := 0 to Pred(Length(AMap[I])) do
      Result += Char(AMap[I][J]); // for now those are Bytes but WARNING/TODO
  end;
end;

function StringToMapArray(const AString: String; const Size: SizeInt): TMapArray;
var
  S: SizeInt;
  I: SizeInt;
  J: Integer;
begin
  Result := nil;
  SetLength(Result, Size);
  S := 1;
  I := 0;
  repeat
    SetLength(Result[I], Ord(AString[S]));
    Inc(S);
    for J := 0 to Pred(Length(Result[I])) do
    begin
      Result[I][J] := Ord(AString[S]);
      Inc(S);
    end;
    Inc(I);
    if (I > Size) then
      raise EInconsistentMapSizeInSaveGame.CreateFmt('Unexpected Map size in StringToMapArray: size = %d; but got at least = %d from string', [Size, I]);
  until S > Length(AString);
  if (I <> Size) then
    raise EInconsistentMapSizeInSaveGame.CreateFmt('Unexpected Map size in StringToMapArray: size = %d; map size in string = %d', [Size, I]);
end;

function VisibleArrayToString(const AVisible: TVisibleArray): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to Pred(Length(AVisible)) do
    Result += Char(AVisible[I]);
end;

function StringToVisibleArray(const AString: String; const Size: SizeInt): TVisibleArray;
var
  I: Integer;
begin
  if Length(AString) <> Size then
    raise EInconsistentMapSizeInSaveGame.CreateFmt('Unexpected Visible array size in StringToVisibleArray: size = %d; string length = %d', [Size, Length(AString)]);
  Result := nil;
  SetLength(Result, Size);
  for I := 0 to Pred(Size) do
    Result[I] := Ord(AString[I + 1]);
end;

end.

