{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameTilesetMap;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleGlImages;

const
  TextureSize = Integer(2048);
  RenderTileSize = Integer(11);
  TileSizeWithPadding = Integer(RenderTileSize + 2);
  TilesPerTexture = Trunc(Single(TextureSize) / Single(TileSizeWithPadding));
  HalfTilesPerTexture = Trunc(TilesPerTexture / 2.0);

  MaxWallHeight = 5;
  PredMaxWallHeight = Pred(MaxWallHeight);
  WallHeightTiles = Trunc(Single(HalfTilesPerTexture) / Single(MaxWallHeight)) - 1;

var
  TilesetMap: TDrawableImage;
  TilesetItems: TDrawableImage;

procedure PrepareTilesetMap;

implementation
uses
  SysUtils,
  CastleImages, CastleVectors, CastleRectangles,
  GameTileset, GameItemData, GameItemDatabase, GameMap, GameRandom;

procedure PrepareTilesetItems;
var
  MapImage: TRgbAlphaImage;
  ItemImageCopy: TRgbAlphaImage;
  ItemData: TItemData;
  X, Y: Integer;
begin
  FreeAndNil(TilesetItems);
  MapImage := TRgbAlphaImage.Create(TextureSize, TextureSize);
  MapImage.Clear(Vector4(0, 0, 0, 0));
  X := 0;
  Y := 0;
  for ItemData in ItemsDataList do // does not include bodyparts!
  begin
    ItemImageCopy := ItemData.MapImage.MakeCopy as TRgbAlphaImage;
    ItemImageCopy.Resize(RenderTileSize, RenderTileSize, riLanczos);
    MapImage.DrawFrom(ItemImageCopy, X * TileSizeWithPadding + 1, Y * TileSizeWithPadding + 1, dmBlendSmart);
    ItemData.MapImageRect := FloatRectangle(X * TileSizeWithPadding + 1, Y * TileSizeWithPadding + 1, RenderTileSize, RenderTileSize);
    ItemImageCopy.Free;
    Inc(X);
    if (X + 1) * TileSizeWithPadding > TextureSize then
    begin
      X := 0;
      Inc(Y);
      if (Y + 1) * TileSizeWithPadding > TextureSize then
        raise Exception.Create('Too many items');
    end;
  end;
  TilesetItems := TDrawableImage.Create(MapImage, true, true);
end;

procedure PrepareTilesetMap;
var
  Tile: TCastleImage;
  MapImage: TCastleImage;
  WallFade: Single;
  X: Integer;

  procedure DrawProcessed(const AX, AY: Integer; const Src: TCastleImage;
    const Lightness, Noisiness, Desaturate, Blur: Single);
  var
    ScaledImage: TRGBImage;
    ProcessedImage: TRgbImage;
    I: Integer;
    P: PVector3Byte;
    Noise: Single;
    AvgColor: Single;

    function Clamp255(const Value: Integer): Byte;
    begin
      if Value <= 255 then
        Exit(Value)
      else
        Exit(255);
    end;

  begin
    ScaledImage := TRGBImage.Create(Src.Width, Src.Height);
    ScaledImage.DrawFrom(Src, 0, 0, 0, 0, ScaledImage.Width, ScaledImage.Height, dmOverwrite); // drop bottom and right pixels, specific to RL Tiles
    if Blur > 0 then
    begin
      ScaledImage.Resize(Round((1 - Blur) * RenderTileSize), Round((1 - Blur) * RenderTileSize), riGaussian);
      ScaledImage.Resize(RenderTileSize, RenderTileSize, riGaussian);
    end else
      ScaledImage.Resize(RenderTileSize, RenderTileSize, riLanczos);
    ProcessedImage := TRGBImage.Create(TileSizeWithPadding, TileSizeWithPadding);
    ProcessedImage.DrawFrom(ScaledImage, 1, 1, 0, 0, RenderTileSize, RenderTileSize);
    FreeAndNil(ScaledImage);
    // Fix Texture Bleeding
    for I := 1 to Pred(Pred(TileSizeWithPadding)) do
    begin
      ProcessedImage.PixelPtr(0, I)^ := ProcessedImage.PixelPtr(1, I)^;
      ProcessedImage.PixelPtr(Pred(TileSizeWithPadding), I)^ := ProcessedImage.PixelPtr(Pred(TileSizeWithPadding) - 1, I)^;
      ProcessedImage.PixelPtr(I, 0)^ := ProcessedImage.PixelPtr(I, 1)^;
      ProcessedImage.PixelPtr(I, Pred(TileSizeWithPadding))^ := ProcessedImage.PixelPtr(I, Pred(TileSizeWithPadding) - 1)^;
    end;
    ProcessedImage.PixelPtr(0, 0)^ := ProcessedImage.PixelPtr(1, 1)^;
    ProcessedImage.PixelPtr(Pred(TileSizeWithPadding), 0)^ := ProcessedImage.PixelPtr(Pred(TileSizeWithPadding) - 1, 1)^;
    ProcessedImage.PixelPtr(0, Pred(TileSizeWithPadding))^ := ProcessedImage.PixelPtr(1, Pred(TileSizeWithPadding) - 1)^;
    ProcessedImage.PixelPtr(Pred(TileSizeWithPadding), Pred(TileSizeWithPadding))^ := ProcessedImage.PixelPtr(Pred(TileSizeWithPadding) - 1, Pred(TileSizeWithPadding) - 1)^;
    // Add noise, Fade
    P := ProcessedImage.Pixels;
    for I := 0 to Pred(ProcessedImage.Width * ProcessedImage.Height) do
    begin
      Noise := 1.00 - Noisiness * Rnd.Random;
      AvgColor := (Single(P^.X) + Single(P^.Y) + Single(P^.Z)) / 3.0;
      P^.X := Clamp255(Round(((1 - Desaturate) * P^.X + Desaturate * AvgColor) * Noise * Lightness));
      P^.Y := Clamp255(Round(((1 - Desaturate) * P^.Y + Desaturate * AvgColor) * Noise * Lightness));
      P^.Z := Clamp255(Round(((1 - Desaturate) * P^.Z + Desaturate * AvgColor) * Noise * Lightness));
      Inc(P);
    end;
    MapImage.DrawFrom(ProcessedImage, TileSizeWithPadding * AX, TileSizeWithPadding * AY,
          0, 0, TileSizeWithPadding, TileSizeWithPadding, dmOverwrite);
    FreeAndNil(ProcessedImage);
  end;

begin
  FreeAndNil(TilesetMap);
  MapImage := TRGBImage.Create(TextureSize, TextureSize);

  for X := 0 to Pred(HalfTilesPerTexture) do
  begin
    Tile := AllTilesets[Map.Tileset].Floor[Rnd.Random(AllTilesets[Map.Tileset].Floor.Count)];
    DrawProcessed(X, 0, Tile, AllTilesets[Map.Tileset].FloorLightness, 0.3, 0.0, AllTilesets[Map.Tileset].FloorBlur);
    DrawProcessed(X, 1, Tile, AllTilesets[Map.Tileset].FloorLightness - 0.1, 0.3, 0.7, AllTilesets[Map.Tileset].FloorBlur);
  end;

  WallFade := 0.0;
  for X := HalfTilesPerTexture to TilesPerTexture do
  begin
    if (X - HalfTilesPerTexture) mod WallHeightTiles = 0 then
      WallFade += 0.02;
    Tile := AllTilesets[Map.Tileset].Wall[Rnd.Random(AllTilesets[Map.Tileset].Wall.Count)];
    DrawProcessed(X, 0, Tile, AllTilesets[Map.Tileset].WallLightness - WallFade, 0.3, 0.0, AllTilesets[Map.Tileset].WallBlur);
    DrawProcessed(X, 1, Tile, AllTilesets[Map.Tileset].WallLightness - 0.1 * AllTilesets[Map.Tileset].WallLightness - WallFade, 0.3, 0.7, AllTilesets[Map.Tileset].WallBlur);
  end;

  TilesetMap := TDrawableImage.Create(MapImage, true, true);

  PrepareTilesetItems;
end;

finalization
  TilesetMap.Free;
  TilesetItems.Free;
end.

