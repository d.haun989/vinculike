{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSounds;

{$INCLUDE compilerconfig.inc}

interface

const
  DefaultVolume = 0.5;

procedure Sound(const SoundName: String);
function SoundWithDuration(const SoundName: String): Single;
procedure Music(const MusicName: String);

procedure InitializeSounds;

implementation
uses
  SysUtils, Classes, Generics.Collections,
  CastleDownload, DOM, CastleXmlUtils, XmlRead,
  CastleSoundEngine, CastleConfig,
  GameRandom, GameLog;

type
  ESoundLoadError = class(Exception);

type
  TLastSoundsDictionary = specialize TDictionary<String, String>;
  TSoundsDictionary = specialize TObjectDictionary<String, TStringList>;

var
  LastSoundsDictionary: TLastSoundsDictionary;
  SoundsDictionary: TSoundsDictionary;

function GetSoundByName(const SoundName: String): TCastleSound; inline;
var
  S: String;
begin
  if SoundsDictionary.ContainsKey(SoundName) then
  begin
    repeat
      S := SoundsDictionary[SoundName][Rnd.Random(SoundsDictionary[SoundName].Count)];
    until (S <> LastSoundsDictionary[SoundName]) or (SoundsDictionary[SoundName].Count = 1);
    LastSoundsDictionary[SoundName] := S;
    Exit(SoundEngine.SoundFromName(S))
  end
  else
    Exit(SoundEngine.SoundFromName(SoundName));
end;

procedure Sound(const SoundName: String);
begin
  SoundEngine.Play(GetSoundByName(SoundName));
end;

function SoundWithDuration(const SoundName: String): Single;
var
  TheSound: TCastleSound;
begin
  TheSound := GetSoundByName(SoundName);
  Result := TheSound.Duration;
  SoundEngine.Play(TheSound);
end;

procedure Music(const MusicName: String);
begin
  SoundEngine.LoopingChannel[0].Sound := GetSoundByName(MusicName);
end;

procedure InitializeSounds;
var
  Stream: TStream;
  SoundDoc: TXMLDocument;
  SName: String;
  I: TXMLElementIterator;
begin
  SoundEngine.RepositoryURL := 'castle-data:/audio/index.xml';

  SoundsDictionary := TSoundsDictionary.Create([doOwnsValues]);

  { WORKAROUND Alias no longer randomizing sounds and SoundEngine.FSound private }
  Stream := Download(SoundEngine.RepositoryURL);
  try
    ReadXMLFile(SoundDoc, Stream);
  finally
    FreeAndNil(Stream);
  end;
  I := SoundDoc.DocumentElement.ChildrenIterator;
  try
    while I.GetNext do
      if I.Current.TagName = 'sound' then
      begin
        SName := I.Current.AttributeString('name');
        if (SName <> LowerCase(SName)) then
          raise ESoundLoadError.CreateFmt('Sound name "%s" is not lowercase', [SName]);

        if Pos('#', SName) > 0 then
        begin
          if not SoundsDictionary.ContainsKey(Copy(SName, 0, Pos('#', SName) - 1)) then
            SoundsDictionary.Add(Copy(SName, 0, Pos('#', SName) - 1), TStringList.Create);
          SoundsDictionary[Copy(SName, 0, Pos('#', SName) - 1)].Add(SName);
        end;
      end
      else
        {$push}
        {$warn 4105 off}
        DebugWarning('Unexpected Sound tag : %s', [I.Current.TagName]);
        {$pop}
  finally
    FreeAndNil(I);
  end;
  FreeAndNil(SoundDoc);
  { /WORKAROUND }

  LastSoundsDictionary := TLastSoundsDictionary.Create;
  for SName in SoundsDictionary.Keys do
    LastSoundsDictionary.Add(SName, '');
end;

finalization
  SoundsDictionary.Free;
  LastSoundsDictionary.Free;
end.

