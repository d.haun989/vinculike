{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GamePauseUi;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleControls, CastleKeysMouse, CastleTimeUtils;

type
  TPauseUi = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ParentDesign: TCastleDesign;
    DebugModeClickCount: Integer;
    DebugModeClickTimer: TTimerResult;
    procedure ClickCharacter(Sender: TObject);
    procedure ClickDebugMode(Sender: TObject);
    procedure ClickExitToMenu(Sender: TObject);
    procedure ClickVinculopedia(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickParty(Sender: TObject);
    procedure ClickPauseMenu(Sender: TObject);
    procedure ClickResume(Sender: TObject);
    procedure FillCharacters;
    procedure SetComponentsActive;
  public
    PartyActive: Boolean;
    PauseMenuActive: Boolean;
    function Exists: Boolean;
  public
    procedure Show;
    procedure Hide;
    procedure Parse(const AParentDesign: TCastleDesign);
  end;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleUiState,
  CastleWindow, CastleConfig, CastleApplicationProperties, CastleUiControls,
  GameSerializableObject,
  GameFonts, GameSounds, GameSaveGame, GamePlayerCharacter, GameMap, GameLog, GameColors,
  GameViewOptions, GameViewGame, GameViewMainMenu, GameViewVinculopedia, GameUiUtils;

procedure TPauseUi.Parse(const AParentDesign: TCastleDesign);
begin
  PartyActive := true;
  PauseMenuActive := true;

  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;
  (ParentDesign.DesignedComponent('ButtonResume') as TCastleButton).OnClick := @ClickResume;
  (ParentDesign.DesignedComponent('ButtonResume') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonOptions') as TCastleButton).OnClick := @ClickOptions;
  (ParentDesign.DesignedComponent('ButtonOptions') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonVinculopedia') as TCastleButton).OnClick := @ClickVinculopedia;
  (ParentDesign.DesignedComponent('ButtonVinculopedia') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonMenu') as TCastleButton).OnClick := @ClickExitToMenu;
  (ParentDesign.DesignedComponent('ButtonMenu') as TCastleButton).CustomFont := FontSoniano90;

  (ParentDesign.DesignedComponent('ButtonParty') as TCastleButton).OnClick := @ClickParty;
  (ParentDesign.DesignedComponent('ButtonParty') as TCastleButton).CustomFont := FontSoniano90;
  (ParentDesign.DesignedComponent('ButtonPauseMenu') as TCastleButton).OnClick := @ClickPauseMenu;
  (ParentDesign.DesignedComponent('ButtonPauseMenu') as TCastleButton).CustomFont := FontSoniano90;

  (ParentDesign.DesignedComponent('LabelPause') as TCastleLabel).CustomFont := FontBender90;

  (ParentDesign.DesignedComponent('ButtonDebugMode') as TCastleButton).OnClick := @ClickDebugMode;

  Hide;
end;

procedure TPauseUi.ClickCharacter(Sender: TObject);
begin
  Sound('menu_button');
  ViewGame.CurrentCharacter := ObjectByReferenceId((Sender as TCastleButton).Tag) as TPlayerCharacter;
  ViewGame.InvalidateInventory(ViewGame.CurrentCharacter);
  ViewGame.InvalidatePosition(ViewGame.CurrentCharacter);
  ShowLog('SELECTED CHARACTER: %s', [ViewGame.CurrentCharacter.Data.DisplayName], ColorLogLevelUp);
  ShowLog('%s current level: %d', [ViewGame.CurrentCharacter.Data.DisplayName, ViewGame.CurrentCharacter.Experience.Level], ColorLogExperience);
  ShowLog('Experience: %d; To next level: %d', [Round(ViewGame.CurrentCharacter.Experience.Xp), Round(ViewGame.CurrentCharacter.Experience.ExperienceToNextLevel)], ColorLogExperience);
  ////////////////////////////////TUIState.Pop(Self);
  ViewGame.UnpauseGame;
end;

procedure TPauseUi.ClickDebugMode(Sender: TObject);
const
  EnterDebugTime = 4;
begin
  if DebugModeClickTimer.ElapsedTime < EnterDebugTime then
  begin
    Inc(DebugModeClickCount);
    if DebugModeClickCount >= 5 then
    begin
      ViewGame.DebugUi.Toggle;
      DebugModeClickTimer := Timer;
      DebugModeClickCount := 0;
    end;
  end else
  begin
    DebugModeClickTimer := Timer;
    DebugModeClickCount := 0;
  end;
end;

procedure TPauseUi.ClickExitToMenu(Sender: TObject);
begin
  Sound('menu_button');
  SaveGame;
  ParentDesign.Container.View := ViewMainMenu;
end;

procedure TPauseUi.ClickVinculopedia(Sender: TObject);
begin
  Sound('menu_button');
  ParentDesign.Container.PushView(ViewVinculopedia);
end;

procedure TPauseUi.ClickOptions(Sender: TObject);
begin
  Sound('menu_button');
  ParentDesign.Container.PushView(ViewOptions);
end;

procedure TPauseUi.ClickParty(Sender: TObject);
begin
  PartyActive := not PartyActive;
  if PartyActive then
    Sound('menu_button')
  else
    Sound('menu_back');
  SetComponentsActive;
end;

procedure TPauseUi.ClickPauseMenu(Sender: TObject);
begin
  PauseMenuActive := not PauseMenuActive;
  if PauseMenuActive then
    Sound('menu_button')
  else
    Sound('menu_back');
  SetComponentsActive;
end;

procedure TPauseUi.ClickResume(Sender: TObject);
begin
  Sound('menu_back');
  ViewGame.UnpauseGame;
end;

procedure TPauseUi.FillCharacters;
var
  VerticalGroupCharacters: TCastleVerticalGroup;
  PlayerCharacter: TPlayerCharacter;
  Button: TCastleButton;
begin
  VerticalGroupCharacters := ParentDesign.DesignedComponent('VerticalGroupCharacters') as TCastleVerticalGroup;
  VerticalGroupCharacters.ClearAndFreeControls;
  for PlayerCharacter in Map.PlayerCharactersList do
    begin
      Button := TCastleButton.Create(VerticalGroupCharacters);
      Button.CustomBackground := true;
      Button.CustomBackgroundNormal.ProtectedSides.AllSides := 15;
      Button.Width := 800;
      Button.Height := 100;
      Button.AutoSize := false;
      Button.AutoSizeWidth := false;
      Button.AutoSizeHeight := false;
      Button.Html := true;
      Button.CustomTextColorUse := true;
      Button.EnableParentDragging := true;
      if not PlayerCharacter.IsCaptured and (PlayerCharacter.AtMap = Map.CurrentDepth) then
      begin
        Button.CustomBackgroundNormal.URL := 'castle-data:/ui/kodiakgraphics/inventory_button_dark.png';
        Button.CustomTextColor := ColorUiCharacterReady;
        if PlayerCharacter = ViewGame.CurrentCharacter then
          Button.Caption := Format('%s (lvl.%d) - ACTIVE (depth %d)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap])
        else
        if PlayerCharacter.AtMap = 0 then
          Button.Caption := Format('%s (lvl.%d) - READY (in settlement)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap])
        else
          Button.Caption := Format('%s (lvl.%d) - READY (depth %d)', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap]);
        Button.Tag := PlayerCharacter.ReferenceId;
        Button.OnClick := @ClickCharacter;
      end else
      if PlayerCharacter.IsCaptured then
      begin
        Button.CustomBackgroundNormal.URL := 'castle-data:/ui/kodiakgraphics/inventory_button_light.png';
        Button.CustomTextColor := ColorUiCharacterCaptured;
        if PlayerCharacter.AtMap = Map.CurrentDepth then
          Button.Caption := Format('%s (lvl.%d) captured at this level', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level])
        else
          Button.Caption := Format('%s (lvl.%d) captured at level %d', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap]);
      end else
      begin
        Button.CustomBackgroundNormal.URL := 'castle-data:/ui/kodiakgraphics/inventory_button_light.png';
        if PlayerCharacter.AtMap = 0 then
        begin
          Button.CustomTextColor := ColorUiCharacterAway;
          Button.Caption := Format('%s (lvl.%d) in the settlement', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level]);
        end else
        begin
          Button.Caption := Format('%s (lvl.%d) away at lvl %d', [PlayerCharacter.Data.DisplayName, PlayerCharacter.Experience.Level, PlayerCharacter.AtMap]);
          Button.CustomTextColor := ColorError; // it's an error for now!
        end;
      end;
      //
      VerticalGroupCharacters.InsertFront(Button);
    end;
end;

procedure TPauseUi.SetComponentsActive;
begin
  (ParentDesign.DesignedComponent('ScrollViewCharacters') as TCastleUserInterface).Exists := PartyActive;
  (ParentDesign.DesignedComponent('VerticalGroupPause') as TCastleUserInterface).Exists := PauseMenuActive;
  (ParentDesign.DesignedComponent('Shade') as TCastleUserInterface).Exists := PartyActive or PauseMenuActive;
end;

function TPauseUi.Exists: Boolean;
begin
  Exit(ParentDesign.Exists);
end;

procedure TPauseUi.Show;
begin
  DebugModeClickTimer := Timer;
  DebugModeClickCount := 0;

  FillCharacters;
  SetComponentsActive;
  ParentDesign.Exists := true;
end;

procedure TPauseUi.Hide;
begin
  ParentDesign.Exists := false;
end;

end.

