{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameDebugUi;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleControls;

type
  TDebugUi = class(TComponent) // CastleClassUtils -> TCastleComponent
  strict private
    ParentDesign: TCastleDesign;
    procedure ClickAddNewbie(Sender: TObject);
    procedure ClickCaptureHealth(Sender: TObject);
    procedure ClickCaptureMental(Sender: TObject);
    procedure ClickCaptureTied(Sender: TObject);
    procedure ClickDropAllItems(Sender: TObject);
    procedure ClickHit10(Sender: TObject);
    procedure ClickHit100(Sender: TObject);
    procedure ClickDrainVigor100(Sender: TObject);
    procedure ClickKillAllMonsters(Sender: TObject);
    procedure ClickLevelUp(Sender: TObject);
    procedure ClickAddRandomItem(Sender: TObject);
    procedure ClickClose(Sender: TObject);
    procedure ClickLogSaveGame(Sender: TObject);
    procedure ClickNextMap(Sender: TObject);
    procedure ClickRandomBody(Sender: TObject);
    procedure ClickResetCharacter(Sender: TObject);
    procedure ClickRestartThisMap(Sender: TObject);
    procedure ClickReturnToSettlement(Sender: TObject);
    procedure ClickShowMap(Sender: TObject);
    procedure ClickSpawnPatrol(Sender: TObject);
    procedure ClickTeleport(Sender: TObject);
    procedure ClickToggleFPS(Sender: TObject);
    procedure ClickVisualizeColliders(Sender: TObject);
    procedure ClickVisualizeExtrema(Sender: TObject);
    procedure ClickVisualizeMonsterOrigins(Sender: TObject);
    procedure ClickVisualizePlayerOrigins(Sender: TObject);
  public
    procedure Toggle;
    procedure Parse(const AParentDesign: TCastleDesign);
  end;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleLog,
  GameMap, GameMapTypes, GameRandom, GameItemDatabase, GameInventoryItem, GameBodyPart,
  GamePlayerCharacter,
  GameViewGame, GameViewEndGame, GameSaveGame, GameMonster, GameApparelSlots;

procedure TDebugUi.ClickLevelUp(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Experience.AddExperience(ViewGame.CurrentCharacter.Experience.ExperienceToNextLevel + 1);
end;

procedure TDebugUi.ClickCaptureHealth(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.Vital := -1;
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egHealth;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickAddNewbie(Sender: TObject);
var
  Newbie: TPlayerCharacter;
begin
  Newbie := ViewGame.NewPlayerCharacter;
  Newbie.Teleport(Map.EntranceX, Map.EntranceY); // it will reset the character to idle + will teleport to entrance (but character will remain at map 0)
  Map.CacheCharactersOnThisLevel;
  ViewGame.InvalidateInventory(nil);
  ViewGame.InvalidatePosition(nil);
end;

procedure TDebugUi.ClickCaptureMental(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.Psyche := -1;
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egMental;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickCaptureTied(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset; // heal completely
  ViewGame.CurrentCharacter.GetCapturedSafe;
  ViewEndGame.EndGameKind := egTiedUp;
  ViewGame.ScheduleEndGame;
end;

procedure TDebugUi.ClickDropAllItems(Sender: TObject);
var
  E: TApparelSlot;
begin
  for E in ViewGame.CurrentCharacter.Inventory.EquipmentSlots do
    ViewGame.CurrentCharacter.Inventory.UnequipAndDrop(E, false);
end;

procedure TDebugUi.ClickHit10(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Hit(10);
end;

procedure TDebugUi.ClickHit100(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Hit(100);
end;

procedure TDebugUi.ClickDrainVigor100(Sender: TObject);
begin
  ViewGame.CurrentCharacter.HitVigor(100);
end;

procedure TDebugUi.ClickKillAllMonsters(Sender: TObject);
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct then
      M.Hit(10000);
end;

procedure TDebugUi.ClickAddRandomItem(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataList[Rnd.Random(ItemsDataList.Count)]));
end;

procedure TDebugUi.ClickClose(Sender: TObject);
begin
  ParentDesign.Exists := false;
end;

procedure TDebugUi.ClickLogSaveGame(Sender: TObject);
begin
  WriteLnLog('================================= SAVE GAME START =================================');
  WriteLnLog(SaveAsString);
  WriteLnLog('================================== SAVE GAME END ==================================');
end;

procedure TDebugUi.ClickNextMap(Sender: TObject);
begin
  ViewGame.NewMap;
end;

procedure TDebugUi.ClickRestartThisMap(Sender: TObject);
begin
  Map.CurrentDepth := Map.CurrentDepth - 1; // CurrentDepth is always at least 0
  ViewGame.NewMap;
end;

procedure TDebugUi.ClickReturnToSettlement(Sender: TObject);
begin
  Map.CurrentDepth := -1;
  ViewGame.NewMap;
end;

procedure TDebugUi.ClickRandomBody(Sender: TObject);
var
  E: TApparelSlot;
begin
  for E in BodySlots do
  begin
    ViewGame.CurrentCharacter.Inventory.DestroyEquippedItemSafe(E);
    ViewGame.CurrentCharacter.Inventory.EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(E)));
  end;
end;

procedure TDebugUi.ClickResetCharacter(Sender: TObject);
begin
  ViewGame.CurrentCharacter.Reset;
end;

procedure TDebugUi.ClickShowMap(Sender: TObject);
begin
  Map.ShowAllMap;
end;

procedure TDebugUi.ClickSpawnPatrol(Sender: TObject);
begin
  Map.EnemySpawnTime := 0;
end;

procedure TDebugUi.ClickTeleport(Sender: TObject);
begin
  ViewGame.CurrentCharacter.TeleportToUnknown(0.5);
  ViewGame.ScheduleMonstersToIdle;  //Map.MonstersToIdle; // we can't do that here, it'll free the parent action and everything goes BOOM
end;

procedure TDebugUi.ClickToggleFPS(Sender: TObject);
begin
  ViewGame.LabelFps.Exists := not ViewGame.LabelFps.Exists;
end;

procedure TDebugUi.ClickVisualizeColliders(Sender: TObject);
begin
  if ViewGame.InternalMapInterface.DebugNavgrid < 0 then
    ViewGame.InternalMapInterface.DebugNavgrid := PredMaxColliderSize
  else
    Dec(ViewGame.InternalMapInterface.DebugNavgrid);
  if ViewGame.InternalMapInterface.DebugNavgrid >= 0 then
    (ParentDesign.DesignedComponent('ButtonVisualizeColliders') as TCastleButton).Caption := 'Collider: ' + IntToStr(ViewGame.InternalMapInterface.DebugNavgrid + 1)
  else
    (ParentDesign.DesignedComponent('ButtonVisualizeColliders') as TCastleButton).Caption := 'Collider: OFF';
end;

procedure TDebugUi.ClickVisualizeExtrema(Sender: TObject);
begin
  ViewGame.InternalMapInterface.DebugExtrema := not ViewGame.InternalMapInterface.DebugExtrema;
end;

procedure TDebugUi.ClickVisualizeMonsterOrigins(Sender: TObject);
begin
  ViewGame.InternalMapInterface.DebugMonsterOrigins := not ViewGame.InternalMapInterface.DebugMonsterOrigins;
end;

procedure TDebugUi.ClickVisualizePlayerOrigins(Sender: TObject);
begin
  ViewGame.InternalMapInterface.DebugPlayerOrigins := not ViewGame.InternalMapInterface.DebugPlayerOrigins;
end;

procedure TDebugUi.Toggle;
begin
  if ParentDesign.Exists then
    ClickClose(nil)
  else
    ParentDesign.Exists := true;
end;

procedure TDebugUi.Parse(const AParentDesign: TCastleDesign);
begin
  ParentDesign := AParentDesign;
  ParentDesign.Exists := false;
  (ParentDesign.DesignedComponent('ButtonClose') as TCastleButton).OnClick := @ClickClose;
  (ParentDesign.DesignedComponent('ButtonToggleFPS') as TCastleButton).OnClick := @ClickToggleFPS;
  (ParentDesign.DesignedComponent('ButtonShowMap') as TCastleButton).OnClick := @ClickShowMap;
  (ParentDesign.DesignedComponent('ButtonSpawnPatrol') as TCastleButton).OnClick := @ClickSpawnPatrol;
  (ParentDesign.DesignedComponent('ButtonKillAllMonsters') as TCastleButton).OnClick := @ClickKillAllMonsters;
  (ParentDesign.DesignedComponent('ButtonNextMap') as TCastleButton).OnClick := @ClickNextMap;
  (ParentDesign.DesignedComponent('ButtonRestartThisMap') as TCastleButton).OnClick := @ClickRestartThisMap;
  (ParentDesign.DesignedComponent('ButtonReturnToSettlement') as TCastleButton).OnClick := @ClickReturnToSettlement;
  (ParentDesign.DesignedComponent('ButtonResetCharacter') as TCastleButton).OnClick := @ClickResetCharacter;
  (ParentDesign.DesignedComponent('ButtonHitPlayer10') as TCastleButton).OnClick := @ClickHit10;
  (ParentDesign.DesignedComponent('ButtonHitPlayer100') as TCastleButton).OnClick := @ClickHit100;
  (ParentDesign.DesignedComponent('ButtonDrainVigor100') as TCastleButton).OnClick := @ClickDrainVigor100;
  (ParentDesign.DesignedComponent('ButtonAddRandomItem') as TCastleButton).OnClick := @ClickAddRandomItem;
  (ParentDesign.DesignedComponent('ButtonDropAllItems') as TCastleButton).OnClick := @ClickDropAllItems;
  (ParentDesign.DesignedComponent('ButtonTeleport') as TCastleButton).OnClick := @ClickTeleport;
  (ParentDesign.DesignedComponent('ButtonLevelUp') as TCastleButton).OnClick := @ClickLevelUp;
  (ParentDesign.DesignedComponent('ButtonLogSaveGame') as TCastleButton).OnClick := @ClickLogSaveGame;
  (ParentDesign.DesignedComponent('ButtonRandomBody') as TCastleButton).OnClick := @ClickRandomBody;

  (ParentDesign.DesignedComponent('ButtonVisualizeExtrema') as TCastleButton).OnClick := @ClickVisualizeExtrema;
  (ParentDesign.DesignedComponent('ButtonVisualizeColliders') as TCastleButton).OnClick := @ClickVisualizeColliders;
  (ParentDesign.DesignedComponent('ButtonVisualizePlayerOrigins') as TCastleButton).OnClick := @ClickVisualizePlayerOrigins;
  (ParentDesign.DesignedComponent('ButtonVisualizeMonsterOrigins') as TCastleButton).OnClick := @ClickVisualizeMonsterOrigins;

  (ParentDesign.DesignedComponent('ButtonAddNewbie') as TCastleButton).OnClick := @ClickAddNewbie;
  (ParentDesign.DesignedComponent('ButtonCaptureHealth') as TCastleButton).OnClick := @ClickCaptureHealth;
  (ParentDesign.DesignedComponent('ButtonCaptureMental') as TCastleButton).OnClick := @ClickCaptureMental;
  (ParentDesign.DesignedComponent('ButtonCaptureTied') as TCastleButton).OnClick := @ClickCaptureTied;
end;

end.

