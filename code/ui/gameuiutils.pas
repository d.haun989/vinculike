{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameUiUtils;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleUiControls;

type
  TCastleUserInterfaceHelper = class helper for TCastleUserInterface
  public
    procedure ClearAndFreeControls;
  end;

implementation

procedure TCastleUserInterfaceHelper.ClearAndFreeControls;
var
  C: TCastleUserInterface;
begin
  while ControlsCount > 0 do
  begin
    C := Controls[0];
    RemoveControl(C);
    C.Free;
  end;
end;

end.

