{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameLog;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleColors;

procedure ShowLog(const AMessage: String; const Args: array of Const; const AColor: TCastleColor);
procedure ShowError(const AMessage: String; const Args: array of Const);
procedure DebugLog(const AMessage: String; const Args: array of Const);
procedure DebugWarning(const AMessage: String; const Args: array of Const);
implementation
uses
  CastleLog,
  GameViewGame;

procedure ShowLog(const AMessage: String; const Args: array of const;
  const AColor: TCastleColor);
begin
  WriteLnLog('>' + AMessage, ARgs);
  if ViewGame.Active then
    ViewGame.Log(AMessage, Args, AColor);
end;

procedure ShowError(const AMessage: String; const Args: array of const);
begin
  ShowLog(AMessage, Args, CastleColors.Red);
end;

procedure DebugLog(const AMessage: String; const Args: array of const);
begin
  WriteLnLog(AMessage, Args);
end;

procedure DebugWarning(const AMessage: String; const Args: array of const);
begin
  WriteLnWarning(AMessage, Args);
end;

end.

