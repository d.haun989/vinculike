{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit CastleScrollViewBottom;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleControls;

type
  { A TCastleScrollView, but anchored to bottom, not top.
    Note, you must call DoScrollChange manually after contents have been changed }
  TCastleScrollViewBottom = class(TCastleScrollView)
  public
    procedure DoScrollChange; override;
  end;

implementation
uses
  CastleComponentSerialize, CastleRectangles;

procedure TCastleScrollViewBottom.DoScrollChange;
begin
  inherited DoScrollChange;
  ScrollArea.Anchor(vpBottom, ScrollMin - ScrollMax + Scroll); // FScrollArea is private, we can't access it here
end;

initialization
  RegisterSerializableComponent(TCastleScrollViewBottom, 'Scroll View Bottom');
end.

