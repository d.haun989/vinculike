{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameCachedImages;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleGlImages, CastleImages,
  GameApparelSlots;

var
  TempPlayerBinds: TDrawableImage;
  TempExit: TDrawableImage;
  TempEcho: TDrawableImage;
  DebugColliderImage: TDrawableImage;
  DebugPlayerOriginImage: TDrawableImage;
  DebugMonsterOriginImage: TDrawableImage;
  DebugExtremaImage: TDrawableImage;
  StealthMark: TDrawableImage;
  SlotImage: array [TApparelSlot] of TDrawableImage; // temporary!

procedure LoadImages;

function LoadRgba(const Url: String): TRgbAlphaImage;
function LoadRgb(const Url: String): TRgbImage;
function LoadDrawable(const Url: String): TDrawableImage;

implementation
uses
  Generics.Collections, Generics.Defaults,
  CastleVectors;

type
  TRgbaImagesCache = specialize TObjectDictionary<String, TRgbAlphaImage>;
  TRgbImagesCache = specialize TObjectDictionary<String, TRgbImage>;
  TDrawableImagesCache = specialize TObjectDictionary<String, TDrawableImage>;
var
  FRgbaImagesCache: TRgbaImagesCache;
  FRgbImagesCache: TRgbImagesCache;
  FDrawableImagesCache: TDrawableImagesCache;

procedure LoadImages;
var
  E: TApparelSlot;
begin
  FRgbaImagesCache := TRgbaImagesCache.Create([doOwnsValues]);
  FRgbImagesCache := TRgbImagesCache.Create([doOwnsValues]);
  FDrawableImagesCache := TDrawableImagesCache.Create([doOwnsValues]);

  TempPlayerBinds := LoadDrawable('castle-data:/marks/light-thorny-triskelion_CC-BY_by_Lorc.png');
  StealthMark := LoadDrawable('castle-data:/map/stealth_mark.png');
  StealthMark.Color := Vector4(1, 1, 1, 0.03);
  TempExit := LoadDrawable('castle-data:/map/enter.png');
  TempEcho := LoadDrawable('castle-data:/monsters/images/question.png');

  DebugColliderImage := LoadDrawable('castle-data:/map/debug/debugcollider.png');
  DebugPlayerOriginImage := LoadDrawable('castle-data:/map/debug/debugplayerorigin.png');
  DebugMonsterOriginImage := LoadDrawable('castle-data:/map/debug/debugmonsterorigin.png');
  DebugExtremaImage := LoadDrawable('castle-data:/map/debug/debugextrema.png');

  SlotImage[esWeapon] := LoadDrawable('castle-data:/map/item_icons/weapon.png');
  SlotImage[esBottomOver] := LoadDrawable('castle-data:/map/item_icons/bottom-over.png');
  SlotImage[esBottomUnder] := LoadDrawable('castle-data:/map/item_icons/bottom-under.png');
  SlotImage[esTopOver] := LoadDrawable('castle-data:/map/item_icons/top-over.png');
  SlotImage[esTopOverOver] := LoadDrawable('castle-data:/map/item_icons/top-overover.png');
  SlotImage[esTopUnder] := LoadDrawable('castle-data:/map/item_icons/top-under.png');
  SlotImage[esAmulet] := LoadDrawable('castle-data:/map/item_icons/amulet.png');
  SlotImage[esNeck] := LoadDrawable('castle-data:/map/item_icons/collar.png');
  SlotImage[esFeet] := LoadDrawable('castle-data:/map/item_icons/feet.png');
end;

function LoadRgba(const Url: String): TRgbAlphaImage;
begin
  if not FRgbaImagesCache.ContainsKey(Url) then
    FRgbaImagesCache.Add(Url, LoadImage(Url, [TRgbAlphaImage]) as TRgbAlphaImage);
  Exit(FRgbaImagesCache[Url]);
end;

function LoadRgb(const Url: String): TRgbImage;
begin
  if not FRgbImagesCache.ContainsKey(Url) then
    FRgbImagesCache.Add(Url, LoadImage(Url, [TRgbImage]) as TRgbImage);
  Exit(FRgbImagesCache[Url]);
end;

function LoadDrawable(const Url: String): TDrawableImage;
begin
  if not FDrawableImagesCache.ContainsKey(Url) then
    FDrawableImagesCache.Add(Url, TDrawableImage.Create(Url, true));
  Exit(FDrawableImagesCache[Url]);
end;

finalization
  FRgbaImagesCache.Free;
  FRgbImagesCache.Free;
  FDrawableImagesCache.Free;
end.

