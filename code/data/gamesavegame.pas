{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSaveGame;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM;

const
  SaveGameVersion = Integer(3);

procedure SaveGame;
procedure LoadGame;
function SaveGameExists: Boolean;
function SaveGameCompatible: Boolean;
procedure DeleteSaveGame;
function SecondsSinceLastSave: Single;
function SaveAsString: String;

implementation
uses
  SysUtils,
  CastleConfig, CastleXmlUtils, CastleTimeUtils,
  GameSerializableObject,
  GameMap, GamePlayerCharacter, GameLog, GameColors,
  GameViewGame;

type
  ELoadGameFailed = class(Exception);

var
  LastSaveTimer: TTimerResult;

procedure SaveGame;
var
  Element: TDOMElement;
  T: TTimerResult;
begin
  DebugLog('Saving the game started', []);
  if Map = nil then
  begin
    DebugWarning('Cannot save the game, it is not started', []);
    Exit;
  end;
  T := Timer;
  UserConfig.DeletePath('save_game');
  Element := UserConfig.MakePathElement('save_game');

  Element.AttributeSet('version', SaveGameVersion);
  Element.AttributeSet('GlobalId', GlobalId);
  Element.AttributeSet('CurrentCharacter', ViewGame.CurrentCharacter.ReferenceId);
  try
    Map.Save(Element);
  except
    on E: Exception do
    begin
      DebugWarning('EXCEPTION %s : %s', [E.ClassName, E.Message]);
      DebugWarning('THERE WAS AN UNHANDLED EXCEPTION SAVING THE MAP.', []);
      DebugWarning('NOTHING CAN BE DONE HERE, WE JUST CRASH TO PREVENT FROM OVERWRITING THE SAVE WITH FAULTY DATA.', []);
      raise;
    end;
  end;

  UserConfig.MarkModified;
  UserConfig.Save;
  DebugLog('Game saved in %.1n s.', [T.ElapsedTime]);
  LastSaveTimer := Timer;
end;

procedure LoadGame;
var
  Element: TDOMElement;
  I: Integer;
begin
  Element := UserConfig.PathElement('save_game', true);

  if not SaveGameCompatible then
    raise ELoadGameFailed.CreateFmt('Cannot load incompatible save game version: %d. Expected: %d', [Element.AttributeIntegerDef('version', 0), SaveGameVersion]);

  GlobalId := Element.AttributeQWord('GlobalId');
  Map.Load(Element.ChildElement(TMap.Signature, true));
  try
    ViewGame.CurrentCharacter := ObjectByReferenceId(Element.AttributeQWord('CurrentCharacter')) as TPlayerCharacter;
  except
    ShowError('ERROR: Cannot read current character.', []);
    ShowError('This will set all characters that are not in settlement as "captured".', []);
    ShowError('We''ll start a new run and hope for the best...', []);
    for I := 0 to Pred(Map.PlayerCharactersList.Count) do
      if not Map.PlayerCharactersList[I].IsCaptured and (Map.PlayerCharactersList[I].AtMap <> 0) then
        Map.PlayerCharactersList[I].GetCapturedSafe;
    ViewGame.StartNewRun;
    //raise ELoadGameFailed.Create('Error while loading CurrentCharacter');
  end;
  LastSaveTimer := Timer;
end;

function SaveGameExists: Boolean;
var
  Element: TDOMElement;
begin
  Element := UserConfig.PathElement('save_game', false);
  Result := (Element <> nil) and (Element.FirstChild <> nil) and SaveGameCompatible;
end;

function SaveGameCompatible: Boolean;
var
  Element: TDOMElement;
begin
  Element := UserConfig.PathElement('save_game', false);
  if Element <> nil then
    Result := Element.AttributeIntegerDef('version', 0) = SaveGameVersion
  else
    Result := true;
end;

procedure DeleteSaveGame;
begin
  UserConfig.DeletePath('save_game');
  UserConfig.Save;
end;

function SecondsSinceLastSave: Single;
begin
  Result := LastSaveTimer.ElapsedTime;
end;

function SaveAsString: String;
begin
  Exit(UserConfig.SaveToString);
end;

end.

