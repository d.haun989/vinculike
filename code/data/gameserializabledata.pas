{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSerializableData;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM,
  GameValidated;

type
  EDataValidationError = class(Exception);
  EClassDeserealizationError = class(Exception);

type
  TSerializableData = class abstract(TObject, IValidated)
  strict protected
    procedure Validate; virtual; abstract;
    procedure Read(const Element: TDOMElement); virtual; abstract;
  public
    class function ReadClass(const Element: TDOMElement): TSerializableData;
    // WARNING: Constructor is not virtual; Children should not create constructors
  end;
  TSerializableDataClass = class of TSerializableData;

procedure RegisterSerializableData(const AClass: TSerializableDataClass);
function SerializableDataByName(const AClassName: String): TSerializableDataClass;
implementation
uses
  Generics.Collections,
  CastleXmlUtils;

type
  TSerializableDataClassDictionary = specialize TDictionary<String, TSerializableDataClass>;

var
  SerializableDataClassDictionary: TSerializableDataClassDictionary;

procedure RegisterSerializableData(const AClass: TSerializableDataClass);
begin
  if SerializableDataClassDictionary = nil then
    SerializableDataClassDictionary := TSerializableDataClassDictionary.Create;
  SerializableDataClassDictionary.Add(AClass.ClassName, AClass);
end;

function SerializableDataByName(const AClassName: String): TSerializableDataClass;
begin
  Exit(SerializableDataClassDictionary[AClassName]);
end;

class function TSerializableData.ReadClass(const Element: TDOMElement): TSerializableData;
var
  ClassString: String;
begin
  if Element = nil then
    Exit(nil);
  try
    ClassString := Element.AttributeString('Class');
    if SerializableDataClassDictionary.ContainsKey(ClassString) then
      Result := SerializableDataClassDictionary[ClassString].Create
    else
      raise Exception.CreateFmt('Could not find class name in serializable data: %s', [ClassString]);
    Result.Read(Element);
    {$IFDEF ValidateData}
    Result.Validate;
    {$ENDIF}
  except
    FreeAndNil(Result);
    raise;
  end;
end;

finalization
  SerializableDataClassDictionary.Free;
end.

