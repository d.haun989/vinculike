{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSimpleSerializableObject;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM;

type
  ESimpleClassDeserealizationError = class(Exception);

type
  TSimpleSerializableObject = class abstract(TObject)
  public
    class function LoadClass(const Element: TDOMElement): TSimpleSerializableObject;
    procedure Save(const Element: TDOMElement); virtual;
  strict protected
    procedure Load(const Element: TDOMElement); virtual;
  public
    constructor Create; virtual;
  end;
  TSimpleSerializableClass = class of TSimpleSerializableObject;

procedure RegisterSimpleSerializableObject(const AClass: TSimpleSerializableClass);
function SimpleSerializableClassByName(const AClassName: String): TSimpleSerializableClass;
implementation
uses
  Generics.Collections,
  CastleXmlUtils;

type
  TSimpleSerializableClassDictionary = specialize TDictionary<String, TSimpleSerializableClass>;

var
  SimpleSerializableClassDictionary: TSimpleSerializableClassDictionary;

procedure RegisterSimpleSerializableObject(const AClass: TSimpleSerializableClass);
begin
  if SimpleSerializableClassDictionary = nil then
    SimpleSerializableClassDictionary := TSimpleSerializableClassDictionary.Create;
  SimpleSerializableClassDictionary.Add(AClass.ClassName, AClass);
end;

function SimpleSerializableClassByName(const AClassName: String): TSimpleSerializableClass;
begin
  Exit(SimpleSerializableClassDictionary[AClassName]);
end;

class function TSimpleSerializableObject.LoadClass(const Element: TDOMElement): TSimpleSerializableObject;
begin
  try
    Result := SimpleSerializableClassDictionary[Element.AttributeString('ClassName')].Create;
    Result.Load(Element);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TSimpleSerializableObject.Save(const Element: TDOMElement);
begin
  Element.AttributeSet('ClassName', ClassName);
end;

procedure TSimpleSerializableObject.Load(const Element: TDOMElement);
begin
  // do nothing
end;

constructor TSimpleSerializableObject.Create;
begin
  inherited Create; // ancestor is empty
  //and do nothing more
end;

finalization
  SimpleSerializableClassDictionary.Free;
end.

