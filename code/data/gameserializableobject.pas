{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameSerializableObject;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils, DOM;

type
  EClassDeserealizationError = class(Exception);

type
  TSerializableObject = class abstract(TObject)
  strict private
    FReferenceId: UInt64;
    procedure SetReferenceId(const AValue: UInt64);
  public
    property ReferenceId: UInt64 read FReferenceId write SetReferenceId;
  public
    class function LoadClass(const Element: TDOMElement): TSerializableObject;
    procedure Save(const Element: TDOMElement); virtual;
  strict protected
    procedure Load(const Element: TDOMElement); virtual;
  public
    constructor Create(const NotLoading: Boolean = true); virtual;
    destructor Destroy; override;
  end;
  TSerializableClass = class of TSerializableObject;

var
  GlobalId: UInt64 = 0;

procedure RegisterSerializableObject(const AClass: TSerializableClass);
function ObjectByReferenceId(const AReferenceId: UInt64): TSerializableObject;
function SerializableClassByName(const AClassName: String): TSerializableClass;
procedure ClearObjectReferences;
implementation
uses
  Generics.Collections,
  CastleXmlUtils;

type
  TSerializableClassDictionary = specialize TDictionary<String, TSerializableClass>;
  TObjectRefIdDictionary = specialize TDictionary<UInt64, TSerializableObject>;

var
  SerializableClassDictionary: TSerializableClassDictionary;
  ObjectRefIdDictionary: TObjectRefIdDictionary;

procedure RegisterSerializableObject(const AClass: TSerializableClass);
begin
  if SerializableClassDictionary = nil then
    SerializableClassDictionary := TSerializableClassDictionary.Create;
  SerializableClassDictionary.Add(AClass.ClassName, AClass);
end;

function ObjectByReferenceId(const AReferenceId: UInt64): TSerializableObject;
begin
  Exit(ObjectRefIdDictionary[AReferenceId]);
end;

function SerializableClassByName(const AClassName: String): TSerializableClass;
begin
  Exit(SerializableClassDictionary[AClassName]);
end;

procedure ClearObjectReferences;
begin
  ObjectRefIdDictionary.Clear;
end;

procedure TSerializableObject.SetReferenceId(const AValue: UInt64);
begin
  if FReferenceId <> AValue then
  begin
    if FReferenceId <> 0 then
      ObjectRefIdDictionary.Remove(FReferenceId);
    FReferenceId := AValue;
    ObjectRefIdDictionary.Add(FReferenceId, Self);
  end;
end;

class function TSerializableObject.LoadClass(const Element: TDOMElement): TSerializableObject;
begin
  try
    Result := SerializableClassDictionary[Element.AttributeString('ClassName')].Create(false);
    Result.Load(Element);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TSerializableObject.Save(const Element: TDOMElement);
begin
  Element.AttributeSet('ClassName', ClassName);
  Element.AttributeSet('ReferenceId', ReferenceId);
end;

procedure TSerializableObject.Load(const Element: TDOMElement);
begin
  ReferenceId := Element.AttributeQWord('ReferenceId');
end;

constructor TSerializableObject.Create(const NotLoading: Boolean = true);
begin
  inherited Create; // ancestor is empty
  if NotLoading then
  begin
    Inc(GlobalId);
    FReferenceId := 0;
    ReferenceId := GlobalId;
  end; // else Load will set it
end;

destructor TSerializableObject.Destroy;
begin
  if (FReferenceId <> 0) and (ObjectRefIdDictionary <> nil) then
    ObjectRefIdDictionary.Remove(FReferenceId);
  inherited Destroy;
end;

initialization
  ObjectRefIdDictionary := TObjectRefIdDictionary.Create;
finalization
  SerializableClassDictionary.Free;
  FreeAndNil(ObjectRefIdDictionary);
end.

