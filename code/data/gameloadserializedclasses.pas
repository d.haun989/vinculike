{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Dummy unit to make sure all classes which are
  not directly referenced from the game code are serialized
  It's not necessary to reference all the classes here,
  only those that are not referenced from anywhere
  (and no need to remove them if they get referenced :) }
unit GameLoadSerializedClasses;

{$INCLUDE compilerconfig.inc}

interface

implementation
uses
  { Actions }
  GameActionAlarm,
  GameActionAlarmFlee,
  GameActionDancingLights,
  GameActionKnockback,
  GameActionRamTarget,
  GameActionShootTarget,
  GameActionSpawnMinions,
  GameActionTrapMarkTarget,
  GameActionTrapSpawnMonsters,
  GameActionPlayerStunned,
  GameActionPlayerUnconscious,
  { AIs }
  GameAiNone,
  GameAiBasic,
  GameAiMimic,
  GameAiTrap,
  GameAiTrapper,
  GameAiVacuumCleaner,
  { Marks }
  GameMarkHurtTarget,
  GameMarkBindTarget,
  GameMarkBindTargetTrap,
  GameMarkBreakClothes,
  GameMarkUndressAndTeleportTarget,
  GameMarkDisrobeTarget,
  GameMarkEquipSelf,
  GameMarkTickleTarget,
  GameMarkPunishClothes,
  GameMarkPullClothes,
  GameMarkMedic,
  GameMarkUndressAndTeleportTargetVacuumCleaner,
  GameMarkAnestheticTarget,
  GameMarkPushTarget,
  GameMarkStealFromTarget,
  GameMarkStealFromTargetPlus,
  GameProjectileBug,
  GameProjectileBugSimple,
  GameProjectileHurt,
  GameProjectileStun,
  GameProjectileWeb,
  { effects & enchantements }
  GameEnchantmentDamageMultiplier,
  GameEnchantmentRollCostMultiplier,
  GameEnchantmentSpeedMultiplier,
  GameEnchantmentDamageBonus,
  { Vinculopedia }
  GameVinculopediaPage,
  GameVinculopediaEntryHeader,
  GameVinculopediaEntryImageText,
  GameVinculopediaEntryText;

end.

