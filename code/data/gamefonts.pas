{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameFonts;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleFonts;

var
  FontSonianoNumbers20: TCastleFont;
  FontSonianoNumbers30: TCastleFont;
  FontSoniano16: TCastleFont;
  FontSoniano90: TCastleFont;
  FontBender20: TCastleFont;
  FontBender40: TCastleFont;
  FontBender90: TCastleFont;
  //FontBenderBold90: TCastleFont;
  FontBenderBold150: TCastleFont;

procedure LoadFonts;
implementation
uses
  Math,
  CastleStringUtils, CastleUnicode;

type
  TNumericFont = class(TCastleFont)
  strict protected
    procedure Measure(out ARowHeight, ARowHeightBase, ADescend: Single); override;
  end;

procedure TNumericFont.Measure(out ARowHeight, ARowHeightBase, ADescend: Single);
begin
  //inherited Measure(ARowHeight, ARowHeightBase, ADescend);
  ARowHeight := TextHeight('1234567890.-');
  ARowHeightBase := TextHeightBase('1234567890.-');
  ADescend := Max(0, TextHeight('.') - TextHeight('0'));
end;

procedure LoadFonts;
var
  CharList: TUnicodeCharList;
begin
  CharList := TUnicodeCharList.Create;
  CharList.Add(SimpleAsciiCharacters);
  CharList.Add('∀');
  //CharList.Add('∞');
  // TODO: Characters
  FontSonianoNumbers20 := TNumericFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 20, true, ['0','1','2','3','4','5','6','7','8','9','.','-']);
  FontSonianoNumbers30 := TNumericFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 30, true, ['0','1','2','3','4','5','6','7','8','9','.','-']);
  FontSoniano16 := TCastleFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 16, true, CharList);
  FontSoniano90 := TCastleFont.Create('castle-data:/ui/fonts/soniano/soniano-sans-unicode.regular_fixed.ttf', 90, true, CharList);
  FontBender20 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.regular_fixed.ttf', 20, true, CharList);
  FontBender40 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.regular_fixed.ttf', 40, true, CharList);
  FontBender90 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.regular_fixed.ttf', 90, true, CharList);
  //FontBenderBold90 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.bold_fixed.ttf', 90, true, CharList);
  FontBenderBold150 := TCastleFont.Create('castle-data:/ui/fonts/bender/bender.bold_fixed.ttf', 150, true, CharList);
  CharList.Free;
end;

finalization
  FontSonianoNumbers20.Free;
  FontSonianoNumbers30.Free;
  FontSoniano16.Free;
  FontSoniano90.Free;
  FontBender20.Free;
  FontBender40.Free;
  FontBender90.Free;
  //FontBenderBold90.Free;
  FontBenderBold150.Free;
end.

