{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameTranslation;

{$INCLUDE compilerconfig.inc}

interface

function GetTranslation(const AKey: String): String;
procedure LoadTranslation;
implementation
uses
  SysUtils,
  DOM, CastleXMLUtils,
  Generics.Collections,
  GameLog;

type
  TTranslationDictionary = specialize TDictionary<String, String>;

var
  TranslationDictionary: TTranslationDictionary;

function GetTranslation(const AKey: String): String;
var
  LowKey: String;
begin
  LowKey := LowerCase(AKey);
  if TranslationDictionary.ContainsKey(LowKey) then
    Exit(TranslationDictionary[LowKey])
  else
  begin
    DebugWarning('Key %s not found in translation', [AKey]);
    Exit(AKey);
  end;
end;

procedure LoadTranslation;
var
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  TranslationDictionary := TTranslationDictionary.Create;

  Doc := URLReadXML('castle-data:/translation/general.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('entry');
    try
      while Iterator.GetNext do
        TranslationDictionary.Add(LowerCase(Iterator.Current.AttributeString('key')), Trim(Iterator.Current.TextData));
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

finalization
  TranslationDictionary.Free;
end.

