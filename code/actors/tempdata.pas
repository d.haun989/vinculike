{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit TempData;

{$INCLUDE compilerconfig.inc}

interface

uses
  GamePlayerCharacterData, GameActionAbstract;

function TempPlayerCharacter: TPlayerCharacterData;
function PlayerActionAttack: TActionAbstractData;
function PlayerActionDisarm: TActionAbstractData;
function PlayerActionRescue: TActionAbstractData;
implementation
uses
  Generics.Collections,
  GameItemData, GameActionDirectAttack, GameActionPlayerDisarmTrap, GameActionPlayerRescue,
  TemporaryNamesGenerator;

function TempPlayerCharacter: TPlayerCharacterData;
begin
  Result := TPlayerCharacterData.Create;
  with Result do
  begin
    DisplayName := RandomName;
    Size := 3;
    MaxVital := 100;
    MaxVigor := 100;
    MaxPsyche := 100;
    MaxEnigma := 0;
    MovementSpeed := 10;
    RollSpeed := 30;
    RollRange := 15;
    RollVigorCost := 20;
    VitalRegen := 1.0;
    MaxVitalRegen := 0.01;
    VigorRegen := 5.0;
    MaxVigorRegen := 0.1;
    PsycheRegen := 1.5;
    MaxPsycheRegen := 0.01;
  end;
end;

var
  FPlayerActionAttack: TActionAbstractData;
  FPlayerActionDisarm: TActionAbstractData;
  FPlayerActionRescue: TActionAbstractData;

function PlayerActionAttack: TActionAbstractData;
begin
  if FPlayerActionAttack = nil then
  begin
    FPlayerActionAttack := TActionDirectAttackData.Create;
    FPlayerActionAttack.AttackRange := 3;
    FPlayerActionAttack.SqrAttackRange := Sqr(FPlayerActionAttack.AttackRange);
    FPlayerActionAttack.WarmUpTime := 0.5;
    FPlayerActionAttack.CoolDownTime := 0.5;
  end;
  Exit(FPlayerActionAttack);
end;

function PlayerActionDisarm: TActionAbstractData;
begin
  if FPlayerActionDisarm = nil then
  begin
    FPlayerActionDisarm := TActionPlayerDisarmTrapData.Create;
    FPlayerActionDisarm.AttackRange := 5;
    FPlayerActionDisarm.SqrAttackRange := Sqr(FPlayerActionDisarm.AttackRange);
    FPlayerActionDisarm.WarmUpTime := 0.5;
    FPlayerActionDisarm.CoolDownTime := 1.0;
  end;
  Exit(FPlayerActionDisarm);
end;

function PlayerActionRescue: TActionAbstractData;
begin
  if FPlayerActionRescue = nil then
  begin
    FPlayerActionRescue := TActionPlayerRescueData.Create;
    FPlayerActionRescue.AttackRange := 5;
    FPlayerActionRescue.SqrAttackRange := Sqr(FPlayerActionRescue.AttackRange);
    FPlayerActionRescue.WarmUpTime := 0.5;
    FPlayerActionRescue.CoolDownTime := 1.0;
  end;
  Exit(FPlayerActionRescue);
end;

finalization
  FPlayerActionAttack.Free;
  FPlayerActionDisarm.Free;
  FPlayerActionRescue.Free;
end.

