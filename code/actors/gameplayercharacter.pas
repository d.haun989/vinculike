{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePlayerCharacter;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  CastleTimeUtils,
  GameSerializableObject,
  GameActor, GamePlayerCharacterData, GameMapTypes,
  GamePlayerCharacterExperience, GameViewEndGame,
  GameInventory, GameApparelSlots, GameItemData, GameAbstractItem, GameInventoryItem;

const
  BrokenItemPsycheDamage = 5;
  StripTopPsycheFractionDamage = 0.12;
  StripBottomPsycheFractionDamage = 0.17;
  HitNakedPsycheDamage = 5;
  LowHealthPsycheDamage = 10;
  UnarmedDamage = 5;
  BodyNoise = Single(1.0);
  StatsDegradationSpeed = 5;

type
  { all heroes are supposed to have two meaningful body halves }
  TBodyHalf = (bhTop, bhBottom);

type
  TPlayerCharacter = class(TActor)
  strict private
    FLastSoundTime: TTimerResult;
    FLastSoundDuration: Single;
    function CollidesWithMonster: Boolean;
    function SphericalCollisionWithMonster: Single;
  strict protected
    procedure Die; override;
  public
    function PassableTiles: TMoveArray; override;
    procedure UpdatePassableTiles; override;
  public
    Inventory: TInventory;
    Experience: TPlayerCharacterExperience;
    Vigor, MaxVigor: Single;
    Psyche, MaxPsyche: Single;
    Enigma, MaxEnigma: Single;
    procedure Reset; override;
    procedure Hit(const Damage: Single); override;
    procedure HitSpecific(const Damage: Single; const BodyHalf: TBodyHalf); // TODO: Damage kind
    procedure DegradeVital(const Damage: Single);
    procedure HitVital(const Damage: Single);
    procedure HitVigor(const Damage: Single);
    procedure DegradeVigor(const Damage: Single);
    procedure HitPsyche(const Damage: Single);
    procedure DegradePsyche(const Damage: Single);
    procedure DamageWeapon; override;
    function CanAct: Boolean; override;
    function CanBeInteractedWith: Boolean; override;
    function Unsuspecting: Boolean; override;
    function Immobilized: Boolean; override;
  public
    procedure RegenerateStats(const SecondsPassed: Single);
    procedure RegenerateMaxStats(
      const SecondsPassed: Single; const Quality: Single);
    function PsycheRegenCoefficient(const Quality: Single): Single;
  public
    procedure PlayGiggleSound;
    procedure PlayGruntSound;
    procedure PlayBreathSound;
    procedure PlaySurpriseSound;
    procedure PlayAttackSound; override;
  public
    { Gets damage of the Player item or unarmed }
    function GetDamage: Single; override;
    function GetDamageMultiplier: Single;
    function GetSpeed: Single; override;
    function GetSpeedMultiplier: Single;
    function GetRollSpeed: Single; override;
    function GetRollCost: Single;
    function GetRollCostMultiplier: Single;
    function GetNoise: Single; override;
    function PlayerCharacterData: TPlayerCharacterData; inline;
  public
    IsCaptured: Boolean;
    AtMap: Byte;
  public
    //procedure RespawnInRandomPlaceNaked;
    procedure GetCaptured(const CaptureKind: TEndGameKind);
    { Same as GetCaptured, but doesn't call anything except this character functions
      For internal use - e.g. to recover from crash }
    procedure GetCapturedSafe;
    procedure Teleport(const ToX, ToY: Int16); override;
    procedure TeleportToUnknown(const MinTeleportDistanceFraction: Single);
    procedure UpdateVisible; override;
    procedure Update(const SecondsPassed: Single); override;
  public const VisibleRange = Single(24); // TODO: variable
  public const Signature = 'playerchar';
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TPlayerCharactersList = specialize TObjectList<TPlayerCharacter>;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameRandom, GameMapItem, GameSounds, GameTranslation, GameMap, GameLog, GameColors,
  GameMonster, GameMonsterData,
  TempData,
  GameEnchantmentDamageBonus, GameEnchantmentSpeedMultiplier,
  GameEnchantmentDamageMultiplier, GameEnchantmentNoiseMultiplier,
  GameEnchantmentRollCostMultiplier,
  GameActionIdle, GameActionPlayerRest, GameActionPlayerUnconscious, GameActionPlayerStunned,
  GameViewGame, GameParticle;

type
  EDamageCalculationError = class(Exception);

procedure TPlayerCharacter.Reset;
begin
  Experience.Recalculate;
  inherited;
  MaxVigor := PlayerCharacterData.MaxVigor;
  Vigor := MaxVigor;
  MaxPsyche := PlayerCharacterData.MaxPsyche;
  Psyche := MaxPsyche;
  MaxEnigma := PlayerCharacterData.MaxEnigma;
  Enigma := MaxEnigma;
end;

procedure TPlayerCharacter.Hit(const Damage: Single);
begin
  //inherited Hit(Damage); ---- different logic here
  HitSpecific(Damage, TBodyHalf(Rnd.Random(Ord(High(TBodyHalf)) + 1)));
end;

procedure TPlayerCharacter.HitSpecific(const Damage: Single; const BodyHalf: TBodyHalf);
var
  RemainingDamage: Single;
  AbsorbedDamage: Single;
  DamageAbsorbers: specialize TObjectList<TInventoryItem>;
  PsycheDamage: Single;
  Item: TInventoryItem;
  E: TApparelSlot;
begin
  // inherited: different formula?
  RemainingDamage := Damage;

  DamageAbsorbers := (specialize TObjectList<TInventoryItem>).Create(false);
  case BodyHalf of
    bhTop: begin
         for E in TopSlots do
           if (Inventory.Equipped[E] <> nil) and not DamageAbsorbers.Contains(Inventory.Equipped[E]) and (Inventory.Equipped[E].ItemData.Protection > 0) then
             DamageAbsorbers.Add(Inventory.Equipped[E]);
      end;
    bhBottom: begin
         for E in BottomSlots do
           if (Inventory.Equipped[E] <> nil) and not DamageAbsorbers.Contains(Inventory.Equipped[E]) and (Inventory.Equipped[E].ItemData.Protection > 0) then
             DamageAbsorbers.Add(Inventory.Equipped[E]);
      end;
    {else
      raise EDamageCalculationError.Create('unexpected DamagePart: ' + DamagePart.ToString);}
  end;

  for Item in DamageAbsorbers do
    //if Rnd.Random < Item.Data.Chance then
    begin
      AbsorbedDamage := RemainingDamage * Item.ItemData.Protection;
      RemainingDamage -= AbsorbedDamage;
      if Self = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ArmorAbsorbsDamageLog'), [Item.Data.DisplayName, AbsorbedDamage], ColorLogAbsorbDamage);
      Inventory.DamageItem(Item, AbsorbedDamage);
    end;

  DamageAbsorbers.Free;

  DegradeVital(RemainingDamage);
  if Self = ViewGame.CurrentCharacter then
  begin
    if Abs(RemainingDamage - Damage) < 0.1 then
      ShowLog(GetTranslation('PlayerReceivedUnabsorbedDamageLog'), [FData.DisplayName, RemainingDamage], ColorLogUnabsorbedDamage)
    else
      ShowLog(GetTranslation('PlayerReceivedAbsorbedDamageLog'), [FData.DisplayName, RemainingDamage, Round(100 * RemainingDamage / Damage)], ColorLogAbsorbedDamage);
  end;

  if Vital <= 0 then
    Die;

  PsycheDamage := LowHealthPsycheDamage * RemainingDamage / MaxVital;

  Inventory.UpdateCoveredStatus;
  if (Inventory.TopCovered = 0) and (BodyHalf = bhTop) then
  begin
    ShowLog(GetTranslation('PlayerReceivedNakedTouchTop'), [FData.DisplayName], ColorLogNaked);
    PsycheDamage += HitNakedPsycheDamage;
  end; //else
  if (Inventory.BottomCovered = 0) and (BodyHalf = bhBottom) then
  begin
    ShowLog(GetTranslation('PlayerReceivedNakedTouchBottom'), [FData.DisplayName], ColorLogNaked);
    PsycheDamage += HitNakedPsycheDamage;
  end;
  HitPsyche(PsycheDamage);

  ViewGame.WakeUp(true, true);
  NewParticle(CenterX, CenterY, Round(Damage).ToString, ColorParticlePlayerHurt);
end;

procedure TPlayerCharacter.DegradeVital(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Vital -= Damage;
    MaxVital -= Damage / StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade vital by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.HitVital(const Damage: Single);
begin
  if Damage > PlayerCharacterData.MaxVital / 100.0 then
    ShowLog('%s lost %d vital', [FData.DisplayName, Round(Damage)], ColorLogUnabsorbedDamage);
  DegradeVital(Damage);
end;

procedure TPlayerCharacter.HitVigor(const Damage: Single);
var
  PreviousVigor: Single;
begin
  if Damage > PlayerCharacterData.MaxVigor / 100.0 then
    ShowLog('%s lost %d vigor', [FData.DisplayName, Round(Damage)], ColorLogVigorDamage);
  PreviousVigor := Vigor;
  DegradeVigor(Damage);
  if (PreviousVigor > 0) and (Vigor < 0) then
    ShowLog('%s is exhausted and can barely stand on her feet', [FData.DisplayName, Damage], ColorLogVigorExhausted);
end;

procedure TPlayerCharacter.DegradeVigor(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Vigor -= Damage;
    MaxVigor -= Damage / StatsDegradationSpeed;
  end else
    ShowError('Cannot degrade vigor by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.HitPsyche(const Damage: Single);
begin
  if Damage > PlayerCharacterData.MaxPsyche / 100.0 then
    ShowLog('%s lost %d psyche', [FData.DisplayName, Round(Damage)], ColorLogPsycheDamage);
  DegradePsyche(Damage);
end;

procedure TPlayerCharacter.DegradePsyche(const Damage: Single);
begin
  if Damage >= 0 then
  begin
    Psyche -= Damage;
    MaxPsyche -= Damage / StatsDegradationSpeed;
    if Psyche <= 0 then // TODO : Bad place here
      ShowLog(GetTranslation('PlayerMentalBreakdown'), [FData.DisplayName], ColorLogDefeat);
  end else
    ShowError('Cannot degrade psyche by negative value %.1n', [Damage]);
end;

procedure TPlayerCharacter.DamageWeapon;
begin
  inherited DamageWeapon;
  Inventory.DamageWeapon;
end;

function TPlayerCharacter.GetDamage: Single;
begin
  if (Inventory.Equipped[esWeapon] <> nil) and not Inventory.Equipped[esWeapon].ItemData.IsBondage then
    Result := Inventory.Equipped[esWeapon].ItemData.Damage
  else
  if (Inventory.Equipped[esWeapon] <> nil) {and Equipped[esWeapon].Data.IsBondage -- obvious} then
  begin
    if Inventory.Equipped[esWeapon].ItemData.Damage > 0 then
      Result := Inventory.Equipped[esWeapon].ItemData.Damage
    else
      Result := UnarmedDamage; // Set damage to -1 if character can use unarmed damage with this bondage item
  end else
    Result := UnarmedDamage;
  Result *= GetDamageMultiplier;

  Inventory.UpdateCoveredStatus; // may be redundant, but shouldn't be too inefficient
  if (Inventory.BottomCovered = 0) and (Inventory.TopCovered = 0) then
    Result += Inventory.FindEffectAdditive(TEnchantmentDamageBonus);
end;

function TPlayerCharacter.GetDamageMultiplier: Single;
begin
  Result := Inventory.FindEffectMultiplier(TEnchantmentDamageMultiplier);
end;

function TPlayerCharacter.GetSpeed: Single;
begin
  Result := inherited GetSpeed;
  if SphericalCollisionWithMonster < 1.0 then
    Result *= 0.1 + 0.9 * Sqrt(Sqrt(SphericalCollisionWithMonster));
  Result *= GetSpeedMultiplier;
end;

function TPlayerCharacter.GetSpeedMultiplier: Single;
begin
  Result := Inventory.FindEffectMultiplier(TEnchantmentSpeedMultiplier);
end;

function TPlayerCharacter.GetRollSpeed: Single;
begin
  Result := inherited GetRollSpeed;
  if CollidesWithMonster then
    Result *= 0.33;
end;

function TPlayerCharacter.GetRollCostMultiplier: Single;
begin
  Result := Inventory.FindEffectMultiplier(TEnchantmentRollCostMultiplier);
end;

function TPlayerCharacter.GetNoise: Single;
begin
  Result := (BodyNoise + Inventory.TotalIdleNoise) * CurrentAction.NoiseMultiplier + CurrentAction.NoiseAddition;

  Result *= Inventory.FindEffectMultiplier(TEnchantmentNoiseMultiplier);
end;

function TPlayerCharacter.GetRollCost: Single;
begin
  Result := PlayerCharacterData.RollVigorCost * GetRollCostMultiplier;
end;

function TPlayerCharacter.CanAct: Boolean;
begin
  Result := not IsCaptured and inherited and (Psyche > 0);
end;

function TPlayerCharacter.CanBeInteractedWith: Boolean;
begin
  //Result := inherited CanBeInteractedWith;
  Result := true; // always? TODO
end;

function TPlayerCharacter.Unsuspecting: Boolean;
begin
  Exit((CurrentAction is TActionPlayerRest) or (CurrentAction is TActionPlayerUnconscious));
end;

function TPlayerCharacter.Immobilized: Boolean;
begin
  Exit(CurrentAction is TActionPlayerStunned);
end;

procedure TPlayerCharacter.PlayGiggleSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_giggle');
  end;
end;

procedure TPlayerCharacter.PlayGruntSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_grunt');
  end;
end;

procedure TPlayerCharacter.PlayBreathSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_breath');
  end;
end;

procedure TPlayerCharacter.PlaySurpriseSound;
begin
  if FLastSoundTime.ElapsedTime > FLastSoundDuration then
  begin
    Self.FLastSoundTime := Timer;
    FLastSoundDuration := SoundWithDuration('female_surprise');
  end;
end;

procedure TPlayerCharacter.PlayAttackSound;
begin
  if Inventory.Equipped[esWeapon] <> nil then
    Sound(Inventory.Equipped[esWeapon].ItemData.SoundAttack)
  else
    Sound('hit_punch');
end;

procedure TPlayerCharacter.Die;
begin
  ShowLog(GetTranslation('PlayerBlacksOutLog'), [FData.DisplayName], ColorLogDefeat);
end;

function TPlayerCharacter.PassableTiles: TMoveArray;
begin
  if Self = ViewGame.CurrentCharacter then
  begin
    if Map.PassablePlayerTiles = nil then
      UpdatePassableTiles;
    Exit(Map.PassablePlayerTiles)
  end else
    Result := inherited;
end;

procedure TPlayerCharacter.UpdatePassableTiles;
begin
  // we don't need to update those every tile change, HOWEVER, we may want to update them more often - as a Task?
  Map.UpdatePassableVisibleTiles(LastTileX, LastTileY);
end;

function TPlayerCharacter.CollidesWithMonster: Boolean;
var
  M: TMonster;
begin
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive and M.Collides(Self, -1) then
      Exit(true);
  Exit(false);
end;

function TPlayerCharacter.SphericalCollisionWithMonster: Single;
var
  M: TMonster;
  D: Single;
begin
  Result := 1.0;
  for M in Map.MonstersList do
    if M.CanAct and M.Aggressive then
    begin
      D := M.SphericalDistanceNormalizedSqr(Self);
      if D < Result then
        Result := D;
    end;
end;

procedure TPlayerCharacter.UpdateVisible;
begin
  //Map.LookAround(LastTileX + PredSize div 2, LastTileY + PredSize div 2);
  Map.ScheduleUpdateVisible;
end;

procedure TPlayerCharacter.Teleport(const ToX, ToY: Int16);
begin
  inherited;
  Map.PassablePlayerTiles := nil; // TODO: Temporary? Optimize?
  if not IsCaptured then
    UpdateVisible;
  if Self = ViewGame.CurrentCharacter then
    ViewGame.StopShakers;
end;

procedure TPlayerCharacter.TeleportToUnknown(const MinTeleportDistanceFraction: Single);
const
  MaxRetryCountInvisible = Integer(10000);
  MaxRetryCountMonsters = Integer(3000);
var
  AX, AY: Int16;
  RetryCount: Integer;
  DistanceSeed: TCoordList;
  DistanceMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
begin
  DistanceSeed := TCoordList.Create;
  // TODO: cycle all active players
  AddSeed(DistanceSeed);
  DistanceSeed.Add(IntCoord(Map.ExitX, Map.ExitY));
  DistanceMap := Map.DistanceMap(PredSize, DistanceSeed);
  MaxDistance := Map.MaxDistance(DistanceMap);
  DistanceSeed.Free;

  RetryCount := 0;
  repeat
    repeat
      AX := Rnd.Random(Map.SizeX);
      AY := Rnd.Random(Map.SizeY);
    until (Map.PassableTiles[PredSize][AX + Map.SizeX * AY]) and (Sqr(AX - Map.ExitX) + Sqr(AY - Map.ExitY) > Sqr(16)) and (DistanceMap[AX + Map.SizeX * AY] > MaxDistance * MinTeleportDistanceFraction);
    Inc(RetryCount);
  until ((Map.Visible[AX + Map.SizeX * AY] = 0) or (RetryCount > MaxRetryCountInvisible)) and (Map.NoMonstersNearby(AX, AY, Size) or (RetryCount > MaxRetryCountMonsters));
  DistanceMap := nil;
  Teleport(AX, AY);
  ViewGame.InternalMapInterface.TeleportTo(AX, AY);
end;

function TPlayerCharacter.PlayerCharacterData: TPlayerCharacterData; inline;
begin
  Exit({$IFDEF SafeActorTypecast}FData as TPlayerCharacterData{$ELSE}TPlayerCharacterData(FData){$ENDIF});
end;

procedure TPlayerCharacter.GetCaptured(const CaptureKind: TEndGameKind);
begin
  IsCaptured := true;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Inventory.UnequipAndDisintegrateEverything;
  ViewGame.ScheduleEndGame;
  ViewEndGame.EndGameKind := CaptureKind;
end;

procedure TPlayerCharacter.GetCapturedSafe;
begin
  IsCaptured := true;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  Inventory.UnequipAndDisintegrateEverything;
end;

function TPlayerCharacter.PsycheRegenCoefficient(const Quality: Single): Single;
begin
  // sleeping peaks psyche regen at 100%
  Result := 1.0;
  if Inventory.TopCovered = 0 then
    Result := Result / 2.0;
  if Inventory.BottomCovered = 0 then
    Result := Result / 5.0;

  // Bonus for sleeping quality
  if Result < Quality - 1.0 then
  begin
    Result := Quality - 1.0;
    if Result > 1.0 then
      Result := 1.0;
  end;
end;

procedure TPlayerCharacter.RegenerateMaxStats(const SecondsPassed: Single; const Quality: Single);
var
  Efficiency: Single;
begin
  Efficiency := SecondsPassed * Quality;

  { I'm not sure about MaxStat < 0 part,
    of course we can do
    (MaxStat < 0) and (Stat > MaxStat * 1.01)
    but this doesn't look like a good solution either
    for now it's causing problems only with Vigor
    but in future capture logic it will need to be addressed properly }

  if (Vital > MaxVital * 0.99) or (MaxVital < 0) then
  begin
    MaxVital += Efficiency * PlayerCharacterData.MaxVitalRegen;
    if MaxVital > PlayerCharacterData.MaxVital then
      MaxVital := PlayerCharacterData.MaxVital;
  end;

  if (Vigor > MaxVigor * 0.99) or (MaxVigor < 0) then
  begin
    MaxVigor += Efficiency * PlayerCharacterData.MaxVigorRegen;
    if MaxVigor > PlayerCharacterData.MaxVigor then
      MaxVigor := PlayerCharacterData.MaxVigor;
  end;

  if (Psyche > MaxPsyche * 0.99) or (MaxPsyche < 0) then
  begin
    MaxPsyche += Efficiency * PsycheRegenCoefficient(Quality) * PlayerCharacterData.MaxPsycheRegen;
    if MaxPsyche > PlayerCharacterData.MaxPsyche then
      MaxPsyche := PlayerCharacterData.MaxPsyche;
  end;
end;

procedure TPlayerCharacter.RegenerateStats(const SecondsPassed: Single);
begin
  Vital += SecondsPassed * PlayerCharacterData.VitalRegen;
  if Vital > MaxVital then
    Vital := MaxVital;

  Vigor += SecondsPassed * PlayerCharacterData.VigorRegen;
  if Vigor > MaxVigor then
    Vigor := MaxVigor;

  Psyche += SecondsPassed * PsycheRegenCoefficient(0) * PlayerCharacterData.PsycheRegen;
  if Psyche > MaxPsyche then
    Psyche := MaxPsyche;
end;

procedure TPlayerCharacter.Update(const SecondsPassed: Single);
begin
  if (Psyche <= 0) and not IsCaptured then
    GetCaptured(egMental);
  if (Vital <= 0) and not IsCaptured then
    GetCaptured(egHealth);

  RegenerateStats(SecondsPassed);
  RegenerateMaxStats(SecondsPassed, 1.0);

  inherited;
end;

procedure TPlayerCharacter.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('TEMPORARY-NAME', FData.DisplayName);
  Element.AttributeSet('IsCaptured', IsCaptured);
  Element.AttributeSet('AtMap', AtMap);
  Element.AttributeSet('Vigor', Vigor);
  Element.AttributeSet('MaxVigor', MaxVigor);
  Element.AttributeSet('Psyche', Psyche);
  Element.AttributeSet('MaxPsyche', MaxPsyche);
  Element.AttributeSet('Enigma', Enigma);
  Element.AttributeSet('MaxEnigma', MaxEnigma);
  Inventory.Save(Element); // Element.CreateChild(TInventory.ClassName)
  Experience.Save(Element.CreateChild(TPlayerCharacterExperience.Signature));
end;

procedure TPlayerCharacter.Load(const Element: TDOMElement);
begin
  inherited;
  Data := TempPlayerCharacter; // TEMPORARY TODO
  Data.DisplayName := Element.AttributeString('TEMPORARY-NAME');
  IsCaptured := Element.AttributeBoolean('IsCaptured');
  AtMap := Element.AttributeInteger('AtMap');
  Vigor := Element.AttributeSingle('Vigor');
  MaxVigor := Element.AttributeSingle('MaxVigor');
  Psyche := Element.AttributeSingle('Psyche');
  MaxPsyche := Element.AttributeSingle('MaxPsyche');
  Enigma := Element.AttributeSingle('Enigma');
  MaxEnigma := Element.AttributeSingle('MaxEnigma');
  Inventory.Load(Element); //Element.Child(TInventory.ClassName, true)
  Experience.Load(Element.Child(TPlayerCharacterExperience.Signature, true));
  SetSize(FData.Size);
  FLastSoundDuration := 0;
end;

constructor TPlayerCharacter.Create(const NotLoading: Boolean = true);
begin
  inherited;
  Experience := TPlayerCharacterExperience.Create;
  Experience.Parent := Self;
  Inventory := TInventory.Create;
  Inventory.Parent := Self;
  IsCaptured := false;
  AtMap := 0;
  FLastSoundDuration := 0;
end;

destructor TPlayerCharacter.Destroy;
begin
  Inventory.Free;
  Experience.Free;
  FreeAndNil(FData);
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TPlayerCharacter);
end.

