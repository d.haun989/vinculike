{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionDancingLights;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActionAbstract, GameActionOnTarget;

type
  TActionDancingLights = class(TActionOnTarget)
  strict private const
    EffectDistanceSqr = Sqr(5);
  strict private
    IsWarmUp: Boolean;
    Phase: Single;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  strict private
    class var GroupCounter: Integer;
    function GroupSize: Integer;
    procedure Perform;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionDancingLightsData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameMap, GameRandom, GameActor, GameMonster, GameActorData, GameMonsterData,
  GameActionPlayerStunned, GamePlayerCharacter, GameApparelSlots,
  GameSounds, GameLog, GameParticle, GameColors, GameViewGame;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ActionData:=(Data as TActionAbstractData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}

procedure TActionDancingLights.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('IsWarmUp', IsWarmUp);
end;

procedure TActionDancingLights.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  IsWarmUp := Element.AttributeBoolean('IsWarmUp');
end;

function TActionDancingLights.GroupSize: Integer;
var
  M: TMonster;
begin
  Result := 0;
  for M in Map.MonstersList do
    if M.CanAct and (M.CurrentAction.ClassType = ParentMonster.CurrentAction.ClassType) and (M.DistanceToSqr(TargetPlayer) < EffectDistanceSqr) then
      Inc(Result);
end;

procedure TActionDancingLights.Perform;

  procedure StunTarget(const Duration: Single);
  begin
    ViewGame.WakeUp(true, true);
    TargetActor.CurrentAction := TActionPlayerStunned.NewAction(TargetActor, Duration);
    TargetActor.CurrentAction.Start;
    ShowLog('Suddenly all %s''s muscles spasm and become stiff (%.1n seconds)', [TargetActor.Data.DisplayName, Duration], ColorLogNotEnoughVigor);
    Sound(ParentMonster.MonsterData.SoundAttack);
  end;

  procedure DisrobeTarget;
  var
    E: TApparelSlot;
  begin
    E := TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem;
    ShowLog('The lights around %s flash and her %s falls off', [TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogNotEnoughVigor);
    TargetPlayer.Inventory.UnequipAndDrop(E, true);
    ViewGame.WakeUp(true, true);
    Sound(ParentMonster.MonsterData.SoundAttack);
  end;

  procedure DisarmTarget;
  begin
    ShowLog('%s feels extremely weak for a moment and lets %s drop to the ground', [TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogNotEnoughVigor);
    TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    ViewGame.WakeUp(true, true);
    Sound(ParentMonster.MonsterData.SoundAttack);
  end;

  procedure FatigueTarget(const Damage: Single);
  begin
    ShowLog('%s suddenly feels weird and tired', [TargetActor.Data.DisplayName], ColorLogNotEnoughVigor);
    TargetPlayer.HitVigor(Damage);
    ViewGame.WakeUp(true, true);
    Sound(ParentMonster.MonsterData.SoundAttack);
  end;

  procedure FearTarget(const Damage: Single);
  begin
    ShowLog('%s has a panic attack', [TargetActor.Data.DisplayName], ColorLogNotEnoughVigor);
    TargetPlayer.HitPsyche(Damage);
    ViewGame.WakeUp(true, true);
    Sound(ParentMonster.MonsterData.SoundAttack);
  end;

begin
  if GroupSize = 1 then
    // Do nothing
    ShowLog('%s orbits around %s without touching her', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTrap)
  else
  if GroupSize >= 2 then
  begin
    if not TargetActor.Immobilized and (Rnd.Random < 0.5) then
      StunTarget(0.25 * GroupSize)
    else
    if (TargetPlayer.Inventory.EquippedClothesRemovable > 0) and (Rnd.Random < 0.5) then
      DisrobeTarget
    else
    if (GroupSize >= 3) and TargetActor.Immobilized and (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and (not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage) and (Rnd.Random < 0.5) then
      DisarmTarget
    else
    if (GroupSize >= 4) and (Rnd.Random < 0.5) then
      FatigueTarget(ParentActor.GetDamage * 2 * GroupSize)
    else
    if (TargetPlayer.Inventory.EquippedClothesRemovable = 0) and TargetActor.Immobilized and (GroupSize >= 5) and (Rnd.Random < 0.5) then
      FearTarget(ParentActor.GetDamage * GroupSize)
    else
      ShowLog('The lights dance around %s in mesmerizing patterns', [TargetActor.Data.DisplayName], ColorLogTrap);
  end else
    raise Exception.CreateFmt('Unexpected GroupSize in TActionDancingLights.Perform: %d', [GroupSize]);
end;

function TActionDancingLights.CanStop: Boolean;
begin
  Exit(IsWarmUp);
end;

function TActionDancingLights.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionDancingLights.NoiseAddition: Single;
begin
  Exit(0.0);
end;

class function TActionDancingLights.NewAction(
  const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionDancingLights).Phase := 0;
  (Result as TActionDancingLights).IsWarmUp := true;
end;

procedure TActionDancingLights.Update(const SecondsPassed: Single);

  procedure TeleportAround;
  var
    OriginalX, OriginalY: Int16;
    NewX, NewY: Int16;
    SizeSum: Integer;
  begin
    OriginalX := Trunc(TargetActor.CenterX);
    OriginalY := Trunc(TargetActor.CenterY);
    SizeSum := ParentActor.Size + TargetActor.Size;
    repeat
      repeat
        NewX := OriginalX + Rnd.Random(2 * SizeSum) - SizeSum;
        NewY := OriginalY + Rnd.Random(2 * SizeSum) - SizeSum;
      until (NewX >= 0) and (NewX < Map.SizeX) and (NewY >= 0) and (NewY < Map.SizeY) and Map.PassableTiles[ParentActor.PredSize][NewX + Map.SizeX * NewY];
      ParentActor.LastTileX := NewX;
      ParentActor.LastTileY := NewY;
      ParentActor.LastTile := NewX + Map.SizeX * NewY;
      ParentActor.X := NewX + Rnd.Random;
      ParentActor.Y := NewY + Rnd.Random;
      ParentActor.CenterX := ParentActor.X + ParentActor.Halfsize;
      ParentActor.CenterY := ParentActor.Y + ParentActor.Halfsize;
    until ParentActor.DistanceToSqr(TargetActor) < EffectDistanceSqr;
  end;

begin
  if not ParentActor.CanAct then
  begin
    OnFinished;
    Exit;
  end;

  if Rnd.Random < 0.01 then
    TeleportAround;

  Phase += SecondsPassed;
  if IsWarmUp and (Phase >= ActionData.WarmUpTime) then
  begin
    Phase -= ActionData.WarmUpTime;
    IsWarmUp := false;
    Inc(GroupCounter);
    if GroupCounter >= GroupSize then // TODO: CacheGroupSize
    begin
      GroupCounter -= GroupSize;
      Perform;
    end;
  end else
  if not IsWarmUp and (Phase >= ActionData.CoolDownTime) then
  begin
    IsWarmUp := true;
    Phase -= ActionData.CoolDownTime;
  end;

  inherited;
end;

{ TActionDancingLightsData -------------------------------}

function TActionDancingLightsData.Action: TActionClass;
begin
  Exit(TActionDancingLights);
end;

initialization
  RegisterSimpleSerializableObject(TActionDancingLights);
  RegisterSerializableData(TActionDancingLightsData);

end.

