{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiSingleActionAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract, GameActionAbstract;

type
  TAiSingleActionAbstract = class abstract(TAiAbstract)
  strict private
    procedure ChasePlayerCore(const ATimeout: Single);
  public
    procedure OnHit(const ATimeout: Single); override;
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  end;

  TAiSingleActionAbstractData = class abstract(TAiAbstractData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ActionData: TActionAbstractData;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameViewGame, GameLog, GameActor, GameMonster, GameActionMove;

{$IFDEF SafeActorTypecast}
{$DEFINE AiData:=(Data as TAiSingleActionAbstractData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE AiData:=TAiSingleActionAbstractData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

{ TAiSingleActionAbstract }

procedure TAiSingleActionAbstract.ChasePlayerCore(const ATimeout: Single);
begin
  if not ParentActor.CanAct then
  begin
    ShowError('%s received ChasePlayer but it is dead.', [ParentActor.Data.Id]);
    Exit;
  end;
  SoftGuard := false;
  AiState := asAttack;
  ParentActor.MoveAndAct(ParentMonster.CurrentTarget, AiData.ActionData);
  Timeout := ATimeout;
end;

procedure TAiSingleActionAbstract.OnHit(const ATimeout: Single);
begin
  ChasePlayerCore(ATimeout);
end;

procedure TAiSingleActionAbstract.ChasePlayer(const ATimeout: Single);
begin
  ChasePlayerCore(ATimeout);
end;

procedure TAiSingleActionAbstract.InvestigateNoise(const AX, AY: Single);
begin
  if not ParentActor.CanAct then
  begin
    ShowError('%s received InvestigateNoise but it is dead.', [ParentActor.Data.Id]);
    Exit;
  end;
  if ParentActor.Unsuspecting and not AiFlee then // Todo: something smarter here
  begin
    AiState := asInvestigate;
    ParentActor.MoveTo(AX, AY);
    Timeout := (ParentActor.CurrentAction as TActionMove).RemainingTime;
  end;
end;

{ TAiSingleActionAbstractData ------------------------------------------ }

procedure TAiSingleActionAbstractData.Validate;
begin
  //do nothing
end;

procedure TAiSingleActionAbstractData.Read(const Element: TDOMElement);
begin
  ActionData := TActionAbstractData.ReadClass(Element.Child('Action')) as TActionAbstractData;
end;

destructor TAiSingleActionAbstractData.Destroy;
begin
  ActionData.Free;
  inherited Destroy;
end;

end.

