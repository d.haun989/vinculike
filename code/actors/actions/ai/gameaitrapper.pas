{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiTrapper;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract, GameAiSingleActionAbstract, GameActionAbstract;

type
  TAiTrapper = class(TAiSingleActionAbstract)
  public
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiTrapperData = class(TAiSingleActionAbstractData)
  strict protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    function Ai: TAiClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameViewGame, GameMap, GameMapTypes, GameActor, GameMonster,
  GameActionIdle, GameMonsterData, GameRandom, GameActorData, GameLog;

{$IFDEF SafeActorTypecast}
{$DEFINE AiData:=(Data as TAiTrapData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE AiData:=TAiTrapData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TAiTrapper.ChasePlayer(const ATimeout: Single);
begin
  // Do Nothing;
end;

procedure TAiTrapper.InvestigateNoise(const AX, AY: Single);
begin
  // Do Nothing;
end;

procedure TAiTrapper.Update(const SecondsPassed: Single);
var
  I: Integer;
  ATrap: TMonster;
  SpawnList: TActorDataList;
begin
  // Warning: Trapper doesn't timeout. Will attack until flees
  if ParentActor.CurrentAction is TActionIdle then
  begin
    // TODO: all characters
    if (Rnd.Random < 0.3) and (not ParentActor.Collides(ViewGame.CurrentCharacter, 1)) then
    begin
      SpawnList := TActorDataList.Create(false);
      for I := 0 to Pred(TrapsData.Count) do
        if ((TrapsData[I] as TMonsterData).StartSpawningAtDepth <= Map.CurrentDepth) and
          ((TrapsData[I] as TMonsterData).StopSpawningAtDepth > Map.CurrentDepth) then
          SpawnList.Add(TrapsData[I]);
      if SpawnList.Count = 0 then
      begin
        ShowError('Cannot find any traps to spawn at level %d', [Map.CurrentDepth]);
      end else
      begin
        ATrap := TMonster.Create;
        ATrap.Data := SpawnList[Rnd.Random(SpawnList.Count)];
        ATrap.Reset;
        ATrap.Teleport(ParentActor.LastTileX, ParentActor.LastTileY);
        Map.MonstersList.Add(ATrap);
      end;
      SpawnList.Free;
    end;
    DoWalkRandomlyLong;
  end;
end;

{ TAiTrapperData ------------------------------------------ }

function TAiTrapperData.Ai: TAiClass;
begin
  Exit(TAiTrapper);
end;

initialization
  RegisterSimpleSerializableObject(TAiTrapper);
  RegisterSerializableData(TAiTrapperData);

end.

