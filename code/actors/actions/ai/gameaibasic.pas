{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiBasic;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract, GameAiSingleActionAbstract, GameActionAbstract;

type
  TAiBasic = class(TAiSingleActionAbstract)
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiBasicData = class(TAiSingleActionAbstractData)
  strict protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    // Curious: Boolean; ---> CanGuard: Boolean;
    function Ai: TAiClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameRandom, GameMap,  GameMonster, GameActor,
  GameActionIdle, GameActionMove, GameActionMoveAndActTarget;

{$IFDEF SafeActorTypecast}
{$DEFINE AiData:=(Data as TAiTrapData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE AiData:=TAiTrapData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TAiBasic.Update(const SecondsPassed: Single);

  procedure DoIdle;
  begin
    AiFlee := false;
    AiState := asIdle;
    if Guard or SoftGuard then
      DoReturnToGuard
    else
      //walk around randomly (patrol)
      if (Rnd.Random < 0.01) and ((Rnd.Random < Map.TimeSpentOnTheMap / 120.0) or ParentMonster.MonsterData.CuriousAi) then
      begin
        if ParentMonster.MonsterData.CuriousAi then
          DoWalkCurious
        else
          DoWalkRandomly;
      end;
  end;

  procedure DoAlert;
  var
    SqrDistanceToPlayer: Single;
  begin
    SqrDistanceToPlayer := Sqr(ParentMonster.CurrentTarget.CenterX - ParentMonster.CenterX) + Sqr(ParentMonster.CurrentTarget.CenterY - ParentMonster.CenterY);
    // if player is close enough attack and alert others to join attack
    if not (ParentActor.CurrentAction is TActionMoveAndActTarget) and (SqrDistanceToPlayer < Sqr(ParentMonster.VisibilityRange / 2)) and ParentMonster.LineOfSight(ParentMonster.CurrentTarget) then
      DoChasePlayerAndAlertNearbyToChase
    else
    // if player is away but visible, investigate and alert otherst to investigate
    if (SqrDistanceToPlayer < Sqr(ParentMonster.VisibilityRange)) and ParentMonster.LineOfSight(ParentMonster.CurrentTarget) then
      DoInvestigateNoiseAndAlertNearbyToInvestigate
    else
    if not (ParentMonster.CurrentAction is TActionMoveAndActTarget) and (SqrDistanceToPlayer < Sqr(ParentMonster.VisibilityRange / 3)) then
      InvestigateNoise(ParentMonster.CurrentTarget.CenterX, ParentMonster.CurrentTarget.CenterY)
    else
    // if Player is not visible, then go idlea after timeout
    if not (ParentMonster.CurrentAction is TActionIdle) then
    begin
      Timeout -= SecondsPassed;
      if Timeout < 0 then
      begin
        ParentMonster.CurrentAction := TActionIdle.NewAction(ParentActor);
        ParentMonster.CurrentAction.Start;
        AiState := asIdle;
      end;
    end;
  end;

begin
  if AiFlee and not (ParentActor.CurrentAction is TActionMove) and not (ParentActor.CurrentAction is TActionIdle) then
    DoFlee;
  if not AiFlee then
    DoAlert;
  if ParentActor.CurrentAction is TActionIdle then
    DoIdle;
end;

{ TAiBasicData ------------------------------------------ }

function TAiBasicData.Ai: TAiClass;
begin
  Exit(TAiBasic);
end;

initialization
  RegisterSimpleSerializableObject(TAiBasic);
  RegisterSerializableData(TAiBasicData);

end.

