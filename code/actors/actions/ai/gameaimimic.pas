{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiMimic;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract, GameAiSingleActionAbstract, GameActionAbstract;

type
  TAiMimic = class(TAiSingleActionAbstract)
  public
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiMimicData = class(TAiSingleActionAbstractData)
  strict protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    // Curious: Boolean; ---> CanGuard: Boolean;
    function Ai: TAiClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameRandom, GameMap,  GameMonster, GameActor,
  GameActionIdle, GameActionMove, GameActionMoveAndActTarget;

{$IFDEF SafeActorTypecast}
{$DEFINE AiData:=(Data as TAiTrapData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE AiData:=TAiTrapData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TAiMimic.ChasePlayer(const ATimeout: Single);
begin
  // Do nothing
end;

procedure TAiMimic.InvestigateNoise(const AX, AY: Single);
begin
  // Do nothing
end;

procedure TAiMimic.Update(const SecondsPassed: Single);
begin
end;

{ TAiMimicData ------------------------------------------ }

function TAiMimicData.Ai: TAiClass;
begin
  Exit(TAiMimic);
end;

initialization
  RegisterSimpleSerializableObject(TAiMimic);
  RegisterSerializableData(TAiMimicData);

end.

