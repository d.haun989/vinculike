{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiTrap;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract, GameActionAbstract;

type
  TAiTrap = class(TAiAbstract)
  public
    procedure OnHit(const ATimeout: Single); override;
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiTrapData = class(TAiAbstractData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ActionData: TActionAbstractData;
    function Ai: TAiClass; override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameMap,
  GameActionOnTarget;

{$IFDEF SafeActorTypecast}
{$DEFINE AiData:=(Data as TAiTrapData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE AiData:=TAiTrapData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

procedure TAiTrap.OnHit(const ATimeout: Single);
begin
  raise EAiInvalidOperation.Create('Trap received OnHit');
end;

procedure TAiTrap.ChasePlayer(const ATimeout: Single);
begin
  // do nothing
end;

procedure TAiTrap.InvestigateNoise(const AX, AY: Single);
begin
  // do nothing
end;

procedure TAiTrap.Update(const SecondsPassed: Single);
var
  Target: TActor;
begin
  if not ParentActor.CurrentAction.InheritsFrom(AiData.ActionData.Action) then
    for Target in Map.CharactersOnThisLevel do // TODO: Mark should pick target/targets (attack area, not mark on target!) // or this is a good trigger condition for trap? // will force player to have "different class of traps" to set for monsters
      if Target.CanAct then
        if ParentActor.Collides(Target, 0) then
        begin
          ParentActor.CurrentAction := AiData.ActionData.Action.NewAction(ParentActor);
          ((ParentActor.CurrentAction as AiData.ActionData.Action) as TActionOnTarget).Data := AiData.ActionData;
          ((ParentActor.CurrentAction as AiData.ActionData.Action) as TActionOnTarget).Target := Target; // shouldn't be targeted action? Or optional targeted action
          ParentActor.CurrentAction.Start;
          Exit;
        end;
end;

{ TAiTrapData ------------------------------------------ }

procedure TAiTrapData.Validate;
begin
  //do nothing
end;

procedure TAiTrapData.Read(const Element: TDOMElement);
begin
  ActionData := TActionAbstractData.ReadClass(Element.Child('Action')) as TActionAbstractData;
end;

function TAiTrapData.Ai: TAiClass;
begin
  Exit(TAiTrap);
end;

destructor TAiTrapData.Destroy;
begin
  ActionData.Free;
  inherited Destroy;
end;

initialization
  RegisterSimpleSerializableObject(TAiTrap);
  RegisterSerializableData(TAiTrapData);

end.

