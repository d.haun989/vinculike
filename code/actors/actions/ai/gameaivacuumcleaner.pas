{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameAiVacuumCleaner;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameAiAbstract, GameAiSingleActionAbstract, GameActionAbstract;

type
  TAiVacuumCleaner = class(TAiSingleActionAbstract)
  public
    procedure ChasePlayer(const ATimeout: Single); override;
    procedure InvestigateNoise(const AX, AY: Single); override;
  public
    procedure Update(const SecondsPassed: Single); override;
  end;

  TAiVacuumCleanerData = class(TAiSingleActionAbstractData)
  strict protected
    //procedure Validate; override;
    //procedure Read(const Element: TDOMElement); override;
  public
    function Ai: TAiClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameViewGame, GameMap, GameMapTypes, GameActor, GameMonster, GameMapItem,
  GameActionIdle;

{$IFDEF SafeActorTypecast}
{$DEFINE AiData:=(Data as TAiTrapData)}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE AiData:=TAiTrapData(Data)}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TAiVacuumCleaner.ChasePlayer(const ATimeout: Single);
begin
  // Do nothing
end;

procedure TAiVacuumCleaner.InvestigateNoise(const AX, AY: Single);
begin
  // Do nothing
end;

procedure TAiVacuumCleaner.Update(const SecondsPassed: Single);
var
  I: Integer;
  Item, Target: TMapItem;
  Dist: TDistanceMapArray;
begin
  // TODO: all characters (or can't sleep in that situation altogether?)
  if ParentActor.Collides(ViewGame.CurrentCharacter, 1) then
    ViewGame.WakeUp(false, false);
  // Warning: VacuumCleaner doesn't timeout, so will chase the player forever until teleported, maybe good
  if ParentActor.CurrentAction is TActionIdle then
  begin
    //pick up all items
    I := 0;
    while I < Map.MapItemsList.Count do
    begin
      if ParentActor.Collides(Map.MapItemsList[I], 1) then
      begin
        ParentMonster.Loot.Add(Map.MapItemsList[I].Item);
        Map.MapItemsList[I].Item := nil;
        Map.MapItemsList.Delete(I);
      end else
        Inc(I);
    end;
    if ParentActor.Collides(ViewGame.CurrentCharacter, 1) then
      ViewGame.InvalidatePosition(ViewGame.CurrentCharacter);
    //choose what to do next
    if Map.MapItemsList.Count > 0 then
    begin
      AiState := asIdle;
      Dist := Map.DistanceMap(ParentActor);
      Target := nil;
      for Item in Map.MapItemsList do
        if (Target = nil) or (Dist[Target.LastTile] > Dist[Item.LastTile]) then
          Target := Item;
      ParentActor.MoveTo(Target.X, Target.Y);
      //MoveAndAct(); for now aimed at TActor derivatives, so unusable, yet
    end else
      DoWalkRandomly;
  end;
end;

{ TAiVacuumCleanerData ------------------------------------------ }

function TAiVacuumCleanerData.Ai: TAiClass;
begin
  Exit(TAiVacuumCleaner);
end;

initialization
  RegisterSimpleSerializableObject(TAiVacuumCleaner);
  RegisterSerializableData(TAiVacuumCleanerData);

end.

