{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMove;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  CastleVectors,
  GameSimpleSerializableObject,
  GameMapTypes,
  GameActionAbstract;

type
  TActionMove = class(TActionAbstract)
  strict private
    DestinationX, DestinationY: Single;
    DelayedMove: Boolean;
  public
    CurrentWaypoint: Integer;
    Waypoints: TWaypoints;
    MoveVector, MoveVectorNormalized: TVector2;
    MovePhase: Single;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    { Very rough approximation of remaining distance and remaining time of the route
      It's based ONLY on number of waypoints to pass and considers waypoints do diagonal movement
      I.e. this _should_ be max of the proper values
      It's used only for monster's AI and doesn't need higher accuracy for now }
    function RemainingDistance: Single;
    function RemainingTime: Single;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure MoveTo(const ToX, ToY: Single; const PreviousAction: TActionAbstract);
    procedure MoveTo(const Actor: TObject; const PreviousAction: TActionAbstract);
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

  { This is an internal action it doesn't need data }
  TActionMoveData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GameMonster, GameMap, GameLog, GameColors,
  GameActionIdle;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ENDIF}

function TActionMove.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionMove.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionMove.RemainingDistance: Single;
begin
  Exit(1.5 * (Waypoints.Count - CurrentWaypoint));
end;

function TActionMove.RemainingTime: Single;
begin
  Exit(1.5 * (Waypoints.Count - CurrentWaypoint) / ParentActor.Data.MovementSpeed);
end;

procedure TActionMove.Update(const SecondsPassed: Single);
var
  V: TVector2;
begin
  if DelayedMove then
    MoveTo(DestinationX, DestinationY, nil);

  MovePhase += SecondsPassed * ParentActor.GetSpeed;
  while MovePhase > MoveVector.Length do
  begin
    MovePhase -= MoveVector.Length;
    Inc(CurrentWaypoint);
    ParentActor.LastTileX := Trunc(Waypoints[CurrentWaypoint][0]);
    ParentActor.LastTileY := Trunc(Waypoints[CurrentWaypoint][1]);
    ParentActor.LastTile := ParentActor.LastTileX + Map.SizeX * ParentActor.LastTileY;
    if not Map.CanMove(ParentActor.LastTile) then
      raise ENavGridError.Create('Nav grid error in aaMove');
    ParentActor.X := Waypoints[CurrentWaypoint][0];
    ParentActor.Y := Waypoints[CurrentWaypoint][1];
    ParentActor.CenterX := ParentActor.X + ParentActor.HalfSize;
    ParentActor.CenterY := ParentActor.Y + ParentActor.HalfSize;

    ParentActor.UpdateVisible;
    if CurrentWaypoint = Pred(Waypoints.Count) then // Arrived at destination
    begin
      FreeAndNil(Waypoints);
      MovePhase := -1;
      OnFinished;
      Exit; // it may be not the last iteration of While loop
    end else
    begin
      MoveVector := Vector2(Waypoints[CurrentWaypoint + 1][0] - Waypoints[CurrentWaypoint][0], Waypoints[CurrentWaypoint + 1][1] - Waypoints[CurrentWaypoint][1]);
      MoveVectorNormalized := MoveVector.Normalize;
    end;
  end;
  V := MovePhase * MoveVectorNormalized;
  ParentActor.X := Waypoints[CurrentWaypoint][0] + V.X;
  ParentActor.Y := Waypoints[CurrentWaypoint][1] + V.Y;
  ParentActor.CenterX := ParentActor.X + ParentActor.HalfSize;
  ParentActor.CenterY := ParentActor.Y + ParentActor.HalfSize;
end;

class function TActionMove.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionMove).CurrentWaypoint := 0;
  (Result as TActionMove).MovePhase := 0;
end;


procedure TActionMove.MoveTo(const ToX, ToY: Single; const PreviousAction: TActionAbstract);
var
  Range: Integer;
  ToXInt, ToYInt, GotoX, GotoY: Int16;
  DestX, DestY: Single;
  CurrentPassableTiles: TMoveArray;

  procedure GetNearestClick;
  var
    DX, DY: Integer;
  begin
    for DX := -Range to Range do
      for DY := -Range to Range do
        if Sqr(DX) + Sqr(DY) <= Sqr(Range) then
        begin
          GotoX := ToXInt + DX;
          GotoY := ToYInt + DY;
          if (GotoX >= 0) and (GotoX <= Map.PredSizeX) and (GotoY >= 0) and (GotoY <= Map.PredSizeY) and CurrentPassableTiles[GotoX + Map.SizeX * GotoY] then
            Exit;
        end;
  end;

var
  NextTileX, NextTileY: Int16;
begin
  if not Map.PassableTiles[ParentActor.PredSize][ParentActor.LastTile] then
    DebugWarning('MoveTo origin is unsafe', []);
  DelayedMove := false;

  CurrentPassableTiles := ParentActor.PassableTiles;
  ToXInt := Round(ToX); // note, not TRUNC, because we want "nearest" tile
  ToYInt := Round(ToY);
  GotoX := ToXInt;
  GotoY := ToYInt;
  Range := 0;
  while (GotoX < 0) or (GotoX > Map.PredSizeX) or (GotoY < 0) or (GotoY > Map.PredSizeY) or not CurrentPassableTiles[GotoX + Map.SizeX * GotoY] do
  begin
    GetNearestClick;
    Inc(Range);
  end;

  if not (PreviousAction is TActionMove) then
  begin
    NextTileX := ParentActor.LastTileX;
    NextTileY := ParentActor.LastTileY;
    if not Map.PassableTiles[ParentActor.PredSize][NextTileX + Map.SizeX * NextTileY] then
      DebugWarning('Last tile is broken', []);
  end else
  if (PreviousAction is TActionMove) and (TActionMove(PreviousAction).Waypoints = nil) then
  begin
    ShowError('%s is trying to to move, but previous action had %s.Waypoints = nil', [ParentActor.Data.Id, PreviousAction.ClassName]);
    NextTileX := ParentActor.LastTileX;
    NextTileY := ParentActor.LastTileY;
    if not Map.PassableTiles[ParentActor.PredSize][NextTileX + Map.SizeX * NextTileY] then
      DebugWarning('Last tile is broken', []);
  end else
  begin
    NextTileX := Trunc(TActionMove(PreviousAction).Waypoints[TActionMove(PreviousAction).CurrentWaypoint + 1][0]);
    NextTileY := Trunc(TActionMove(PreviousAction).Waypoints[TActionMove(PreviousAction).CurrentWaypoint + 1][1]);
    if not Map.PassableTiles[ParentActor.PredSize][NextTileX + Map.SizeX * NextTileY] then
      DebugWarning('Next waypoint is broken', []);
  end;

  {if (GotoX = ToXInt) and (GotoY = ToYInt) then   // Unfortunately this creates "wavy" movement when MoveAndActTarget, as target moves, ToX/ToY change and sometimes "new path" requires to go backwards, also due to rounding
  begin
    DestX := ToX;
    DestY := ToY;
  end else}
  begin
    DestX := GotoX;
    DestY := GotoY;
  end;
  Waypoints := Map.GetWaypoints(Map.GenerateDijkstra(CurrentPassableTiles, GotoX, GotoY, 1024, Parent is TMonster, NextTileX, NextTileY, true), (ParentActor.LastTileX <> NextTileX) or (ParentActor.LastTileY <> NextTileY), NextTileX, NextTileY, ParentActor.X, ParentActor.Y, DestX, DestY);
  {if Waypoints = nil then
  begin
    OnFinished;
    Exit;
  end;}

  // Map.GetWaypoints is guaranteed to give at least 2 waypoints!
  // Waypoint[0] === (X,Y)
  MoveVector := Vector2(Waypoints[1][0] - ParentActor.X, Waypoints[1][1] - ParentActor.Y);
  MoveVectorNormalized := MoveVector.Normalize;
end;

procedure TActionMove.MoveTo(const Actor: TObject; const PreviousAction: TActionAbstract);
begin
  // TODO
  MoveTo(TActor(Actor).CenterX, TActor(Actor).CenterY, PreviousAction);
end;

procedure TActionMove.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  // To avoid painful saving everything we save... only destination.
  Element.AttributeSet('DestinationX', Waypoints[Pred(Waypoints.Count)][0]);
  Element.AttributeSet('DestinationY', Waypoints[Pred(Waypoints.Count)][1]);
end;

procedure TActionMove.Load(const Element: TDOMElement);
begin
  inherited;
  DestinationX := Element.AttributeSingle('DestinationX');
  DestinationY := Element.AttributeSingle('DestinationY');
  DelayedMove := true;
  //MoveTo(DestinationX, DestinationY, nil); Parent is set at this moment BUT Parent.Data is still nil
end;

destructor TActionMove.Destroy;
begin
  FreeAndNil(Waypoints);
  inherited Destroy;
end;

{ TActionMoveData -------------------------------}

function TActionMoveData.Action: TActionClass;
begin
  Exit(TActionMove);
end;

initialization
  RegisterSimpleSerializableObject(TActionMove);
  RegisterSerializableData(TActionMoveData);

end.

