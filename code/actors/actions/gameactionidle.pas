{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionIdle;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract;

type
  TActionIdle = class(TActionAbstract)
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    //nothing special to save/load
  end;

  { Idle is an internal action and it doesn't need data
    here just for consistency, maybe will never be used }
  TActionIdleData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  GameSimpleSerializableObject, GameSerializableData;

function TActionIdle.NoiseMultiplier: Single;
begin
  Exit(0.9);
end;

function TActionIdle.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionIdle.Update(const SecondsPassed: Single);
begin
  //do nothing?
end;

{ TActionIdleData ------------------------------ }

function TActionIdleData.Action: TActionClass;
begin
  Exit(TActionIdle);
end;

initialization
  RegisterSimpleSerializableObject(TActionIdle);
  RegisterSerializableData(TActionIdleData);

end.

