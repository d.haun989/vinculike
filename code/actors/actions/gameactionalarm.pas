{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlarm;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionAlarmAbstract;

type
  TActionAlarm = class(TActionAlarmAbstract)
  strict protected
    // nobody's asking for now, but for consistency
    procedure Perform; override;
  public
    function CanStop: Boolean; override;
  end;

  TActionAlarmData = class(TActionAlarmAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

function TActionAlarm.CanStop: Boolean;
begin
  Exit(true);
end;

procedure TActionAlarm.Perform;
begin
  inherited Perform;
  ParentActor.Vital := -1;
  ParentActor.ActionFinished;
end;

{ TActionAlarmData --------------------------------------- }

function TActionAlarmData.Action: TActionClass;
begin
  Exit(TActionAlarm);
end;

initialization
  RegisterSimpleSerializableObject(TActionAlarm);
  RegisterSerializableData(TActionAlarmData);

end.

