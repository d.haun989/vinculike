{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerStruggleUnequipItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSimpleSerializableObject,
  GameApparelSlots,
  GameActionAbstract;

type
  TActionPlayerStruggleUnequipItem = class(TActionAbstract)
  strict private const
    VigorPerSecond = Single(20);
    VigorToProgress = Single(5);
  strict private
    Phase: Single;
    NextProgressSound: Single;
    AccumulatedXP: Single;
  public
    // WARNING!!! item may be force-unequipped/force-equipped here!, resulting in hard-to-find bugs
    ItemSlot: TApparelSlot;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionPlayerStruggleUnequipItemData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableData,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation, GameLog, GameColors,
  GameViewGame, GameRandom;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TActionPlayerStruggleUnequipItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('ItemSlot', ApparelSlotToStr(ItemSlot));
end;

procedure TActionPlayerStruggleUnequipItem.Load(const Element: TDOMElement);
begin
  inherited;
  ItemSlot := StrToApparelSlot(Element.AttributeString('ItemSlot'));
end;

function TActionPlayerStruggleUnequipItem.NoiseMultiplier: Single;
begin
  Exit(1.6);
end;

function TActionPlayerStruggleUnequipItem.NoiseAddition: Single;
begin
  if ParentPlayer.Inventory.Equipped[ItemSlot] = nil then
    Exit(0.0) // just in case
  else
    Exit(ParentPlayer.Inventory.Equipped[ItemSlot].ItemData.Noise); // total 2.6x multiplier
end;

procedure TActionPlayerStruggleUnequipItem.Start;
begin
  inherited Start;
  Phase := 0;
  NextProgressSound := 0;
  ShowLog(GetTranslation('ActorStartsStrugglingItemLog'), [ParentPlayer.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName], ColorLogActionStart);
end;

procedure TActionPlayerStruggleUnequipItem.Update(const SecondsPassed: Single);
var
  VigorExpense: Single;
begin
  if ParentPlayer.Inventory.Equipped[ItemSlot] = nil then // if someone else unequipped the item for us
  begin
    ShowLog(GetTranslation('ActorFreeFromBondageItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName, EquipSlotToHumanReadableString(ItemSlot)], ColorLogInventory);
    OnFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (Phase > NextProgressSound) and (ParentPlayer.Inventory.Equipped[ItemSlot].Durability > VigorPerSecond / 2 / VigorToProgress) and (ParentPlayer.Vigor > VigorPerSecond / 2) then
  begin
    if Rnd.Random < 0.3 then
      ParentPlayer.PlayGruntSound
    else
      ParentPlayer.PlayBreathSound;
    NextProgressSound += 0.5 + 0.5 * ParentPlayer.Vigor / ParentPlayer.PlayerCharacterData.MaxVigor + 0.5 * Rnd.Random;
  end;

  VigorExpense := SecondsPassed * VigorPerSecond;
  if ParentPlayer.Vigor > VigorExpense then
  begin
    ParentPlayer.DegradeVigor(VigorExpense);
    ParentPlayer.Inventory.Equipped[ItemSlot].Durability -= VigorExpense / VigorToProgress;
    AccumulatedXP += VigorExpense / VigorToProgress;
    if ParentPlayer.Inventory.Equipped[ItemSlot].Durability <= 0 then
    begin
      ShowLog(GetTranslation('ActorBreaksBondageItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName, EquipSlotsToHumanReadableString(ParentPlayer.Inventory.Equipped[ItemSlot].Data.EquipSlots)], ColorLogInventory);
      Sound(ParentPlayer.Inventory.Equipped[ItemSlot].ItemData.SoundUnequip);
      ParentPlayer.Inventory.UnequipAndDisintegrate(ItemSlot, false);
      ParentPlayer.Experience.AddExperience(AccumulatedXP);
      ParentPlayer.PlayGruntSound;
      AccumulatedXP := 0;
      OnFinished;
      Exit;
    end;
  end else
  begin
    ShowLog(GetTranslation('ActorFailBreakBondageItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName, EquipSlotsToHumanReadableString(ParentPlayer.Inventory.Equipped[ItemSlot].Data.EquipSlots)], ColorLogNotEnoughVigor);
    ParentPlayer.Experience.AddExperience(AccumulatedXP);
    ParentPlayer.PlayBreathSound;
    AccumulatedXP := 0;
    OnFinished;
    Exit;
  end;
end;

class function TActionPlayerStruggleUnequipItem.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
end;

destructor TActionPlayerStruggleUnequipItem.Destroy;
begin
  if (Parent <> nil) and (ParentPlayer.Inventory.Equipped[ItemSlot] <> nil) and (ParentActor.Data <> nil) then
  begin
    ShowLog(GetTranslation('ActorCancelStruggleItemLog'), [ParentActor.Data.DisplayName, ParentPlayer.Inventory.Equipped[ItemSlot].Data.DisplayName], ColorLogCancel);
    ParentPlayer.Experience.AddExperience(AccumulatedXP);
    //OnFinished; // we most likely already in OnFinished so endless loop
  end;
  inherited Destroy;
end;

{ TActionPlayerStruggleUnequipItemData -------------------------------}

function TActionPlayerStruggleUnequipItemData.Action: TActionClass;
begin
  Exit(TActionPlayerStruggleUnequipItem);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerStruggleUnequipItem);
  RegisterSerializableData(TActionPlayerStruggleUnequipItemData);

end.
