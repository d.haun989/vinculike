{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlarmAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionOnTarget;

const
  SqrAlarmRange = Sqr(48.0);

type
  TActionAlarmAbstract = class abstract(TActionOnTarget) // TODO: not OnTarget
  strict protected
    procedure Perform; virtual;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
  end;
  TActionAlarmAbstractData = class abstract(TActionOnTargetData)
    // alarm range
  end;

implementation
uses
  SysUtils,
  GameSimpleSerializableObject,
  GameSounds, GameMap, GameColors, GameMonster, GameActor, GameLog, GameParticle;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

function TActionAlarmAbstract.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionAlarmAbstract.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionAlarmAbstract.Perform;
var
  AMonster: TMonster;
begin
  ParentActor.PlayAttackSound;
  ShowLog('%s emits a loud noise.', [ParentActor.Data.DisplayName], ColorLogTrap);
  NewParticle(ParentActor.CenterX, ParentActor.CenterY, 'ALARM', ColorParticleSpawn);
  for AMonster in Map.MonstersList do
    if AMonster.CanAct and (Sqr(AMonster.X - ParentActor.X) + Sqr(AMonster.Y - ParentActor.Y) <= SqrAlarmRange) then
      AMonster.Ai.InvestigateNoise(ParentActor.CenterX, ParentActor.CenterY);
end;

procedure TActionAlarmAbstract.Update(const SecondsPassed: Single);
begin
  //inherited: no inherited because no "on-target" TODO

  if not ParentActor.CanAct then
  begin
    OnFinished;
    Exit;
  end;

  Perform;
end;

initialization
end.

