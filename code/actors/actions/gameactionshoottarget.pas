{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionShootTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameProjectileAbstract,
  GameActionAbstract, GameMarkAbstract, GameActionOnTarget;

type
  TActionShootTarget = class(TActionOnTarget)
  strict private
    Phase: Single;
    IsWarmUp: Boolean;
    procedure Perform;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
  end;

  TActionShootTargetData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleVectors,
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GameMonsterData, GameMap, GameRandom,
  GameMonster, GamePlayerCharacter;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE ActionData:=(Data as TActionAbstractData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}

function TActionShootTarget.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionShootTarget.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionShootTarget.CanStop: Boolean;
begin
  Exit(IsWarmUp);
end;

procedure TActionShootTarget.Perform;
var
  Projectile: TProjectileAbstract;
begin
  Projectile := ActionData.MarkData.Mark.Create as TProjectileAbstract;

  Projectile.Parent := ParentActor;
  Projectile.Data := ActionData.MarkData;
  Projectile.SetSize(Projectile.Data.Size);
  Projectile.MoveVector := Vector2(TargetActor.CenterX - ParentActor.CenterX, TargetActor.CenterY - ParentActor.CenterY).Normalize;
  Projectile.CenterX := ParentActor.CenterX;
  Projectile.CenterY := ParentActor.CenterY;
  Projectile.X := Projectile.CenterX - Projectile.HalfSize;
  Projectile.Y := Projectile.CenterY - Projectile.HalfSize;
  Projectile.LastTileX := Trunc(Projectile.X);
  Projectile.LastTileY := Trunc(Projectile.Y);
  Projectile.LastTile := Projectile.LastTileX + Map.SizeX * Projectile.LastTileY;
  Map.MarksList.Add(Projectile);
end;

procedure TActionShootTarget.Update(const SecondsPassed: Single);
begin
  inherited;

  if not TargetActor.CanAct then //CanBeInteractedWith : TODO
  begin
    OnFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (IsWarmUp) and (Phase >= ActionData.WarmUpTime) then
  begin
    Perform;
    Phase -= ActionData.WarmUpTime;
    IsWarmUp := false;
  end else
  if (not IsWarmUp) and (Phase >= ActionData.CoolDownTime) then
  begin
    Phase -= ActionData.CoolDownTime;
    IsWarmUp := true;
  end;
end;

class function TActionShootTarget.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionShootTarget).Phase := 0;
  (Result as TActionShootTarget).IsWarmUp := true;
end;

{ TActionShootTargetData -------------------------------}

function TActionShootTargetData.Action: TActionClass;
begin
  Exit(TActionShootTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionShootTarget);
  RegisterSerializableData(TActionShootTargetData);

end.

