{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionAlarmFlee;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionAlarmAbstract;

type
  TActionAlarmFlee = class(TActionAlarmAbstract)
  strict private
    Phase: Single;
  strict protected
    procedure Perform; override;
  public
    function CanStop: Boolean; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionAlarmFleeData = class(TActionAlarmAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameViewGame, GameMonster;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE ActionData:=(Data as TActionAbstractData)}
{$ELSE}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}

procedure TActionAlarmFlee.Perform;
begin
  if Phase < ActionData.WarmUpTime then
    Exit;
  inherited;
  ViewGame.WakeUp(true, false);
  ParentMonster.Ai.AiFlee := true;
end;

function TActionAlarmFlee.CanStop: Boolean;
begin
  Exit(Phase > ActionData.WarmUpTime);
end;

procedure TActionAlarmFlee.Start;
begin
  inherited Start;
  Phase := 0;
end;

procedure TActionAlarmFlee.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  inherited Update(SecondsPassed);
end;

{ TActionAlarmFleeData -------------------------------- }

function TActionAlarmFleeData.Action: TActionClass;
begin
  Exit(TActionAlarmFlee);
end;

initialization
  RegisterSimpleSerializableObject(TActionAlarmFlee);
  RegisterSerializableData(TActionAlarmFleeData);

end.

