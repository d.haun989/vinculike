{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerRescue;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSimpleSerializableObject,
  GameMonster,
  GameActionAbstract, GameActionOnTarget;

const
  XpPerRescue = 50;
  XpPerDungeonLevel = 10;
  XpPerRescueeLevel = 5;

type
  TActionPlayerRescue = class(TActionOnTarget)
  public const
    Duration = Single(7);
  strict private
    NextProgressSound: Single;
    Phase: Single;
    procedure Perform;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionPlayerRescueData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation,
  GameViewGame, GameInventoryItem, GameItemDatabase, GameMap, GameRandom;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TActionPlayerRescue.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
end;

procedure TActionPlayerRescue.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  Target := ObjectByReferenceId(Element.AttributeQWord('Target')) as TMonster; // TMonsters are deserealized before Player characters, so this should be fine
end;

function TActionPlayerRescue.NoiseMultiplier: Single;
begin
  Exit(1.5);
end;

function TActionPlayerRescue.NoiseAddition: Single;
begin
  Exit(48.0);
end;

procedure TActionPlayerRescue.Perform;
var
  RemainingCaptured: Integer;
begin
  ShowLog(GetTranslation('PlayerActionRescueSuccess1'), [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogCharacterRescued);
  ShowLog(GetTranslation('PlayerActionRescueSuccess2'), [], ColorLogCharacterRescued);
  ShowLog(GetTranslation('PlayerActionRescueSuccess3'), [], ColorLogCharacterRescued);
  ShowLog(GetTranslation('PlayerActionRescueSuccess4'), [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogCharacterRescued);
  Sound('rescue_success'); // TODO: Temporary
  ParentPlayer.Experience.AddExperience(XpPerRescue + XpPerRescueeLevel * TargetPlayer.Experience.Level + XpPerDungeonLevel * Map.CurrentDepth);
  // TODO: fail chance
  TargetPlayer.IsCaptured := false;
  TargetPlayer.AtMap := 0;
  TargetPlayer.Reset;
  { TODO: this is a workaround. The problem is that after the player character got captured
    the same frame another attack may hit and equip some bondage item which will not be
    disintegrated in GetCaptured.
    Most likely this should be "fixed" by having the captured character have a specific restraints set
    when generating on level, so this workaround should be ok as temporary system }
  TargetPlayer.Inventory.UnequipAndDisintegrateEverything;
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['metal_collar']));
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['sandals']));
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['rags-top']));
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['rags-bottom']));
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['stick']));
  Target := nil;
  // TODO: This may cause "Collection Modified" bug, as most likely this action happens in PlayerCharacter.Update
  Map.CacheCharactersOnThisLevel;
  RemainingCaptured := Map.CapturedCharactersAtLevel;
  if RemainingCaptured = 1 then
    ShowLog('There is one more character imprisoned on this level', [RemainingCaptured], ColorLogCapturedCharacterHere)
  else
  if RemainingCaptured > 1 then
    ShowLog('There are %d characters remain imprisoned on this level', [RemainingCaptured], ColorLogCapturedCharacterHere);
end;

procedure TActionPlayerRescue.Start;
begin
  inherited Start;
  NextProgressSound := 0;
  ShowLog(GetTranslation('PlayerActionRescueStart'), [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TActionPlayerRescue.Duration], ColorLogActionStart);
end;

procedure TActionPlayerRescue.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if Phase > NextProgressSound then
  begin
    Sound('rescue_progress'); // TODO: Temporary
    NextProgressSound += Rnd.Random;
  end;
  if Phase > Duration then
  begin
    Perform;
    OnFinished;
    Exit;
  end
end;

class function TActionPlayerRescue.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerRescue).Phase := 0;
end;

destructor TActionPlayerRescue.Destroy;
begin
  if (Target <> nil) and (TargetPlayer.Data <> nil) and (Parent <> nil) and (ParentActor.Data <> nil) then // ParentActor.Data = nil may happen for PlayerCharacter when it's being freed. Maybe: rework it safer (TODO)
  begin
    ShowLog(GetTranslation('PlayerActionRescueCancel'), [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogCancel);
    //OnFinished; // we most likely already in OnFinished so endless loop
  end;
  inherited Destroy;
end;

{ TActionPlayerRescueData -------------------------------}

function TActionPlayerRescueData.Action: TActionClass;
begin
  Exit(TActionPlayerRescue);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerRescue);
  RegisterSerializableData(TActionPlayerRescueData);

end.
