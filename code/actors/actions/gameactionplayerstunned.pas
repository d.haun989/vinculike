{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerStunned;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActionAbstract;

type
  TActionPlayerStunned = class(TActionAbstract)
  strict private
    Phase: Single;
    Duration: Single;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
  public
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    class function NewAction(const AParent: TObject; const StunDuration: Single): TActionAbstract;
    destructor Destroy; override;
  end;

  TActionPlayerStunnedData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameTranslation, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TActionPlayerStunned.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
end;

procedure TActionPlayerStunned.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
end;

function TActionPlayerStunned.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionPlayerStunned.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionPlayerStunned.CanStop: Boolean;
begin
  // don't use inherited
  Result := false;
  ShowLog('%s is stunned and cannot move', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughVigor);
end;

procedure TActionPlayerStunned.Update(const SecondsPassed: Single);
begin
  //inherited; // abstract
  Phase += SecondsPassed;
  if Phase > Duration then
  begin
    OnFinished;
    Exit;
  end
end;

class function TActionPlayerStunned.NewAction(
  const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerStunned).Phase := 0;
end;

class function TActionPlayerStunned.NewAction(
  const AParent: TObject; const StunDuration: Single): TActionAbstract;
begin
  Result := NewAction(AParent);
  (Result as TActionPlayerStunned).Duration := StunDuration;
end;

destructor TActionPlayerStunned.Destroy;
begin
  if (Parent <> nil) and (ParentPlayer.Data <> nil) then
    ShowLog('%s regains control of her limbs', [ParentPlayer.Data.DisplayName], ColorLogCancel);
  inherited Destroy;
end;

{ TActionPlayerStunnedData -------------------------------}

function TActionPlayerStunnedData.Action: TActionClass;
begin
  Exit(TActionPlayerStunned);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerStunned);
  RegisterSerializableData(TActionPlayerStunnedData);

end.

