{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionSpawnMinions;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActionAbstract, GameActionOnTarget;

type
  TActionSpawnMinions = class(TActionOnTarget) // TODO: Not on target?
  strict private
    IsWarmUp: Boolean;
    Phase: Single;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  strict private
    procedure Perform;
  public
    function CanStop: Boolean; override;
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionSpawnMinionsData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameMap, GameRandom, GameActor, GameMonster, GameActorData, GameMonsterData,
  GameSounds, GameLog, GameParticle, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE ActionData:=(Data as TActionAbstractData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}

procedure TActionSpawnMinions.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('IsWarmUp', IsWarmUp);
end;

procedure TActionSpawnMinions.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  IsWarmUp := Element.AttributeBoolean('IsWarmUp');
end;

procedure TActionSpawnMinions.Perform;
var
  I: Integer;
  AMonster: TMonster;
  MonsterKind: TActorData;
  SX, SY: Int16;
  SpawnList: TActorDataList;
begin
  SpawnList := TActorDataList.Create(false);
  SpawnList.Add(MonstersDataDictionary['wisp']);

  if SpawnList.Count = 0 then
  begin
    ShowError('Cannot spawn minions: SpawnList is empty', []);
    Exit;
  end;

  Sound(ParentMonster.MonsterData.SoundAttack);
  MonsterKind := SpawnList[Rnd.Random(SpawnList.Count)];
  for I := 0 to 1 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / (MonsterKind as TMonsterData).Danger / 2)) + 1) do // 2 - 2 on lvl.1 2 - 6 on lvl.25
  begin
    AMonster := TMonster.Create;
    AMonster.Data := MonsterKind;
    AMonster.Reset;
    repeat
      SX := ParentActor.LastTileX - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
      SY := ParentActor.LastTileY - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
    until (SX > 0) and (SY > 0) and (SX < Map.PredSizeX - AMonster.PredSize) and (SY < Map.PredSizeY - AMonster.PredSize) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
    AMonster.Teleport(SX, SY);
    AMonster.Ai.Guard := false;
    AMonster.Ai.ChasePlayer(30);
    Map.MonstersList.Add(AMonster);
  end;
  ShowLog('%s blinks and several shining orbs detach from it', [ParentActor.Data.DisplayName], ColorLogTrap);
  NewParticle(ParentActor.CenterX, ParentActor.CenterY, 'SPAWN', ColorParticleSpawn);

  SpawnList.Free;
end;

function TActionSpawnMinions.CanStop: Boolean;
begin
  Exit(IsWarmUp);
end;

function TActionSpawnMinions.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionSpawnMinions.NoiseAddition: Single;
begin
  Exit(0.0);
end;

class function TActionSpawnMinions.NewAction(
  const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionSpawnMinions).Phase := 0;
  (Result as TActionSpawnMinions).IsWarmUp := true;
end;

procedure TActionSpawnMinions.Update(const SecondsPassed: Single);
begin
  if not ParentActor.CanAct then
  begin
    OnFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if IsWarmUp and (Phase >= ActionData.WarmUpTime) then
  begin
    Phase -= ActionData.WarmUpTime;
    IsWarmUp := false;
    Perform;
  end else
  if not IsWarmUp and (Phase >= ActionData.CoolDownTime) then
  begin
    IsWarmUp := true;
    Phase -= ActionData.CoolDownTime;
  end;

  inherited;
end;

{ TActionSpawnMinionsData -------------------------------}

function TActionSpawnMinionsData.Action: TActionClass;
begin
  Exit(TActionSpawnMinions);
end;

initialization
  RegisterSimpleSerializableObject(TActionSpawnMinions);
  RegisterSerializableData(TActionSpawnMinionsData);

end.

