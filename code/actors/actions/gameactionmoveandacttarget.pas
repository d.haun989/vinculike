{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionMoveAndActTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSimpleSerializableObject,
  GameActionAbstract, GameActionOnTarget;

type
  TActionMoveAndActTarget = class(TActionOnTarget)
  strict private
    LastTargetX, LastTargetY: Int16;
    CurrentSubAction: TActionAbstract;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    destructor Destroy; override;
  end;

  TActionMoveAndActTargetData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameActor, GameMap, GameRandom, GameLog,
  GameActionMove;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE ActionData:=(Data as TActionAbstractData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}

procedure TActionMoveAndActTarget.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  if CurrentSubAction <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    CurrentSubAction.Save(Element.CreateChild('SubAction'));
end;

procedure TActionMoveAndActTarget.Load(const Element: TDOMElement);
begin
  inherited;
  CurrentSubAction := TActionAbstract.LoadClass(Element.ChildElement('SubAction', true)) as TActionAbstract;
  raise EActionLoadNotImplemented.Create('Loading TActionMoveAndActTarget is not implemented yet as the action data is not serialized!');
end;

function TActionMoveAndActTarget.NoiseMultiplier: Single;
begin
  if CurrentSubAction <> nil then
    Exit(CurrentSubAction.NoiseMultiplier)
  else
    Exit(1.0);
end;

function TActionMoveAndActTarget.NoiseAddition: Single;
begin
  if CurrentSubAction <> nil then
    Exit(CurrentSubAction.NoiseAddition)
  else
    Exit(0.0);
end;

procedure TActionMoveAndActTarget.Update(const SecondsPassed: Single);
var
  InRange: Boolean;
  CanSee: Boolean;
  SubAction: TActionAbstract;
begin
  inherited;

  if Data = nil then
  begin
    ShowError('Data = nil in TActionMoveAndActTarget.Update for %s', [ParentActor.Data.DisplayName]);
    OnFinished;
    Exit;
  end;

  if not TargetActor.CanBeInteractedWith then
  begin
    OnFinished;
    Exit;
  end;

  InRange := Sqr(TargetActor.CenterX - ParentActor.CenterX) +
             Sqr(TargetActor.CenterY - ParentActor.CenterY) <= ActionData.SqrAttackRange;
  if ActionData.TemporaryRequiresFullViewOfTarget then
    CanSee := ParentActor.LineOfSightMore(TargetActor) // raycast from "x" in the center
  else
    CanSee := ParentActor.LineOfSight(Trunc(TargetActor.CenterX), Trunc(TargetActor.CenterY)); // simple raycast in 1 ray from center to center
  if InRange and CanSee and not (CurrentSubAction is ActionData.Action) then     // nil or TActionMove
  begin
    FreeAndNil(CurrentSubAction);
    CurrentSubAction := ActionData.Action.NewAction(ParentActor);
    ((CurrentSubAction as ActionData.Action) as TActionOnTarget).Data := Data;
    ((CurrentSubAction as ActionData.Action) as TActionOnTarget).Target := TargetActor;
    CurrentSubAction.Start;
  end else
  if not InRange or not CanSee then
  begin
    if (CurrentSubAction = nil) or
      ((CurrentSubAction is ActionData.Action) and CurrentSubAction.CanStop) or
      ((CurrentSubAction is TActionMove) and ((LastTargetX <> TargetActor.LastTileX) or (LastTargetY <> TargetActor.LastTileY))) then
    begin
      SubAction := TActionMove.NewAction(Parent);
      LastTargetX := TargetActor.LastTileX;
      LastTargetY := TargetActor.LastTileY;
      TActionMove(SubAction).MoveTo(TargetActor.LastTileX, TargetActor.LastTileY, CurrentSubAction);
      FreeAndNil(CurrentSubAction);
      CurrentSubAction := SubAction;
      CurrentSubAction.Start;
    end;
  end;
  CurrentSubAction.Update(SecondsPassed);
end;

destructor TActionMoveAndActTarget.Destroy;
begin
  FreeAndNil(CurrentSubAction);
  inherited Destroy;
end;

{ TActionMoveAndActTargetData ---------------------------}

function TActionMoveAndActTargetData.Action: TActionClass;
begin
  Exit(TActionMoveAndActTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionMoveAndActTarget);
  RegisterSerializableData(TActionMoveAndActTargetData);

end.

