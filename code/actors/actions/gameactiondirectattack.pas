{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionDirectAttack;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSimpleSerializableObject,
  GameActionAbstract, GameActionOnTarget;

type
  TActionDirectAttack = class(TActionOnTarget)
  strict private
    Phase: Single;
    IsWarmUp: Boolean;
    procedure Perform;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    function CanStop: Boolean; override;
  end;

  TActionDirectAttackData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableData,
  GameViewGame, GameActor, GameMonsterData, GamePlayerCharacter, GameRandom, GameLog, GameColors,
  GameInventoryItem, GameItemData, GameApparelSlots, GameTranslation, GameSounds,
  GameMonster, GameMap;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE ActionData:=(Data as TActionAbstractData)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE ActionData:=TActionAbstractData(Data)}
{$ENDIF}

procedure TActionDirectAttack.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('IsWarmUp', IsWarmUp);
end;

procedure TActionDirectAttack.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  IsWarmUp := Element.AttributeBoolean('IsWarmUp');
end;

function TActionDirectAttack.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionDirectAttack.NoiseAddition: Single;
begin
  if ParentPlayer.Inventory.Equipped[esWeapon] <> nil then
    Exit(3 * ParentPlayer.Inventory.Equipped[esWeapon].ItemData.Noise)
  else
    Exit(1.0);
end;

procedure TActionDirectAttack.Perform;
var
  DoDamage: Single;
  M: TMonster;
  AccumulatedExperience: Integer;
begin
  DoDamage := ParentActor.GetDamage;
  if (TargetActor as TMonster).MonsterData.Chest and not (TargetActor as TMonster).MonsterData.Mimic then
  begin
    Sound('open_chest');
    TargetActor.Hit(DoDamage);
  end else
  begin
    ParentActor.PlayAttackSound;
    AccumulatedExperience := 0;
    for M in Map.MonstersList do
      if M.CanAct and ((not M.MonsterData.Chest) or M.MonsterData.Mimic) and not M.MonsterData.Trap and M.IsHere(TargetActor.CenterX, TargetActor.CenterY, 0.5) then
      begin
        if Parent = ViewGame.CurrentCharacter then
          ShowLog(GetTranslation('ActorAttacksTargetLog'), [ParentActor.Data.DisplayName, M.Data.DisplayName, DoDamage], ColorLogAttack);
        M.Hit(DoDamage);
        if M.CanAct and (Parent is TPlayerCharacter) then
        begin
          M.Ai.AiFlee := false;
          M.Ai.OnHit(15);
        end;
        if not M.CanAct and (Parent is TPlayerCharacter) then
          AccumulatedExperience += M.MonsterData.XP;
      end;
    if AccumulatedExperience > 0 then
      ParentPlayer.Experience.AddExperience(AccumulatedExperience);

    ParentActor.DamageWeapon;
  end;
end;

procedure TActionDirectAttack.Update(const SecondsPassed: Single);
begin
  inherited;

  Phase += SecondsPassed;
  if (IsWarmUp) and (Phase >= ActionData.WarmUpTime) then
  begin
    Perform;
    Phase -= ActionData.WarmUpTime;
    IsWarmUp := false;
  end else
  if (not IsWarmUp) and (Phase >= ActionData.CoolDownTime) then
  begin
    Phase -= ActionData.CoolDownTime;
    IsWarmUp := true;
  end;
  if not TargetActor.CanAct then
  begin
    OnFinished;
    Exit;
  end;
end;

class function TActionDirectAttack.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited;
  (Result as TActionDirectAttack).Phase := 0;
  (Result as TActionDirectAttack).IsWarmUp := true;
end;

function TActionDirectAttack.CanStop: Boolean;
begin
  Result := true;
end;

{ TActionDirectAttackData ------------------------------ }

function TActionDirectAttackData.Action: TActionClass;
begin
  Exit(TActionDirectAttack);
end;

initialization
  RegisterSimpleSerializableObject(TActionDirectAttack);
  RegisterSerializableData(TActionDirectAttackData);

end.

