{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionTrapSpawnMonsters;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameActionAbstract, GameActionOnTarget;

type
  TActionTrapSpawnMonsters = class(TActionOnTarget) // TODO: not OnTarget
  strict private
    procedure Perform;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
  end;

  TActionTrapSpawnMonstersData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameMap, GameRandom, GameActor, GameMonster, GameActorData, GameMonsterData,
  GameSounds, GameLog, GameParticle, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

procedure TActionTrapSpawnMonsters.Perform;
var
  I: Integer;
  AMonster: TMonster;
  MonsterKind: TActorData;
  SX, SY: Int16;
  SpawnList: TActorDataList;
begin
  SpawnList := TActorDataList.Create(false);
  for I := 0 to Pred(Map.PatrolList.Count) do
    if not (Map.PatrolList[I] as TMonsterData).Rare then
      SpawnList.Add(Map.PatrolList[I]);

  if SpawnList.Count = 0 then
  begin
    ShowError('Trap cannot spawn: SpawnList is empty', []);
    Exit;
  end;

  Sound('spawner');
  MonsterKind := SpawnList[Rnd.Random(SpawnList.Count)];
  for I := 0 to 1 + Rnd.Random(Round(Sqrt(Map.CurrentDepth / (MonsterKind as TMonsterData).Danger / 2)) + 1) do // 2 - 2 on lvl.1 2 - 6 on lvl.25
  begin
    AMonster := TMonster.Create;
    AMonster.Data := MonsterKind;
    AMonster.Reset;
    repeat
      SX := ParentActor.LastTileX - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
      SY := ParentActor.LastTileY - (3 + Map.CurrentDepth div 3) + Rnd.Random(3 + (3 + Map.CurrentDepth div 3) * 2);
    until (SX > 0) and (SY > 0) and (SX < Map.PredSizeX - AMonster.PredSize) and (SY < Map.PredSizeY - AMonster.PredSize) and Map.PassableTiles[AMonster.PredSize][SX + Map.SizeX * SY];
    AMonster.Teleport(SX, SY);
    AMonster.Ai.Guard := false;
    AMonster.Ai.ChasePlayer(30);
    Map.MonstersList.Add(AMonster);
  end;

  SpawnList.Free;
end;

function TActionTrapSpawnMonsters.NoiseMultiplier: Single;
begin
  Exit(1.0);
end;

function TActionTrapSpawnMonsters.NoiseAddition: Single;
begin
  Exit(0.0);
end;

procedure TActionTrapSpawnMonsters.Start;
begin
  inherited Start;
  ShowLog('A pressure plate triggers a trapdoor open in the ceiling', [], ColorLogTrap);
  NewParticle(ParentActor.CenterX, ParentActor.CenterY, 'SPAWN', ColorParticleSpawn);
  // it may be a good idea not to spawn monsters in Start, but in Update?
end;

procedure TActionTrapSpawnMonsters.Update(const SecondsPassed: Single);
begin
  if not ParentActor.CanAct then
  begin
    OnFinished;
    Exit;
  end;

  Perform;

  ParentActor.Vital := -1;
  ParentActor.ActionFinished;
  inherited;
end;

{ TActionTrapSpawnMonstersData -------------------------------}

function TActionTrapSpawnMonstersData.Action: TActionClass;
begin
  Exit(TActionTrapSpawnMonsters);
end;

initialization
  RegisterSimpleSerializableObject(TActionTrapSpawnMonsters);
  RegisterSerializableData(TActionTrapSpawnMonstersData);

end.

