{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionRamTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActionAbstract, GameActionOnTarget, GameActionRoll;

type
  TActionRamTarget = class(TActionOnTarget)
  strict private
    Phase: Single;
    PreHit: Boolean;
    SubAction: TActionRoll;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionRamTargetData = class(TActionOnTargetData)
  public
    function Action: TActionClass; override;
  end;


implementation
uses
  SysUtils,
  CastleVectors, CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GameActor, GamePlayerCharacter, GameActionKnockback, GameApparelSlots,
  GameViewGame, GameLog, GameColors, GameRandom;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TActionRamTarget.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('PreHit', PreHit);
  if SubAction <> nil then // TODO: It most likely will crash Loading --- but can happen as soon as action started but didn't receive Update yet.
    SubAction.Save(Element.CreateChild('SubAction'));
end;

procedure TActionRamTarget.Load(const Element: TDOMElement);
begin
  inherited Load(Element);
  Phase := Element.AttributeSingle('Phase');
  PreHit := Element.AttributeBoolean('PreHit');
  SubAction := TActionAbstract.LoadClass(Element.ChildElement('SubAction', true)) as TActionRoll;
end;

function TActionRamTarget.NoiseMultiplier: Single;
begin
  Exit(1.1);
end;

function TActionRamTarget.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionRamTarget.CanStop: Boolean;
begin
  Exit(false);
end;

procedure TActionRamTarget.Update(const SecondsPassed: Single);
var
  EscapeVector: TVector2;
  E: TApparelSlot;
begin
  inherited;

  if not TargetPlayer.CanAct then
  begin
    OnFinished;
    Exit;
  end;

  Phase += SecondsPassed;
  if (SubAction = nil) and (Phase >= 2) then
  begin
    SubAction := TActionRoll.NewAction(Parent) as TActionRoll;
    TActionRoll(SubAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
    if (TActionRoll(SubAction).MoveVector.Length > 3)
      or TargetPlayer.Unsuspecting {hack to avoid bug that rams cannot attack sleeping target, because they are too close - it doesn't seem to work too good, but it works} then
    begin
      TActionRoll(SubAction).MoveVectorNormalized := TActionRoll(SubAction).MoveVector.Normalize;
      TActionRoll(SubAction).MoveVector := 15 * TActionRoll(SubAction).MoveVectorNormalized;
    end else
    begin
      //roll away if target is too close;
      TActionRoll(SubAction).MoveVectorNormalized := TActionRoll(SubAction).MoveVector.Normalize;
      repeat
        EscapeVector := Vector2(Rnd.Random - 0.5, Rnd.Random - 0.5).Normalize;
      until TVector2.DotProduct(EscapeVector, TActionRoll(SubAction).MoveVectorNormalized) <= 0;
      TActionRoll(SubAction).MoveVectorNormalized := EscapeVector;
      TActionRoll(SubAction).MoveVector := 5 * TActionRoll(SubAction).MoveVectorNormalized;
      PreHit := false;
    end;
    SubAction.Start;
  end;
  if SubAction <> nil then
  begin
    if PreHit and ParentActor.Collides(TargetPlayer, 0) then
    begin
      SubAction.Free;
      SubAction := TActionRoll.NewAction(Parent) as TActionRoll;
      TActionRoll(SubAction).MoveVectorNormalized := -Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY).Normalize;
      TActionRoll(SubAction).MoveVector := 4 * TActionRoll(SubAction).MoveVectorNormalized;
      SubAction.Start;
      PreHit := false;

      if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
      begin
        E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;
        ShowLog('%s rams into %s knocking %s off her %s', [ParentActor.Data.DisplayName, TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, EquipSlotsToHumanReadableString(TargetPlayer.Inventory.Equipped[E].Data.EquipSlots)], ColorLogItemSteal);
        TargetPlayer.Inventory.UnequipAndDrop(E, true);
        TargetPlayer.HitVigor(ParentActor.GetDamage);
        TargetPlayer.Hit(ParentActor.GetDamage);
      end else
      begin
        //otherwise hit double power
        TargetPlayer.HitVigor(2 * ParentActor.GetDamage);
        TargetPlayer.Hit(2 * ParentActor.GetDamage);
      end;

      // hurt player (=TMarkHurt)
      ParentActor.PlayAttackSound;
      ParentActor.DamageWeapon;
      // Push target
      TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
      TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
      TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
      TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
      TargetPlayer.CurrentAction.Start;
      ViewGame.ShakeMap;
    end;
    // CAREFUL, TActionRoll will call OnActionFinished (kill this and parent TMoveAndActTarget)
    SubAction.Update(SecondsPassed);
  end;
end;

class function TActionRamTarget.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionRamTarget).Phase := 0;
  (Result as TActionRamTarget).PreHit := true;
end;

destructor TActionRamTarget.Destroy;
begin
  if SubAction <> nil then
    SubAction.Free;
  inherited Destroy;
end;

{ TActionRamTargetData -------------------------------}

function TActionRamTargetData.Action: TActionClass;
begin
  Exit(TActionRamTarget);
end;

initialization
  RegisterSimpleSerializableObject(TActionRamTarget);
  RegisterSerializableData(TActionRamTargetData);

end.

