{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerEquipItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSimpleSerializableObject,
  GameMapItem,
  GameActionAbstract;

type
  TActionPlayerEquipItem = class(TActionAbstract)
  public const
    Duration = Single(1);
  strict private
    Phase: Single;
    procedure Perform;
  public
    // WARNING!!! If different characters may use this in parallel - they may perform different actions on the same item, resulting in hard-to-find bugs
    MapItem: TMapItem;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionPlayerEquipItemData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameItemData, GameMap, GameLog, GameColors, GameEffectAbstract,
  GameActor, GamePlayerCharacter, GameSounds, GameTranslation, GameApparelSlots,
  GameViewGame;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TActionPlayerEquipItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('MapItem', MapItem.ReferenceId);
end;

procedure TActionPlayerEquipItem.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  MapItem := ObjectByReferenceId(Element.AttributeQWord('MapItem')) as TMapItem; // MapItems are deserealized before Player characters, so this should be fine
end;

function TActionPlayerEquipItem.NoiseMultiplier: Single;
begin
  Exit(1.2);
end;

function TActionPlayerEquipItem.NoiseAddition: Single;
begin
  Exit(MapItem.Item.ItemData.Noise * 2); // will result in 2x multiplier, item isn't in list yet
end;

procedure TActionPlayerEquipItem.Start;
begin
  inherited Start;
  ShowLog(GetTranslation('ActorStartsEquippingItemLog'), [ParentPlayer.Data.DisplayName, MapItem.Item.Data.DisplayName, TActionPlayerEquipItem.Duration], ColorLogActionStart);
end;

procedure TActionPlayerEquipItem.Perform;
var
  E: TEffectAbstract;
begin
  ParentPlayer.Inventory.EquipItem(MapItem.Item);
  if MapItem.Item.ItemData.IsBondage then
  begin
    ShowLog(GetTranslation('ActorEquipedBondageItemLog1'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName, EquipSlotsToHumanReadableString(MapItem.Item.Data.EquipSlots)], ColorLogBondage);
    ShowLog(GetTranslation('ActorEquipedBondageItemLog2'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogBondage);
  end else
    ShowLog(GetTranslation('ActorEquipsItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogInventory);
  // TEMPORARY: display item stats
  ShowLog('Item durability: %d/%d', [Round(MapItem.Item.Durability), Round(MapItem.Item.MaxDurability)], ColorLogTemporary);
  if MapItem.Item.Data.MainSlot = esWeapon then
    ShowLog('Weapon damage: %.1n', [MapItem.Item.ItemData.Damage], ColorLogTemporary)
  else
    ShowLog('Damage absorption: %d%%', [Round(100 * MapItem.Item.ItemData.Protection)], ColorLogTemporary);
  if MapItem.Item.ItemData.WardrobeMalfunctionChance > 0 then
    ShowLog('Wardrobe malfunction chance: %.1n%%', [Round(1000 * MapItem.Item.ItemData.WardrobeMalfunctionChance) / 10.0], ColorLogTemporary);

  ShowLog('Noisiness: %.1n', [MapItem.Item.ItemData.Noise], ColorLogTemporary);
  for E in MapItem.Item.Enchantments do
    ShowLog(E.Description, [], ColorLogTemporary);

  if (MapItem.Item.Data.MainSlot = esWeapon) and (ParentPlayer.GetDamageMultiplier < 1) then
    ShowLog('Restraints prevent %s from fighting efficiently, damage is reduced by %d%%', [ParentActor.Data.DisplayName, Round(100 * (1 - ParentPlayer.GetDamageMultiplier))], ColorLogBondageReport)
  else
  if (ParentPlayer.GetSpeedMultiplier < 1.0) or (ParentPlayer.GetRollCostMultiplier > 1.0) then
  begin
    ShowLog('Entanglements constrict %s''s movements',[ParentActor.Data.DisplayName], ColorLogBondageReport);
    if ParentPlayer.GetSpeedMultiplier < 1.0 then
      ShowLog('Movement speed is reduced by %d%% ', [Round(100 * (1 - ParentPlayer.GetSpeedMultiplier))], ColorLogBondageReport);
    if ParentPlayer.GetRollCostMultiplier > 1.0 then
      ShowLog('Effort needed to dash roll is increased by %d%%', [Round(100 * (ParentPlayer.GetRollCostMultiplier - 1))], ColorLogBondageReport);
  end;

  Sound(MapItem.Item.ItemData.SoundEquip);
  MapItem.Item := nil;
  Map.MapItemsList.Remove(MapItem);
  MapItem := nil;
end;

procedure TActionPlayerEquipItem.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if Phase > Duration then
  begin
    Perform;
    OnFinished;
    Exit;
  end
end;

class function TActionPlayerEquipItem.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerEquipItem).Phase := 0;
end;

destructor TActionPlayerEquipItem.Destroy;
begin
  if (MapItem <> nil) and (MapItem.Item <> nil) and (Parent <> nil) and (ParentActor.Data <> nil) then // ParentActor.Data = nil may happen for PlayerCharacter when it's being freed. Maybe: rework it safer (TODO)
  begin
    ShowLog(GetTranslation('ActorCancelEquipItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogCancel);
    //OnFinished; // we most likely already in OnFinished so endless loop
  end;
  inherited Destroy;
end;

{ TActionPlayerEquipItemData -------------------------------}

function TActionPlayerEquipItemData.Action: TActionClass;
begin
  Exit(TActionPlayerEquipItem);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerEquipItem);
  RegisterSerializableData(TActionPlayerEquipItemData);

end.
