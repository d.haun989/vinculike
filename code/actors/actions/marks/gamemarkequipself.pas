{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkEquipSelf;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract,
  GameInventoryItem;

type
  TMarkEquipSelf = class(TMarkTargetAbstract)
  strict private
    procedure Perform;
    procedure DoDrop(const ItemStolen: TInventoryItem);
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkEquipSelfData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation,
  GameViewGame, GameMap, GameSounds, GameMapItem, GameActor,
  GameItemData, GameItemDatabase, GameApparelSlots, GamePlayerCharacter, GameMonster;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TMarkEquipSelf.DoDrop(const ItemStolen: TInventoryItem);
begin
  Sound(ItemStolen.ItemData.SoundUnequip);
  Map.MapItemsList.Add(TMapItem.CreateItem(ParentMonster.LastTileX + ParentMonster.Size div 2, ParentMonster.LastTileY + ParentMonster.Size div 2, ItemStolen));
end;

procedure TMarkEquipSelf.Perform;
var
  ItemData: TItemData;

  function GetRemovableItem(const Forced: Boolean): TInventoryItem;
  var
    E: TApparelSlot;
  begin
    for E in ItemData.EquipSlots do
      if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
        Exit(TargetPlayer.Inventory.UnequipItemAndReturn(E, Forced));
    Exit(nil);
  end;
  function DestroyAllRemainingItems: Boolean;
  var
    E: TApparelSlot;
  begin
    Result := false;
    for E in ItemData.EquipSlots do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
      begin
        Result := true;
        TargetPlayer.Inventory.UnequipAndDisintegrate(E, false);
      end;
  end;

var
  WakeUpChance, ResistChance: Single;
  ItemStolen: TInventoryItem;
  Item: TInventoryItem;
begin
  ItemData := ItemsDataDictionary['full_suit'] as TItemData;

  // If same already equipped, reinforce and flee
  if TargetPlayer.Inventory.HasItem(ItemData) then
  begin
    TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot);
    ShowLog('As %s is already wearing a %s, %s patches it up and retreats', [TargetActor.Data.DisplayName, ItemData.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
    Sound(ItemData.SoundRepair);
    ParentMonster.Ai.AiFlee := true;
    Exit;
  end;

  ItemStolen := GetRemovableItem(not TargetPlayer.Unsuspecting);
  if ItemStolen <> nil then
  begin
    if TargetPlayer.Unsuspecting then
    begin
      WakeUpChance := 0.5 * TargetPlayer.Vigor / TargetPlayer.PlayerCharacterData.MaxVigor;
      if WakeUpChance < 0.2 then
        WakeUpChance := 0.2;
      if Rnd.Random < WakeUpChance then
      begin
        ShowLog('%s feels %s being pulled off her %s', [TargetActor.Data.DisplayName, ItemStolen.Data.DisplayName, EquipSlotsToHumanReadableString(ItemStolen.Data.EquipSlots)], ColorLogItemSteal);
        Sound(ParentMonster.MonsterData.SoundAttack);
        ViewGame.WakeUp(true, true);
      end else
        ShowLog('%s carefully removes %s from %s without waking her up', [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogItemSteal);
    end else
    begin
      ShowLog('%s catches %s and pulls off her %s before she manages to break free', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
      Sound(ParentMonster.MonsterData.SoundAttack);
      TargetPlayer.HitVigor(ParentActor.GetDamage);
    end;
    DoDrop(ItemStolen);
  end else
  begin
    ResistChance := TargetPlayer.Vigor / TargetPlayer.PlayerCharacterData.MaxVigor;
    if ResistChance < 0.3 then
      ResistChance := 0.3;
    if TargetActor.Immobilized then
      ResistChance := 0;
    if not TargetPlayer.Inventory.CanUseHands or TargetPlayer.Unsuspecting then
      ResistChance := ResistChance / 2;
    if Rnd.Random < ResistChance then
    begin
      ViewGame.WakeUp(true, true);
      ShowLog('%s tries to wrap around %s and she barely struggles out', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
      TargetPlayer.HitVigor(2 * ParentActor.GetDamage);
      Sound(ParentMonster.MonsterData.SoundAttack);
    end else
    begin
      ViewGame.WakeUp(true, false);
      if DestroyAllRemainingItems then
      begin
        ShowLog('With surprising ease %s gets rid of restraints on %s', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
        ShowLog('And before she gets a chance to escape, embraces her %s', [EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogTickleBondage);
      end else
        ShowLog('%s wraps around %s''s exposed %s and tightens itself before she can react', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotsToHumanReadableString(ItemData.EquipSlots)], ColorLogTickleBondage);
      ViewGame.ShakeMap;
      Item := TInventoryItem.NewItem(ItemData);
      Item.Durability := Item.MaxDurability * (ParentActor.Vital / ParentActor.Data.MaxVital) * (1 - 0.1 * Rnd.Random); // MaxDurability is still random
      Sound(ItemData.SoundEquip);
      TargetPlayer.Inventory.EquipItem(Item);
      ParentActor.Vital := -1;
      // We cannot Parent.Die as it'll reset this action, and EndAction will crash next (TODO)
      // Also note we don't want to drop loot here? As living clothes will drop itself
      Exit;
    end;
  end;
end;

procedure TMarkEquipSelf.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkEquipSelfData ----------------------------- }

function TMarkEquipSelfData.Mark: TMarkClass;
begin
  Exit(TMarkEquipSelf);
end;

initialization
  RegisterSerializableObject(TMarkEquipSelf);
  RegisterSerializableData(TMarkEquipSelfData);

end.

