{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkUndressAndTeleportTargetVacuumCleaner;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkUndressAndTeleportTarget, GameMarkAbstract;

type
  TMarkUndressAndTeleportTargetVacuumCleaner = class(TMarkUndressAndTeleportTarget)
  strict protected
    procedure Perform; override;
  end;

  TMarkUndressAndTeleportTargetVacuumCleanerData = class(TMarkUndressAndTeleportTargetData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameApparelSlots, GamePlayerCharacter, GameLog, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkUndressAndTeleportTargetVacuumCleaner.Perform;
begin
  if (TargetPlayer.Inventory.EquippedClothesRemovable = 0) and (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage then
  begin
    ShowLog('Without any warning %s flies off %s''s hands pulled by invisible force', [TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTeleport);
    TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
  end;
  inherited;
end;

{ TMarkUndressAndTeleportTargetVacuumCleanerData ----------------------------- }

function TMarkUndressAndTeleportTargetVacuumCleanerData.Mark: TMarkClass;
begin
  Exit(TMarkUndressAndTeleportTargetVacuumCleaner);
end;

initialization
  RegisterSerializableObject(TMarkUndressAndTeleportTargetVacuumCleaner);
  RegisterSerializableData(TMarkUndressAndTeleportTargetVacuumCleanerData);

end.

