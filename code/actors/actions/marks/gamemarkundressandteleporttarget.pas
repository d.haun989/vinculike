{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkUndressAndTeleportTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameApparelSlots, GameMarkAbstract;

type
  TMarkUndressAndTeleportTarget = class(TMarkTargetAbstract)
  private const
    MinTeleportDistanceFraction = Single(0.5);
    DisorientationAmount = 2 * 25 * 25;
    DisrobingSlots = [esFeet, esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver];
  strict protected
    procedure Perform; virtual;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkUndressAndTeleportTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation, GameInventoryItem, GameItemData, GameRandom,
  GameViewGame, GameParticle, GameMap, GameMapTypes,
  GamePlayerCharacter, GameMonster, GameMonsterData, GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkUndressAndTeleportTarget.Perform;
var
  E: TApparelSlot;
  Count: Integer;
begin
  Count := 0;
  for E in DisrobingSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
    begin
      Inc(Count);
      TargetPlayer.Inventory.UnequipAndDrop(E, true);
    end;

  TargetPlayer.TeleportToUnknown(MinTeleportDistanceFraction);
  ViewGame.ScheduleMonstersToIdle;  //Map.MonstersToIdle; // we can't do that here, it'll free the parent action and everything goes BOOM
  Map.ForgetVisible(DisorientationAmount);

  ViewGame.ShakeCharacter;
  if Count > 0 then
    ShowLog(GetTranslation('TrapTeleport'), [TargetPlayer.Data.DisplayName], ColorLogTeleport)
  else
    ShowLog(GetTranslation('TrapTeleportWasNaked'), [TargetPlayer.Data.DisplayName], ColorLogTeleport);
  NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'TELEPORT', ColorParticlePlayerTeleport);
end;

procedure TMarkUndressAndTeleportTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkUndressAndTeleportTargetData ----------------------------- }

function TMarkUndressAndTeleportTargetData.Mark: TMarkClass;
begin
  Exit(TMarkUndressAndTeleportTarget);
end;

initialization
  RegisterSerializableObject(TMarkUndressAndTeleportTarget);
  RegisterSerializableData(TMarkUndressAndTeleportTargetData);

end.

