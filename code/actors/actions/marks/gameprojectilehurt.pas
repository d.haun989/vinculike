{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileHurt;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract;

type
  TProjectileHurt = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitPlayer; override;
  end;
  TProjectileHurtData = class(TProjectileAbstractData)
  strict protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameViewGame, GameActor, GameSounds;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileHurtData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileHurtData(Data)}
{$ENDIF}

procedure TProjectileHurt.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileHurt.HitPlayer;
begin
  ViewGame.CurrentCharacter.Hit(MarkData.Damage);
  Sound(MarkData.HitSound);
end;

{ TProjectileHurtData ----------------------------- }

function TProjectileHurtData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileHurtData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
end;

procedure TProjectileHurtData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
end;

function TProjectileHurtData.Mark: TMarkClass;
begin
  Exit(TProjectileHurt);
end;

initialization
  RegisterSerializableObject(TProjectileHurt);
  RegisterSerializableData(TProjectileHurtData);

end.

