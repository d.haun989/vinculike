{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkStealFromTargetPlus;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkStealFromTarget, GameMarkAbstract;

type
  TMarkStealFromTargetPlus = class(TMarkStealFromTarget)
  strict private
    procedure TeleportToSafety;
  strict protected
    procedure Perform; override;
  end;

  TMarkStealFromTargetPlusData = class(TMarkStealFromTargetData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation,
  GameMap, GameMapTypes, GameActor,
  GameParticle, GameMonster, GamePlayerCharacter, GameActionIdle;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TMarkStealFromTargetPlus.TeleportToSafety;
var
  TX, TY: Int16;
  DistMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
  P: TPlayerCharacter;
begin
  NewParticle(ParentActor.CenterX, ParentActor.CenterY, 'TELEPORT', ColorParticleItemStolen);
  DistMap := Map.DistanceMap(Target);
  MaxDistance := Map.MaxDistance(DistMap);
  repeat
    TX := Rnd.Random(Map.SizeX);
    TY := Rnd.Random(Map.SizeY);
  until Map.PassableTiles[ParentActor.PredSize][TX + Map.SizeX * TY] and (DistMap[TX + Map.SizeX * TY] > MaxDistance div 2);
  DistMap := nil;
  ShowLog('%s teleports away', [ParentActor.Data.DisplayName], ColorLogItemSteal);
  //ParentActor.Teleport(TX, TY); // resets to Idle and breaks this action
  ParentActor.X := TX;
  ParentActor.Y := TY;
  ParentActor.CenterX := ParentActor.X + ParentActor.HalfSize;
  ParentActor.CenterY := ParentActor.Y + ParentActor.HalfSize;
  ParentActor.LastTileX := TX;
  ParentActor.LastTileY := TY;
  ParentActor.LastTile := ParentActor.LastTileX + Map.SizeX * ParentActor.LastTileY;
  for P in Map.CharactersOnThisLevel do
    if P.CanAct and (P.GetActionTarget = Parent) then
    begin
      P.CurrentAction := TActionIdle.NewAction(P);
      P.CurrentAction.Start;
    end;
end;

procedure TMarkStealFromTargetPlus.Perform;
begin
  inherited;
  if ParentMonster.Ai.AiFlee then
  begin
    //ParentMonster.AiFlee := false; // otherwise the action doesn't reset to idle :) TODO
    TeleportToSafety;
  end;
end;

{ TMarkStealFromTargetPlusData ----------------------------- }

function TMarkStealFromTargetPlusData.Mark: TMarkClass;
begin
  Exit(TMarkStealFromTargetPlus);
end;

initialization
  RegisterSerializableObject(TMarkStealFromTargetPlus);
  RegisterSerializableData(TMarkStealFromTargetPlusData);

end.

