{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkMedic;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract, GameApparelSlots;

type
  TMarkMedic = class(TMarkTargetAbstract)
  strict private
    const InteractionSlots = [esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver, esAmulet];
  strict private
    { Variation of TPlayerCharacter.UnequipAndDrop but drops at monster's position }
    procedure UnequipAndDrop(const E: TApparelSlot; const Forced: Boolean);
    function TryReinforceBondage(const AItemName: String): Boolean;
    procedure InteractWithSleepingTarget;
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkMedicData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  SysUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation, GameActor,
  GameViewGame, GameParticle, GameSounds, GameMap, GameItemData, GameItemDatabase,
  GameInventoryItem, GameMapItem, GamePlayerCharacter, GameMonster;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TMarkMedic.UnequipAndDrop(const E: TApparelSlot; const Forced: Boolean);
var
  ItemStolen: TInventoryItem;
begin
  ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(E, Forced);
  if ItemStolen <> nil then
  begin
    if Forced then
      Sound(ItemStolen.ItemData.SoundUnequip);
    Map.MapItemsList.Add(TMapItem.CreateItem(ParentMonster.LastTileX + ParentMonster.Size div 2, ParentMonster.LastTileY + ParentMonster.Size div 2, ItemStolen));
  end;
end;

function TMarkMedic.TryReinforceBondage(const AItemName: String): Boolean;
var
  A: TApparelSlot;
begin
  if TargetPlayer.Inventory.ShouldMaximizeDurability(AItemName) then
  begin
    ShowLog('%s makes sure %s is locked tight and secure', [ParentActor.Data.DisplayName, ItemsDataDictionary[AItemName].DisplayName], ColorLogTickleBondage);
    TargetPlayer.Inventory.ReinforceItem(ItemsDataDictionary[AItemName].MainSlot);
    Sound((ItemsDataDictionary[AItemName] as TItemData).SoundEquip);
    Exit(true);
  end else
  begin
    for A in ItemsDataDictionary[AItemName].EquipSlots do
      if TargetPlayer.Inventory.Equipped[A] <> nil then
        if TargetPlayer.Inventory.Equipped[A].Data.Id = 'medical_rack_full' then
          Exit(false) // this is a workaround, and I really don't like it
        else
        if TargetPlayer.Inventory.Equipped[A].Data.Id = AItemName then
          Exit(false) // this is a workaround, and I really don't like it
        else
        if TargetPlayer.Inventory.Equipped[A].ItemData.IsBondage then // "cannot be dropped" todo
          TargetPlayer.Inventory.UnequipAndDisintegrate(A, false)
        else
          TargetPlayer.Inventory.UnequipAndDrop(A, true); // this shouldn't happen?
    Exit(false);
  end;
end;

procedure TMarkMedic.InteractWithSleepingTarget;
var
  E: TApparelSlot;
  MedicalRack: TItemData;
  SomethingUnequipped: Boolean;
begin
  ShowLog('One tiny drop of anesthetic on %s''s nose makes sure she won''t make any trouble while getting prepared', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
  MedicalRack := ItemsDataDictionary['medical_rack_full'] as TItemData;

  // If character has medical rack already equipped
  if (TargetPlayer.Inventory.Equipped[MedicalRack.MainSlot] <> nil) and (TargetPlayer.Inventory.Equipped[MedicalRack.MainSlot].Data = MedicalRack) then
  begin
    // Note: here we don't unequip anything, so virtually the character may have had underwear on
    // However, as equipping medical rack always paired with complete stripping
    // And medical rack prevents from equipping anything (using hands)
    // The only additional items here may be applied bondage by yellow slime
    ShowLog('%s is already properly rigged up for a medical checkup', [TargetPlayer.Data.DisplayName], ColorLogTickleBondage);
    ShowLog('%s only briefly makes sure that no locks or straps on %s are loose', [ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[MedicalRack.MainSlot].Data.DisplayName], ColorLogTickleBondage);
    TargetPlayer.Inventory.ReinforceItem(MedicalRack.MainSlot);
    Exit;
  end;

  SomethingUnequipped := false;
  // Unequip clothes that cover something
  for E in TargetPlayer.Inventory.EquipmentSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and (TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop or TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom) then
    begin
      UnequipAndDrop(E, false);
      SomethingUnequipped := true;
    end;
  // Free up space for medical rack (will also remove bondage items, including bars)
  for E in MedicalRack.EquipSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and (TargetPlayer.Inventory.Equipped[E].Data <> MedicalRack) then
    begin
      SomethingUnequipped := true;
      if TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
        TargetPlayer.Inventory.UnequipAndDisintegrate(E, false)
      else
        UnequipAndDrop(E, false);
    end;

  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(MedicalRack));
  Sound(MedicalRack.SoundEquip);
  if SomethingUnequipped then
    ShowLog('Stripped stark naked %s feels something''s wrong but is unable to wake up', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
  ShowLog('A complicated portable rack is tedious to properly set up and tighten all the straps', [], ColorLogTickleBondage);
  ShowLog('But %s is not capable of interfering and %s takes its time to make sure she''s all locked up', [TargetPlayer.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
end;

procedure TMarkMedic.Perform;
var
  EquipSlot: TApparelSlot;

  procedure DisarmAndRestrainHands;
  begin
    if TargetPlayer.Inventory.Equipped[esWeapon] = nil then
    begin
      // if hands are free - apply wrists bar
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_hands']));
      ShowLog('%s swiftly locks %s around %s''s wrists', [ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickleBondage);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundEquip);
    end else
    if TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage then // but player character can use hands
    begin
      TargetPlayer.Inventory.UnequipAndDisintegrate(esWeapon, false);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_hands']));
      ShowLog('While %s''s hands are already restrained, %s decides that %s will be more convenient to work with', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleBondage);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
      Sound(TargetPlayer.Inventory.Equipped[esWeapon].ItemData.SoundEquip);
    end else
    begin
      ShowLog('Efforlessly catching %s''s wrist %s taps a painful pressure point and makes her drop %s', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleStrip);
      UnequipAndDrop(esWeapon, true);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISARM', ColorParticleTickleStrip);
    end;
  end;

  function RestrainTarget: Boolean;
  begin
    case EquipSlot of
      esFeet:
        begin
          if TryReinforceBondage('medical_rack_feet') then
            Exit;
          if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
          begin
            ShowLog('With her hands immmobilized %s cannot prevent her feet from following', [TargetPlayer.Data.DisplayName], ColorLogTickleBondage);
            TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_feet']));
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
            Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
            Exit(true);
          end;
        end;
      esBottomOver:
        begin
          if TryReinforceBondage('medical_rack_legs') then
            Exit;
          if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
          begin
            ShowLog('Holding %s''s hands firmly %s easily fastens straps around her shins', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickleBondage);
            TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_legs']));
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
            Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
            Exit(true);
          end;
        end;
      esTopOver,esTopOverOver:
        begin
          if TryReinforceBondage('medical_rack_shoulders') then
            Exit;
          if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
          begin
            ShowLog('With %s''s hands out of the way %s link-locks her shoulders with a bar meeting little resistance', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickleBondage);
            TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['medical_rack_shoulders']));
            NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
            Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
            Exit(true);
          end;
        end;
      else
      begin
        // do nothing
      end;
    end;
    Exit(false);
  end;

  procedure CureTarget;
  begin
    TargetPlayer.MaxVital += 1; // we guarantee that MaxVital + 1 <= Data.MaxVital
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'CURED', ColorParticleTickleBondage);
    ViewGame.WakeUp(true, true);
    case EquipSlot of
      esTopOver, esBottomOver:
        begin
          ShowLog('%s shrieks in pain as %s sprays antiseptic on her scratches', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Vital -= 4 * ParentActor.GetDamage;
          TargetPlayer.PlayGruntSound;
        end;
      esTopOverOver:
        begin
          ShowLog('%s gasps and chokes as having forced her mouth wide open %s applies oinment inside her throat', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Vital -= ParentActor.GetDamage;
          TargetPlayer.HitVigor(6 * ParentActor.GetDamage);
          TargetPlayer.PlayGruntSound;
        end;
      esTopUnder:
        begin
          ShowLog('%s helplessly blushes as %s oils and massages her breasts', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Vital -= ParentActor.GetDamage;
          TargetPlayer.HitPsyche(3 * ParentActor.GetDamage);
          TargetPlayer.PlaySurpriseSound;
        end;
      esBottomUnder:
        begin
          ShowLog('%s squeaks from embarassment as %s applies some salve to inside of her nethers', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.HitPsyche(5 * ParentActor.GetDamage);
          TargetPlayer.PlaySurpriseSound;
        end;
      esFeet:
        begin
          ShowLog('%s involuntarily giggles as %s acupunctures her soles', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Vital -= ParentActor.GetDamage;
          TargetPlayer.HitVigor(5 * ParentActor.GetDamage);
          TargetPlayer.HitPsyche(ParentActor.GetDamage);
          TargetPlayer.PlayGiggleSound;
        end;
      esAmulet{esNeck}:
        begin
          ShowLog('%s groans in pain as %s pulls her neck up until it crackles', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);
          TargetPlayer.Vital -= 2 * ParentActor.GetDamage;
          TargetPlayer.DegradeVigor(4 * ParentActor.GetDamage);
          TargetPlayer.PlayGruntSound;
        end;
      else
        raise Exception.CreateFmt('Unexpected equipment slot: %s', [ApparelSlotToStr(EquipSlot)]);
        //esWeapon - impossible here
    end;
  end;

  procedure UndressBeforeVoluntaryCheckup;
  var
    Count: Integer;
    Disarmed: Boolean;
  begin
    if (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage then
    begin
      Disarmed := true;
      TargetPlayer.Inventory.UnequipAndDrop(esWeapon, true);
    end else
      Disarmed := false;
    Count := 0;
    for EquipSlot in TargetPlayer.Inventory.ClothesSlots do
      if (TargetPlayer.Inventory.Equipped[EquipSlot] <> nil) and
         (not TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.IsBondage or TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop or
          TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom) then
          begin
            TargetPlayer.Inventory.UnequipAndDrop(EquipSlot, true);
            Inc(Count);
          end;
    if Disarmed and (Count > 0) then
      ShowLog('Before %s can react, she is disarmed and undressed for the checkup', [TargetActor.Data.DisplayName], ColorParticleTickleStrip)
    else
    if Disarmed then
      ShowLog('Before %s can react, she is disarmed and stands ready for the checkup', [TargetActor.Data.DisplayName], ColorParticleTickleStrip)
    else
    if Count > 0 then
      ShowLog('Before %s can react, she is stripped naked for the checkup', [TargetActor.Data.DisplayName], ColorParticleTickleStrip);
    if Disarmed or (Count > 0) then
      ViewGame.ShakeMap;
  end;

begin
  if TargetPlayer.Unsuspecting then
    InteractWithSleepingTarget;
  // and proceed as normal

  // if Player character doesn't have restrictive bondage on hands, try add some or disarm
  if TargetWasResisting and TargetPlayer.Inventory.CanUseHands then
    DisarmAndRestrainHands
  // if hands are properly immobilized - act further
  else
  begin
    if not TargetWasResisting then
      UndressBeforeVoluntaryCheckup;

    if ((TargetPlayer.Inventory.Equipped[esWeapon] = nil) or (TargetPlayer.Inventory.Equipped[esFeet] = nil) or
       (TargetPlayer.Inventory.Equipped[esBottomOver] = nil) or (TargetPlayer.Inventory.Equipped[esTopOver] = nil))
       and not TargetWasResisting then
      ShowLog('As %s is not resisting, %s is not provoked to restrain her', [TargetPlayer.Data.DisplayName, ParentMonster.Data.DisplayName], ColorLogTickle);

    // check if target needs "healing"
    if TargetPlayer.MaxVital >= TargetPlayer.PlayerCharacterData.MaxVital - 1 then
    begin
      ShowLog('%s quickly checks up on %s''s health and recedes satisfied', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTickle);
      ParentMonster.Ai.AiFlee := true;
      Exit;
    end;
    // if target needs +1 maxVital:

    repeat
      EquipSlot := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
    until EquipSlot in InteractionSlots;

    // if clothes equipped - strip
    if (TargetPlayer.Inventory.Equipped[EquipSlot] <> nil) and (not TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.IsBondage or
      TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop or TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom) then
    begin
      ShowLog('%s is in the way, so off it goes', [TargetPlayer.Inventory.Equipped[EquipSlot].Data.DisplayName], ColorLogTickleStrip);
      UnequipAndDrop(EquipSlot, true);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticleTickleStrip);
    end else
    // if slot is empty or bondage
    begin
      // restrain character or replace bondage items if they're weaker than expected (we guarantee it's a bondage item here)
      if TargetWasResisting and RestrainTarget then
        Exit;
      // If we didn't Exit yet - apply treatment
      CureTarget;
    end;
  end;
end;

procedure TMarkMedic.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkMedicData ----------------------------- }

function TMarkMedicData.Mark: TMarkClass;
begin
  Exit(TMarkMedic);
end;

initialization
  RegisterSerializableObject(TMarkMedic);
  RegisterSerializableData(TMarkMedicData);

end.

