{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkStealFromTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract;

type
  TMarkStealFromTarget = class(TMarkTargetAbstract)
  strict protected
    procedure Perform; virtual;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkStealFromTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation,
  GameViewGame, GameParticle, GameApparelSlots, GameActor,
  GameItemData, GameInventoryItem, GamePlayerCharacter, GameMonster;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TMarkStealFromTarget.Perform;
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  WasCoveredTop, WasCoveredBottom: Boolean;
begin
  if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
  begin
    TargetPlayer.Inventory.UpdateCoveredStatus; // just to make sure we're deaing with current values
    if (TargetPlayer.Inventory.TopCoveredRemovable > 0) and (TargetPlayer.Inventory.TopCoveredRemovable <= TargetPlayer.Inventory.BottomCoveredRemovable) then
      repeat
        E := TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem;
      until TargetPlayer.Inventory.Equipped[E].ItemData.CoversTop
    else
    if TargetPlayer.Inventory.BottomCoveredRemovable > 0 then
      repeat
        E := TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem;
      until TargetPlayer.Inventory.Equipped[E].ItemData.CoversBottom
    else
      E := TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem;

    WasCoveredTop := TargetPlayer.Inventory.TopCoveredRemovable > 0;
    WasCoveredBottom := TargetPlayer.Inventory.BottomCoveredRemovable > 0;
    if TargetPlayer.Unsuspecting then
    begin
      ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(E, false);
      ShowLog(GetTranslation('ActorItemStolenSleeping'), [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogItemSteal);
    end else
    begin
      ParentActor.PlayAttackSound;
      ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(E, true);
      ShowLog(GetTranslation('ActorItemStolen'), [ParentActor.Data.DisplayName, ItemStolen.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogItemSteal);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STEAL', ColorParticleItemStolen);
    end;
    ParentMonster.Loot.Add(ItemStolen);

    if Rnd.Random < (4 + ParentMonster.Loot.Count) / (TargetPlayer.Inventory.EquippedSlotsRemovable + 5) then  // Warning Loot.Count needs rework when monsters will have special loot or can pick up items, this is only stolen items count
      ParentMonster.Ai.AiFlee := true;
    TargetPlayer.Inventory.UpdateCoveredStatus;
    if WasCoveredTop and (TargetPlayer.Inventory.TopCovered = 0) then // note, here we do not use Removable - bondage items also count
      ParentMonster.Ai.AiFlee := true;
    if WasCoveredBottom and (TargetPlayer.Inventory.BottomCovered = 0) then
      ParentMonster.Ai.AiFlee := true;
  end else
  begin
    TargetActor.Hit(ParentActor.GetDamage);
    ParentActor.PlayAttackSound;
    ParentActor.DamageWeapon;
    ViewGame.WakeUp(true, true);
  end;
end;

procedure TMarkStealFromTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkStealFromTargetData ----------------------------- }

function TMarkStealFromTargetData.Mark: TMarkClass;
begin
  Exit(TMarkStealFromTarget);
end;

initialization
  RegisterSerializableObject(TMarkStealFromTarget);
  RegisterSerializableData(TMarkStealFromTargetData);

end.

