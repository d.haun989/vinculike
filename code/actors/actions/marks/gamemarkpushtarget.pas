{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPushTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract, GameActor;

type
  TMarkPushTarget = class(TMarkTargetAbstract)
  strict private
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkPushTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleVectors,
  GameSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameMonster, GameInventoryItem,
  GameActionKnockback,
  GameLog, GameViewGame, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkPushTarget.Perform;
var
  ItemStolen: TInventoryItem;
begin
  // steal item
  if TargetPlayer.Inventory.EquippedSlotsRemovable > 0 then
  begin
    ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(TargetPlayer.Inventory.GetRandomEquipmentSlotEquippedItem, true);
    ParentMonster.Loot.Add(ItemStolen);
    ShowLog('%s grabs %s and tosses her away, stealing %s in process', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemSteal);
  end else
  if (TargetPlayer.Inventory.TopCovered = 0) or (TargetPlayer.Inventory.BottomCovered = 0) then
  begin
    ShowLog('%s squeezes %s touching her private bits and throws her away', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogNaked);
    TargetPlayer.HitPsyche(ParentActor.GetDamage);
  end else
    ShowLog('%s catches %s by her restraints and sends her flying', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
  TargetPlayer.HitVigor(ParentActor.GetDamage);

  // hurt player (=TMarkHurt)
  TargetActor.Hit(ParentActor.GetDamage);
  ParentActor.PlayAttackSound;
  ParentActor.DamageWeapon;
  // Push target
  TargetPlayer.CurrentAction := TActionKnockback.NewAction(Target);
  TActionKnockback(TargetPlayer.CurrentAction).MoveVector := Vector2(TargetPlayer.CenterX - ParentActor.CenterX, TargetPlayer.CenterY - ParentActor.CenterY);
  TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized := TActionKnockback(TargetPlayer.CurrentAction).MoveVector.Normalize;
  TActionKnockback(TargetPlayer.CurrentAction).MoveVector := 20 * TActionKnockback(TargetPlayer.CurrentAction).MoveVectorNormalized;
  TargetPlayer.CurrentAction.Start;
  ViewGame.ShakeMap;
end;

procedure TMarkPushTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkPushTargetData ----------------------------- }

function TMarkPushTargetData.Mark: TMarkClass;
begin
  Exit(TMarkPushTarget);
end;

initialization
  RegisterSerializableObject(TMarkPushTarget);
  RegisterSerializableData(TMarkPushTargetData);

end.

