{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAbstract;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils, CastleGlImages,
  GameSerializableData,
  GamePositionedObject;

type
  TMarkAbstractData = class; // forward
  TMarkAbstract = class abstract(TPositionedObject)
  public
    Parent: TObject; // TODO: Parenless marks
    Phase: Single;
    OnFinished: TSimpleNotifyEvent; // TODO: Parentless marks
    Data: TMarkAbstractData;
    procedure Update(SecondsPassed: Single); virtual;
    procedure EndAction;
  public const Signature = 'mark';
  end;
  TMarkClass = class of TMarkAbstract;
  TMarksList = specialize TObjectList<TMarkAbstract>;

  TMarkTargetAbstract = class abstract(TMarkAbstract)
  strict protected
    TargetIdleCounter: Single;
    function TargetWasResisting: Boolean;
  public
    Target: TObject;
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkAbstractData = class abstract(TSerializableData)
  strict protected
    { If this mark can exist without parent
      If not, then parent must take care of mark
      Otherwise it can be "left alone"
      Currently only for validating Independent parameter from data }
    function CanBeIndependent: Boolean; virtual; // TODO: abstract;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Duration: Single;
    Size: Byte;
    Range: Single;
    SqrRange: Single;
    Image: TDrawableImage;
    SqueezeHorizontal: Boolean;
    { Independent marks can live after parent changed action or died
      WARNING: Not all actions can be independent }
    Independent: Boolean;
    HitSound: String;
    function Mark: TMarkClass; virtual; abstract;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameCachedImages, GameMap, GameLog, GameActionIdle, GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$ENDIF}


procedure TMarkAbstract.EndAction;
begin
  if Assigned(OnFinished) then
    OnFinished;
  OnFinished := nil;
  Map.MarksList.Remove(Self); // this will also free Self!
end;

procedure TMarkAbstract.Update(SecondsPassed: Single);
begin
  Phase += SecondsPassed;

  if (not Data.Independent) and (not ParentActor.CanAct) then
  begin
    ShowError('ERROR: Mark received an update event, but its parent no longer exists', []);
    EndAction;
    Phase := -Single.MaxValue; // because even if we exit Inherited, child will still run
    Exit;
  end;
end;

{ TMarkTargetAbstract ---------------------------------------------}

function TMarkTargetAbstract.TargetWasResisting: Boolean;
begin
  Exit(TargetIdleCounter < Data.Duration * 0.9);
end;

procedure TMarkTargetAbstract.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if TargetActor.CurrentAction is TActionIdle then
    TargetIdleCounter += SecondsPassed;
end;

{ TMarkAbstractData ---------------------------------}

procedure TMarkAbstractData.Validate;
begin
  // inherited -- parent is abstract
  if Duration <= 0 then
    raise EDataValidationError.CreateFmt('Duration <= 0 in %s', [Self.ClassName]);
  if Duration <= 0 then
    raise EDataValidationError.CreateFmt('Size <= 0 in %s', [Self.ClassName]);
  if Range <= 0 then
    raise EDataValidationError.CreateFmt('Range <= 0 in %s', [Self.ClassName]);
  if SqrRange <= 0 then
    raise EDataValidationError.CreateFmt('SqrRange <= 0 in %s', [Self.ClassName]);
  if Image = nil then
    raise EDataValidationError.CreateFmt('Image = nil in %s', [Self.ClassName]);
  if not Mark.InheritsFrom(TMarkAbstract) then
    raise EDataValidationError.CreateFmt('not Mark.InheritsFrom(TMarkAbstract) in %s', [Self.ClassName]);
{  if HitSound = '' then
    raise EDataValidationError.CreateFmt('HitSound = '' in %s', [Self.ClassName]);}
    {$WARNING critical todo: convert all monster.attacksound -> mark.hitsound}
  if Independent and not CanBeIndependent then
    raise EDataValidationError.CreateFmt('Independent and not CanBeIndependent in %s', [Self.ClassName]);
end;

procedure TMarkAbstractData.Read(const Element: TDOMElement);
begin
  Duration := Element.AttributeSingle('Duration');
  Size := Element.AttributeInteger('Size');
  Range := Element.AttributeSingle('Range');
  SqrRange := Sqr(Range);
  Image := LoadDrawable('castle-data:/' + Element.AttributeString('Image'));
  SqueezeHorizontal := Element.AttributeBooleanDef('SqueezeHorizontal', true);
  Independent := Element.AttributeBooleanDef('Independent', false); // TODO
  HitSound := Element.AttributeStringDef('HitSound', ''); // TODO
end;

function TMarkAbstractData.CanBeIndependent: Boolean;
begin
  Exit(false);
end;

end.

