{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileAbstract;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils, CastleVectors,
  GameMarkAbstract;

type
  TProjectileAbstract = class abstract(TMarkAbstract)
  public
    /// TODO: Do we even need Parent/Phase here?
    MoveVector: TVector2;
    procedure HitWall; virtual; abstract;
    procedure HitPlayer; virtual; abstract;
    procedure Update(SecondsPassed: Single); override;
    {$WARNING Critical TODO: Save-load}
  end;

  TProjectileAbstractData = class abstract(TMarkAbstractData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Speed: Single;
    Homing: Single;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableData,
  GameMap, GamePlayerCharacter, GamePositionedObject,
  GameViewGame, GameLog;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileAbstractData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileAbstractData(Data)}
{$ENDIF}

procedure TProjectileAbstractData.Validate;
begin
  inherited Validate;
  if Speed <= 0 then
    raise EDataValidationError.CreateFmt('Speed <= 0 in %s', [Self.ClassName]);
  if Homing < 0 then
    raise EDataValidationError.CreateFmt('Homing < 0 in %s', [Self.ClassName]);
end;

procedure TProjectileAbstractData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Speed := Element.AttributeSingle('Speed');
  Homing := Element.AttributeSingleDef('Homing', 0);
end;

procedure TProjectileAbstract.Update(SecondsPassed: Single);
const
  MinDeltaSeconds = Single(0.01);
var
  RemainingSeconds: Single;
  DeltaSeconds: Single;
  NewTileX, NewTileY: Int16;
  NewX, NewY: Single;
  HomingFraction: Single;
  NearestValidTarget: TPositionedObject;
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  (* TODO: BUG
     if SecondsPassed is large enough we can "jump over a tile".
     This may be very bad for navmesh and can even lead to a crash due to navmesh error *)
  RemainingSeconds := SecondsPassed;
  while RemainingSeconds > 0 do
  begin
    if RemainingSeconds > MinDeltaSeconds then
    begin
      DeltaSeconds := MinDeltaSeconds;
      RemainingSeconds -= MinDeltaSeconds;
    end else
    begin
      DeltaSeconds := RemainingSeconds;
      RemainingSeconds := 0;
    end;
    if MarkData.Homing > 0 then
    begin
      NearestValidTarget := ViewGame.CurrentCharacter;
      HomingFraction := MarkData.Homing * DeltaSeconds;
      MoveVector := MoveVector * (1.0 - HomingFraction) + Vector2(NearestValidTarget.CenterX - CenterX, NearestValidTarget.CenterY - CenterY).Normalize * HomingFraction;
    end;
    NewX := X + DeltaSeconds * MarkData.Speed * MoveVector.X;
    NewY := Y + DeltaSeconds * MarkData.Speed * MoveVector.Y;
    NewTileX := Trunc(NewX);
    NewTileY := Trunc(NewY);
    if ViewGame.CurrentCharacter.Collides(Self, 0) then
    begin
      HitPlayer;
      EndAction;
      Exit;
    end else
    if Map.PassableTiles[0][NewTileX + Map.SizeX * NewTileY] then
    begin
      X := NewX;
      Y := NewY;
      CenterX := X + HalfSize;
      CenterY := Y + HalfSize;
      LastTileX := NewTileX;
      LastTileY := NewTileY;
      LastTile := LastTileX + Map.SizeX * LastTileY;
    end else
    begin
      HitWall;
      EndAction;
      Exit;
    end;
  end;
end;

end.

