{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkHurtTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract, GameActor;

type
  TMarkHurtTarget = class(TMarkTargetAbstract)
  strict private
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkHurtTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$ENDIF}

procedure TMarkHurtTarget.Perform;
begin
  TargetActor.Hit(ParentActor.GetDamage);
  ParentActor.PlayAttackSound;
  ParentActor.DamageWeapon;
end;

procedure TMarkHurtTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkHurtTargetData ----------------------------- }

function TMarkHurtTargetData.Mark: TMarkClass;
begin
  Exit(TMarkHurtTarget);
end;

initialization
  RegisterSerializableObject(TMarkHurtTarget);
  RegisterSerializableData(TMarkHurtTargetData);

end.

