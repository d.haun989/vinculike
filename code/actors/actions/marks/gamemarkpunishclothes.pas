{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPunishClothes;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract, GameApparelSlots;

type
  TMarkPunishClothes = class(TMarkTargetAbstract)
  strict private const
    SugarSweetHealingTiny = 0.004;
    SugarSweetHealingBig = 0.02;
    VigorSleepDrain1 = -100;
    VigorSleepDrain2 = -300;
    VigorSleepDrain3 = -500;
    DressedVigorDamage = 200;
    StardustSlots = [esWeapon, esTopOverOver, esTopOver, esTopUnder, esBottomOver, esBottomUnder, esFeet];
  strict private
    function GetRandomEquipSlotForStardust: TApparelSlot;
    procedure EquipStardust(E: TApparelSlot);
    procedure InteractWithNudeTarget;
    procedure InteractWithSleepingNudeTarget;
    procedure InteractWithSleepingDressedTarget;
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkPunishClothesData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  SysUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation,
  GameViewGame, GameParticle, GameSounds, GameItemData, GameItemDatabase,
  GameInventoryItem, GamePlayerCharacter, GameMonster, GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

function TMarkPunishClothes.GetRandomEquipSlotForStardust: TApparelSlot;
begin
  repeat
    Result := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
  until Result in StardustSlots;
end;

procedure TMarkPunishClothes.EquipStardust(E: TApparelSlot);
var
  Stardust: TItemData;
begin
  // TODO: list as action data
  case E of
    esWeapon: Stardust := ItemsDataDictionary['stardust_hands'] as TItemData;
    esTopOverOver: Stardust := ItemsDataDictionary['stardust_top_overover'] as TItemData;
    esTopOver: Stardust := ItemsDataDictionary['stardust_top_over'] as TItemData;
    esTopUnder: Stardust := ItemsDataDictionary['stardust_top_under'] as TItemData;
    esBottomOver: Stardust := ItemsDataDictionary['stardust_bottom_over'] as TItemData;
    esBottomUnder: Stardust := ItemsDataDictionary['stardust_bottom_under'] as TItemData;
    esFeet: Stardust := ItemsDataDictionary['stardust_feet'] as TItemData;
    else
      raise Exception.CreateFmt('Unexpected equip slot %s', [ApparelSlotToStr(E)]);
  end;
  TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(Stardust));
end;

procedure TMarkPunishClothes.InteractWithNudeTarget;
begin
  // Character is awake
  if not TargetPlayer.Unsuspecting then
    ShowLog('%s retreats', [ParentMonster.Data.DisplayName], ColorLogTwinkleStrip)
  else
  // Character is sleeping
  begin
    InteractWithSleepingNudeTarget;
  end;
  ParentMonster.Ai.AiFlee := true;
end;

procedure TMarkPunishClothes.InteractWithSleepingNudeTarget;
var
  CanEquipDust: Boolean;
  EquipSlot: TApparelSlot;
begin
  CanEquipDust := false;
  for EquipSlot in StardustSlots do
    if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
    begin
      CanEquipDust := true;
      break;
    end;
  if CanEquipDust then
  begin
    repeat
      EquipSlot := GetRandomEquipSlotForStardust;
    until TargetPlayer.Inventory.Equipped[EquipSlot] = nil;
    Sound('stardust'); // TODO
    EquipStardust(EquipSlot);
    ShowLog('The sun, the sand, the ocean and the breeze. Everything in such harmony', [], ColorLogTwinkleHurt);
    ShowLog('Why is everybody looking at her? How has she forgotten to wear any clothes to the beach?', [], ColorLogTwinkleHurt);
    ShowLog('%s tries to cover her nudity, but her hands are heavy as lead', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    ShowLog('She feels a pleasant tingling on her %s and sweet taste of sugar on her lips.', [EquipSlotToHumanReadableString(EquipSlot)], ColorLogTwinkleHurt);
    ShowLog('It''s only a dream. Another silly dream', [], ColorLogTwinkleHurt);
    // heal the heroine
    TargetPlayer.Vigor := VigorSleepDrain1;
    if TargetPlayer.MaxVital + TargetPlayer.PlayerCharacterData.MaxVital * SugarSweetHealingBig < TargetPlayer.PlayerCharacterData.MaxVital then
      TargetPlayer.MaxVital := TargetPlayer.MaxVital + TargetPlayer.PlayerCharacterData.MaxVital * SugarSweetHealingBig
    else
      TargetPlayer.MaxVital := TargetPlayer.PlayerCharacterData.MaxVital;
    if TargetPlayer.MaxPsyche + TargetPlayer.PlayerCharacterData.MaxPsyche * SugarSweetHealingBig < TargetPlayer.PlayerCharacterData.MaxPsyche then
      TargetPlayer.MaxPsyche := TargetPlayer.MaxPsyche + TargetPlayer.PlayerCharacterData.MaxPsyche * SugarSweetHealingBig
    else
      TargetPlayer.MaxPsyche := TargetPlayer.PlayerCharacterData.MaxPsyche;
  end;
end;

procedure TMarkPunishClothes.InteractWithSleepingDressedTarget;
var
  I: Integer;
  EquipSlot: TApparelSlot;
  AvailableSlots: TApparelSlotsList;
begin
  // if we come here, we have top or bottom covered
  AvailableSlots := TApparelSlotsList.Create;
  for EquipSlot in StardustSlots do
    if (TargetPlayer.Inventory.Equipped[EquipSlot] = nil) or not TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.IsBondage then
    begin
      // make sure to unequip clothes that cover something, others can stay for now, stardust may unequip them too
      if (TargetPlayer.Inventory.Equipped[EquipSlot] <> nil) and (TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom or TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop) then
        TargetPlayer.Inventory.UnequipAndDrop(EquipSlot, false);
      AvailableSlots.Add(EquipSlot);
    end;
  if AvailableSlots.Count > 0 then
  begin
    for I := 0 to Rnd.Random(AvailableSlots.Count - 1) do // Will leave at least one slot free, but will add at least one
    begin
      EquipSlot := AvailableSlots[Rnd.Random(AvailableSlots.Count)];
      EquipStardust(EquipSlot);
      AvailableSlots.Remove(EquipSlot);
    end;
  end;
  AvailableSlots.Free;

  Sound('stardust'); // TODO
  if (TargetPlayer.Inventory.TopCovered > 0) or (TargetPlayer.Inventory.BottomCovered > 0) then
  begin
    // could not unequip some covering items - maybe because those are bondage items? Now it'll hurt badly.
    ShowLog('%s dreams of a strong breeze from the ocean trying to pull her clothes off', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    ShowLog('She feels an urge to help, but they stuck tight to her body', [], ColorLogTwinkleHurt);
    ShowLog('Suddenly a violent gust blasts her face blinding her and forcing itself into her mouth', [], ColorLogTwinkleHurt);
    ShowLog('%s tries to scream but can''t even breathe as sugar sweet powder blocks her throat', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    ShowLog('After a few minutes of panic struggle in agony she passes out from asphyxiation', [], ColorLogTwinkleHurt);
    for EquipSlot in StardustSlots do
      if (TargetPlayer.Inventory.Equipped[EquipSlot] <> nil) and ((TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversBottom) or (TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.CoversTop)) then
      begin
        TargetPlayer.Inventory.UnequipAndDisintegrate(EquipSlot, true);
        EquipStardust(EquipSlot);
      end;
    ShowLog('It all stops as suddenly as started, leaving %s unconscious, hurt and exhausted', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
    TargetPlayer.DegradeVigor(TargetPlayer.PlayerCharacterData.MaxVigor + DressedVigorDamage);
    TargetPlayer.HitVital(TargetPlayer.Vital / 2);
    TargetPlayer.HitPsyche(TargetPlayer.Psyche / 2);
    TargetPlayer.Vigor := VigorSleepDrain3;
  end else
  begin
    ShowLog('%s has a beautiful dream of lying nude on a beach in a hot sunny day', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
    ShowLog('Why is she naked? She doesn''t remember taking her clothes off', [], ColorLogTwinkleStrip);
    ShowLog('Yet it feels so good as calm soft breeze tingles her sensitive parts', [], ColorLogTwinkleStrip);
    ShowLog('There are others around eying her body, but %s is too tired and sleepy to care', [TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
    ShowLog('Soon the delightful sensation fades away leaving sweet grains of sugar on her lips', [], ColorLogTwinkleStrip);
    // heal the heroine
    TargetPlayer.DegradeVigor(DressedVigorDamage);
    if TargetPlayer.MaxVital + TargetPlayer.PlayerCharacterData.MaxVital * SugarSweetHealingTiny < TargetPlayer.PlayerCharacterData.MaxVital then
      TargetPlayer.MaxVital := TargetPlayer.MaxVital + TargetPlayer.PlayerCharacterData.MaxVital * SugarSweetHealingTiny
    else
      TargetPlayer.MaxVital := TargetPlayer.PlayerCharacterData.MaxVital;
    if TargetPlayer.MaxPsyche + TargetPlayer.PlayerCharacterData.MaxPsyche * SugarSweetHealingTiny < TargetPlayer.PlayerCharacterData.MaxPsyche then
      TargetPlayer.MaxPsyche := TargetPlayer.MaxPsyche + TargetPlayer.PlayerCharacterData.MaxPsyche * SugarSweetHealingTiny
    else
      TargetPlayer.MaxPsyche := TargetPlayer.PlayerCharacterData.MaxPsyche;
    TargetPlayer.Vigor := VigorSleepDrain2;
  end;
  ParentMonster.Ai.AiFlee := true;
end;

procedure TMarkPunishClothes.Perform;
var
  EquipSlot: TApparelSlot;
  TargetBodyHalf: TBodyHalf;
begin
  if TargetPlayer.Unsuspecting then
  begin
    InteractWithSleepingDressedTarget;
    Exit;
  end;

  TargetBodyHalf := TBodyHalf(Rnd.Random(Ord(High(TBodyHalf)) + 1));
  if (TargetPlayer.Inventory.TopCovered > 0) and (TargetPlayer.Inventory.BottomCovered > 0) then
    //nothing, use random from above
  else
  if TargetPlayer.Inventory.TopCovered > 0 then // will prioritize upper body
    TargetBodyHalf := bhTop
  else
    TargetBodyHalf := bhBottom;

  ParentActor.PlayAttackSound;
  TargetPlayer.HitSpecific(ParentMonster.GetDamage, TargetBodyHalf);
  if TargetBodyHalf = bhTop then
    ShowLog('%s breath spasms as her breasts are hit by electric charge', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt)
  else
    ShowLog('%s nearly fell zapped in her nethers by electricity', [TargetPlayer.Data.DisplayName], ColorLogTwinkleHurt);
  TargetPlayer.HitVigor(10 * ParentMonster.GetDamage);

  if (Rnd.Random > TargetPlayer.Vigor / TargetPlayer.PlayerCharacterData.MaxVigor) or TargetActor.Immobilized then
  begin
    EquipSlot := GetRandomEquipSlotForStardust;
    if TargetPlayer.Inventory.Equipped[EquipSlot] = nil then
    begin
      EquipStardust(EquipSlot);
      Sound(TargetPlayer.Inventory.Equipped[EquipSlot].ItemData.SoundEquip);
      ShowLog('%s showers weakened %s with shiny particles that sick fast to her %s', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName, EquipSlotToHumanReadableString(EquipSlot)], ColorLogTwinkleStrip);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'SHINY', ColorParticleTwinkle);
    end;
  end;

  if (not TargetPlayer.Inventory.TopCovered > 0) and (not TargetPlayer.Inventory.BottomCovered > 0) then
  begin
    ShowLog('%s is satisfied with the result and stops attacking %s', [ParentMonster.Data.DisplayName, TargetPlayer.Data.DisplayName], ColorLogTwinkleStrip);
    ParentMonster.Ai.AiFlee := true;
  end;
end;

procedure TMarkPunishClothes.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if (TargetPlayer.Inventory.TopCovered = 0) and (TargetPlayer.Inventory.BottomCovered = 0) then
  begin
    InteractWithNudeTarget;
    EndAction;
    Exit;
  end;

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkPunishClothesData ----------------------------- }

function TMarkPunishClothesData.Mark: TMarkClass;
begin
  Exit(TMarkPunishClothes);
end;

initialization
  RegisterSerializableObject(TMarkPunishClothes);
  RegisterSerializableData(TMarkPunishClothesData);

end.

