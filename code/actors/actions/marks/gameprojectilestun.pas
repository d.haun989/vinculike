{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileStun;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract;

type
  TProjectileStun = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitPlayer; override;
  end;
  TProjectileStunData = class(TProjectileAbstractData)
  strict protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StunDuration: Single;
    VigorDamage: Single;
    ClothesDamage: Single;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameActor, GameSounds, GamePlayerCharacter, GameApparelSlots, GameActionPlayerStunned,
  GameViewGame, GameLog, GameColors, GameRandom;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileStunData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileStunData(Data)}
{$ENDIF}

procedure TProjectileStun.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileStun.HitPlayer;
const
  StunSlots = [esNeck, esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver];
var
  TargetPlayer: TPlayerCharacter;
  E: TApparelSlot;
begin
  TargetPlayer := ViewGame.CurrentCharacter;
  ViewGame.WakeUp(true, true);
  repeat
    E := TargetPlayer.Inventory.GetRandomClothesSlot;
  until E in StunSlots;
  if TargetPlayer.Inventory.Equipped[E] = nil then
  begin
    if Rnd.Random < 0.5 then
    begin
      TargetPlayer.CurrentAction := TActionPlayerStunned.NewAction(TargetPlayer, MarkData.StunDuration);
      TargetPlayer.CurrentAction.Start;
      ShowLog('Hit by a stun needle in her %s %s''s muscles spasm (%.1n seconds)', [EquipSlotToHumanReadableString(E), TargetPlayer.Data.DisplayName, MarkData.StunDuration], ColorLogNotEnoughVigor);
    end else
    begin
      ShowLog('A feathered needle leaves a scratch on %s''s %s. She suddenly feels tired.', [TargetPlayer.Data.DisplayName, EquipSlotToHumanReadableString(E)], ColorLogNotEnoughVigor);
      TargetPlayer.HitVigor(MarkData.VigorDamage);
    end;
  end else
  begin
    ShowLog('A feathered needle slides along %s leaving a harmless droplet of tranquilizer on it', [TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogAbsorbDamage);
    TargetPlayer.Inventory.DamageItem(E, MarkData.ClothesDamage);
  end;
  Sound(MarkData.HitSound);
end;

{ TProjectileStunData ----------------------------- }

function TProjectileStunData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileStunData.Validate;
begin
  inherited Validate;
  if ClothesDamage < 0 then
    raise EDataValidationError.CreateFmt('ClothesDamage < 0 in %s', [Self.ClassName]);
  if VigorDamage < 0 then
    raise EDataValidationError.CreateFmt('VigorDamage < 0 in %s', [Self.ClassName]);
  if StunDuration <= 0 then
    raise EDataValidationError.CreateFmt('StunDuration <= 0 in %s', [Self.ClassName]);
end;

procedure TProjectileStunData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  ClothesDamage := Element.AttributeSingle('ClothesDamage');
  VigorDamage := Element.AttributeSingle('VigorDamage');
  StunDuration := Element.AttributeSingle('StunDuration');
end;

function TProjectileStunData.Mark: TMarkClass;
begin
  Exit(TProjectileStun);
end;

initialization
  RegisterSerializableObject(TProjectileStun);
  RegisterSerializableData(TProjectileStunData);

end.

