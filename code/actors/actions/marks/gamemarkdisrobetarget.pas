{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkDisrobeTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameApparelSlots, GameMarkAbstract;

type
  TMarkDisrobeTarget = class(TMarkTargetAbstract)
  private const
    MinTeleportDistanceFraction = Single(0.5);
    HitNakedPsycheDamage = 50;
    HitNakedPsycheDamagePercent = 0.67;
    DisrobingSlots = [esFeet, esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver];
  strict private
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkDisrobeTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation, GameInventoryItem, GameItemData, GameRandom, GameMap, GameMapTypes,
  GameViewGame, GameMapItem, GameParticle,
  GamePlayerCharacter, GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkDisrobeTarget.Perform;
const
  MaxRetryCount = Integer(10000);
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  AX, AY: Int16;
  Count: Integer;
  RetryCount: Integer;

  DistanceSeed: TCoordList;
  DistanceMap: TDistanceMapArray;
  MaxDistance: TDistanceQuant;
begin
  Count := 0;
  DistanceSeed := TCoordList.Create;
  // TODO: cycle all active players
  TargetPlayer.AddSeed(DistanceSeed);
  DistanceMap := Map.DistanceMap(TargetPlayer.PredSize, DistanceSeed);
  MaxDistance := Map.MaxDistance(DistanceMap);
  DistanceSeed.Free;

  RetryCount := 0;
  repeat
    repeat
      AX := Rnd.Random(Map.SizeX);
      AY := Rnd.Random(Map.SizeY);
    until Map.CanMove(AX + Map.SizeX * AY) and (DistanceMap[AX + Map.SizeX * AY] > MaxDistance * MinTeleportDistanceFraction);
    Inc(RetryCount);
  until (Map.Visible[AX + Map.SizeX * AY] = 0) or (RetryCount > MaxRetryCount);

  DistanceMap := nil;
  for E in DisrobingSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
    begin
      Inc(Count);
      ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(E, true);
      if ItemStolen <> nil then
        Map.MapItemsList.Add(TMapItem.CreateItem(AX, AY, ItemStolen));
    end;
  ViewGame.ShakeCharacter;
  if Count > 0 then
  begin
    ShowLog(GetTranslation('TrapDisrobeTarget'), [TargetPlayer.Data.DisplayName], ColorLogTeleport);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISROBED', ColorParticlePlayerDisrobed);
  end else
  begin
    ShowLog(GetTranslation('TrapDisrobeTargetWasNaked'), [TargetPlayer.Data.DisplayName], ColorLogTeleport);
    TargetPlayer.DegradePsyche(Min(TargetPlayer.Psyche / 2, HitNakedPsycheDamage));
    TargetPlayer.Psyche := TargetPlayer.Psyche * (1 - HitNakedPsycheDamagePercent);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DIZZY', ColorParticlePlayerDisrobed);
    ViewGame.ShakeMap;
  end;
end;

procedure TMarkDisrobeTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkDisrobeTargetData ----------------------------- }

function TMarkDisrobeTargetData.Mark: TMarkClass;
begin
  Exit(TMarkDisrobeTarget);
end;

initialization
  RegisterSerializableObject(TMarkDisrobeTarget);
  RegisterSerializableData(TMarkDisrobeTargetData);

end.

