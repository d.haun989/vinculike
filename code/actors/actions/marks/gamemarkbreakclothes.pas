{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBreakClothes;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameApparelSlots, GameMarkAbstract;

type
  TMarkBreakClothes = class(TMarkTargetAbstract)
  strict private const
    HitNakedDamage = 50;
    HitNakedDamagePercent = 0.5;
    DisrobingSlots = [esFeet, esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver];
  strict private
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkBreakClothesData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  Math,
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation,
  GameViewGame, GameMap, GameParticle,
  GameMapItem, GameInventoryItem,
  GameActor, GamePlayerCharacter;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkBreakClothes.Perform;
var
  E: TApparelSlot;
  ItemStolen: TInventoryItem;
  Count: Integer;
  BondageCount: Integer;
begin
  Count := 0;
  BondageCount := 0;
  for E in DisrobingSlots do
    if TargetPlayer.Inventory.Equipped[E] <> nil then
    begin
      if not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
      begin
        Inc(Count);
        ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(E, true);
        if ItemStolen <> nil then
        begin
          ItemStolen.Broken := true;
          Map.MapItemsList.Add(TMapItem.CreateItem(TargetPlayer.LastTileX + TargetPlayer.PredSize div 2, TargetPlayer.LastTileY + TargetPlayer.PredSize div 2, ItemStolen));
        end;
      end else
        Inc(BondageCount);
    end;
  ViewGame.ShakeCharacter;
  if Count > 0 then
  begin
    ShowLog('Tiny sharp hooks catch %s''s clothes and pull them off without hurting her', [TargetPlayer.Data.DisplayName], ColorLogItemBreak);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISROBED', ColorParticlePlayerDisrobed);
  end else
  begin
    if BondageCount < 4 then
      ShowLog('Dozens of sharp hooks slide over %s''s nude skin leaving shallow but very painful scratches', [TargetPlayer.Data.DisplayName], ColorLogUnabsorbedDamage)
    else
      ShowLog('Sharp hooks slide over %s''s restraints leaving painful scratches on uncovered areas', [TargetPlayer.Data.DisplayName], ColorLogUnabsorbedDamage);
    TargetPlayer.DegradeVital(Min(TargetPlayer.Vital / 2, HitNakedDamage));
    TargetPlayer.Vital := TargetPlayer.Vital * (1 - HitNakedDamagePercent);
    ViewGame.ShakeMap;
  end;
end;

procedure TMarkBreakClothes.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkBreakClothesData ----------------------------- }

function TMarkBreakClothesData.Mark: TMarkClass;
begin
  Exit(TMarkBreakClothes);
end;

initialization
  RegisterSerializableObject(TMarkBreakClothes);
  RegisterSerializableData(TMarkBreakClothesData);

end.

