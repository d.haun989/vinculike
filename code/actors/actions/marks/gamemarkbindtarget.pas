{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBindTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract;

type
  TMarkBindTarget = class(TMarkTargetAbstract)
  strict private const ImmobilizedStruggleDamage = 200;
  strict private
    procedure Perform;
  strict protected
    procedure InteractAllTiedUp; virtual;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkBindTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation,
  GameViewGame, GameViewEndGame, GameParticle, GameApparelSlots, GameActionIdle,
  GameItemData, GameItemDataAbstract, GameItemDatabase, GameInventoryItem, GamePlayerCharacter, GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkBindTarget.Perform;
var
  I: Integer;
  ItemData: TItemData;
  ItemDamage: Single;
  ResistBindChance: Single;
  ActionBondageItemsList: TItemsDataAbstractList;
  BondageItemsList: TItemsDataAbstractList;
begin
  ParentActor.PlayAttackSound;
  if TargetPlayer.Inventory.AllTiedUp then
  begin
    InteractAllTiedUp;
    Exit;
  end;

  ActionBondageItemsList := TItemsDataAbstractList.Create(false);
  ActionBondageItemsList.Add(ItemsDataDictionary['rope-harness-feet']);
  ActionBondageItemsList.Add(ItemsDataDictionary['rope-harness-bottom-under']);
  ActionBondageItemsList.Add(ItemsDataDictionary['rope-harness-top-under']);
  ActionBondageItemsList.Add(ItemsDataDictionary['rope-harness-bottom-over']);
  ActionBondageItemsList.Add(ItemsDataDictionary['rope-harness-top-overover']);
  ActionBondageItemsList.Add(ItemsDataDictionary['rope-harness-hands']);
  if ActionBondageItemsList.Count = 0 then
  begin
    ShowError('%s could not find bondage items to apply to %s', [ParentActor.Data.Id, TargetPlayer.Data.DisplayName]);
    Exit;
  end;

  BondageItemsList := TItemsDataAbstractList.Create(false);
  for I := 0 to Pred(ActionBondageItemsList.Count) do
    if (TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot] = nil) or // naked slot
      not TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot].ItemData.IsBondage or // will try to undress
      (
         ActionBondageItemsList.Contains(TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot].Data) and
         TargetPlayer.Inventory.ShouldMaximizeDurability(ActionBondageItemsList[I].MainSlot)
      ) // will try to reinforce
      then
        BondageItemsList.Add(ActionBondageItemsList[I]);

  if BondageItemsList.Count = 0 then
  begin
    ShowError('%s cannot apply any bondage to %s, all slots are locked/reinforced', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName]);
    BondageItemsList.Free;
    Exit;
  end;

  ItemData := BondageItemsList[Rnd.Random(BondageItemsList.Count)] as TItemData;
  BondageItemsList.Free;
  ActionBondageItemsList.Free; // TODO

  if TargetPlayer.Inventory.Equipped[ItemData.MainSlot] <> nil then
  begin
    if TargetPlayer.Inventory.Equipped[ItemData.MainSlot].ItemData.IsBondage then
    begin
      TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot);
      if TargetActor.Immobilized or TargetActor.Unsuspecting then
        TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot); // reinforce twice if immobilized
      ShowLog(GetTranslation('ActorBondageReinforced'), [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondage);
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
    end else
    begin
      ItemDamage := ParentActor.GetDamage;
      ResistBindChance := ItemDamage / TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Durability;
      ShowLog(GetTranslation('ActorBondageDamageItem'), [TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Data.DisplayName, ItemDamage], ColorLogBondageItemSave);
      TargetPlayer.Inventory.DamageItem(ItemData.MainSlot, ItemDamage);
      if TargetPlayer.Inventory.Equipped[ItemData.MainSlot] <> nil then
      begin
        ShowLog(GetTranslation('ActorBondageItemSave'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondageItemSave);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'EVADE', ColorParticleItemSave);
      end else
      begin
        if Rnd.Random > ResistBindChance then
        begin
          ShowLog(GetTranslation('ActorBondageItemBreakSave'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondageItemSave);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'EVADE', ColorParticleItemSave);
        end else
        begin
          ShowLog(GetTranslation('ActorBondageItemBreakFail'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondage);
          ViewGame.ShakeCharacter;
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
          TargetPlayer.PlaySurpriseSound;
        end;
      end;
    end;
  end else
  begin
    ShowLog(GetTranslation('ActorBondageApplied'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondage);
    if TargetActor.Immobilized or TargetActor.Unsuspecting then
    begin
      TargetPlayer.Inventory.ReinforceItem(ItemData.MainSlot); // reinforce twice if immobilized
      ShowLog('As %s cannot resist %s takes time to make sure knots are secure and tight', [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogBondage);
    end;
    ViewGame.ShakeCharacter;
    TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
    TargetPlayer.PlaySurpriseSound;
  end;

  ViewGame.WakeUp(true, true);

  if TargetPlayer.Inventory.AllTiedUp then
    ShowLog(GetTranslation('ActorBondageAllTiedUp'), [TargetActor.Data.DisplayName], ColorLogBondage);
end;

procedure TMarkBindTarget.InteractAllTiedUp;
begin
  if (TargetPlayer.Vigor > 0) and not TargetPlayer.Immobilized then
  begin
    ShowLog('%s easily catches almost completely restrained %s with a lasso', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
    ShowLog('After a short desperate struggle she still manages to wriggle out', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogBondageItemSave);
    ViewGame.ShakeCharacter;
    TargetPlayer.HitVigor(ImmobilizedStruggleDamage);
    TargetPlayer.CurrentAction := TActionIdle.Create;
    TargetPlayer.CurrentAction.Start;
  end else
  begin
    ShowLog(GetTranslation('ActorBondageImmobilized'), [TargetActor.Data.DisplayName], ColorLogDefeat);
    TargetPlayer.GetCaptured(egTiedUp);
  end;
end;

procedure TMarkBindTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkBindTargetData ----------------------------- }

function TMarkBindTargetData.Mark: TMarkClass;
begin
  Exit(TMarkBindTarget);
end;

initialization
  RegisterSerializableObject(TMarkBindTarget);
  RegisterSerializableData(TMarkBindTargetData);

end.

