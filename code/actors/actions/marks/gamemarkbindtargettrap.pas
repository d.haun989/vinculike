{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkBindTargetTrap;

{$INCLUDE compilerconfig.inc}

interface

uses
  GameMarkBindTarget, GameMarkAbstract;

type
  TMarkBindTargetTrap = class(TMarkBindTarget)
  strict private const TrapVigorDamage = 200;
  strict protected
    procedure InteractAllTiedUp; override;
  end;

  TMarkBindTargetTrapData = class(TMarkBindTargetData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation, GameActor,
  GameViewGame, GamePlayerCharacter, GameActionIdle;

{$IFDEF SafeActorTypecast}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE TargetActor:=(Target as TActor)}
{$ELSE}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE TargetActor:=TActor(Target)}
{$ENDIF}

procedure TMarkBindTargetTrap.InteractAllTiedUp;
begin
  ShowLog('The straps pin %s to the ground', [TargetActor.Data.DisplayName], ColorLogBondageItemSave);
  ShowLog('With a lot of effort she still manages to stand up quickly', [TargetActor.Data.DisplayName], ColorLogBondageItemSave);
  ViewGame.ShakeCharacter;
  TargetPlayer.HitVigor(TrapVigorDamage);
  TargetPlayer.CurrentAction := TActionIdle.Create;
  TargetPlayer.CurrentAction.Start;
end;

{ TMarkBindTargetTrapData ----------------------------- }

function TMarkBindTargetTrapData.Mark: TMarkClass;
begin
  //inherited
  Exit(TMarkBindTargetTrap);
end;

initialization
  RegisterSerializableObject(TMarkBindTargetTrap);
  RegisterSerializableData(TMarkBindTargetTrapData);

end.

