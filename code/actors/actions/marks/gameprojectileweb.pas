{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileWeb;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils,
  GameProjectileAbstract, GameMarkAbstract;

type
  TProjectileWeb = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitPlayer; override;
  end;

  TProjectileWebData = class(TProjectileAbstractData)
  strict protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Damage: Single;
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameItemData, GameItemDatabase, GamePlayerCharacter, GameApparelSlots, GameParticle, GameInventoryItem,
  GameViewGame, GameLog, GameColors, GameRandom, GameTranslation, GameActor, GameSounds;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileWebData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileWebData(Data)}
{$ENDIF}

procedure TProjectileWeb.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileWeb.HitPlayer;
var
  I: Integer;
  ItemData: TItemData;
  ActionBondageItemsList: TItemsDataList;
  BondageItemsList: TItemsDataList;
  TargetPlayer: TPlayerCharacter;
begin
  TargetPlayer := ViewGame.CurrentCharacter;
  ViewGame.WakeUp(true, true);

  Sound(MarkData.HitSound);

  ActionBondageItemsList := TItemsDataList.Create(false);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_bottom_over'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_bottom_under'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_top_over'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_top_overover'] as TItemData);
  ActionBondageItemsList.Add(ItemsDataDictionary['webbing_top_under'] as TItemData);
  if ActionBondageItemsList.Count = 0 then
  begin
    ShowError('could not find bondage items to apply to %s', [TargetPlayer.Data.DisplayName]);
    Exit;
  end;

  BondageItemsList := TItemsDataList.Create(false);
  for I := 0 to Pred(ActionBondageItemsList.Count) do
    if (TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot] = nil) or // naked slot
      not TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot].ItemData.IsBondage or // will try to undress
      (
         ActionBondageItemsList.Contains(TargetPlayer.Inventory.Equipped[ActionBondageItemsList[I].MainSlot].ItemData)
         //TargetPlayer.Inventory.ShouldMaximizeDurability(ActionBondageItemsList[I].MainSlot)
      ) // will try to reinforce
      then
        BondageItemsList.Add(ActionBondageItemsList[I]);
  ActionBondageItemsList.Free;

  if BondageItemsList.Count > 0 then
  begin
    ItemData := BondageItemsList[Rnd.Random(BondageItemsList.Count)];

    if TargetPlayer.Inventory.Equipped[ItemData.MainSlot] <> nil then
    begin
      if TargetPlayer.Inventory.Equipped[ItemData.MainSlot].ItemData.IsBondage then // we guarantee it's web, selected above
      begin
        TargetPlayer.Inventory.Equipped[ItemData.MainSlot].MaxDurability := TargetPlayer.Inventory.Equipped[ItemData.MainSlot].MaxDurability * 1.05 + Sqrt(Rnd.Random) * ItemData.Durability;
        TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Durability := (TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Durability + TargetPlayer.Inventory.Equipped[ItemData.MainSlot].MaxDurability) / 2;
        ShowLog('Another layer of web is wrapped around %s''s %s', [TargetPlayer.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondage);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
      end else
      begin
        ShowLog('A bolt of web hits %s for %.1n', [TargetPlayer.Inventory.Equipped[ItemData.MainSlot].Data.DisplayName, MarkData.Damage], ColorLogAbsorbDamage);
        TargetPlayer.Inventory.DamageItem(ItemData.MainSlot, MarkData.Damage);
      end;
    end else
    begin
      ShowLog('Web sticks to %s''s %s', [TargetPlayer.Data.DisplayName, EquipSlotToHumanReadableString(ItemData.MainSlot)], ColorLogBondage);
      ViewGame.ShakeCharacter;
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemData));
      NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticlePlayerBound);
      TargetPlayer.PlaySurpriseSound;
    end;
    if TargetPlayer.Inventory.AllTiedUp then
      ShowLog(GetTranslation('ActorBondageAllTiedUp'), [TargetPlayer.Data.DisplayName], ColorLogBondage);
  end else // if there are no slots to apply web
    TargetPlayer.Hit(MarkData.Damage);

  BondageItemsList.Free;
end;

{ TProjectileWebData ----------------------------- }

function TProjectileWebData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileWebData.Validate;
begin
  inherited Validate;
  if Damage <= 0 then
    raise EDataValidationError.CreateFmt('Damage <= 0 in %s', [Self.ClassName]);
end;

procedure TProjectileWebData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  Damage := Element.AttributeSingle('Damage');
end;

function TProjectileWebData.Mark: TMarkClass;
begin
  Exit(TProjectileWeb);
end;

initialization
  RegisterSerializableObject(TProjectileWeb);
  RegisterSerializableData(TProjectileWebData);

end.

