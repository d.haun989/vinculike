{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkTickleTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract, GameApparelSlots;

type
  TMarkTickleTarget = class(TMarkTargetAbstract)
  strict private
    { Variation of TPlayerCharacter.UnequipAndDrop but drops at monster's position }
    procedure UnequipAndDrop(const E: TApparelSlot; const Forced: Boolean);
    procedure InteractWithSleepingTarget;
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkTickleTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  SysUtils,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation, GameSounds,
  GameViewGame, GameParticle, GameMap, GameMapItem, GameItemData, GameItemDatabase,
  GameInventoryItem, GamePlayerCharacter, GameMonster, GameActor;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TMarkTickleTarget.UnequipAndDrop(const E: TApparelSlot;
  const Forced: Boolean);
var
  ItemStolen: TInventoryItem;
begin
  ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(E, Forced);
  if ItemStolen <> nil then
  begin
    if Forced then
      Sound(ItemStolen.ItemData.SoundUnequip);
    Map.MapItemsList.Add(TMapItem.CreateItem(ParentMonster.LastTileX + ParentMonster.Size div 2, ParentMonster.LastTileY + ParentMonster.Size div 2, ItemStolen));
  end;
end;

procedure TMarkTickleTarget.InteractWithSleepingTarget;
var
  E: TApparelSlot;
  HadWeapon: Boolean;
  HadClothes: Boolean;
begin
  HadWeapon := (TargetPlayer.Inventory.Equipped[esWeapon] <> nil) and (not TargetPlayer.Inventory.Equipped[esWeapon].ItemData.IsBondage);
  HadClothes := (TargetPlayer.Inventory.EquippedSlotsRemovable > 1) or ((TargetPlayer.Inventory.EquippedSlotsRemovable > 0) and not HadWeapon);

  if HadWeapon and HadClothes then
    ShowLog(GetTranslation('ActorTickleStripAndDisarmSleeping'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleStrip);
  if not HadWeapon and HadClothes then
    ShowLog(GetTranslation('ActorTickleStripSleeping'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleStrip);
  if HadWeapon and not HadClothes then
    ShowLog(GetTranslation('ActorTickleDisarmSleeping'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleStrip);

  for E in TargetPlayer.Inventory.EquipmentSlots do
    if (TargetPlayer.Inventory.Equipped[E] <> nil) and not TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
      UnequipAndDrop(E, false); // Note: not forced strip, will not generate "disrobing" events

  if not HadWeapon and not HadClothes then
  begin
    if TargetPlayer.Inventory.CanUseHands then
    begin
      ShowLog(GetTranslation('ActorTickleFullBondageSleeping1'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog(GetTranslation('ActorTickleFullBondageSleeping2'), [], ColorLogTickleBondage);
      TargetPlayer.Inventory.UnequipAndDisintegrate(esWeapon, false); // make sure hands are not bound
      TargetPlayer.Inventory.UnequipAndDisintegrate(esFeet, false); // make sure feet are not bound
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
      //TODO: feet mittens!
      //TODO: special for replace bondage with mittens
    end else
    begin
      ShowLog('For a moment contemplating helpless %s sound asleep', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog('First of all %s makes sure that %s are locked in tightly', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.ReinforceItem(esWeapon);
      if (TargetPlayer.Inventory.Equipped[esFeet] <> nil) and TargetPlayer.Inventory.HasItem('bondage_feet_mittens') then
      begin
        ShowLog('And checks if feet mittens are secure and ring loudly', [], ColorLogTickleBondage);
        TargetPlayer.Inventory.ReinforceItem(esFeet);
      end else
      if TargetPlayer.Inventory.Equipped[esFeet] = nil then // equip feet mittens only if feet are not already restrained
      begin
        ShowLog('And zips on a pair of new feet mittens with golden bells', [], ColorLogTickleBondage);
        TargetPlayer.Inventory.UnequipAndDisintegrate(esFeet, false); // make sure feet are not bound
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
      end;
    end;
  end;
  if not HadWeapon and HadClothes then
  begin
    if TargetPlayer.Inventory.CanUseHands then
    begin
      if TargetPlayer.Inventory.Equipped[esWeapon] = nil then
      begin
        // hands were empty
        ShowLog(GetTranslation('ActorTickleHandsBondageSleeping1'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
        ShowLog(GetTranslation('ActorTickleHandsBondageSleeping2'), [], ColorLogTickleBondage);
      end else
      begin
        // here we know it was bondage item
        ShowLog('%s is more than happy to see %s''s hands restrained', [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
        ShowLog('But bondage mittens are more appropriate for peaceful entertainment', [], ColorLogTickleBondage);
        TargetPlayer.Inventory.UnequipAndDisintegrate(esWeapon, false); // make sure hands are not bound
      end;
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
    end else
    begin
      // hands were immobilized
      ShowLog('%s''s restrained hands is a very good start', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog('%s only makes sure %s won''t accidentally slip off', [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[esWeapon].Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.ReinforceItem(esWeapon);
    end;
  end;
  if HadWeapon and not HadClothes then
  begin
    if (TargetPlayer.Inventory.Equipped[esFeet] <> nil) and TargetPlayer.Inventory.HasItem('bondage_feet_mittens') then
    begin
      // there were feet mittens on feet
      ShowLog('It is good that %s likes her feet mittens and decided to keep them on', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      ShowLog('%s only makes sure they won''t accidentally slip off', [ParentActor.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.ReinforceItem(esFeet);
    end else
    begin
      // feet were nil or restrained with other restrain types
      ShowLog(GetTranslation('ActorTickleFeetBondageSleeping1'), [], ColorLogTickleBondage);
      ShowLog(GetTranslation('ActorTickleFeetBondageSleeping2'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.UnequipAndDisintegrate(esFeet, false); // make sure feet are not bound
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
    end;
  end;

  ShowLog(GetTranslation('ActorTickleStripSleepingNext'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
end;

procedure TMarkTickleTarget.Perform;
var
  E, E2: TApparelSlot;
  ChanceToDrag: Single;
begin
  if TargetPlayer.Unsuspecting then
    InteractWithSleepingTarget;
  // and proceed as normal

  E := TargetPlayer.Inventory.GetRandomEquipmentSlot;
  //todo: if top/bottom covered
  ChanceToDrag := -1;
  if (TargetPlayer.Inventory.Equipped[E] <> nil) then
  begin
    if TargetPlayer.Inventory.Equipped[E].ItemData.IsBondage then
    begin
      ShowLog(GetTranslation('ActorTickleBondage'), [ParentActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(E)], ColorLogTickleBondage);
      TargetPlayer.HitVigor(3 * ParentMonster.GetDamage);
      // go for another round
      repeat
        E2 := TargetPlayer.Inventory.GetRandomEquipmentSlot;
      until EquipSlotToHumanReadableString(E2) <> EquipSlotToHumanReadableString(E); // TODO something more useful here
      if (TargetPlayer.Inventory.Equipped[E2] <> nil) and not TargetPlayer.Inventory.Equipped[E2].ItemData.IsBondage then
      begin
        if E2 = esWeapon then
        begin
          ShowLog(GetTranslation('ActorTickleDisarmFurther'), [TargetPlayer.Inventory.Equipped[E2].Data.DisplayName], ColorLogTickleStrip);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISARM', ColorParticleTickleStrip);
        end else
        begin
          ShowLog(GetTranslation('ActorTickleStripFurther'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E2].Data.DisplayName, EquipSlotToHumanReadableString(E2)], ColorLogTickleStrip);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticleTickleStrip);
        end;
        UnequipAndDrop(E2, true);
        ChanceToDrag := 0.1;
      end else
      begin
        if (E2 = esWeapon) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < 0.5) or TargetActor.Immobilized) then
        begin
          ShowLog(GetTranslation('ActorTickleForceBondageHandsFurther'), [TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(E2), ParentActor.Data.DisplayName], ColorLogTickleBondage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
          TargetPlayer.PlaySurpriseSound;
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
          Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
          ChanceToDrag := 0;
        end else
        if (E2 = esFeet) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < 0.5) or TargetActor.Immobilized) then
        begin
          ShowLog(GetTranslation('ActorTickleForceBondageFeetFurther'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName, EquipSlotToHumanReadableString(E2)], ColorLogTickleBondage);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
          TargetPlayer.PlaySurpriseSound;
          TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
          Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
          ChanceToDrag := 0;
        end else
        begin
          TargetPlayer.PlayGiggleSound;
          ShowLog(GetTranslation('ActorTickleNakedFurther'), [TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(E2)], ColorLogTickle);
          NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'TICKLE', ColorParticleTickle);
          TargetPlayer.HitVigor(7 * ParentMonster.GetDamage);
          TargetPlayer.HitPsyche(ParentMonster.GetDamage);
          ChanceToDrag := 0.5;
          ViewGame.WakeUp(true, true);
        end;
      end;
    end else
    begin
      if E = esWeapon then
      begin
        ShowLog(GetTranslation('ActorTickleDisarm'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, EquipSlotToHumanReadableString(E)], ColorLogTickleStrip);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'DISARM', ColorParticleTickleStrip);
      end else
      begin
        ShowLog(GetTranslation('ActorTickleStrip'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName, EquipSlotToHumanReadableString(E)], ColorLogTickleStrip);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'STRIP', ColorParticleTickleStrip);
      end;
      UnequipAndDrop(E, true);
      ChanceToDrag := 0.05;
    end;
  end else
  begin
    TargetPlayer.PlayGiggleSound;
    ShowLog(GetTranslation('ActorTickleNaked'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName, EquipSlotToHumanReadableString(E)], ColorLogTickle);
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'TICKLE', ColorParticleTickle);
    TargetPlayer.HitVigor(5 * ParentMonster.GetDamage);
    TargetPlayer.HitPsyche(ParentMonster.GetDamage);
    ChanceToDrag := 0.15;
    if (E = esAmulet) and ((Rnd.Random < Sqr(TargetPlayer.Vigor / TargetPlayer.PlayerCharacterData.MaxVigor)) or (not TargetPlayer.Inventory.CanUseHands) or TargetActor.Immobilized) then
    begin
      ShowLog('As a souvenir for the fun time %s gets an amulet with loud golden bells', [TargetActor.Data.DisplayName], ColorLogTickleBondage);
      TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['amulet_bells']));
      Sound(TargetPlayer.Inventory.Equipped[E].ItemData.SoundEquip);
    end;
    ViewGame.WakeUp(true, true);
    // if target is exhausted - try equipping bondage items
    if (TargetPlayer.Vigor <= 0) or (Rnd.Random > Sqrt(Sqrt(TargetPlayer.Vigor / TargetPlayer.PlayerCharacterData.MaxVigor))) or TargetActor.Immobilized then // sqrtsqrt=16% chance for 12-20% chance at 50% stamina (sqrtsqrtsqrt=9%)
    begin
      repeat
        if Rnd.RandomBoolean then
          E2 := esWeapon
        else
          E2 := esFeet;
      until E2 <> E;
      if (E2 = esWeapon) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < 0.04 * ParentMonster.GetDamage) or TargetActor.Immobilized) then
      begin
        ShowLog(GetTranslation('ActorTickleForceBondageHandsExhausted'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
        TargetPlayer.PlaySurpriseSound;
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_mittens']));
        Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
        ChanceToDrag := 0;
      end else
      if (E2 = esFeet) and (TargetPlayer.Inventory.Equipped[E2] = nil) and ((Rnd.Random < 0.04 * ParentMonster.GetDamage) or TargetActor.Immobilized) then
      begin
        ShowLog(GetTranslation('ActorTickleForceBondageFeetExhausted'), [TargetActor.Data.DisplayName, ParentActor.Data.DisplayName], ColorLogTickleBondage);
        NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, 'BOUND', ColorParticleTickleBondage);
        TargetPlayer.PlaySurpriseSound;
        TargetPlayer.Inventory.EquipItem(TInventoryItem.NewItem(ItemsDataDictionary['bondage_feet_mittens']));
        Sound(TargetPlayer.Inventory.Equipped[E2].ItemData.SoundEquip);
        ChanceToDrag := 0;
      end;
    end;
  end;
  if ChanceToDrag < 0 then
    raise Exception.Create('ChanceToDrag was not set');
  if (Rnd.Random < ChanceToDrag) and (TargetPlayer.PassableTiles[ParentActor.LastTile]) then
  begin
    ShowLog(GetTranslation('ActorTickleDrag'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
    //TargetPlayer.ActionFinished;
    TargetPlayer.PlayGruntSound;
    TargetPlayer.Teleport(ParentActor.LastTileX, ParentActor.LastTileY);
  end;
end;

procedure TMarkTickleTarget.Update(SecondsPassed: Single);

begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkTickleTargetData ----------------------------- }

function TMarkTickleTargetData.Mark: TMarkClass;
begin
  Exit(TMarkTickleTarget);
end;

initialization
  RegisterSerializableObject(TMarkTickleTarget);
  RegisterSerializableData(TMarkTickleTargetData);

end.

