{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileBug;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils,
  GameProjectileBugSimple, GameMarkAbstract;

type
  TProjectileBug = class(TProjectileBugSimple)
  strict private const
    MinTeleportRange = 15;
    MaxTeleportRange = 32;
    TeleportChanceMiss = 1.0;
    TeleportChanceHit = 0.5;
  strict private
    procedure TeleportToTarget;
  public
    procedure HitWall; override;
    procedure HitPlayer; override;
  end;

  TProjectileBugData = class(TProjectileBugSimpleData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameInventoryItem, GameMapItem, GameApparelSlots,
  GameMap, GameMapTypes, GameActionIdle, GameActor,
  GameViewGame, GameLog, GameTranslation, GameColors, GameRandom;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$ENDIF}

{$DEFINE TargetPlayer:=ViewGame.CurrentCharacter}

procedure TProjectileBug.TeleportToTarget;
const
  RetryCount = Integer(1000);
var
  Dist: TDistanceMapArray;
  ToX, ToY: Int16;
  Count: Integer;
begin
  if not ParentActor.CanAct then // TODO: unsafe?
    Exit;

  Dist := Map.DistanceMap(TargetPlayer);
  Count := 0;
  repeat
    Inc(Count);
    repeat // TODO: Optimize
      ToX := Rnd.Random(Map.SizeX);
      ToY := Rnd.Random(Map.SizeY);
    until (Dist[ToX + Map.SizeX * ToY] > MinTeleportRange) and (Dist[ToX + Map.SizeX * ToY] <= MaxTeleportRange) and Map.PassableTiles[ParentActor.PredSize][ToX + Map.SizeX * ToY]
  until TargetPlayer.LineOfSightMore(ToX + ParentActor.Size div 2, ToY + ParentActor.Size div 2) or (Count > RetryCount);
  ShowLog('%s blinks and changes position', [ParentActor.Data.DisplayName], ColorLogTickle);
  // TODO: Teleport without resetting action to Idle (and thus making Self nil here)
  ParentActor.X := ToX;
  ParentActor.Y := ToY;
  ParentActor.CenterX := ParentActor.X + ParentActor.HalfSize;
  ParentActor.CenterY := ParentActor.Y + ParentActor.HalfSize;
  ParentActor.LastTileX := ToX;
  ParentActor.LastTileY := ToY;
  ParentActor.LastTile := ParentActor.LastTileX + Map.SizeX * ParentActor.LastTileY;
end;

procedure TProjectileBug.HitWall;
begin
  inherited;
  if Rnd.Random < TeleportChanceMiss then
    TeleportToTarget;
end;

procedure TProjectileBug.HitPlayer;
begin
  inherited;
  if Rnd.Random < TeleportChanceHit then
    TeleportToTarget;
end;

{ TProjectileBugData ----------------------------- }

function TProjectileBugData.Mark: TMarkClass;
begin
  Exit(TProjectileBug);
end;

initialization
  RegisterSerializableObject(TProjectileBug);
  RegisterSerializableData(TProjectileBugData);

end.

