{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameProjectileBugSimple;

{$INCLUDE compilerconfig.inc}

interface
uses
  Generics.Collections, DOM,
  CastleUtils,
  GameApparelSlots, GameProjectileAbstract, GameMarkAbstract;

type
  TProjectileBugSimple = class(TProjectileAbstract)
  public
    procedure HitWall; override;
    procedure HitPlayer; override;
  end;

  TProjectileBugSimpleData = class(TProjectileAbstractData)
  public const
    // TargetSlots = [esFeet, esBottomOver, esBottomUnder, esTopUnder, esTopOver, esTopOverOver]; // needs reworking the Perform a bit
  strict protected
    function CanBeIndependent: Boolean; override;
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    StaminaDamage: Single;
    WillDamage: Single;
    StaminaDamageNude: Single;
    WillDamageNude: Single;
    StaminaDamageBondage: Single;
    WillDamageBondage: Single;
    function Mark: TMarkClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameInventoryItem, GameMapItem,
  GameActionIdle, GameMap, GameActor, GameSounds,
  GameViewGame, GameLog, GameTranslation, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE MarkData:=(Data as TProjectileBugSimpleData)}
{$ELSE}
{$DEFINE MarkData:=TProjectileBugSimpleData(Data)}
{$ENDIF}

{$DEFINE TargetPlayer:=ViewGame.CurrentCharacter}

procedure TProjectileBugSimple.HitWall;
begin
  //todo: spawn explosion
end;

procedure TProjectileBugSimple.HitPlayer;
var
  ItemStolen: TInventoryItem;
  E: TApparelSlot;
begin
  ViewGame.WakeUp(true, true);
  if TargetPlayer.Immobilized then
  begin
    ShowLog('Bugs crawl all over immobilized %s and she can only desperately gasp in panic', [TargetPlayer.Data.DisplayName], ColorLogItemBreak);
    TargetPlayer.HitPsyche(MarkData.WillDamageNude * 2);
    ViewGame.ShakeMap;
  end;
  if (TargetPlayer.Inventory.EquippedClothesRemovable > 0) and TargetPlayer.Inventory.CanUseHands then
  begin
    ItemStolen := TargetPlayer.Inventory.UnequipItemAndReturn(TargetPlayer.Inventory.GetRandomClothesSlotEquippedItem, true);
    ItemStolen.Broken := true;
    Map.MapItemsList.Add(TMapItem.CreateItem(TargetPlayer.LastTileX + TargetPlayer.PredSize div 2, TargetPlayer.LastTileY + TargetPlayer.PredSize div 2, ItemStolen));
    ShowLog('A swarm of insects sticks to %s''s %s and she throws it down in panic', [TargetPlayer.Data.DisplayName, ItemStolen.Data.DisplayName], ColorLogItemBreak);
    TargetPlayer.HitVigor(MarkData.StaminaDamage);
    TargetPlayer.HitPsyche(MarkData.WillDamage);
  end else
  begin
    E := TargetPlayer.Inventory.GetRandomClothesSlot;
    if TargetPlayer.Inventory.Equipped[E] <> nil then // IsBondage redundant?
    begin
      ShowLog('A few beetles crawl under %s''s %s and she desperately struggles to get rid of them', [TargetPlayer.Data.DisplayName, TargetPlayer.Inventory.Equipped[E].Data.DisplayName], ColorLogVigorDamage);
      TargetPlayer.HitPsyche(MarkData.WillDamageNude);
      TargetPlayer.HitVigor(MarkData.StaminaDamageNude);
      ViewGame.ShakeMap;
      TargetPlayer.CurrentAction := TActionIdle.NewAction(TargetPlayer);
      TargetPlayer.CurrentAction.Start;
    end else
    begin
      ShowLog('A dozen of bugs catch to %s''s %s and she dances like crazy to shake them off', [TargetPlayer.Data.DisplayName, EquipSlotToHumanReadableString(E)], ColorLogVigorDamage);
      TargetPlayer.HitPsyche(MarkData.WillDamageBondage);
      TargetPlayer.HitVigor(MarkData.StaminaDamageBondage);
      ViewGame.ShakeMap;
      TargetPlayer.CurrentAction := TActionIdle.NewAction(TargetPlayer);
      TargetPlayer.CurrentAction.Start;
    end;
  end;
  Sound(MarkData.HitSound);
end;

{ TProjectileBugSimpleData ----------------------------- }

function TProjectileBugSimpleData.CanBeIndependent: Boolean;
begin
  Exit(true);
end;

procedure TProjectileBugSimpleData.Validate;
begin
  inherited Validate;
  if StaminaDamage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamage <= 0 in %s', [Self.ClassName]);
  if WillDamage <= 0 then
    raise EDataValidationError.CreateFmt('WillDamage <= 0 in %s', [Self.ClassName]);
  if StaminaDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageNude <= 0 in %s', [Self.ClassName]);
  if WillDamageNude <= 0 then
    raise EDataValidationError.CreateFmt('WillDamageNude <= 0 in %s', [Self.ClassName]);
  if StaminaDamageBondage <= 0 then
    raise EDataValidationError.CreateFmt('StaminaDamageBondage <= 0 in %s', [Self.ClassName]);
  if WillDamageBondage <= 0 then
    raise EDataValidationError.CreateFmt('WillDamageBondage <= 0 in %s', [Self.ClassName]);
end;

procedure TProjectileBugSimpleData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  StaminaDamage := Element.AttributeSingle('StaminaDamage');
  WillDamage := Element.AttributeSingle('WillDamage');
  StaminaDamageNude := Element.AttributeSingle('StaminaDamageNude');
  WillDamageNude := Element.AttributeSingle('WillDamageNude');
  StaminaDamageBondage := Element.AttributeSingle('StaminaDamageBondage');
  WillDamageBondage := Element.AttributeSingle('WillDamageBondage');
end;

function TProjectileBugSimpleData.Mark: TMarkClass;
begin
  Exit(TProjectileBugSimple);
end;

initialization
  RegisterSerializableObject(TProjectileBugSimple);
  RegisterSerializableData(TProjectileBugSimpleData);

end.

