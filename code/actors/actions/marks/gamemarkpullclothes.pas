{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkPullClothes;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract, GameApparelSlots;

type
  TMarkPullClothes = class(TMarkTargetAbstract)
  strict private const
    DragChance = 0.7;
    TeleportToTargetChanceAfterAttackHit = 0.7;
    TeleportToTargetChanceAfterAttackMiss = 0.4;
    MaxTeleportRange = 29;
    PullableClothes = [esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver];
  strict private
    procedure Perform;
    procedure TeleportToTarget;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkPullClothesData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  SysUtils, Math,
  GameSerializableObject, GameSerializableData,
  GameRandom, GameLog, GameColors, GameTranslation, GameSounds,
  GameViewGame, GameParticle, GameMap, GameMapTypes, GameActor,
  GameItemData, GameInventoryItem, GamePlayerCharacter, GameMonster;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$DEFINE ParentMonster:=(Parent as TMonster)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$DEFINE ParentMonster:=TMonster(Parent)}
{$ENDIF}

procedure TMarkPullClothes.Perform;
var
  E: TApparelSlot;
  DamageDone: Single;
  ItemNameCache: String;
  CanDrag: Boolean;

  function TargetHasPullableClothes: Boolean;
  var
    E: TApparelSlot;
  begin
    for E in PullableClothes do
      if TargetPlayer.Inventory.Equipped[E] <> nil then
        Exit(true);
    Exit(false);
  end;

  function GetRandomPullableSlot: TApparelSlot;
  begin
    repeat
      Result := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
    until (Result in PullableClothes) and (TargetPlayer.Inventory.Equipped[Result] <> nil);
  end;

begin
  if TargetHasPullableClothes then
  begin
    E := GetRandomPullableSlot;
    ItemNameCache := TargetPlayer.Inventory.Equipped[E].Data.DisplayName; // TargetPlayer.Equipped[E] can become nil if item will be broken by attack
    ParentActor.PlayAttackSound;
    CanDrag := false;
    if TargetPlayer.Unsuspecting then
    begin
      DamageDone := Min(3 * ParentActor.GetDamage, TargetPlayer.Inventory.Equipped[E].Durability);
      // bug: item damage log comes before "our" log, but we don't know beforehands if the item will break
      TargetPlayer.Inventory.DamageItem(E, DamageDone); // it can destroy bondage items too
      if TargetPlayer.Inventory.Equipped[E] <> nil then
      begin
        // WARNING: ParentActor.GetDamage is almost always less than DamageDone, and therefore "Min" doesn't make much sense here
        // See https://gitlab.com/EugeneLoza/vinculike/-/issues/444
        DamageDone := Min(DamageDone, 2 * ParentActor.GetDamage) * 5; // Min may be an overkill here as we're workarounding the bug when item was disintegrated - and thus not going into this condition
        CanDrag := true;
      end;
      ShowLog(GetTranslation('ActorClothesPullSleeping'), [TargetActor.Data.DisplayName, ItemNameCache, DamageDone], ColorLogItemSteal);
    end else
    begin
      DamageDone := Min(ParentActor.GetDamage, TargetPlayer.Inventory.Equipped[E].Durability);
      // bug: item damage log comes before "our" log, but we don't know beforehands if the item will break
      TargetPlayer.Inventory.DamageItem(E, DamageDone); // it can destroy bondage items too
      if TargetPlayer.Inventory.Equipped[E] <> nil then
      begin
        DamageDone := DamageDone * 5;
        CanDrag := true;
      end;
      ShowLog(GetTranslation('ActorClothesPull'), [ParentActor.Data.DisplayName, ItemNameCache, TargetActor.Data.DisplayName, DamageDone], ColorLogItemSteal);
    end;
    NewParticle(TargetPlayer.CenterX, TargetPlayer.CenterY, Round(DamageDone).ToString, ColorParticlePlayerHurt);
    TargetPlayer.DegradeVital(DamageDone);
    if CanDrag then
    begin
      if (Rnd.Random < DragChance) and (TargetPlayer.PassableTiles[ParentActor.LastTile]) then
      begin
        ShowLog(GetTranslation('ActorTickleDrag'), [ParentActor.Data.DisplayName, TargetActor.Data.DisplayName], ColorLogTickle);
        //TargetPlayer.ActionFinished;
        Sound('female_grunt');
        TargetPlayer.Teleport(ParentActor.LastTileX, ParentActor.LastTileY);
      end else
      if Rnd.Random < TeleportToTargetChanceAfterAttackHit then
        TeleportToTarget;
    end;
  end else
  begin
    TargetActor.Hit(ParentActor.GetDamage / 2);
    ParentActor.PlayAttackSound;
    ParentActor.DamageWeapon;
  end;
  ViewGame.WakeUp(true, true);
end;

procedure TMarkPullClothes.TeleportToTarget;
var
  Dist: TDistanceMapArray;
  ToX, ToY: Int16;
begin
  Dist := Map.DistanceMap(Target);
  repeat // TODO: Optimize
    ToX := Rnd.Random(Map.SizeX);
    ToY := Rnd.Random(Map.SizeY);
  until (Dist[ToX + Map.SizeX * ToY] > 5) and (Dist[ToX + Map.SizeX * ToY] <= MaxTeleportRange) and Map.PassableTiles[ParentActor.PredSize][ToX + Map.SizeX * ToY];
  ShowLog(GetTranslation('ActorTeleportCloser'), [ParentActor.Data.DisplayName], ColorLogTickle);
  // TODO: Teleport without resetting action to Idle (and thus making Self nil here)
  ParentActor.X := ToX;
  ParentActor.Y := ToY;
  ParentActor.CenterX := ParentActor.X + ParentActor.HalfSize;
  ParentActor.CenterY := ParentActor.Y + ParentActor.HalfSize;
  ParentActor.LastTileX := ToX;
  ParentActor.LastTileY := ToY;
  ParentActor.LastTile := ParentActor.LastTileX + Map.SizeX * ParentActor.LastTileY;
end;

procedure TMarkPullClothes.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform
    else
    if Rnd.Random < TeleportToTargetChanceAfterAttackMiss then
      TeleportToTarget;
    EndAction;
    Exit;
  end;
end;

{ TMarkPullClothesData ----------------------------- }

function TMarkPullClothesData.Mark: TMarkClass;
begin
  Exit(TMarkPullClothes);
end;

initialization
  RegisterSerializableObject(TMarkPullClothes);
  RegisterSerializableData(TMarkPullClothesData);

end.

