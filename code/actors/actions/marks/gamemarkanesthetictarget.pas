{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMarkAnestheticTarget;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameMarkAbstract;

type
  TMarkAnestheticTarget = class(TMarkTargetAbstract)
  strict private const
    VigorBlackhole = 100;
  strict private
    procedure Perform;
  public
    procedure Update(SecondsPassed: Single); override;
  end;

  TMarkAnestheticTargetData = class(TMarkAbstractData)
  public
    function Mark: TMarkClass; override;
  end;

implementation
uses
  GameSerializableObject, GameSerializableData,
  GameLog, GameColors, GameTranslation,
  GameViewGame, GameActor,
  GamePlayerCharacter, GameActionPlayerUnconscious;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE TargetActor:=(Target as TActor)}
{$DEFINE TargetPlayer:=(Target as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE TargetActor:=TActor(Target)}
{$DEFINE TargetPlayer:=TPlayerCharacter(Target)}
{$ENDIF}

procedure TMarkAnestheticTarget.Perform;
begin
  TargetPlayer.DegradeVigor(ParentActor.GetDamage);
  TargetPlayer.Vigor := -VigorBlackhole;
  ParentActor.PlayAttackSound;
  ShowLog('%s smells anesthetic and immediately exhales, but it''s too late and her consciousness fades into blackness', [TargetActor.Data.DisplayName], ColorLogTrap);
  TargetPlayer.CurrentAction := TActionPlayerUnconscious.NewAction(TargetPlayer);
  //TargetPlayer.CurrentAction.Start; // It will log "start resting", TODO: should better be a "new" action TPlayerUnconscious
  ViewGame.UnPauseGame;
end;

procedure TMarkAnestheticTarget.Update(SecondsPassed: Single);
begin
  inherited;
  if Phase < -1 then
    Exit; // we already show error in Inherited

  if Phase > Data.Duration then
  begin
    if Sqr(TargetActor.CenterX - CenterX) + Sqr(TargetActor.CenterY - CenterY) <= Sqr(HalfSize + TargetActor.HalfSize) then
      Perform;
    EndAction;
    Exit;
  end;
end;

{ TMarkAnestheticTargetData ----------------------------- }

function TMarkAnestheticTargetData.Mark: TMarkClass;
begin
  Exit(TMarkAnestheticTarget);
end;

initialization
  RegisterSerializableObject(TMarkAnestheticTarget);
  RegisterSerializableData(TMarkAnestheticTargetData);

end.

