{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerUnconscious;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActionAbstract;

type
  TActionPlayerUnconscious = class(TActionAbstract)
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    function CanStop: Boolean; override;
  public
    procedure Update(const SecondsPassed: Single); override;
    destructor Destroy; override;
  end;

  TActionPlayerUnconsciousData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  CastleXmlUtils,
  GameSimpleSerializableObject, GameSerializableData,
  GamePlayerCharacter, GameLog, GameTranslation, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

function TActionPlayerUnconscious.NoiseMultiplier: Single;
begin
  Exit(0.7);
end;

function TActionPlayerUnconscious.NoiseAddition: Single;
begin
  Exit(0.0);
end;

function TActionPlayerUnconscious.CanStop: Boolean;
begin
  // don't use inherited
  Result := ParentPlayer.Vigor > 0;
  if not Result then
    ShowLog('%s is unconscious and cannot wake up', [ParentPlayer.Data.DisplayName], ColorLogNotEnoughVigor);
end;

procedure TActionPlayerUnconscious.Update(const SecondsPassed: Single);
begin
  //inherited;
  ParentPlayer.RegenerateMaxStats(SecondsPassed, 1.5);
end;

destructor TActionPlayerUnconscious.Destroy;
begin
  if (Parent <> nil) and (ParentPlayer.Data <> nil) then
    ShowLog('%s jumps up trying to remember where she is and what has happened', [ParentPlayer.Data.DisplayName], ColorLogCancel);
  inherited Destroy;
end;

{ TActionPlayerUnconsciousData -------------------------------}

function TActionPlayerUnconsciousData.Action: TActionClass;
begin
  Exit(TActionPlayerUnconscious);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerUnconscious);
  RegisterSerializableData(TActionPlayerUnconsciousData);

end.

