{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActionPlayerFixItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameSimpleSerializableObject,
  GameMapItem,
  GameActionAbstract;

const
  FixItemExperience = 10;

type
  TActionPlayerFixItem = class(TActionAbstract)
  public const
    Duration = Single(4);
  strict private
    Phase: Single;
    procedure Perform;
  public
    MapItem: TMapItem;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    function NoiseMultiplier: Single; override;
    function NoiseAddition: Single; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single); override;
    class function NewAction(const AParent: TObject): TActionAbstract; override;
    destructor Destroy; override;
  end;

  TActionPlayerFixItemData = class(TActionAbstractData)
  public
    function Action: TActionClass; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSerializableObject, GameSerializableData,
  GameActor, GamePlayerCharacter, GameTranslation, GameLog, GameColors, GameRandom,
  GameViewGame;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TActionPlayerFixItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('Phase', Phase);
  Element.AttributeSet('MapItem', MapItem.ReferenceId);
end;

procedure TActionPlayerFixItem.Load(const Element: TDOMElement);
begin
  inherited;
  Phase := Element.AttributeSingle('Phase');
  MapItem := ObjectByReferenceId(Element.AttributeQWord('MapItem')) as TMapItem; // MapItems are deserealized before Player characters, so this should be fine
end;

function TActionPlayerFixItem.NoiseMultiplier: Single;
begin
  Exit(1.3);
end;

function TActionPlayerFixItem.NoiseAddition: Single;
begin
  Exit(MapItem.Item.ItemData.Noise * 2); // will result in 2x multiplier, item isn't equipped
end;

procedure TActionPlayerFixItem.Start;
begin
  inherited Start;
  ShowLog(GetTranslation('ActorStartsRepairingItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName, TActionPlayerFixItem.Duration], ColorLogActionStart);
end;

procedure TActionPlayerFixItem.Perform;
var
  OldDurability: Single;
begin
  OldDurability := MapItem.Item.Durability;
  MapItem.Item.Fix(0.5, 0.7 + 0.3 * Rnd.Random);
  ShowLog(GetTranslation('ActorRepairsItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogInventory);
  ParentPlayer.Experience.AddExperience(FixItemExperience * (MapItem.Item.Durability - OldDurability) / MapItem.Item.MaxDurability);
  MapItem := nil;
end;

procedure TActionPlayerFixItem.Update(const SecondsPassed: Single);
begin
  Phase += SecondsPassed;
  if Phase > Duration then
  begin
    Perform;
    OnFinished;
    Exit;
  end
end;

class function TActionPlayerFixItem.NewAction(const AParent: TObject): TActionAbstract;
begin
  Result := inherited NewAction(AParent);
  (Result as TActionPlayerFixItem).Phase := 0;
end;

destructor TActionPlayerFixItem.Destroy;
begin
  if (MapItem <> nil) and (Parent <> nil) and (ParentActor.Data <> nil) then
  begin
    ShowLog(GetTranslation('ActorCancelRepairItemLog'), [ParentActor.Data.DisplayName, MapItem.Item.Data.DisplayName], ColorLogCancel);
    //OnFinished; // we most likely already in OnFinished so endless loop
  end;
  inherited Destroy;
end;

{ TActionPlayerFixItemData -------------------------------}

function TActionPlayerFixItemData.Action: TActionClass;
begin
  Exit(TActionPlayerFixItem);
end;

initialization
  RegisterSimpleSerializableObject(TActionPlayerFixItem);
  RegisterSerializableData(TActionPlayerFixItemData);

end.

