{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePositionedObject;

{$INCLUDE compilerconfig.inc}

interface
uses
  DOM,
  GameSerializableObject, GameMapTypes;

type
  TPositionedObject = class abstract(TSerializableObject)
  strict protected
    procedure TeleportToRandomPoint;
    { Picks best visible grade of the object area }
    function VisibleGrade: TVisibleGrade;
  public
    { CACHE of object's size. It should not be saved/loaded/changed }
    Size, PredSize: Byte;
    HalfSize: Single;
    procedure SetSize(const ASize: Byte);
  public
    X, Y: Single;
    CenterX, CenterY: Single;
    LastTileX, LastTileY: Int16;
    LastTile: SizeInt;
    procedure Teleport(const ToX, ToY: Int16); virtual;
    function PassableTiles: TMoveArray; virtual;
    { Add Seed of this object to the provided TSeedList }
    procedure AddSeed(const SeedList: TCoordList);
  public
    function IsVisible: Boolean; virtual;
    { If this object has unobstructed line of sight to that object? - performs a raycast on integer map coordinates }
    function LineOfSight(const ThatObject: TPositionedObject): Boolean;
    function LineOfSightMore(const ThatObject: TPositionedObject): Boolean;
    function LineOfSightMore(const AX, AY: Int16): Boolean;
    function LineOfSight(const AX, AY: Int16): Boolean;
    { Is object at given coordinates? Used for UI clicks }
    function IsHere(const AX, AY: Single; const AdditionalMargin: Single): Boolean; inline;
    function IsHere(const AX, AY: Int16; const AdditionalMargin: Int16): Boolean; inline;
    function SphericalDistanceNormalizedSqr(const ThatObject: TPositionedObject): Single;
    function Collides(const ThatObject: TPositionedObject; const AdditionalMargin: Single): Boolean; inline;
    function CollidesInt(const ThatObject: TPositionedObject; const AdditionalMargin: Int16): Boolean; inline;
    function CollidesInt(const AX, AY: Int16; const AdditionalMargin: Int16): Boolean; inline;
    function DistanceTo(const ThatObject: TPositionedObject): Single;
    function DistanceToSqr(const ThatObject: TPositionedObject): Single;
    function DistanceTo(const AX, AY: Single): Single;
    function DistanceToSqr(const AX, AY: Single): Single;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  end;

implementation
uses
  CastleXmlUtils,
  GameMap, GameRandom, GameLog, GameColors;

procedure TPositionedObject.TeleportToRandomPoint;
begin
  repeat
    LastTileX := Rnd.Random(Map.SizeX);
    LastTileY := Rnd.Random(Map.SizeY);
  until PassableTiles[LastTileX + Map.SizeX * LastTileY];
  Teleport(LastTileX, LastTileY);
end;

procedure TPositionedObject.SetSize(const ASize: Byte);
begin
  Size := ASize;
  PredSize := Pred(ASize);
  HalfSize := Single(ASize) / 2.0;
end;

procedure TPositionedObject.Teleport(const ToX, ToY: Int16);
begin
  if (ToX <= 0) or (ToY <=0) or (ToX >= Map.PredSizeX) or (ToY >= Map.PredSizeY) then
  begin
    ShowError('%s is trying to teleport to broken coordinates (%d, %d) on map %dx%d. Teleporting to random point instead.', [ClassName, ToX, ToY, Map.SizeX, Map.SizeY]);
    TeleportToRandomPoint;
    Exit;
  end;
  X := ToX;
  Y := ToY;
  CenterX := X + HalfSize;
  CenterY := Y + HalfSize;
  LastTileX := ToX;
  LastTileY := ToY;
  LastTile := LastTileX + Map.SizeX * LastTileY;
end;

function TPositionedObject.PassableTiles: TMoveArray;
begin
  Exit(Map.PassableTiles[PredSize]);
end;

procedure TPositionedObject.AddSeed(const SeedList: TCoordList);
var
  DX, DY: Integer;
begin
  for DX := 0 to PredSize do
    for DY := 0 to PredSize do
      SeedList.Add(IntCoord(LastTileX + DX, LastTileY + DY));
end;

function TPositionedObject.IsVisible: Boolean;
begin
  Exit(VisibleGrade >= RememberedVisible);
end;

function TPositionedObject.LineOfSight(const ThatObject: TPositionedObject): Boolean;
var
  TX, TY: Int16;
  //IX, IY: Integer;
  JX, JY: Integer;
begin
  //for IX := 0 to PredSize do
    //for IY := 0 to PredSize do
      for JX := 0 to ThatObject.PredSize do
        for JY := 0 to ThatObject.PredSize do
        begin
          TX := ThatObject.LastTileX + JX;
          TY := ThatObject.LastTileY + JY;
          // WARNING in Ray ToX, ToY are var and values are changed! - TODO
          if Map.Ray({Data.PredSize}0, LastTileX + Size div 2, LastTileY + Size div 2, TX, TY) then
            Exit(true);
        end;
  Exit(false);
end;

function TPositionedObject.LineOfSightMore(
  const ThatObject: TPositionedObject): Boolean;
var
  IX, IY, TX, TY: Int16;
begin
  TX := Trunc(ThatObject.CenterX);
  TY := Trunc(ThatObject.CenterY);
  for IX := 0 to PredSize do
    for IY := 0 to PredSize do
      if not Map.Ray(0, LastTileX + IX, LastTileY + IY, TX, TY) then
        Exit(false);
  Exit(true);
end;

function TPositionedObject.LineOfSightMore(const AX, AY: Int16): Boolean;
var
  IX, IY, TX, TY: Int16;
begin
  TX := AX;
  TY := AY;
  for IX := 0 to PredSize do
    for IY := 0 to PredSize do
      if not Map.Ray(0, LastTileX + IX, LastTileY + IY, TX, TY) then
        Exit(false);
  Exit(true);
end;

function TPositionedObject.LineOfSight(const AX, AY: Int16): Boolean;
var
  TX, TY: Int16;
begin
  TX := AX;
  TY := AY;
  Exit(Map.Ray({Data.PredSize}0, LastTileX + Size div 2, LastTileY + Size div 2, TX, TY));
end;

function TPositionedObject.VisibleGrade: TVisibleGrade;
var
  IX, IY: Integer;
begin
  if PredSize = 0 then
    Exit(Map.Visible[LastTile])
  else
  begin
    Result := 0;
    for IX := LastTileX to LastTileX + PredSize do  // here we count on that the object cannot be beyond map border
      for IY := LastTileY to LastTileY + PredSize do
        if Map.Visible[IX + Map.SizeX * IY] > Result then
          Result := Map.Visible[IX + Map.SizeX * IY]; // TODO: Optimize!
  end;
end;

function TPositionedObject.IsHere(const AX, AY: Single; const AdditionalMargin: Single): Boolean; inline;
begin
  Exit((AX >= X - AdditionalMargin) and (AY >= Y - AdditionalMargin) and (AX <= X + Size + AdditionalMargin) and (AY <= Y + Size + AdditionalMargin));
end;

function TPositionedObject.IsHere(const AX, AY: Int16; const AdditionalMargin: Int16): Boolean; inline;
begin
  // note, we need PredSize for Int IsHere, and Size for float IsHere
  Exit((AX >= LastTileX - AdditionalMargin) and (AY >= LastTileY - AdditionalMargin) and
       (AX <= LastTileX + PredSize + AdditionalMargin) and (AY <= LastTileY + PredSize + AdditionalMargin));
end;

function TPositionedObject.SphericalDistanceNormalizedSqr(const ThatObject: TPositionedObject): Single;
begin
  Exit(
    (Sqr(CenterX - ThatObject.CenterX) + Sqr(CenterY - ThatObject.CenterY)) // DistanceToSqr
    / (HalfSize + ThatObject.HalfSize));
end;

function TPositionedObject.Collides(const ThatObject: TPositionedObject;
  const AdditionalMargin: Single): Boolean; inline;
begin
  Exit((X <= ThatObject.X + ThatObject.Size + AdditionalMargin) and (X + Size + AdditionalMargin >= ThatObject.X) and
       (Y <= ThatObject.Y + ThatObject.Size + AdditionalMargin) and (Y + Size + AdditionalMargin >= ThatObject.Y));
end;

function TPositionedObject.CollidesInt(const ThatObject: TPositionedObject;
  const AdditionalMargin: Int16): Boolean; inline;
begin
  // note, we need PredSize for Int collision, and Size for float collision
  Exit((LastTileX <= ThatObject.LastTileX + ThatObject.PredSize + AdditionalMargin) and (LastTileX + PredSize + AdditionalMargin >= ThatObject.LastTileX) and
       (LastTileY <= ThatObject.LastTileY + ThatObject.PredSize + AdditionalMargin) and (LastTileY + PredSize + AdditionalMargin >= ThatObject.LastTileY));
end;

function TPositionedObject.CollidesInt(const AX, AY: Int16;
  const AdditionalMargin: Int16): Boolean;
begin
  Exit((LastTileX <= AX + AdditionalMargin) and (LastTileX + PredSize + AdditionalMargin >= AX) and
       (LastTileY <= AY + AdditionalMargin) and (LastTileY + PredSize + AdditionalMargin >= AY));
end;

function TPositionedObject.DistanceTo(const ThatObject: TPositionedObject): Single;
begin
  Exit(Sqrt(Sqr(CenterX - ThatObject.CenterX) + Sqr(CenterY - ThatObject.CenterY)));
end;

function TPositionedObject.DistanceToSqr(const ThatObject: TPositionedObject): Single;
begin
  Exit(Sqr(CenterX - ThatObject.CenterX) + Sqr(CenterY - ThatObject.CenterY));
end;

function TPositionedObject.DistanceTo(const AX, AY: Single): Single;
begin
  Exit(Sqrt(Sqr(CenterX - AX) + Sqr(CenterY - AY)));
end;

function TPositionedObject.DistanceToSqr(const AX, AY: Single): Single;
begin
  Exit(Sqr(CenterX - AX) + Sqr(CenterY - AY));
end;

procedure TPositionedObject.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Element.AttributeSet('X', X);
  Element.AttributeSet('Y', Y);
  Element.AttributeSet('CenterX', CenterX); // as PositionedObject doesn't have Size we have to store CenterX/Y too
  Element.AttributeSet('CenterY', CenterY);
  Element.AttributeSet('LastTileX', LastTileX);
  Element.AttributeSet('LastTileY', LastTileY);
end;

procedure TPositionedObject.Load(const Element: TDOMElement);
begin
  inherited;
  X := Element.AttributeSingle('X');
  Y := Element.AttributeSingle('Y');
  CenterX := Element.AttributeSingle('CenterX');
  CenterY := Element.AttributeSingle('CenterY');
  LastTileX := Element.AttributeInteger('LastTileX');
  LastTileY := Element.AttributeInteger('LastTileY');
  if (LastTileX <= 0) or (LastTileY <=0) or (LastTileX >= Map.PredSizeX) or (LastTileY >= Map.PredSizeY) then
    ShowError('%s is trying to load broken coordinates (%d, %d) on map %dx%d.', [ClassName, LastTileX, LastTileY, Map.SizeX, Map.SizeY]);
  LastTile := LastTileX + Map.SizeX * LastTileY;
end;

end.

