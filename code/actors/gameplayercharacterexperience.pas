{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePlayerCharacterExperience;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActorData;

const
  ExperienceBase = 200;
  ExperienceCurve = 0.666666667;

const
  LevelVitalityBonus = 5;
  LevelVigorBonus = 10;
  LevelPsycheBonus = 5;

type
  TPlayerCharacterExperience = class(TObject)
  strict private
    procedure DoLevelUp;
  public
    Xp: Single; // I want extensive per-skill experience system, but no idea how yet
    procedure Recalculate;
    function Level: UInt16;
    function ExperienceToNextLevel: Single;
    procedure AddExperience(const AddXp: Single);
  public const Signature = 'experience';
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
  public
    Parent: TObject;
  end;

implementation
uses
  SysUtils, Math,
  CastleXmlUtils,
  GamePlayerCharacter, GameSounds, GameLog, GameColors;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentPlayerCharacter:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentPlayerCharacter:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TPlayerCharacterExperience.DoLevelUp;
begin
  ShowLog('LEVEL UP! Earned new level = %d', [Level], ColorLogLevelup);
  ShowLog('Experience: %d; To next level: %d', [Round(Xp), Round(ExperienceToNextLevel)], ColorLogExperience);
  Sound('level_up');
  ParentPlayerCharacter.MaxVital += LevelVitalityBonus;
  ParentPlayerCharacter.MaxVigor += LevelVigorBonus;
  ParentPlayerCharacter.MaxPsyche += LevelPsycheBonus;
  Recalculate;
end;

procedure TPlayerCharacterExperience.Recalculate;
begin
  ParentPlayerCharacter.PlayerCharacterData.MaxVital := 50 + LevelVitalityBonus * Level;
  ParentPlayerCharacter.PlayerCharacterData.MaxVigor := 50 + LevelVigorBonus * Level;
  ParentPlayerCharacter.PlayerCharacterData.MaxPsyche := 50 + LevelPsycheBonus * Level;
end;

function TPlayerCharacterExperience.Level: UInt16;
begin
  Result := Trunc(Power(Xp / ExperienceBase, ExperienceCurve));
end;

function TPlayerCharacterExperience.ExperienceToNextLevel: Single;
begin
  Result := ExperienceBase * Power(Level + 1, 1 / ExperienceCurve) - Xp;
  if Result < 1 then
    Result := 1;
end;

procedure TPlayerCharacterExperience.AddExperience(const AddXp: Single);
var
  OldLevel: UInt16;
begin
  if AddXp > 0 then
  begin
    OldLevel := Level;
    Xp += AddXp;
    ShowLog('Received %d XP', [Round(AddXp)], ColorLogExperience);
    if Level > OldLevel then
      DoLevelUp;
  end;
end;

procedure TPlayerCharacterExperience.Save(const Element: TDOMElement);
begin
  Element.AttributeSet('Xp', Xp);
end;

procedure TPlayerCharacterExperience.Load(const Element: TDOMElement);
begin
  Xp := Element.AttributeSingle('Xp');
  Recalculate;
end;


end.

