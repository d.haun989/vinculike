{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInventory;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameApparelSlots, GameItemData, GameAbstractItem, GameInventoryItem,
  GamePaperDollSprite, GameEnchantmentAbstract;

const
  WeaponDamageOnHit = Single(2.0);
  BondageDamageOnHit = Single(0.5); // I really don't like having 2 different coefficients, but we need to balance stamina-durability-damage

type
  TInventory = class(TObject)
  public
    TopCovered, BottomCovered: Integer;
    TopCoveredRemovable, BottomCoveredRemovable: Integer;
    procedure UpdateCoveredStatus;
  strict private
    function GetEquipped(const ApparelSlot: TApparelSlot): TInventoryItem;
    procedure SetEquipped(const ApparelSlot: TApparelSlot; const EquipItem: TInventoryItem);
    function EnchantmentRequirementsMet(const Enchantment: TEnchantmentAbstract): Boolean;
  public
    Apparel: array [TApparelSlot] of TAbstractItem;
    property Equipped [ApparelSlot: TApparelSlot]: TInventoryItem read GetEquipped write SetEquipped;
    function ClothesSlots: TApparelSlotsSet;
    function GetRandomClothesSlot: TApparelSlot;
    function GetRandomClothesSlotEquippedItem: TApparelSlot;
    function GetRandomClothesSlotEquippedItemOrBondage: TApparelSlot;
    { Clothes and weapons }
    function EquipmentSlots: TApparelSlotsSet;
    function GetRandomEquipmentSlot: TApparelSlot;
    function GetRandomEquipmentSlotEquippedItem: TApparelSlot;
    function GetRandomEquipmentSlotEquippedItemOrBondage: TApparelSlot;
    function EquippedSlotsRemovable: Integer; // for now 0 - 6
    function EquippedClothesRemovable: Integer;
    function EquippedClothesAndBondage: Integer;
    function TotalIdleNoise: Single;
    procedure DamageItem(const Slot: TApparelSlot; const Damage: Single);
    procedure DamageItem(const AItem: TInventoryItem; const Damage: Single);
    procedure DamageWeapon;
    procedure WardrobeMalfunction;
    procedure EquipItem(const AItem: TInventoryItem);
    procedure EquipApparel(const AItem: TAbstractItem);
    function AllTiedUp: Boolean;
    function UnequipItemAndReturn(const AEquipSlot: TApparelSlot; const Forced: Boolean): TInventoryItem;
    procedure UnequipAndDrop(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    procedure UnequipAndDisintegrate(const AEquipSlot: TApparelSlot; const Forced: Boolean);
    procedure DestroyEquippedItemSafe(const AEquipSlot: TApparelSlot);
    procedure UnequipAndDisintegrateEverything;
    function CanUseHands: Boolean; //override; todo?
    function FindEffectAdditive(const EffectClass: TEnchantmentClass): Single;
    function FindEffectMultiplier(const EffectClass: TEnchantmentClass): Single;
  public
    procedure MaximizeDurability(const AEquipSlot: TApparelSlot);
    procedure ReinforceItem(const AEquipSlot: TApparelSlot);
    function ShouldMaximizeDurability(const AEquipSlot: TApparelSlot): Boolean;
    function ShouldMaximizeDurability(const AItemName: String): Boolean;
    function HasItem(const AOtherItemData: TItemData): Boolean;
    function HasItem(const AOtherItemName: String): Boolean;
  public
    PaperDollSprite: TPaperDollSprite;
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
  public
    Parent: TObject;
    constructor Create; // override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameViewGame, GameRandom, GameLog, GameTranslation, GameSounds, GameColors,
  GameMap, GameMapItem, GameActor, GamePlayerCharacter, GameItemDatabase,
  GameBodyPart;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentActor:=(Parent as TActor)}
{$DEFINE ParentPlayer:=(Parent as TPlayerCharacter)}
{$ELSE}
{$DEFINE ParentActor:=TActor(Parent)}
{$DEFINE ParentPlayer:=TPlayerCharacter(Parent)}
{$ENDIF}

procedure TInventory.UpdateCoveredStatus;
var
  E: TApparelSlot;
begin
  TopCovered := 0;
  BottomCovered := 0;
  TopCoveredRemovable := 0;
  BottomCoveredRemovable := 0;
  for E in EquipmentSlots do
    if (Equipped[E] <> nil) and (E = Equipped[E].Data.MainSlot) then
      begin
        if Equipped[E].ItemData.CoversTop then
        begin
          Inc(TopCovered);
          if not Equipped[E].ItemData.IsBondage then
            Inc(TopCoveredRemovable);
        end;
        if Equipped[E].ItemData.CoversBottom then
        begin
          Inc(BottomCovered);
          if not Equipped[E].ItemData.IsBondage then
            Inc(BottomCoveredRemovable);
        end;
      end;
end;

function TInventory.GetEquipped(const ApparelSlot: TApparelSlot): TInventoryItem;
begin
  Exit(Apparel[ApparelSlot] as TInventoryItem);
end;

procedure TInventory.SetEquipped(const ApparelSlot: TApparelSlot;
  const EquipItem: TInventoryItem);
begin
  Apparel[ApparelSlot] := EquipItem;
end;

function TInventory.ClothesSlots: TApparelSlotsSet;
begin
  Exit([esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver, esTopOverOver, esAmulet, esNeck]);
end;

function TInventory.EquipmentSlots: TApparelSlotsSet;
begin
  Exit(ClothesSlots + [esWeapon]);
end;

function TInventory.GetRandomClothesSlot: TApparelSlot;
begin
  repeat
    Result := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
  until Result in ClothesSlots;
end;

function TInventory.GetRandomClothesSlotEquippedItem: TApparelSlot;
begin
  repeat
    Result := GetRandomClothesSlot;
  until (Equipped[Result] <> nil) and not Equipped[Result].ItemData.IsBondage; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomClothesSlotEquippedItemOrBondage: TApparelSlot;
begin
  repeat
    Result := GetRandomClothesSlot;
  until Equipped[Result] <> nil; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomEquipmentSlot: TApparelSlot;
begin
  repeat
    Result := TApparelSlot(Rnd.Random(Ord(High(TApparelSlot)) + 1));
  until Result in EquipmentSlots;
end;

function TInventory.GetRandomEquipmentSlotEquippedItem: TApparelSlot;
begin
  repeat
    Result := GetRandomEquipmentSlot;
  until (Equipped[Result] <> nil) and not Equipped[Result].ItemData.IsBondage; // warning, no checks! It can freeze if all are nil
end;

function TInventory.GetRandomEquipmentSlotEquippedItemOrBondage: TApparelSlot;
begin
  repeat
    Result := GetRandomEquipmentSlot;
  until Equipped[Result] <> nil; // warning, no checks! It can freeze if all are nil
end;

function TInventory.EquippedSlotsRemovable: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in EquipmentSlots do
    if (Equipped[E] <> nil) and not Equipped[E].ItemData.IsBondage then
      Inc(Result);
end;

function TInventory.EquippedClothesRemovable: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ClothesSlots do
    if (Equipped[E] <> nil) and not Equipped[E].ItemData.IsBondage then
      Inc(Result);
end;

function TInventory.EquippedClothesAndBondage: Integer;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in ClothesSlots do
    if Equipped[E] <> nil then
      Inc(Result);
end;

function TInventory.TotalIdleNoise: Single;
var
  E: TApparelSlot;
begin
  Result := 0;
  for E in EquipmentSlots do
    if (Equipped[E] <> nil) and (Equipped[E].Data.MainSlot = E) then
      Result += Equipped[E].ItemData.Noise;
end;

procedure TInventory.DamageItem(const Slot: TApparelSlot;
  const Damage: Single);
begin
  DamageItem(Equipped[Slot], Damage);
end;

procedure TInventory.DamageItem(const AItem: TInventoryItem;
  const Damage: Single);
begin
  if AItem.ItemData.Indestructible then
    Exit;

  AItem.Durability -= Damage;
  // item breaks
  if AItem.ItemData.IsBondage then
  begin
    if AItem.Durability <= 0 then
    begin
      ViewGame.ShakeCharacter;
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('BondageItemDisintegratesLog'), [AItem.Data.DisplayName], ColorLogItemDisintegrated);
      Sound(AItem.ItemData.SoundDisintegrate);
      UnequipAndDisintegrate(AItem.Data.MainSlot, true);
    end;
  end else
  if Rnd.Random > (3 * AItem.Durability / AItem.MaxDurability) then
  begin
    ViewGame.ShakeCharacter;

    if (Rnd.Random < AItem.Durability / ItemDisintegrationThreshold) and AItem.ItemData.CanBeRepaired then
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemBreaksLog'), [AItem.Data.DisplayName], ColorLogItemBreak);
      AItem.Broken := true;
      Sound(AItem.ItemData.SoundBreak);
      UnequipAndDrop(AItem.Data.MainSlot, true);
    end else
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemDisintegratesLog'), [AItem.Data.DisplayName], ColorLogItemDisintegrated);
      Sound(AItem.ItemData.SoundDisintegrate);
      UnequipAndDisintegrate(AItem.Data.MainSlot, true);
    end;

    UpdateCoveredStatus;
    ParentPlayer.HitPsyche(BrokenItemPsycheDamage);
  end else
  // a chance to slip off
  if (Rnd.Random < AItem.ItemData.WardrobeMalfunctionChance) then
  begin
    if Parent = ViewGame.CurrentCharacter then
      ShowLog(GetTranslation('ItemWardrobeMalfunctionLog'), [AItem.Data.DisplayName], ColorLogItemBreak);
    Sound(AItem.ItemData.SoundUnequip);
    UnequipAndDrop(AItem.Data.MainSlot, true);
  end;
end;

procedure TInventory.WardrobeMalfunction;
var
  E: TApparelSlot;
begin
  for E in EquipmentSlots do
    if (Equipped[E] <> nil) and (Equipped[E].Data.MainSlot = E) and (Rnd.Random < Equipped[E].ItemData.WardrobeMalfunctionChance) then
    begin
      if Parent = ViewGame.CurrentCharacter then
        ShowLog(GetTranslation('ItemWardrobeMalfunctionLog'), [Equipped[E].Data.DisplayName], ColorLogItemBreak);
      Sound(Equipped[E].ItemData.SoundUnequip);
      UnequipAndDrop(E, true);
    end;
end;

procedure TInventory.EquipItem(const AItem: TInventoryItem);
var
  E: TApparelSlot;
begin
  for E in AItem.Data.EquipSlots do
    UnequipAndDrop(E, false);
  //Equipped[AItem.Data.MainSlot] := AItem; // why not working???
  for E in AItem.Data.EquipSlots do
  begin
    Equipped[E] := AItem;
    // TODO: BUG! the topoverover is rendered later (order is broken) and therefore renders below Overlay
    //Exit; // no need to equip in other slots
  end;
  ViewGame.InvalidateInventory(Parent);
  UpdateCoveredStatus;
end;

procedure TInventory.EquipApparel(const AItem: TAbstractItem);
var
  E: TApparelSlot;
begin
  if AItem is TInventoryItem then
    EquipItem(TInventoryItem(AItem))
  else
    for E in AItem.Data.EquipSlots do
      Apparel[E] := AItem;
end;

function TInventory.AllTiedUp: Boolean;
var
  E: TApparelSlot;
begin
  for E in [esFeet, esBottomUnder, esBottomOver, esTopUnder, esTopOver] do // TODO: either more generic, or may be property of BinderSlime
    if (Equipped[E] = nil) or (not Equipped[E].ItemData.IsBondage) then
      Exit(false);
  Exit(true);
end;

procedure TInventory.UnequipAndDrop(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
var
  AItem: TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit; // TODO: This is not needed, make call safe on logic level
  AItem := UnequipItemAndReturn(AEquipSlot, Forced);
  Map.MapItemsList.Add(TMapItem.CreateItem(ParentPlayer.LastTileX + ParentPlayer.PredSize div 2, ParentPlayer.LastTileY + ParentPlayer.PredSize div 2, AItem));
end;

procedure TInventory.UnequipAndDisintegrate(const AEquipSlot: TApparelSlot;
  const Forced: Boolean);
var
  AItem: TInventoryItem;
begin
  if Apparel[AEquipSlot] = nil then
    Exit; // TODO: This is not needed, make call safe on logic level
  AItem := UnequipItemAndReturn(AEquipSlot, Forced);
  AItem.Free;
end;

procedure TInventory.DestroyEquippedItemSafe(const AEquipSlot: TApparelSlot);
var
  AItem: TAbstractItem;
  E: TApparelSlot;
begin
  AItem := Apparel[AEquipSlot];
  if (AItem <> nil) then
  begin
    for E in AItem.Data.EquipSlots do
      if Apparel[E] = AItem then
        Apparel[E] := nil;
    ViewGame.InvalidateInventory(Parent);
    AItem.Free;
  end;
end;

procedure TInventory.UnequipAndDisintegrateEverything;
var
  E: TApparelSlot;
begin
  for E in EquipmentSlots do
    UnequipAndDisintegrate(E, false);
end;

function TInventory.UnequipItemAndReturn(const AEquipSlot: TApparelSlot;
  const Forced: Boolean): TInventoryItem;
var
  E: TApparelSlot;
  WasTopCovered, WasBottomCovered: Boolean;
  SomethingExposed: Boolean;
begin
  if Forced then
  begin
    UpdateCoveredStatus; // Just to make 100% sure we're dealing with properly equipped items
    WasTopCovered := TopCovered > 0;
    WasBottomCovered := BottomCovered > 0;
  end;

  Result := Equipped[AEquipSlot];
  for E in Result.Data.EquipSlots do
    if Apparel[E] = Result then
      Apparel[E] := nil;
  ViewGame.InvalidateInventory(Parent);
  UpdateCoveredStatus;

  if Forced then
  begin
    SomethingExposed := false;
    if WasTopCovered and (TopCovered = 0) then
    begin
      ShowLog(GetTranslation('PlayerLostTop'), [ParentActor.Data.DisplayName], ColorLogNaked);
      ParentPlayer.HitPsyche(ParentPlayer.Psyche * StripTopPsycheFractionDamage);
      SomethingExposed := true;
    end;
    if WasBottomCovered and (BottomCovered = 0) then
    begin
      ShowLog(GetTranslation('PlayerLostBottom'), [ParentActor.Data.DisplayName], ColorLogNaked);
      ParentPlayer.HitPsyche(ParentPlayer.Psyche * StripBottomPsycheFractionDamage);
      SomethingExposed := true;
    end;
    if SomethingExposed then
      ParentPlayer.PlaySurpriseSound;
    if (WasTopCovered or WasBottomCovered) and (TopCovered = 0) and (BottomCovered = 0) then
      ShowLog(GetTranslation('PlayerNaked'), [ParentActor.Data.DisplayName], ColorLogNaked);
  end;
end;

procedure TInventory.DamageWeapon;
begin
  inherited;
  if (Equipped[esWeapon] <> nil) and not Equipped[esWeapon].ItemData.IsBondage then // TODO: IsBondage
    DamageItem(Equipped[esWeapon], WeaponDamageOnHit)
  else
  if (Equipped[esWeapon] <> nil) then
  begin
    // TODO: more generic
    if Equipped[esWeapon].ItemData.Damage > 0 then
      DamageItem(Equipped[esWeapon], BondageDamageOnHit)
  end else
  //Hit(1)
    ;
end;

function TInventory.CanUseHands: Boolean;
begin
  // TODO: more generic, will hurt when poses will be introduced
  if Equipped[esWeapon] <> nil then
    Exit(not Equipped[esWeapon].ItemData.HandsUnavailable)
  else
    Exit(true);
end;

function TInventory.EnchantmentRequirementsMet(const Enchantment: TEnchantmentAbstract): Boolean;
begin
  Exit(
    (not Enchantment.RequiresNudeTop or (TopCovered = 0)) and
    (not Enchantment.RequiresDressedTop or (TopCovered > 0)) and
    (not Enchantment.RequiresNudeBottom or (BottomCovered = 0)) and
    (not Enchantment.RequiresDressedBottom or (BottomCovered = 0)) and
    (not Enchantment.RequiresUnarmed or ((Equipped[esWeapon] = nil) or (Equipped[esWeapon].ItemData.Damage <= 0))) and
    (ParentPlayer.Vital / ParentPlayer.PlayerCharacterData.MaxVital <= Enchantment.RequiresHealthBelow)
  );
end;

function TInventory.FindEffectAdditive(const EffectClass: TEnchantmentClass): Single;
var
  A: TApparelSlot;
  E: TEnchantmentAbstract;
begin
  Result := 0;
  for A in EquipmentSlots do
    if (Equipped[A] <> nil) and (Equipped[A].Data.MainSlot = A) then
      for E in Equipped[A].Enchantments do
        if (E is EffectClass) and EnchantmentRequirementsMet(E) then
          Result += E.Strength;
end;

function TInventory.FindEffectMultiplier(const EffectClass: TEnchantmentClass): Single;
var
  A: TApparelSlot;
  E: TEnchantmentAbstract;
begin
  Result := 1.0;
  for A in EquipmentSlots do
    if (Equipped[A] <> nil) and (Equipped[A].Data.MainSlot = A) then
      for E in Equipped[A].Enchantments do
        if (E is EffectClass) and EnchantmentRequirementsMet(E) then
          Result *= E.Strength;
end;

procedure TInventory.MaximizeDurability(const AEquipSlot: TApparelSlot);
begin
  Equipped[AEquipSlot].MaxDurability := Equipped[AEquipSlot].ItemData.Durability;
  Equipped[AEquipSlot].Durability := Equipped[AEquipSlot].MaxDurability;
end;

procedure TInventory.ReinforceItem(const AEquipSlot: TApparelSlot);
begin
  Equipped[AEquipSlot].MaxDurability := (Equipped[AEquipSlot].MaxDurability + Equipped[AEquipSlot].ItemData.Durability) / 2;
  Equipped[AEquipSlot].Durability := (Equipped[AEquipSlot].Durability + Equipped[AEquipSlot].MaxDurability) / 2;
end;

function TInventory.ShouldMaximizeDurability(const AEquipSlot: TApparelSlot): Boolean;
begin
  Exit((Equipped[AEquipSlot] <> nil) and (Equipped[AEquipSlot].Durability < Equipped[AEquipSlot].ItemData.Durability / 2));
end;

function TInventory.ShouldMaximizeDurability(const AItemName: String): Boolean;
var
  AItem: TItemData;
begin
  AItem := ItemsDataDictionary[AItemName] as TItemData;
  if HasItem(AItem) then
    Exit(ShouldMaximizeDurability(AItem.MainSlot))
  else
    Exit(false);
end;

function TInventory.HasItem(const AOtherItemData: TItemData): Boolean;
begin
  Exit((Equipped[AOtherItemData.MainSlot] <> nil) and (Equipped[AOtherItemData.MainSlot].Data = AOtherItemData));
end;

function TInventory.HasItem(const AOtherItemName: String): Boolean;
begin
  //Exit(HasItem(ItemsDataDictionary[AOtherItemName]));
  Exit((Equipped[ItemsDataDictionary[AOtherItemName].MainSlot] <> nil) and (Equipped[ItemsDataDictionary[AOtherItemName].MainSlot].Data.Id = AOtherItemName));
end;

procedure TInventory.Save(const Element: TDOMElement);
var
  E: TApparelSlot;
begin
  for E in TApparelSlot do
    if (Apparel[E] <> nil) and (Apparel[E].Data.MainSlot = E) then
      Apparel[E].Save(Element.CreateChild(ApparelSlotToStr(E)));
end;

procedure TInventory.Load(const Element: TDOMElement);
var
  E: TApparelSlot;
begin
  for E in TApparelSlot do
    if Element.Child(ApparelSlotToStr(E), false) <> nil then
    begin
      try
        EquipApparel(TAbstractItem.LoadClass(Element.Child(ApparelSlotToStr(E), true)) as TAbstractItem);
      except
        if Element.Child(ApparelSlotToStr(E), false) = nil then
          ShowError('Cannot load player "%s" inventory slot %s. Record not found.', [ParentPlayer.Data.DisplayName, ApparelSlotToStr(E)])
        else
          ShowError('Failed to load item %s in player "%s" inventory.', [Element.Child(ApparelSlotToStr(E), true).AttributeStringDef('Data.Id', 'N/A'), ParentPlayer.Data.DisplayName]);
        Apparel[E] := nil; // just to make sure
      end;
    end;
  for E in BodySlots do
    if (Apparel[E] = nil) or not (Apparel[E] is TBodyPart) then
    begin
      ShowError('Failed to load body slot %s for %s. Applying random body slot.', [ApparelSlotToStr(E), ParentPlayer.Data.DisplayName]);
      EquipApparel(TBodyPart.NewBodyPart(GetRandomItemForSlot(E)));
    end;
  UpdateCoveredStatus;
end;

constructor TInventory.Create;
begin
  inherited; //Parent is non-virtual and empty
  PaperDollSprite := TPaperDollSprite.Create;
  PaperDollSprite.Parent := Self;
  // TODO: something more meaningful? This one will just blink an empty sprite for 1 frame.
  PaperDollSprite.UpdateToInventory; // just to avoid image = nil // TODO - optimize
end;

destructor TInventory.Destroy;
var
  E, E2: TApparelSlot;
begin
  PaperDollSprite.Free;
  for E in TApparelSlot do
  begin
    for E2 in TApparelSlot do
      if (E2 <> E) and (Apparel[E] = Apparel[E2]) then
        Apparel[E2] := nil;
    FreeAndNil(Apparel[E]);
  end;
  inherited Destroy;
end;

end.

