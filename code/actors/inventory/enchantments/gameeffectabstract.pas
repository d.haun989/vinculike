{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEffectAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Generics.Collections, DOM,
  GameSerializableData;

type
  TEffectAbstract = class abstract(TSerializableData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Strength: Single;
    function Description: String; virtual; abstract; // Temporary: TODO
  end;
  TEffectsList = specialize TObjectList<TEffectAbstract>;
  TEffectClass = class of TEffectAbstract;

implementation
uses
  CastleXmlUtils;

procedure TEffectAbstract.Validate;
begin
  // todo
end;

procedure TEffectAbstract.Read(const Element: TDOMElement);
begin
  Strength := Element.AttributeSingle('Strength');
end;

end.

