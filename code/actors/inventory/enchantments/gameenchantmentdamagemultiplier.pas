{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameEnchantmentDamageMultiplier;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, DOM,
  GameEnchantmentAbstract;

type
  TEnchantmentDamageMultiplier = class(TEnchantmentAbstract)
  public
    function Description: String; override;
  end;

implementation
uses
  GameSerializableData;

function TEnchantmentDamageMultiplier.Description: String;
begin
  if Strength > 1 then
    Result := Format('Increases damage by %d%%%%' + RequirementDescription, [Round(100 * (Strength - 1))]) // We must call stupid "%%%%" here because when showing log we again call Format whic will "eat" %%->% symbol, we need two
  else
    Result := Format('Reduces damage by %d%%%%' + RequirementDescription, [Round(100 * (1 - Strength))]);
end;

initialization
  RegisterSerializableData(TEnchantmentDamageMultiplier);

end.

