{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePaperDoll;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleGlImages,
  GameApparelSlots;

type
  TPaperDoll = class(TCastleUserInterface)
  strict private
    RenderImage: TDrawableImage;
  public
    procedure UpdateToInventory;
    procedure Render; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils,
  CastleConfig, CastleRectangles, CastleImages, CastleVectors, CastleColors,
  GameViewGame;

procedure TPaperDoll.UpdateToInventory;
var
  E: TApparelSlot;
  Image: TRgbAlphaImage;
  Hidden: TApparelSlotsSet;
begin
  FreeAndNil(RenderImage);
  Image := TRgbAlphaImage.Create(
    ViewGame.CurrentCharacter.Inventory.Apparel[bsBody].Data.Image.Width,
    ViewGame.CurrentCharacter.Inventory.Apparel[bsBody].Data.Image.Height);
  Image.Clear(Vector4Byte(0, 0, 0, 0));

  Hidden := [];
  if UserConfig.GetValue('censored', false) then // TODO: Inventory.HiddenSlots
    Hidden += [bsNipples, bsNethers, bsPubic];
  for E in TApparelSlot do
    if (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) then
      Hidden += ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.HidesSlots;

  // This is surprisingly slow :(
  for E in TApparelSlot do
    if not (E in Hidden) and (ViewGame.CurrentCharacter.Inventory.Apparel[E] <> nil) then
      Image.DrawFrom(ViewGame.CurrentCharacter.Inventory.Apparel[E].Data.Image, 0, 0, dmBlendSmart);
  RenderImage := TDrawableImage.Create(Image, true, true);
end;

procedure TPaperDoll.Render;
var
  E: TApparelSlot;
  R: TFloatRectangle;
  W: Single;
begin
  inherited Render;
  // TODO: cache in Resize?
  W := Single(RenderRect.Height) * Single(RenderImage.Width) / Single(RenderImage.Height);
  R := FloatRectangle(RenderRect.Left + (RenderRect.Width - W) / 2, RenderRect.Bottom, W, RenderRect.Height);
  for E in TApparelSlot do
    RenderImage.Draw(R);
end;

constructor TPaperDoll.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FullSize := true; // TODO: Maybe temporary
end;

destructor TPaperDoll.Destroy;
begin
  RenderImage.Free;
  inherited Destroy;
end;


end.

