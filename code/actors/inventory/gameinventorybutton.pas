{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInventoryButton;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleControls, CastleMobileButton,
  {temporary}GameViewGame, GameLog,{/temporary}
  GameInventoryItem, GameMapItem, GameApparelSlots;

type
  TInventoryButtonAbstract = class abstract(TCastleMobileButton)
  strict protected
    HorizontalGroup: TCastleHorizontalGroup;
    SlotsVerticalGroup: TCastleVerticalGroup;
    function CreateImageForSlot(const ASlot: TApparelSlot): TCastleImageControl;
    procedure SetupCore(const Slots: Integer);
  public
    procedure Setup; virtual; abstract;
  end;

  TEmptyInventoryButton = class(TInventoryButtonAbstract)
  public
    Slot: TApparelSlot;
    procedure Setup; override;
  end;

  TInventoryButton = class(TInventoryButtonAbstract)
  strict private
    CaptionLabel: TCastleLabel;
    CaptionImage: TCastleImageControl;
    DurabilityLabel: TCastleLabel;
    procedure SetItemColor;
    procedure SetupCaption;
  public
    Item: TInventoryItem;
    procedure Setup; override;
    procedure UpdateCaption;
  end;

  TMapItemButton = class(TInventoryButton)
  public
    MapItem: TMapItem;
    procedure Setup; override;
  end;

implementation
uses
  SysUtils, Math,
  CastleUiControls, CastleVectors, CastleColors,
  GameFonts, GameColors, GameCachedImages, GameInventory,
  GameItemData;

function TInventoryButtonAbstract.CreateImageForSlot(
  const ASlot: TApparelSlot): TCastleImageControl;
begin
  Result := TCastleImageControl.Create(SlotsVerticalGroup);
  Result.DrawableImage := SlotImage[ASlot];
  Result.Color := Vector4(1, 1, 1, 0.2);
  Result.OwnsDrawableImage := false;
end;

procedure TInventoryButtonAbstract.SetupCore(const Slots: Integer);
begin
  CustomBackground := true;
  CustomBackgroundNormal.URL := 'castle-data:/ui/kodiakgraphics/inventory_button_light.png';
  CustomBackgroundNormal.ProtectedSides.AllSides := 15;
  Caption := '';
  Width := 400; // WARNING, will/must be reset extrenally (todo?)
  Height := 20 + 40 * Slots;
  AutoSize := false;
  AutoSizeWidth := false; // tODO redundant
  AutoSizeHeight := false;
  EnableParentDragging := true;
  HorizontalGroup := TCastleHorizontalGroup.Create(Self);
  HorizontalGroup.Anchor(vpMiddle, vpMiddle, 0);
  HorizontalGroup.Anchor(hpLeft, hpLeft, 10);
  HorizontalGroup.Spacing := 8;
  InsertFront(HorizontalGroup);
  SlotsVerticalGroup := TCastleVerticalGroup.Create(HorizontalGroup);
  SlotsVerticalGroup.Spacing := 8;
  HorizontalGroup.InsertFront(SlotsVerticalGroup);
end;

{=====================================================}

procedure TEmptyInventoryButton.Setup;
begin
  SetupCore(1);
  Caption := '';
  SlotsVerticalGroup.InsertFront(CreateImageForSlot(Slot));
end;

procedure TInventoryButton.SetupCaption;
var
  EI: TApparelSlot;
begin
  for EI in UiApparelSlots do
    if EI in Item.Data.EquipSlots then
      SlotsVerticalGroup.InsertBack(CreateImageForSlot(EI));
  CaptionLabel := TCastleLabel.Create(HorizontalGroup);
  CaptionLabel.Html := true;
  CaptionLabel.AutoSize := false;
  CaptionLabel.Height := 40;
  CaptionLabel.Alignment := hpMiddle;
  CaptionLabel.CustomFont := FontBender40;
  CaptionImage := TCastleImageControl.Create(HorizontalGroup);
  CaptionImage.Stretch := true;
  CaptionImage.Height := 40;
  CaptionImage.Width := CaptionImage.Height;
  DurabilityLabel := TCastleLabel.Create(HorizontalGroup);
  DurabilityLabel.CustomFont := FontBender20;
  HorizontalGroup.InsertFront(CaptionLabel);
  HorizontalGroup.InsertFront(CaptionImage);
  HorizontalGroup.InsertFront(DurabilityLabel);
end;

procedure TInventoryButton.SetItemColor;
var
  ItemColor: TCastleColor;
begin
  if Item.Broken then
    ItemColor := ColorUiItemBroken
  else
  if Item.ItemData.IsBondage then
    ItemColor := ColorUiItemBondage
  else
  if Item.Durability / Item.MaxDurability > 1 / 3.0 then
  begin
    if (Item.Durability > ItemDisintegrationThreshold) or (not Item.ItemData.CanBeRepaired) then
      ItemColor := ColorUiItemNormal
    else
      ItemColor := ColorUiItemNormalCanDisintegrate
  end else
  begin
    if (Item.Durability > ItemDisintegrationThreshold) or (not Item.ItemData.CanBeRepaired) then
      ItemColor := ColorUiItemDamaged
    else
      ItemColor := ColorUiItemDamagedCanDisintegrate;
  end;
  CaptionLabel.Color := ItemColor;
  if TCastleColor.Equals(ItemColor, ColorUiItemNormal) then
    CaptionImage.Color := CastleColors.White // hack, but for now acceptable
  else
  if TCastleColor.Equals(ItemColor, ColorUiItemNormalCanDisintegrate) then
    CaptionImage.Color := CastleColors.Silver // hack, but for now acceptable
  else
    CaptionImage.Color := ItemColor;
  DurabilityLabel.Color := ItemColor;
end;

procedure TInventoryButton.UpdateCaption;
begin
  SetItemColor;
  if ViewGame.FullInventory then
  begin
    CaptionLabel.Exists := true;
    CaptionLabel.Width := EffectiveWidth - 40 - 40; // TODO: move into onresize?
    CaptionImage.Exists := false;
    DurabilityLabel.Exists := false;
    if Item.ItemData.Indestructible then
      CaptionLabel.Caption := Item.Data.DisplayName
    else
      CaptionLabel.Caption := Format('%s %d/%d', [Item.Data.DisplayName, Max(1, Round(Item.Durability)), Round(Item.MaxDurability)]);
  end else
  begin
    CaptionLabel.Exists := false;
    CaptionImage.Exists := true;
    CaptionImage.Image := Item.ItemData.MapImage;
    CaptionImage.OwnsImage := false; // ugh, this is not good here, but if we ask it at creation - we don't free default image which gets automatically created
    if Item.ItemData.Indestructible then
      DurabilityLabel.Exists := false
    else
      DurabilityLabel.Caption := Round(Max(1, Round(Item.Durability))).ToString;
  end;
end;

procedure TInventoryButton.Setup;
begin
  SetupCore(Item.ItemData.NumberOfSlots);
  SetupCaption;
  UpdateCaption;
end;

procedure TMapItemButton.Setup;
begin
  Item := MapItem.Item;
  inherited;
end;

end.

