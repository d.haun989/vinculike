{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameApparelSlots;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections;

type
  { Possible apparel (body/equipment) slots of the item. Prefixes:
    asXxxx - apparel slots (generic)
    bsXxxx - body slots
    esXxxx - equipment slots }
  TApparelSlot = (

    //asNone,

    bsTail,

    //esBackTopOver
    //esBackBottomOver

    //bsHairBack

    bsBody, //split into more detailed pieces later
    //bsBreasts
    bsNipples,
    bsNethers,
    bsPubic,
    bsBodyTattoo,

    esFeet,
    esBottomUnder,
    esTopUnder,
    esBottomOver,
    esTopOver,

    bsHands,

    esTopOverOver,
    esAmulet,

    bsEyes,
    //bsPupils
    bsEyebrows,
    bsMouth,
    bsNose,
    bsHair,
    bsEars,

    esWeapon,
    esNeck
  );

type
  TApparelSlotsSet = Set of TApparelSlot;
  TApparelSlotsList = specialize TList<TApparelSlot>;

const
  TopSlots = [esTopOverOver, esTopOver, esTopUnder, esAmulet];
  BottomSlots = [esFeet, esBottomUnder, esBottomOver];
  BodySlots = [bsTail, bsBody, bsNipples, bsNethers, bsPubic, bsBodyTattoo, bsEyes, bsEyebrows, bsMouth, bsNose, bsHair, bsEars, bsHands];

var
  UiApparelSlots: TApparelSlotsList;

function ApparelSlotToStr(const AEquipSlot: TApparelSlot): String;
function StrToApparelSlot(const AString: String): TApparelSlot;
function EquipSlotToHumanReadableString(const AEquipSlot: TApparelSlot): String; // TODO
function EquipSlotsToHumanReadableString(const AEquipSlots: TApparelSlotsSet): String;
implementation
uses
  SysUtils, TypInfo,
  CastleStringUtils;

type
  EItemSlotConversionError = class(Exception);

function ApparelSlotToStr(const AEquipSlot: TApparelSlot): String;
begin
  Result := GetEnumName(TypeInfo(TApparelSlot), Ord(AEquipSlot));
end;
function StrToApparelSlot(const AString: String): TApparelSlot;
var
  E: TApparelSlot;
begin
  // TODO: Optimize
  for E in TApparelSlot do
    if ApparelSlotToStr(E) = AString then
      Exit(E);
  raise EItemSlotConversionError.Create('Cannot find EquipSlot: ' + AString);
end;

function EquipSlotToHumanReadableString(const AEquipSlot: TApparelSlot): String;
begin
  case AEquipSlot of
    esWeapon: Result := 'hands';
    esTopOver: Result := 'arms and chest';
    esTopOverOver: Result := 'shoulders';
    esTopUnder: Result := 'breasts';
    esBottomOver: Result := 'legs';
    esBottomUnder: Result := 'waist and groin';
    esNeck, esAmulet: Result := 'neck';
    esFeet: Result := 'feet';
    else
      raise EItemSlotConversionError.Create('Cannot convert EquipSlot to human readable string: ' + ApparelSlotToStr(AEquipSlot));
  end;
end;

function EquipSlotsToHumanReadableString(const AEquipSlots: TApparelSlotsSet): String;
var
  WSlots: Integer;
  TSlots: Integer;
  BSlots: Integer;
  E: TApparelSlot;
begin
  WSlots := 0;
  TSlots := 0;
  BSlots := 0;
  for E in AEquipSlots do
    if E = esWeapon then
      Inc(WSlots)
    else
    if E in TopSlots then
      Inc(TSlots)
    else
    if E in BottomSlots then
      Inc(BSlots);
  if WSlots + TSlots + BSlots = 1 then
    for E in AEquipSlots do
      Result := EquipSlotToHumanReadableString(E) // this is really weird looking cat, TODO: fix properly
  else
  if WSlots + TSlots = 0 then
    Result := 'lower body'
  else
  if WSlots + BSlots = 0 then
    Result := 'upper body'
  else
    Result := 'body';
end;

initialization
  UiApparelSlots := TApparelSlotsList.Create;
  UiApparelSlots.Add(esFeet);
  UiApparelSlots.Add(esBottomOver);
  UiApparelSlots.Add(esBottomUnder);
  UiApparelSlots.Add(esTopUnder);
  UiApparelSlots.Add(esTopOver);
  UiApparelSlots.Add(esTopOverOver);
  UiApparelSlots.Add(esAmulet);
  UiApparelSlots.Add(esNeck);
  UiApparelSlots.Add(esWeapon);
finalization
  UiApparelSlots.Free;
end.

