{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePaperDollSprite;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes,
  CastleUiControls, CastleGlImages,
  GameApparelSlots;

type
  TPaperDollSprite = class(TObject)
  public
    Image: TDrawableImage;
    Parent: TObject;
    procedure UpdateToInventory;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils,
  CastleImages, CastleConfig, CastleVectors,
  GameInventory, GameDrawFromColorized;

{$IFDEF SafeActorTypecast}
{$DEFINE ParentInventory:=(Parent as TInventory)}
{$ELSE}
{$DEFINE ParentInventory:=TInventory(Parent)}
{$ENDIF}

procedure TPaperDollSprite.UpdateToInventory;
var
  E: TApparelSlot;
  Img: TRgbAlphaImage;
  Hidden: TApparelSlotsSet;
begin
  FreeAndNil(Image);
  Img := TRgbAlphaImage.Create(99, 99);
  Img.Clear(Vector4Byte(0, 0, 0, 0));

  Hidden := [];
  if UserConfig.GetValue('censored', false) then // TODO: Inventory.HiddenSlots
    Hidden += [bsNipples, bsNethers, bsPubic];
  for E in TApparelSlot do
    if (ParentInventory.Apparel[E] <> nil) then
      Hidden += ParentInventory.Apparel[E].Data.HidesSlots;

  for E in TApparelSlot do
    if not (E in Hidden) and (ParentInventory.Apparel[E] <> nil) then
      if ParentInventory.Apparel[E].Data.Sprite <> nil then
        Img.DrawFromColorized(ParentInventory.Apparel[E].Data.Sprite, ParentInventory.Apparel[E].Data.AverageColor);
  Image := TDrawableImage.Create(Img, true, true);
end;

destructor TPaperDollSprite.Destroy;
begin
  FreeAndNil(Image);
  inherited Destroy;
end;


end.

