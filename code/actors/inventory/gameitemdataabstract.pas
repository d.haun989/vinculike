{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameItemDataAbstract;

{$INCLUDE compilerconfig.inc}

interface

uses
  SysUtils, Generics.Collections, DOM,
  CastleImages, CastleColors,
  GameApparelSlots, GameSerializableData;

type
  EItemDataValidationError = class(Exception);

type
  TItemDataAbstract = class abstract(TSerializableData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Id: String;
    DisplayName: String;
    EquipSlots: TApparelSlotsSet;
    HidesSlots: TApparelSlotsSet;
    MainSlot: TApparelSlot;
    Image: TRgbAlphaImage;
    AverageColor: TCastleColor;
    Sprite: TRgbAlphaImage;
  end;
  TItemsDataAbstractList = specialize TObjectList<TItemDataAbstract>;
  TItemsDataAbstractDictionary = specialize TObjectDictionary<String, TItemDataAbstract>;

implementation
uses
  CastleXMLUtils, CastleStringUtils,
  GameRandom, GameCachedImages, GameAverageColorCalculator;

procedure TItemDataAbstract.Validate;
begin
  //inherited; - parent is abstract
  if Id = '' then
    raise EItemDataValidationError.Create('Id = ""');
  if DisplayName = '' then
    raise EItemDataValidationError.Create('DisplayName = "" : ' + Id);
  if EquipSlots = [] then
    raise EItemDataValidationError.Create('EquipSlots = [] : ' + Id);
  if not (MainSlot in EquipSlots) then
    raise EItemDataValidationError.Create('not MainSlot in EquipSlots : ' + Id);
  if Image = nil then
    raise EItemDataValidationError.Create('Image = nil : ' + Id);
  // sprite can be nil
  // AverageColor not validated
end;

procedure TItemDataAbstract.Read(const Element: TDOMElement);
var
  SlotStringList: TCastleStringList;
  S: String;
  SpriteUrl: String;
begin
  //inherited -- Parent is abstract
  Id := Element.AttributeString('Id');
  DisplayName := Element.AttributeString('DisplayName');

  Image := LoadRgba('castle-data:/' + Element.AttributeString('ImageUrl'));
  SpriteUrl := Element.AttributeString('SpriteUrl');
  if SpriteUrl <> '' then
    Sprite := LoadRgba('castle-data:/' + SpriteUrl);

  SlotStringList := CreateTokens(Element.AttributeStringDef('EquipSlot', ''), [',']); // TODO: Not "def"?
  EquipSlots := [];
  for S in SlotStringList do
    EquipSlots := EquipSlots + [StrToApparelSlot(S)];
  MainSlot := StrToApparelSlot(SlotStringList[0]);
  SlotStringList.Free;

  SlotStringList := CreateTokens(Element.AttributeStringDef('HidesSlots', ''), [',']);
  HidesSlots := [];
  for S in SlotStringList do
    HidesSlots := HidesSlots + [StrToApparelSlot(S)];
  SlotStringList.Free;

  if Element.HasAttribute('AverageColor') then
    AverageColor := Element.AttributeColor('AverageColor')
  else
    AverageColor := Image.AverageColor;
end;

end.

