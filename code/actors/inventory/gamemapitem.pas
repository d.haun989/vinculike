{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMapItem;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSerializableObject,
  GamePositionedObject, GameInventoryItem;

type
  TMapItem = class(TPositionedObject)
  public
    Item: TInventoryItem;
  public const Signature = 'mapitem';
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    class function CreateItem(const AX, AY: Int16; const AItem: TInventoryItem): TMapItem;
    destructor Destroy; override;
  end;
  TMapItemsList = specialize TObjectList<TMapItem>;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameRandom, GameMap, GameLog, GameColors,
  GameViewGame;

type
  EMapItemLoadError = class(Exception);

procedure TMapItem.Save(const Element: TDOMElement);
begin
  inherited Save(Element);
  Item.Save(Element.CreateChild(TInventoryItem.Signature));
end;

procedure TMapItem.Load(const Element: TDOMElement);
begin
  inherited;
  try
    Item := TInventoryItem.LoadClass(Element.Child(TInventoryItem.Signature, true)) as TInventoryItem;
  except
    if Element.Child(TInventoryItem.Signature, false) = nil then
      ShowError('Map item doesn''t contain item record', [])
    else
      ShowError('Failed to load map item %s', [Element.Child(TInventoryItem.Signature, true).AttributeStringDef('Data.Id', 'N/A')]);
    raise EMapItemLoadError.Create('Failed to load map item');
  end;
  SetSize(1);
end;

class function TMapItem.CreateItem(const AX, AY: Int16;
  const AItem: TInventoryItem): TMapItem;
var
  NX: Int16;
  NY: Int16;

  function CanPutHere: Boolean;
  var
    I: TMapItem;
  begin
    if not Map.CanMove(NX + Map.SizeX * NY) then
      Exit(false);
    // Temporary TODO
    if (NX >= Map.ExitX - 3) and (NX <= Map.ExitX + 6) and
      (NY >= Map.ExitY - 3) and (NY <= Map.ExitY + 6) then
        Exit(false);
    for I in Map.MapItemsList do
      if (I.LastTileX = NX) and (I.LastTileY = NY) then
        Exit(false);
    Exit(true);
  end;

begin
  Result := TMapItem.Create(true);

  NX := AX;
  NY := AY;
  if (NX <= 0) or (NY <= 0) or (NX >= Map.PredSizeX) or (NY >= Map.PredSizeY) then
  begin
    ShowError('ERROR: Can''t put item %s at %d,%d.', [AItem.Data.Id, AX, AY]);
    if NX < 1 then NX := 1;
    if NY < 1 then NY := 1;
    if NX > Map.PredSizeX - 1 then NX := Map.PredSizeX - 1;
    if NY > Map.PredSizeY - 1 then NY := Map.PredSizeY - 1;
  end;

  // this is a bad idea TODO
  while not CanPutHere do
  begin
    NX += Rnd.Random(3) - 1;
    NY += Rnd.Random(3) - 1;
    if NX < 1 then NX := 1;
    if NY < 1 then NY := 1;
    if NX > Map.PredSizeX - 1 then NX := Map.PredSizeX - 1;
    if NY > Map.PredSizeY - 1 then NY := Map.PredSizeY - 1;
  end;
  Result.LastTileX := NX;
  Result.LastTileY := NY;
  Result.LastTile := Result.LastTileX + Map.SizeX * Result.LastTileY;
  Result.Item := AItem;
  Result.SetSize(1);
  Result.X := NX;
  Result.Y := NY;
  Result.CenterX := Result.X + Result.HalfSize;
  Result.CenterY := Result.Y + Result.HalfSize;
  ViewGame.InvalidatePosition(nil);
end;

destructor TMapItem.Destroy;
begin
  Item.Free;
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMapItem);
end.

