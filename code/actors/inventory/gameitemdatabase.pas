{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameItemDatabase;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections,
  GameApparelSlots, GameItemDataAbstract, GameItemData;

var
  { ... included bodyparts }
  ItemsDataDictionary: TItemsDataAbstractDictionary;
  { ... excludes bodyparts }
  ItemsDataList: TItemsDataList;
  //BodypartsList: TItemsDataList; // TODO: TBodyPartsList;

procedure LoadItemsData;
{ ... included bodyparts }
function GetItemsForSlot(const ApparelSlot: TApparelSlot): TItemsDataAbstractList;
{ ... included bodyparts }
function GetRandomItemForSlot(const ApparelSlot: TApparelSlot): TItemDataAbstract;
implementation
uses
  SysUtils, TypInfo, DOM,
  CastleXMLUtils,
  GameRandom, GameBodypartData;

procedure LoadItemsData;
var
  ItemData: TItemDataAbstract;
  BodypartData: TBodypartData;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  ItemsDataDictionary := TItemsDataAbstractDictionary.Create([doOwnsValues]);

  Doc := URLReadXML('castle-data:/character/itemdata.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('ItemData');
    try
      while Iterator.GetNext do
      begin
        ItemData := TItemData.ReadClass(Iterator.Current) as TItemData;
        ItemsDataDictionary.Add(ItemData.Id, ItemData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;

  ItemsDataList := TItemsDataList.Create(false);
  ItemsDataList.Capacity := ItemsDataDictionary.Count;
  for ItemData in ItemsDataDictionary.Values do
    ItemsDataList.Add(ItemData as TItemData);

  //BodypartsList := TItemsDataList.Create(false);
  Doc := URLReadXML('castle-data:/character/bodyparts.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('BodyPart');
    try
      while Iterator.GetNext do
      begin
        BodypartData := TBodypartData.ReadClass(Iterator.Current) as TBodypartData;
        ItemsDataDictionary.Add(BodypartData.Id, BodypartData);
        //BodyPartsList.Add(ItemData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

function GetRandomItemForSlot(
  const ApparelSlot: TApparelSlot): TItemDataAbstract;
var
  DataList: TItemsDataAbstractList;
begin
  DataList := GetItemsForSlot(ApparelSlot);
  if DataList.Count = 0 then
    raise Exception.CreateFmt('No items for slot %s', [ApparelSlotToStr(ApparelSlot)]);
  Result := DataList[Rnd.Random(DataList.Count)];
  DataList.Free;
end;

function GetItemsForSlot(const ApparelSlot: TApparelSlot): TItemsDataAbstractList;
var
  I: TItemDataAbstract;
begin
  Result := TItemsDataAbstractList.Create(false);
  for I in ItemsDataDictionary.Values do
    if ApparelSlot in I.EquipSlots then
      Result.Add(I);
end;

finalization
  ItemsDataDictionary.Free;
  ItemsDataList.Free;
end.

