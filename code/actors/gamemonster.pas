{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMonster;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections, DOM,
  GameSerializableObject,
  GameActor, GameActorData, GamePlayerCharacter, GameInventoryItem, GameMonsterData,
  GameAiAbstract;

type
  TMonster = class(TActor)
  strict protected
    procedure Die; override;
    procedure SetData(const AData: TActorData); override;
  public
    Loot: TInventoryItemsList;
    Ai: TAiAbstract; // TODO: move to TActor? As player will also use Ai and chests/traps should eventually also be separate from monsters
    function CurrentTarget: TPlayerCharacter; deprecated;
    function IsVisible: Boolean; override;
    function IsHearable: Boolean;
    procedure Update(const SecondsPassed: Single); override;
    function Unsuspecting: Boolean; override;
    function Immobilized: Boolean; override;
    function Aggressive: Boolean; deprecated;
    function GetDamage: Single; override; deprecated;
    function GetNoise: Single; override;
    function VisibilityRange: Single; deprecated;
    function MonsterData: TMonsterData; inline;
    procedure PlayAttackSound; override; deprecated;
    procedure Hit(const Damage: Single); override;
  public const Signature = 'monster'; //deprecated;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    constructor Create(const NotLoading: Boolean = true); override;
    destructor Destroy; override;
  end;
  TMonstersList = specialize TObjectList<TMonster>;

implementation
uses
  SysUtils, Math,
  CastleXmlUtils,
  GameViewGame, GameMap, GameMapTypes, GameRandom, GameTranslation, GameSounds,
  GameStats,
  GameLog, GameColors, GameMapItem, GameParticle, GameActionOnTarget,
  GameActionMoveAndActTarget, GameActionIdle, GameActionMove;

function TMonster.CurrentTarget: TPlayerCharacter;
begin
  Exit(ViewGame.CurrentCharacter);
end;

procedure TMonster.Die;
var
  II: TInventoryItem;
begin
  if MonsterData.Chest then
  begin
    if Loot.Count > 0 then
      ShowLog(GetTranslation('ChestDiesLog'), [FData.DisplayName], ColorLogMonsterDies)
    else
      ShowLog(GetTranslation('ChestEmptyDiesLog'), [FData.DisplayName], ColorLogMonsterDies);
  end else
  begin
    LocalStats.IncStat(Data.Id + '_killed');
    GlobalStats.IncStat('monsters_killed');
    ShowLog(GetTranslation('MonsterDiesLog'), [FData.DisplayName], ColorLogMonsterDies);
  end;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
  for II in Loot do
    Map.MapItemsList.Add(TMapItem.CreateItem(LastTileX + PredSize div 2, LastTileY + PredSize div 2, II));
  Loot.OwnsObjects := false; // and we don't seem to need to set it back
  Loot.Clear;
end;

procedure TMonster.SetData(const AData: TActorData);
begin
  inherited SetData(AData);
  Ai := MonsterData.AiData.Ai.Create;
  Ai.Parent := Self;
  Ai.Data := MonsterData.AiData;
end;

function TMonster.IsVisible: Boolean;
begin
  if MonsterData.Chest or MonsterData.Trap then
    Exit(inherited)
  else
    Exit(VisibleGrade >= DirectlyVisible);
end;

function TMonster.IsHearable: Boolean;
var
  P: TPlayerCharacter;
begin
  if MonsterData.Chest or MonsterData.Trap then
    Exit(false)
  else
  if not Unsuspecting then // TODO: maybe cannot hear attacking monsters always
    Exit(true)
  else
    for P in Map.CharactersOnThisLevel do
      if P.CanAct and (DistanceToSqr(ViewGame.CurrentCharacter) < MonsterData.NoisinessSqr) then
        Exit(true);
  Exit(false);
end;

function TMonster.VisibilityRange: Single;
begin
  Result := CurrentTarget.GetNoise + MonsterData.VisionBonus;
  if (CurrentTarget.Inventory.BottomCovered > 0) and (Result < MonsterData.VisionBottomClothed) then
    Result := MonsterData.VisionBottomClothed;
  if (CurrentTarget.Inventory.BottomCovered = 0) and (Result < MonsterData.VisionBottomNaked) then
    Result := MonsterData.VisionBottomNaked;
  if (CurrentTarget.Inventory.TopCovered > 0) and (Result < MonsterData.VisionTopClothed) then
    Result := MonsterData.VisionTopClothed;
  if (CurrentTarget.Inventory.TopCovered = 0) and (Result < MonsterData.VisionTopNaked) then
    Result := MonsterData.VisionTopNaked;
  Result := CurrentTarget.HalfSize + HalfSize + Result;
end;

procedure TMonster.Update(const SecondsPassed: Single);
begin
  Ai.Update(SecondsPassed);
  inherited;
end;

function TMonster.Unsuspecting: Boolean;
begin
  //parent is abstract
  Result := (CurrentAction is TActionIdle) or (CurrentAction is TActionMove); // Temporary: TODO
end;

function TMonster.Immobilized: Boolean;
begin
  Exit(false);
end;

function TMonster.Aggressive: Boolean;
begin
  Exit((not MonsterData.Chest and not MonsterData.Trap and not MonsterData.VacuumCleaner) or not Unsuspecting);
end;

function TMonster.GetDamage: Single;
begin
  Exit(MonsterData.Damage);
end;

function TMonster.GetNoise: Single;
begin
  Exit(MonsterData.NoisinessSqr); // In Player we use non-sqr?
end;

function TMonster.MonsterData: TMonsterData; inline;
begin
  Exit({$IFDEF SafeActorTypecast}FData as TMonsterData{$ELSE}TMonsterData(FData){$ENDIF});
end;

procedure TMonster.PlayAttackSound;
begin
  Sound(MonsterData.SoundAttack);
end;

procedure TMonster.Hit(const Damage: Single);
begin
  inherited Hit(Damage);
  NewParticle(CenterX, CenterY, Round(Damage).ToString, ColorParticleMonsterHurt);
end;

procedure TMonster.Save(const Element: TDOMElement);
var
  II: TInventoryItem;
begin
  inherited Save(Element);
  // Ai.Save(new element) // todo
  //Element.AttributeSet('Timeout', Timeout);
  //Element.AttributeSet('Guard', Guard);
  //Element.AttributeSet('SoftGuard', SoftGuard);
  //Element.AttributeSet('GuardPointX', GuardPointX);
  //Element.AttributeSet('GuardPointY', GuardPointY);
  for II in Loot do
    II.Save(Element.CreateChild('LootItem'));
end;

procedure TMonster.Load(const Element: TDOMElement);
var
  Iterator: TXMLElementIterator;
begin
  inherited;
////  Timeout := Element.AttributeSingle('Timeout');
//  Guard := Element.AttributeBoolean('Guard');
//  SoftGuard := Element.AttributeBooleanDef('SoftGuard', false); // TODO: compatibility, remove def later
//  GuardPointX := Element.AttributeInteger('GuardPointX');
//  GuardPointY := Element.AttributeInteger('GuardPointY');
  Data := MonstersDataDictionary[Element.AttributeString('Data.Id')];
  SetSize(FData.Size);
  try
    Iterator := Element.ChildrenIterator('LootItem');
    while Iterator.GetNext do
      Loot.Add(TInventoryItem.LoadClass(Iterator.Current) as TInventoryItem);
    FreeAndNil(Iterator);
  except
    Loot.Clear;
    ShowError('Failed to load loot for %s.', [FData.Id]);
  end;
end;

constructor TMonster.Create(const NotLoading: Boolean = true);
begin
  inherited Create(NotLoading);
  Loot := TInventoryItemsList.Create(true);
end;

destructor TMonster.Destroy;
begin
  Loot.Free;
  Ai.Free;
  inherited Destroy;
end;

initialization
  RegisterSerializableObject(TMonster);
end.

