{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActorData;

{$INCLUDE compilerconfig.inc}

interface
uses
  SysUtils,
  DOM, Generics.Collections,
  GameSerializableData;

type
  EActorDataValidationError = class(Exception);

type
  TActorData = class(TSerializableData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Id: String;
    DisplayName: String;
    MaxVital: Single;
    Size: Byte;
    MovementSpeed: Single;
    RollSpeed: Single;
    RollRange: Single;
  public
    destructor Destroy; override;
  end;
  TActorDataList = specialize TObjectList<TActorData>;
  TActorDataDictionary = specialize TObjectDictionary<String, TActorData>;

implementation
uses
  CastleXMLUtils,
  GameMap, GameMapTypes;

procedure TActorData.Validate;
begin
  if Id = '' then
    raise EActorDataValidationError.Create('Id = ""');
  if DisplayName = '' then
    raise EActorDataValidationError.Create('DisplayName = "" : ' + Id);
  if MaxVital <= 1 then
    raise EActorDataValidationError.Create('MaxVital <= 1 : ' + Id);
  if Size < 1 then
    raise EActorDataValidationError.Create('Size < 1 : ' + Id);
  if Size > PredMaxColliderSize + 1 then
    raise EActorDataValidationError.Create('Size > PredMaxColliderSize + 1 : ' + Id);
  if MovementSpeed < 0 then
    raise EActorDataValidationError.Create('MovementSpeed < 0 : ' + Id);
  if RollSpeed < 0 then
    raise EActorDataValidationError.Create('RollSpeed < 0 : ' + Id);
  if RollRange < 0 then
    raise EActorDataValidationError.Create('RollRange < 0 : ' + Id);
end;

procedure TActorData.Read(const Element: TDOMElement);
begin
  Id := Element.AttributeString('Id');
  DisplayName := Element.AttributeString('Name');
  MaxVital := Element.AttributeSingle('Vital');
  Size := Element.AttributeInteger('Size');
  MovementSpeed := Element.AttributeSingle('Speed');
  RollSpeed := Element.AttributeSingleDef('RollSpeed', 1); // default values should not necessarily make sense, but must allow validation
  RollRange := Element.AttributeSingleDef('RollRange', 1);
end;

destructor TActorData.Destroy;
begin
  inherited Destroy;
end;

end.

