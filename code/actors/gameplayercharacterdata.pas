{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GamePlayerCharacterData;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  GameActorData;

type
  TPlayerCharacterData = class(TActorData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    MaxVigor: Single;
    MaxPsyche: Single;
    MaxEnigma: Single;
    VitalRegen, MaxVitalRegen: Single;
    VigorRegen, MaxVigorRegen: Single;
    PsycheRegen, MaxPsycheRegen: Single;
    RollVigorCost: Single;
  end;

implementation
uses
  GameSerializableData;

procedure TPlayerCharacterData.Validate;
begin
  inherited Validate;
  if MaxVigor <= 0 then
    raise EActorDataValidationError.Create('MaxVigor <= 0 : ' + DisplayName);
  if MaxPsyche <= 0 then
    raise EActorDataValidationError.Create('MaxPsyche <= 0 : ' + DisplayName);
  if MaxEnigma <= 0 then
    raise EActorDataValidationError.Create('MaxEnigma <= 0 : ' + DisplayName);
  if VitalRegen <= 0 then
    raise EActorDataValidationError.Create('VitalRegen <= 0 : ' + DisplayName);
  if MaxVitalRegen <= 0 then
    raise EActorDataValidationError.Create('MaxVitalRegen <= 0 : ' + DisplayName);
  if VigorRegen <= 0 then
    raise EActorDataValidationError.Create('VigorRegen <= 0 : ' + DisplayName);
  if MaxVigorRegen <= 0 then
    raise EActorDataValidationError.Create('MaxVigorRegen <= 0 : ' + DisplayName);
  if PsycheRegen <= 0 then
    raise EActorDataValidationError.Create('PsycheRegen <= 0 : ' + DisplayName);
  if MaxPsycheRegen <= 0 then
    raise EActorDataValidationError.Create('MaxPsycheRegen <= 0 : ' + DisplayName);
  if RollVigorCost <= 0 then
    raise EActorDataValidationError.Create('RollVigorCost <= 0 : ' + DisplayName);
end;

procedure TPlayerCharacterData.Read(const Element: TDOMElement);
begin
  inherited Read(Element);
  // TODO
end;

initialization
  RegisterSerializableData(TPlayerCharacterData);
end.

