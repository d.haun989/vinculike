{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameActor;

{$INCLUDE compilerconfig.inc}

interface

uses
  DOM,
  CastleVectors,
  GameSerializableObject,
  GameMapTypes, GameActorData, GamePositionedObject,
  GameActionAbstract;

type
  TActor = class abstract(TPositionedObject)
  strict private
    FCurrentAction: TActionAbstract;
    procedure SetCurrentAction(const AValue: TActionAbstract);
  strict protected
    FData: TActorData;
    procedure Die; virtual; abstract;
    procedure SetData(const AData: TActorData); virtual;
  public
    Vital, MaxVital: Single;
    procedure Reset; virtual;
  public
    property Data: TActorData read FData write SetData;
    property CurrentAction: TActionAbstract read FCurrentAction write SetCurrentAction;
    procedure UpdatePassableTiles; virtual;
    procedure ActionFinished;
    procedure Teleport(const ToX, ToY: Int16); override;
    procedure RollTo(const ToX, ToY: Single);
    procedure MoveTo(const ToX, ToY: Single);
    procedure MoveAndAct(const Target: TActor; const Action: TActionAbstractData);
    procedure UpdateVisible; virtual;
    procedure Update(const SecondsPassed: Single); virtual;
    procedure Hit(const Damage: Single); virtual;
    function CanAct: Boolean; virtual;
    function CanBeInteractedWith: Boolean; virtual;
    function Unsuspecting: Boolean; virtual; abstract;
    function Immobilized: Boolean; virtual; abstract;
    procedure DamageWeapon; virtual;
    procedure PlayAttackSound; virtual; abstract;
    function GetActionTarget: TObject;
  public
    function GetDamage: Single; virtual; abstract;
    function GetSpeed: Single; virtual;
    function GetRollSpeed: Single; virtual;
    function GetNoise: Single; virtual; abstract;
  public
    procedure Save(const Element: TDOMElement); override;
  strict protected
    procedure Load(const Element: TDOMElement); override;
  public
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameSimpleSerializableObject,
  GameMap, GameLog, GameColors, GameActionOnTarget,
  GameActionIdle, GameActionMove, GameActionRoll, GameActionMoveAndActTarget;

procedure TActor.SetCurrentAction(const AValue: TActionAbstract);
begin
  FreeAndNil(FCurrentAction);
  FCurrentAction := AValue;
end;

procedure TActor.SetData(const AData: TActorData);
begin
  FData := AData;
end;

procedure TActor.Reset;
begin
  MaxVital := FData.MaxVital;
  Vital := MaxVital;
  SetSize(FData.Size);
end;

procedure TActor.ActionFinished;
begin
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
end;

procedure TActor.Teleport(const ToX, ToY: Int16);
begin
  inherited;
  CurrentAction := TActionIdle.NewAction(Self);
  CurrentAction.Start;
end;

procedure TActor.UpdatePassableTiles;
begin
  // do nothing, it's only for current player character
end;

procedure TActor.RollTo(const ToX, ToY: Single);
begin
  UpdatePassableTiles;
  if not Map.PassableTiles[PredSize][LastTile] then
    DebugWarning('RollTo origin is unsafe', []);
  CurrentAction := TActionRoll.NewAction(Self);
  TActionRoll(CurrentAction).MoveVector := Vector2(ToX - CenterX, ToY - CenterY);
  TActionRoll(CurrentAction).MoveVectorNormalized := TActionRoll(CurrentAction).MoveVector.Normalize;
  CurrentAction.Start;
end;

procedure TActor.MoveTo(const ToX, ToY: Single);
var
  NewAction: TActionAbstract;
begin
  UpdatePassableTiles;
  NewAction := TActionMove.NewAction(Self);
  TActionMove(NewAction).MoveTo(ToX, ToY, CurrentAction);
  CurrentAction := NewAction;
  CurrentAction.Start;
end;

procedure TActor.MoveAndAct(const Target: TActor; const Action: TActionAbstractData);
begin
  if not (CurrentAction is TActionMoveAndActTarget) or (TActionMoveAndActTarget(CurrentAction).Target <> Target) then
  begin
    UpdatePassableTiles;
    CurrentAction := TActionMoveAndActTarget.NewAction(Self);
    TActionMoveAndActTarget(CurrentAction).Data := Action;
    TActionMoveAndActTarget(CurrentAction).Target := Target;
    CurrentAction.Start;
  end;
end;

procedure TActor.UpdateVisible;
begin
  //do nothing
end;

procedure TActor.Update(const SecondsPassed: Single);
begin
  CurrentAction.Update(SecondsPassed);
end;

procedure TActor.Hit(const Damage: Single);
begin
  Vital -= Damage;
  MaxVital -= Damage / 10;
  if Vital <= 0 then
    Die;
end;

function TActor.CanAct: Boolean;
begin
  Exit(Vital > 0);
end;

function TActor.CanBeInteractedWith: Boolean;
begin
  Exit(Vital > 0); // Equal to CanAct for most actor types
end;

procedure TActor.DamageWeapon;
begin
  // do nothing
end;

function TActor.GetActionTarget: TObject;
begin
  if CurrentAction is TActionOnTarget then
    Exit(TActionOnTarget(CurrentAction).Target)
  else
    Exit(nil);
end;

function TActor.GetSpeed: Single;
begin
  Exit(FData.MovementSpeed);
end;

function TActor.GetRollSpeed: Single;
begin
  Exit(FData.RollSpeed);
end;

procedure TActor.Save(const Element: TDOMElement);
var
  ActionElement: TDOMElement;
begin
  inherited Save(Element);
  Element.AttributeSet('Vital', Vital);
  Element.AttributeSet('MaxVital', MaxVital);
  Element.AttributeSet('Data.Id', Data.Id);
  ActionElement := Element.CreateChild('Action');
  try
    CurrentAction.Save(ActionElement);
  except
    ShowError('ERROR: There was an exception saving the action %s', [CurrentAction.ClassName]);
    Element.RemoveChild(ActionElement);
    ActionElement := Element.CreateChild('Action');
    CurrentAction := TActionIdle.NewAction(Self);
    CurrentAction.Start;
    CurrentAction.Save(ActionElement);
  end;
end;

procedure TActor.Load(const Element: TDOMElement);
begin
  inherited;
  Vital := Element.AttributeSingle('Vital');
  MaxVital := Element.AttributeSingle('MaxVital');
  //Data.Id will be processed by child - MAYBE TODO!
  try
    // CAUTION: Actor.Data is still nil at this point! Deserealization of the action must not depend on it or must resume action on the next frame.
    CurrentAction := TSimpleSerializableObject.LoadClass(Element.Child('Action', true)) as TActionAbstract;
    CurrentAction.Parent := Self;
    // We do not call CurrentAction.Start here because the action has already started
  except
    ShowError('Action not loaded. Resetting %s to idle.', [Element.AttributeStringDef('Data.Id', 'undefined')]);
    CurrentAction := TActionIdle.NewAction(Self);
    CurrentAction.Start;
  end;
  // WARNING: Player character has Psyche = 0 at this point. We can't rely on CanAct
  if (Vital <= 0) and not (CurrentAction is TActionIdle) then
  begin
    ShowError('Dead actor had non-idle action. Resetting %s to idle.', [Element.AttributeStringDef('Data.Id', 'undefined')]);
    CurrentAction := TActionIdle.NewAction(Self);
    CurrentAction.Start;
  end;
end;

destructor TActor.Destroy;
begin
  FreeAndNil(FCurrentAction);
  inherited Destroy;
end;

end.

