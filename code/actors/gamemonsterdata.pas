{ Copyright (C) 2022-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameMonsterData;

{$INCLUDE compilerconfig.inc}

interface
uses
  DOM,
  CastleGlImages,
  GameActorData, GameAiAbstract;

type
  TMonsterData = class(TActorData)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Image: TDrawableImage; // Temporary: animated texture
    Damage: Single;
    Trap: Boolean; // TODO: Traps as a separate class
    Chest: Boolean; // TODO: as a separate class
    VacuumCleaner: Boolean; // TODO: ????
    Mimic: Boolean; // TODO
    IsInvisible: Boolean;
    CuriousAi: Boolean;
    Rare: Boolean;
    NoisinessSqr: Single;
    SoundAttack: String;
    AiData: TAiAbstractData;
    SpawnFrequency: Single;
    StartSpawningAtDepth: Integer;
    StopSpawningAtDepth: Integer;
    StartPatrolAtDepth: Integer;
    StopPatrolAtDepth: Integer;
    VisionBonus: Single;
    VisionBottomClothed: Single;
    VisionBottomNaked: Single;
    VisionTopClothed: Single;
    VisionTopNaked: Single;
    Danger: Single;
    XP: Integer;
  public
    destructor Destroy; override;
  end;

var
  MonstersData: TActorDataList;
  ChestsData: TActorDataList;
  MimicsData: TActorDataList;
  TrapsData: TActorDataList;
  VacuumCleaner: TMonsterData; // TODO: Only one for now!
  MonstersDataDictionary: TActorDataDictionary;

procedure LoadMonstersData;
implementation
uses
  SysUtils,
  CastleXMLUtils, CastleStringUtils,
  GameCachedImages, GameSerializableData;

procedure LoadMonstersData;
var
  AMonsterData: TMonsterData;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  MonstersData := TActorDataList.Create(true);
  ChestsData := TActorDataList.Create(true);
  MimicsData := TActorDataList.Create(true);
  TrapsData := TActorDataList.Create(true);
  MonstersDataDictionary := TActorDataDictionary.Create([]);

  Doc := URLReadXML('castle-data:/monsters/monsters.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('MonsterData');
    try
      while Iterator.GetNext do
      begin
        AMonsterData := TMonsterData.ReadClass(Iterator.Current) as TMonsterData;
        if AMonsterData.VacuumCleaner then
          VacuumCleaner := AMonsterData
        else
        if AMonsterData.Trap then
          TrapsData.Add(AMonsterData)
        else
        if AMonsterData.Mimic then // WARNING: Mimics are also chests, so we shouldn't add them to ChestsData TODO
          MimicsData.Add(AMonsterData)
        else
        if AMonsterData.Chest then
          ChestsData.Add(AMonsterDAta)
        else
          MonstersData.Add(AMonsterData);
        MonstersDataDictionary.Add(AMonsterData.Id, AMonsterData);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

procedure TMonsterData.Validate;
begin
  inherited;
  if Damage < 0 then
    raise EActorDataValidationError.Create('Damage < 0 : ' + DisplayName);
  if SoundAttack = '' then
    raise EActorDataValidationError.Create('Monster has no attack sound : ' + DisplayName);
  if Image = nil then
    raise EActorDataValidationError.Create('Image = nil : ' + DisplayName);
  if Trap and Chest then
    raise EActorDataValidationError.Create('Trap and Chest : ' + DisplayName);
  if Trap and Mimic then
    raise EActorDataValidationError.Create('Trap and Mimic : ' + DisplayName);
  if Trap and VacuumCleaner then
    raise EActorDataValidationError.Create('Trap and VacuumCleaner : ' + DisplayName);
  if Chest and VacuumCleaner then
    raise EActorDataValidationError.Create('Chest and VacuumCleaner : ' + DisplayName);
  if Mimic and not Chest then
    raise EActorDataValidationError.Create('Mimic and not Chest : ' + DisplayName);
  if Mimic and VacuumCleaner then
    raise EActorDataValidationError.Create('Mimic and VacuumCleaner : ' + DisplayName);
  if SpawnFrequency < 0 then
    raise EActorDataValidationError.Create('SpawnFrequency < 0 : ' + Id);
  if StartSpawningAtDepth < 0 then
    raise EActorDataValidationError.Create('StartSpawningAtDepth < 0 : ' + DisplayName);
  if NoisinessSqr < 0 then
    raise EActorDataValidationError.Create('NoisinessSqr < 0 : ' + DisplayName);
  if StopSpawningAtDepth < StartSpawningAtDepth then
    raise EActorDataValidationError.Create('StopSpawningAtDepth < StartSpawningAtDepth : ' + DisplayName);
  if StartPatrolAtDepth < 0 then
    raise EActorDataValidationError.Create('StartPatrolAtDepth < 0 : ' + DisplayName);
  if StopPatrolAtDepth < StartPatrolAtDepth then
    raise EActorDataValidationError.Create('StopPatrolAtDepth < StartPatrolAtDepth : ' + DisplayName);
  if VisionBonus < 0 then
    raise EActorDataValidationError.Create('VisionBonus < 0 : ' + DisplayName);
  if VisionBottomClothed < 0 then
    raise EActorDataValidationError.Create('VisionBottomClothed < 0 : ' + DisplayName);
  if VisionBottomNaked < 0 then
    raise EActorDataValidationError.Create('VisionBottomNaked < 0 : ' + DisplayName);
  if VisionTopClothed < 0 then
    raise EActorDataValidationError.Create('VisionTopClothed < 0 : ' + DisplayName);
  if VisionTopNaked < 0 then
    raise EActorDataValidationError.Create('VisionTopNaked < 0 : ' + DisplayName);
  if Danger < 0 then
    raise EActorDataValidationError.Create('Danger < 0 : ' + DisplayName);
  if XP < 0 then
    raise EActorDataValidationError.Create('XP < 0 : ' + DisplayName);
  {TODO: will break chests
  if ActionData = nil then
    raise EActorDataValidationError.Create('ActionData = nil : ' + DisplayName);}
end;

procedure TMonsterData.Read(const Element: TDOMElement);
begin
  Damage := Element.AttributeSingle('Damage');
  NoisinessSqr := Sqr(Element.AttributeSingle('Noisiness'));
  SoundAttack := Element.AttributeString('SoundAttack');
  Trap := Element.AttributeBooleanDef('Trap', false);
  Chest := Element.AttributeBooleanDef('Chest', false);
  VacuumCleaner := Element.AttributeBooleanDef('VacuumCleaner', false);
  Mimic := Element.AttributeBooleanDef('Mimic', false);
  IsInvisible := Element.AttributeBooleanDef('IsInvisible', false);
  CuriousAi := Element.AttributeBooleanDef('CuriousAi', false);
  Rare := Element.AttributeBooleanDef('Rare', false);
  Danger := Element.AttributeSingle('Danger');
  XP := Element.AttributeInteger('XP');
  SpawnFrequency := Element.AttributeFloatDef('SpawnFrequency', 1.0);
  StartSpawningAtDepth := Element.AttributeInteger('StartSpawningAtDepth');
  StopSpawningAtDepth := Element.AttributeInteger('StopSpawningAtDepth');
  StartPatrolAtDepth := Element.AttributeInteger('StartPatrolAtDepth');
  StopPatrolAtDepth := Element.AttributeInteger('StopPatrolAtDepth');
  VisionBonus := Element.AttributeSingleDef('VisionBonus', 0);
  VisionBottomClothed := Element.AttributeSingleDef('VisionBottomClothed', 0);
  VisionBottomNaked := Element.AttributeSingleDef('VisionBottomNaked', 0);
  VisionTopClothed := Element.AttributeSingleDef('VisionTopClothed', 0);
  VisionTopNaked := Element.AttributeSingleDef('VisionTopNaked', 0);
  AiData := TAiAbstractData.ReadClass(Element.Child('Ai')) as TAiAbstractData;
  Image := LoadDrawable('castle-data:/' + Element.AttributeString('Image'));
  inherited;
end;

destructor TMonsterData.Destroy;
begin
  AiData.Free;
  inherited;
end;

initialization
  RegisterSerializableData(TMonsterData);
finalization
  MonstersData.Free;
  ChestsData.Free;
  TrapsData.Free;
  VacuumCleaner.Free;
  MimicsData.Free;
  MonstersDataDictionary.Free;
end.

