{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryHeader;

{$INCLUDE compilerconfig.inc}

interface
uses
  Classes, DOM,
  CastleUiControls,
  GameUnlockableEntry;

type
  TVinculopediaEntryHeader = class(TUnlockableEntry)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Header: String;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils, CastleControls,
  GameSerializableData,
  GameColors, GameFonts;

procedure TVinculopediaEntryHeader.Validate;
begin
  inherited;
  if Header = '' then
    raise EDataValidationError.Create('Header = "" in TVinculopediaEntryHeader');
end;

procedure TVinculopediaEntryHeader.Read(const Element: TDOMElement);
begin
  inherited;
  Header := Element.AttributeString('Header');
end;

procedure TVinculopediaEntryHeader.InsertEntry(AOwner: TCastleUserInterface);
var
  L: TCastleLabel;
begin
  L := TCastleLabel.Create(AOwner);
  L.Caption := Header;
  L.Color := ColorVinculopediaHeader;
  L.CustomFont := FontBender90;
  L.MaxWidth := 1300; // TODO: more generic
  {L.Width := 1300;
  L.Alignment := hpMiddle;} // TODO: This doesn't work
  AOwner.InsertFront(L);
end;

initialization
  RegisterSerializableData(TVinculopediaEntryHeader);

end.

