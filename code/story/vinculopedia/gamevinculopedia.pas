{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopedia;

{$INCLUDE compilerconfig.inc}

interface
uses
  Classes, DOM, Generics.Collections,
  CastleControls,
  GameSerializableData, GameUnlockableEntry, GameVinculopediaPage;

type
  TVinculopedia = class(TObject)
  strict private
    procedure Read;
  public
    Pages: TVinculopediaPagesList;
    procedure CheckUnlock(const AStat: String; const AStatValue: Integer);
  public
    constructor Create; //override
    destructor Destroy; override;
  end;

var
  Vinculopedia: TVinculopedia;

procedure InitVinculopedia;
implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameColors, GameFonts, GameSounds;

procedure InitVinculopedia;
begin
  Vinculopedia := TVinculopedia.Create;
end;

procedure TVinculopedia.Read;
var
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  Doc := URLReadXML('castle-data:/story/vinculopedia.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('Page');
    try
      while Iterator.GetNext do
      begin
        Pages.Add(TVinculopediaPage.ReadClass(Iterator.Current) as TVinculopediaPage);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

procedure TVinculopedia.CheckUnlock(const AStat: String;
  const AStatValue: Integer);
var
  VP: TVinculopediaPage;
  Unlocked: Boolean;
  Updated: Boolean;
begin
  Unlocked := false;
  Updated := false;
  for VP in Pages do
  begin
    {$WARN 6060 off} // WHY do you ignore this, FPC? I don't want to add it to Interface, it's a local thing, on-and-off.
    case VP.CheckUnlock(AStat, AStatValue) of
      ukUnlocked: Unlocked := true;
      ukUpdated: Updated := true;
    end;
    {$WARN 6060 on}
  end;
  if Unlocked then
    Sound('page_unlocked')
  else if Updated then
    Sound('page_updateed');
end;

constructor TVinculopedia.Create;
begin
  inherited; //parent is empty
  Pages := TVinculopediaPagesList.Create(true);
  Read;
end;

destructor TVinculopedia.Destroy;
begin
  Pages.Free;
  inherited Destroy;
end;

finalization
  Vinculopedia.Free;

end.

