{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaEntryImageText;

{$INCLUDE compilerconfig.inc}

interface
uses
  Classes, DOM,
  CastleUiControls, CastleGlImages,
  GameVinculopediaEntryText;

type
  TVinculopediaEntryImageText = class(TVinculopediaEntryText)
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    ImageUrl: String;
    ImageWidth: Integer;
    ImageHeight: Integer;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
  end;

implementation
uses
  SysUtils,
  CastleXmlUtils, CastleControls,
  GameSerializableData,
  GameColors, GameFonts;

procedure TVinculopediaEntryImageText.Validate;
begin
  inherited;
  if ImageUrl = '' then
    raise EDataValidationError.Create('ImageUrl = "" in TVinculopediaEntryImageText');
  if ImageWidth <= 0 then
    raise EDataValidationError.Create('ImageWidth <= 0 in TVinculopediaEntryImageText');
  if ImageHeight <= 0 then
    raise EDataValidationError.Create('ImageHeight <= 0 in TVinculopediaEntryImageText');
end;

procedure TVinculopediaEntryImageText.Read(const Element: TDOMElement);
begin
  inherited;
  ImageUrl := Element.AttributeString('Image');
  ImageWidth := Element.AttributeInteger('ImageWidth');
  ImageHeight := Element.AttributeInteger('ImageHeight');
end;

procedure TVinculopediaEntryImageText.InsertEntry(AOwner: TCastleUserInterface);
var
  C: TCastleUserInterface;
  L: TCastleLabel;
  Img: TCastleImageControl;
begin
  C := TCastleUserInterface.Create(AOwner);
  C.Width := 1300;
  C.Height := ImageHeight;

  Img := TCastleImageControl.Create(C);
  Img.Stretch := true;
  Img.Width := ImageWidth;
  Img.Height := ImageHeight;
  Img.Url := 'castle-data:/' + ImageUrl;
  Img.Anchor(vpMiddle, vpMiddle, 0);
  Img.Anchor(hpLeft, hpLeft, 0);
  C.InsertFront(Img);

  L := TCastleLabel.Create(C);
  L.Caption := Contents;
  L.Color := ColorDefault;
  L.CustomFont := FontBender40;
  L.Anchor(vpTop, vpTop, 0);
  L.Anchor(hpLeft, hpLeft, ImageWidth + 30);
  L.MaxWidth := 1300 - ImageWidth - 30; // TODO: more generic
  C.InsertFront(L);

  AOwner.InsertFront(C);
end;

initialization
  RegisterSerializableData(TVinculopediaEntryImageText);

end.

