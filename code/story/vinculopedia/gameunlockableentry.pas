{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameUnlockableEntry;

{$INCLUDE compilerconfig.inc}

interface
uses
  Classes, DOM, Generics.Collections,
  CastleUiControls,
  GameSerializableData;

type
  TUnlockableEntry = class abstract(TSerializableData)
  strict private
    UnlockStat: String;
    UnlockValue: Integer;
    //UnhideCondition: String; ----- at which moment it'll appear in the lists as "Locked"
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    //function IsHidden: Boolean;
    function IsUnlocked: Boolean;
    function WillBeUnlocked(const AStat: String; const AStatValue: Integer): Boolean;
    procedure InsertEntry(AOwner: TCastleUserInterface); virtual; abstract;
  end;
  TEntriresList = specialize TObjectList<TUnlockableEntry>;

implementation
uses
  CastleXmlUtils,
  GameStats;

procedure TUnlockableEntry.Validate;
begin
  // Empty unlock string means entry is initially unlocked (it must be specified in the XML but can be empty? Maybe some magic like "none" would be better?)
  {
  if UnlockCondition = '' then
    raise EDataValidationError.CreateFmt('UnlockCondition = "" in %s', [Self.ClassName]);
  }
  // it could have been much more useful to have a validation against _POSSIBLE_ values, but I'm not sure how to do that YET
  if UnlockValue < 0 then
    raise EDataValidationError.CreateFmt('UnlockValue < 0 in %s', [Self.ClassName]);
end;

procedure TUnlockableEntry.Read(const Element: TDOMElement);
begin
  UnlockStat := Element.AttributeStringDef('UnlockStat', '');
  UnlockValue := Element.AttributeIntegerDef('UnlockValue', 0);
end;

function TUnlockableEntry.IsUnlocked: Boolean;
begin
  if UnlockStat <> '' then
    Exit((GlobalStats.StatValue(UnlockStat) >= UnlockValue) or (LocalStats.StatValue(UnlockStat) >= UnlockValue))
  else
    Exit(true);
end;

function TUnlockableEntry.WillBeUnlocked(
  const AStat: String; const AStatValue: Integer): Boolean;
begin
  Exit((AStat = UnlockStat) and (AStatValue = UnlockValue));
end;

end.

