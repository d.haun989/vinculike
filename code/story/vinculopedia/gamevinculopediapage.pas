{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameVinculopediaPage;

{$INCLUDE compilerconfig.inc}

interface
uses
  Classes, DOM, Generics.Collections,
  CastleUiControls,
  GameUnlockableEntry;

type
  TUnlockKind = (ukNone, ukUnlocked, ukUpdated);
type
  TVinculopediaPage = class(TUnlockableEntry)
  strict private
    Entries: TEntriresList;
  strict protected
    procedure Validate; override;
    procedure Read(const Element: TDOMElement); override;
  public
    Caption: String;
    procedure InsertEntry(AOwner: TCastleUserInterface); override;
    function CheckUnlock(const AStat: String; const AStatValue: Integer): TUnlockKind;
  public
    destructor Destroy; override;
  end;
  TVinculopediaPagesList = specialize TObjectList<TVinculopediaPage>;

implementation
uses
  SysUtils,
  CastleXmlUtils, CastleControls,
  GameSerializableData,
  GameLog, GameColors, GameFonts;

procedure TVinculopediaPage.Validate;
begin
  inherited;
  if Caption = '' then
    raise EDataValidationError.Create('Caption = "" in TVinculopediaPage');
  //image
end;

procedure TVinculopediaPage.Read(const Element: TDOMElement);
var
  Iterator: TXMLElementIterator;
begin
  inherited;
  Caption := Element.AttributeString('Caption');
  //Image := Drawable(Element.AttributeString('Image'));
  Entries := TEntriresList.Create(true);
  try
    Iterator := Element.ChildrenIterator('Entry');
    while Iterator.GetNext do
    begin
      Entries.Add(TUnlockableEntry.ReadClass(Iterator.Current) as TUnlockableEntry);
    end;
  finally FreeAndNil(Iterator) end;
end;

destructor TVinculopediaPage.Destroy;
begin
  Entries.Free;
  inherited Destroy;
end;

procedure TVinculopediaPage.InsertEntry(AOwner: TCastleUserInterface);
var
  I: Integer;
begin
  for I := 0 to Pred(Entries.Count) do
    Entries[I].InsertEntry(AOwner);
end;

function TVinculopediaPage.CheckUnlock(const AStat: String;
  const AStatValue: Integer): TUnlockKind;
var
  UE: TUnlockableEntry;
begin
  if Self.WillBeUnlocked(AStat, AStatValue) then
  begin
    ShowLog('New entry: "%s"', [Self.Caption], ColorLogVinculopedia);
    Exit(ukUnlocked);
  end else
    for UE in Entries do
      if UE.WillBeUnlocked(AStat, AStatValue) then
      begin
        ShowLog('Entry updated: "%s"', [Self.Caption], ColorLogVinculopedia);
        Exit(ukUpdated);
      end;
  Exit(ukNone);
end;

initialization
  RegisterSerializableData(TVinculopediaPage);

end.

