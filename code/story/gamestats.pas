{ Copyright (C) 2023-2023 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStats;

{$INCLUDE compilerconfig.inc}

interface
uses
  DOM, Generics.Collections;

type
  TStatsDictionary = specialize TDictionary<string, UInt32>;
  TStats = class(TObject)
  strict private
    Stats: TStatsDictionary;
    procedure SendStatsIncreasedEvent(const StatName: String; const StatValue: Integer);
  public
    function StatValue(const StatName: String): Integer;
    procedure IncStat(const StatName: String);
    procedure MaxStat(const StatName: String; const NewValue: Integer);
  public
    procedure Save(const Element: TDOMElement);
    procedure Load(const Element: TDOMElement);
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  { Global stats do not get reset when starting a new game or loading game
    Best suited for accumulative achievements like "kill 1000 monsters" }
  GlobalStats: TStats;
  { Local stats are part of the save game
    They get reverted when the Player loads game
    They get lost when the Player starts a new game
    Best suited for unlocking vinculopedia content }
  LocalStats: TStats;
  { Maybe at some point: "streak achievements" (endurance)
    like "kill 100 monsters in one run"
    They reset when the character/party ends the run }
  //RunStats: TStats;

procedure InitStats;
implementation
uses
  SysUtils,
  CastleXmlUtils,
  GameLog, GameVinculopedia;

procedure InitStats;
begin
  GlobalStats := TStats.Create;
  LocalStats := TStats.Create;
end;

procedure TStats.SendStatsIncreasedEvent(
  const StatName: String; const StatValue: Integer);
begin
  Vinculopedia.CheckUnlock(StatName, StatValue);
  //Achievements.CheckUnlock(StatName, StatValue);
end;

function TStats.StatValue(const StatName: String): Integer;
begin
  if Stats.ContainsKey(StatName) then
    Exit(Stats[StatName])
  else
    Exit(-1);
end;

procedure TStats.Save(const Element: TDOMElement);
var
  SaveElement: TDOMELement;
  S: String;
begin
  for S in Stats.Keys do
  begin
    SaveElement := Element.CreateChild('Stat');
    SaveElement.AttributeSet('Key', S);
    SaveElement.AttributeSet('Val', Stats[S]);
  end;
end;

procedure TStats.Load(const Element: TDOMElement);
var
  I: TXMLElementIterator;
begin
  Stats.Clear;
  I := Element.ChildrenIterator('Stat');
  try
    while I.GetNext do
      Stats.Add(I.Current.AttributeString('Key'), I.Current.AttributeInteger('Val'));
  finally
    FreeAndNil(I);
  end;
end;

procedure TStats.IncStat(const StatName: String);
var
  StatVal: Integer;
begin
  if not Stats.ContainsKey(StatName) then
  begin
    StatVal := 1;
    Stats.Add(StatName, StatVal);
  end else
  begin
    StatVal := Stats[StatName] + 1;
    Stats[StatName] := StatVal;
  end;
  SendStatsIncreasedEvent(StatName, StatVal);
end;

procedure TStats.MaxStat(const StatName: String; const NewValue: Integer);
begin
  if not Stats.ContainsKey(StatName) then
    Stats.Add(StatName, NewValue)
  else
  begin
    if Stats[StatName] < NewValue then
      Stats[StatName] := NewValue
    else
      Exit; // nothing to achieve
  end;
  SendStatsIncreasedEvent(StatName, NewValue);
end;

constructor TStats.Create;
begin
  inherited Create; // parent is non-virtual and empty
  Stats := TStatsDictionary.Create;
end;

destructor TStats.Destroy;
begin
  Stats.Free;
  inherited Destroy;
end;

finalization
  LocalStats.Free;
  GlobalStats.Free;
end.

