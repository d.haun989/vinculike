unit CastleMobileButton;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleControls, CastleKeysMouse, CastleTimeUtils, CastleVectors;

type
  { An extension of TCastleButton accepting Long-Click }
  TCastleMobileButton = class(TCastleButton)
  strict private const
    FDefaultLongClickDuration = Single(0.5);
  strict private
    PressStartTime: TTimerResult;
    FLongClickDuration: Single;
    PressStarted: Boolean;
    PressStartedFinger: TFingerIndex;
    PressStartedPosition: TVector2;
    FOnLongClick: TNotifyEvent;
    FOnShortClick: TNotifyEvent;
  public
    { This event is called if the button was pressed longer than LongClickDuration
      or if any other than left mouse buttons were pressed for desktop compatibility }
    property OnLongClick: TNotifyEvent read FOnLongClick write FOnLongClick;
    { This event is called if the button was pressed shorter than LongClickDuration
      with touch or left mouse button }
    property OnShortClick: TNotifyEvent read FOnShortClick write FOnShortClick;
    function Press(const Event: TInputPressRelease): Boolean; override;
    function Release(const Event: TInputPressRelease): Boolean; override;
    constructor Create(AOwner: TComponent); override;
  published
    { Duration of the long press in seconds
      Note: it should be TFloatTime, not single,
      but Double is not allowed in properties for some obscure reasons }
    property LongClickDuration: Single read FLongClickDuration write FLongClickDuration {$ifdef FPC}default FDefaultLongClickDuration{$endif};
  end;

implementation
uses
  Math,
  CastleComponentSerialize;

function TCastleMobileButton.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited Press(Event);
  // We cannot rely on Result, as parent just has set Result := true. There is no way to know if it was true before that or not.
  if {Result or} (Event.EventType <> itMouseButton) then Exit;

  if Enabled then
  begin
    // regardless of Toggle value, set ClickStarted, to be able to reach OnClick.
    PressStarted := true;
    PressStartedFinger := Event.FingerIndex;
    PressStartedPosition := Event.Position;
    PressStartTime := Timer;
  end;
end;

function TCastleMobileButton.Release(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited Release(Event);
  // We cannot rely on Result, as parent just has set Result := true. There is no way to know if it was true before that or not.
  if {Result or} (Event.EventType <> itMouseButton) then Exit;
  if PressStarted and (PressStartedFinger = Event.FingerIndex) then
  begin
    PressStarted := false;
    if EnableParentDragging and ((PressStartedPosition - Event.Position).LengthSqr > Sqr(Min(RenderRect.Width, RenderRect.Height))) then
      Exit;
    if not CapturesEventsAtPosition(Event.Position) then
      Exit;
    if (PressStartTime.ElapsedTime < FLongClickDuration) and (Event.MouseButton = buttonLeft) then
    begin
      if Assigned(FOnShortClick) then
        FOnShortClick(Self);
    end else
      if Assigned(FOnLongClick) then
        FOnLongClick(Self);
  end;
end;

constructor TCastleMobileButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FLongClickDuration := FDefaultLongClickDuration;
end;

initialization
  RegisterSerializableComponent(TCastleMobileButton, 'Button Mobile');
end.

