# Vinculike

A roguelike-like dungeon crawler with NSFW elements.

## About current version of the game

WARNING 1: This is a very early prototype of the game, featuring only the most basic gameplay mechanics. Everything can and most likely will change. As an unstable build this app may be subject to bugs, lags, low FPS, poor representation, etc.

WARNING 2: I live in Kyiv, Ukraine. You know what it means. I hope I'll be able to continue the development but anything can happen.

## Content warning

This game contains material suitable only for mature audience (cartoon nudity, mild suggestive themes). You can play this game only if you are a legal adult.

Optional censored mode (off by default) removes the unnecessary detail from the graphics but doesn't change the mature nature of the game.

## Story

Chased by horrors of war, battered by hunger and thirst a lone refugee arrives to an old mining town in search for salvation only to find it desolate, worse than that, now trapped inside by a mythical barrier. The abandoned mine seems to be the only passage... It can't get any worse from now, or can it?

## Gameplay

Dive deep into ever-changing dungeons, fight monsters, manage resources: clothes and food, handle health of body and mind, get captured and rescued, rinse and repeat in this action roguelike-like dungeon crawler. Uncover long forgotten and uncomfortable mysteries as the stories advances and your army now works hard to survive and rebuild the settlement.

You control the heroine by mouse or touch gestures. Try to keep her dressed and healthy and preferably not tied up too tightly, while monsters will try hard to do the inverse. Items like clothes or weapons degrade very quickly, you'll need to be on a constant lookout for something better to pick up.

Fighting monsters requires good timing and skill to land a blow and evade before the enemy can counterattack. But the deeper you go, the harder it gets.

Check the "How to play" tutorial in game for details on controls and game mechanics.

The last but not the least, there is a cheat/debug menu. You can access it by clicking F1 on Windows/Linux and/or by clicking/tapping 5 times in top-left of the screen during Pause menu. 

## Installation

### Windows

No installation required - just extract the game into one folder and play.

Known bug on Windows 10: Avoid putting the game into a folder with non-ASCII characters if your user name also contains non-ASCII characters.

### Linux

No installation required - just extract the game into one folder and play.

Sometimes you might also need to set "executable" flag for the binary (`chmod +x vinculike` or through file properties in the file manager).

You need the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

Alternatively on Debian-based systems like Ubuntu you can download the DEB package and install it through a package manager or by `dpkg -i vinculike-*-linux-x86_64.deb` command.

Note: do not mix installed Debian package and standalone version, always use either one or the other.

### Android

Download and install the APK. The game requires Android 4.2 and higher.

You may need to allow installation of third-party APKs in the phone/tablet Settings.

## Support the development

If you like the game and want more content like this - play the game and have fun! As simple as that! The download count alone is something that helps me to keep going. Perhaps even spread the word around social networks to help more people have fun?

In case you want to help even more - leave a good comment at the game's page. It's free and shouldn't take you long, but you really can't overestimate what miracles can player feedback do to my motivation :) Did you like the game? Why not tell about it? Or maybe you see something that can be improved? - I can't promise to implement every requested feature (there are limits to what can be done within a reasonable amount of time), but will most certainly consider it.

If you want to report bug, propose an idea or anything like this you can also join Discord: https://discord.gg/uhkMA73nc7

And if you really want to be more eloquent and believe that merely saying "thank you" is not enough... No, I don't have a Patreon, at least not yet, but I highly encourage you to consider supporting Castle Game Engine https://www.patreon.com/castleengine . If it weren't for this Game Engine this game would have never existed. And thanks to this Engine I've gotten my full-time dream job as a game developer, so by supporting it you are indirectly financially supporting me too ;)

## Credits

Game by EugeneLoza

Created in Castle Game Engine (https://castle-engine.io/)

Music by audionautix.com, MaxStack and Trevor Lentz.

Sounds by Kenney Vleugels, altfuture, HarpyHarpHarp, reg7783, RubberDuck, uEffects, IFM123, ssierra1202, Ashe Kirk, martian, Montblanc Candies & egomassive, nicklas3799, drotzruhn, Singger, Reitanna, sophiehall3535, CJspellsfish, pkri, Juhani Junkala (SubspaceAudio).

Graphics by RL-Tiles, Dungeon Crawl Stone Soup, Blarumyrran, KodiakGraphics, Skoll, Lorc, pzUH, Andrea Stöckel, AntumDeluge, Hyptosis.

Fonts by Oleg Zhuravlev, Gladkikh Ivan, Jayvee D. Enaguas, Meztone Design.

## Links

Download the latest version: https://decoherence.itch.io/vinculike [Windows, Linux, Android]

Source code: https://gitlab.com/EugeneLoza/vinculike (GPLv3)

Nightly builds: https://gitlab.com/EugeneLoza/vinculike/-/pipelines [Windows, Linux] (Warning, can be unstable)
